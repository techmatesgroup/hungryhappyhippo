package io.tmg.hungryhappyhippo.poc;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import javax.servlet.Filter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SecurityIntegrationTest {

	@Resource
	private WebApplicationContext context;

	@Resource
	private Filter springSecurityFilterChain;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).addFilters(springSecurityFilterChain).build();
	}

	@Test
	public void givenNoCsrf_whenAnyRequest_thenForbidden() throws Exception {
		mvc.perform(post("/anywhere")
				.with(testUser()))
				.andExpect(status().isForbidden());
	}

	@Test
	public void givenCsrf_whenAnyRequest_thenCreated() throws Exception {
		mvc.perform(post("/anywhere")
			.with(testUser())
			.with(csrf()))
			.andExpect(status().isNotFound());
	}

	@Test
	public void xssProtectionHeaderShouldBePresent() throws Exception {
		mvc.perform(post("/anywhere")
			.with(testUser())
			.with(csrf()))
			.andExpect(header().string("X-XSS-Protection", "1; mode=block"));
	}

	private RequestPostProcessor testUser() {
		return user("user");
	}

}
