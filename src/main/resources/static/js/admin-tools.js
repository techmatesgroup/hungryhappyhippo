ACC.adminTools = {


    _autoload: [
        ["bindEvents", $('.js-admin-tool-container').length > 0]
    ],

    bindEvents: function () {
        $('.js-add-search-terms-form').on('submit', function () {

            let searchTerms = new URL(window.location.href).searchParams.get('searchTerms');

            if (searchTerms) {
                $(this).append(`<input type="hidden" name="searchTerms" value="${searchTerms}">`);
            }

            return true;
        });

        $('.js-add-search-terms-link').on('click', function () {

            let searchTerms = new URL(window.location.href).searchParams.get('searchTerms');

            if (searchTerms) {
                $(this).attr('href', $(this).attr('href') + "?searchTerms=" + searchTerms)
            }

            return true;
        });
    }

};