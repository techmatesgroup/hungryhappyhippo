ACC.restaurants = {

    _autoload: [
        ["bindRestaurantsSearchPageEvents", $('#restaurantsSearchPageWrapper').length > 0]
    ],

    currentPageNumber : 0,

    bindRestaurantsSearchPageEvents: function () {
        let $deliveryButton = $('#deliveryButton');
        let $pickupButton = $('#pickupButton');
        let $serviceOption = $('#serviceOption');

        $deliveryButton.on("click", function () {
            $(this).toggleClass("btn-success").toggleClass("btn-dark")
            $pickupButton.addClass("btn-dark").removeClass("btn-success");
            $serviceOption.val($(this).hasClass('btn-dark') ? '' : $(this).data('service-option'));
            $(this).trigger('change');
        });
        $pickupButton.on("click", function () {
            $(this).toggleClass("btn-success").toggleClass("btn-dark")
            $deliveryButton.addClass("btn-dark").removeClass("btn-success");
            $serviceOption.val($(this).hasClass('btn-dark') ? '' : $(this).data('service-option'));
            $(this).trigger('change');
        });

        $('#loadMoreControl').on('click', function () {
            ACC.searchRestaurants.executeLoadMoreRequest();
        });

        $(document).on('click', '#backToTopOfPageControl', function () {
            $('html,body').animate({scrollTop: 0}, 'slow');
            return false;
        });

    },

    populateSearchResult: function (result) {
        let responseData = result.restaurantSearchResponseDto;
        let searchResults = responseData.restaurants;

        let pageNumber = responseData.pageNumber;
        let pageSize = responseData.pageSize;
        let dataSize = searchResults.length;
        let loadedResults = pageNumber == 0 ? 0 : $('.searchResult').length;
        let totalResults = responseData.totalResults;

        let searchResultInfo = '';

        if (totalResults <= 0) {
            searchResultInfo = "No items found matching the provided search criteria/filters";
        } else {
            searchResultInfo = `Showing ${loadedResults + dataSize} items of ${totalResults} total results`;

        }
        $("#searchResultsInfo").text(searchResultInfo);

        if (ACC.restaurants.currentPageNumber === 0) {
            $("#searchResultsContainer").text("");
        }

        if (responseData.hasMoreResults) {
            $('#loadMoreControl').removeClass('d-none');
        } else {
            $('#loadMoreControl').addClass('d-none');
        }

        if (searchResults.length) {
            $('#backToTopOfPageControl').removeClass('d-none');
            $('#sortOptionsControl').removeClass('d-none');
        } else {
            $('#backToTopOfPageControl').addClass('d-none');
            $('#sortOptionsControl').addClass('d-none');
        }

        for (let i = 0; i < searchResults.length; i++) {
            let searchResult = searchResults[i];
            let searchResultMarkup = ACC.restaurants.makeSearchResultMarkup(searchResult);
            $("#searchResultsContainer").append(searchResultMarkup);
        }
    },

    makeSearchResultMarkup: function (searchResult) {
        let searchResultMarkup = ACC.restaurants.markup.headerAndImageMarkup(searchResult);

        if(searchResult.promotionalContent) {
            searchResultMarkup += ACC.restaurants.markup.promotionalContentMarkup(searchResult);
        }
        else {
            if(searchResult.promotionalWeight > 0) {
                searchResult.promotionalContent = "Featured";
                searchResultMarkup += ACC.restaurants.markup.promotionalContentMarkup(searchResult);
            }
            else {
                searchResultMarkup += ACC.restaurants.markup.declarationWithoutContentMarkup();
            }
        }

        searchResultMarkup += ACC.restaurants.markup.baseSearchResultMarkup(searchResult);
        return searchResultMarkup;
    },

    markup: {

        headerAndImageMarkup: function (searchResult) {
            return `<div class="searchResult border border-dark rounded text-left m-2 p-3 row">`;
        },

        promotionalContentMarkup: function (searchResult) {
            return `<div class="col-8">
						<h5>
								<span class="text-success">${searchResult.promotionalContent}</span>
						</h5>`;
        },

        declarationWithoutContentMarkup: function () {
            return `<div class="col-8">`;
        },

        baseSearchResultMarkup: function (searchResult) {
            searchResult.vendors = searchResult.vendors || [];
            return `<h5>
					    <a class="hyppo-text-primary font-weight-bold" href="/internal/restaurants/${searchResult.id}">${searchResult.name}</a>
					</h5>
					<div class="no-gutters">
						<small class="">${searchResult.address}</small>
					</div>
					<div class="no-gutters">
						<small class="">${searchResult.distance.toFixed(2)} miles away | Served to you by</small>
					</div>
                    <div>`
                        + searchResult.vendors.map(function(vendor) {
                            return `<span class="d-inline-block my-2 mr-3 hyppo-vendor-` + vendor.substr(0, 6).toLowerCase() + `"></span>`;
                        }).join('') +
					`</div>
                 </div>
                 <div class="col-4 text-right">`
                        + [1, 2, 3, 4, 5].map(function(i) {
                            return `<span class="mx-1 fa fa-star` + (searchResult.rating >= i ? '' : '-o') + `"></span>`;
                }).join('') +
                 `</div>
			</div>`;
        }
    }
};