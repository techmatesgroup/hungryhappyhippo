ACC.restaurantDetails = {

    _autoload: [
        ["bindVendorPanelTab", $('#restaurantDetailsPageWrapper').length > 0]
    ],

    vendorRestaurantMenuData: {},

    vendorRestaurantAvailabilityData: {},

    bindVendorPanelTab: function () {
        $('#vendorTab a').on('click', function (e) {
            e.preventDefault();
            $(this).tab('show');

            if (!$(this).attr('availability-load-in-progress')) {
                let $vendorTabContentId = $(this).attr('href');
                let vendorRestaurantId = $($vendorTabContentId).attr('id');
                let vendor = $($vendorTabContentId).data('vendor');

                let savedAvailabilityData = ACC.restaurantDetails.vendorRestaurantAvailabilityData[vendorRestaurantId];
                if (savedAvailabilityData) {
                    ACC.restaurantDetails.populateAvailabilityContent(vendorRestaurantId, savedAvailabilityData);
                } else {
                    ACC.restaurantDetails.executeAvailabilitySearch(vendorRestaurantId, vendor);
                }
            }

            if (!$(this).attr('menu-load-in-progress')) {
                let $vendorTabContentId = $(this).attr('href');
                let vendorRestaurantId = $($vendorTabContentId).attr('id');
                let vendor = $($vendorTabContentId).data('vendor');

                let savedMenuCategoryDataList = ACC.restaurantDetails.vendorRestaurantMenuData[vendorRestaurantId];
                if (savedMenuCategoryDataList) {
                    ACC.restaurantDetails.populateMenusContent(vendorRestaurantId, savedMenuCategoryDataList);
                } else {
                    ACC.restaurantDetails.executeMenuSearch(vendorRestaurantId, vendor);
                }
            }

        });

        $('#vendorTab a:first').click();
    },

    executeMenuSearch: function (vendorRestaurantId, vendor) {
        let $tabLinkSelector = $('#' + ACC.restaurantDetails.escapeVendorId(vendorRestaurantId) + '-tab');

        $tabLinkSelector.attr('menu-load-in-progress', true);
        $.ajax({
            url: "/internal/restaurants/menu/",
            data: {
                vendorRestaurantId: vendorRestaurantId,
                vendor: vendor
            },
            success: [
                function (result) {
                    let menuCategoryDataList = result.menuCategoryDataList;
                    if (result.menuCategoryDataList) {
                        ACC.restaurantDetails.vendorRestaurantMenuData[vendorRestaurantId] = menuCategoryDataList;
                    }
                    ACC.restaurantDetails.populateMenusContent(vendorRestaurantId, menuCategoryDataList);
                    $tabLinkSelector.attr('menu-load-in-progress', false);
                }
            ]
        });
    },

    populateMenusContent: function (vendorRestaurantId, menuCategoryDataList) {
        let menusMarkup = '';
        if (menuCategoryDataList) {
            for (let i = 0; i < menuCategoryDataList.length; i++) {
                let menuCategory = menuCategoryDataList[i];
                menusMarkup += ACC.restaurantDetails.markup.menuCategoryMarkup(menuCategory);
            }
        } else {
            console.log("Empty result for menu. VendorRestaurantId - " + vendorRestaurantId);
            menusMarkup = 'No menu found';
        }

        let $vendorRestaurantMenuInfo = $('#' + ACC.restaurantDetails.escapeVendorId(vendorRestaurantId)).find('#vendorRestaurantMenuInfo');
        $vendorRestaurantMenuInfo.html(menusMarkup);
    },

    executeAvailabilitySearch: function (vendorRestaurantId, vendor) {
        let $tabLinkSelector = $('#' + ACC.restaurantDetails.escapeVendorId(vendorRestaurantId) + '-tab');

        $tabLinkSelector.attr('availability-load-in-progress', true);
        $.ajax({
            url: "/internal/restaurants/availability/" + vendorRestaurantId + "/" + vendor + "/" + window.location.search,
            success: [
                function (result) {
                    let restaurantAvailabilityDetails = result.vendorRestaurantAvailabilityDetails;
                    if (result.restaurantAvailabilityDetails) {
                        ACC.restaurantDetails.vendorRestaurantAvailabilityData[vendorRestaurantId] = restaurantAvailabilityDetails;
                    }
                    ACC.restaurantDetails.populateAvailabilityContent(vendorRestaurantId, restaurantAvailabilityDetails);
                    $tabLinkSelector.attr('availability-load-in-progress', false);
                }
            ]
        });
    },

    populateAvailabilityContent: function (vendorRestaurantId, restaurantAvailabilityDetails) {
        let availabilityMarkup = '';
        if (restaurantAvailabilityDetails) {
            availabilityMarkup += ACC.restaurantDetails.markup.availabilityMarkup(restaurantAvailabilityDetails);
        } else {
            console.log("Empty availability data. VendorRestaurantId - " + vendorRestaurantId);
            availabilityMarkup = "No availability information";
        }

        let $restaurantAvailabilityDetails = $('#' + ACC.restaurantDetails.escapeVendorId(vendorRestaurantId)).find('#restaurantAvailabilityDetails');
        $restaurantAvailabilityDetails.html(availabilityMarkup);
    },

    nvlProperties: function (obj) {
        for (let property in obj) {
            let objElement = obj[property];
            obj[property] = objElement != null ? objElement : 'Unavailable';
        }
        return obj;
    },

    escapeVendorId(value) {
        return value ? value.replace('.', '\\.') : '';
    },

    markup: {
        menuCategoryMarkup: function (menuCategory) {
            return `            
            <div class="vendor-restaurant-menu pt-2 pb-2">
                <div class="col">
                    <strong>${menuCategory.menuCategoryName}</strong>
                </div>
                <div class="col">
                    <span class="text-muted">` + (menuCategory.description ? menuCategory.description : '') + `</span>
                </div>
                <div>
                    ` + ACC.restaurantDetails.markup.buildProductItemsContent(menuCategory) + `
                </div>
            </div>
                     `;
        },

        buildProductItemsContent: function (menuCategory) {
            let content = ``;
            let productItemList = menuCategory.productItemList;
            if (productItemList) {
                for (let i = 0; i < productItemList.length; i++) {
                    let productItem = productItemList[i];

                    content += `
                            <div class="col pt-1 pb-1">
                               <div class="col">
                                    <span class="text-info">${productItem.name}</span>
                               </div> 
                               <div class="col">
                                    <small><span class="text-muted">${productItem.description}</span></small>
                               </div> 
                               <div class="col">
                                    <small>` + this.restaurantPriceMarkup(productItem.price) + `</small>
                               </div> 
                            </div> 
                        `;

                }
            }
            return content;
        },

        restaurantPriceMarkup: function (price, showFree) {
            return `<span class="price ${price != null && price > 0 ? 'price-available' : price != null && price === 0 ? 'price-free' : 'price-unavailable'} text-info">
						${price != null && price > 0 ? '$' + price.toFixed(2) : price != null && price === 0 ? (showFree ? 'Free' : 'None') : 'Unavailable'}
					</span>`;
        },

        availabilityMarkup: function (availabilityDetails) {
            availabilityDetails = ACC.restaurantDetails.nvlProperties(availabilityDetails);
            return `
        <div class="col">
            Open: <span class="text-muted">${availabilityDetails.open}</span>
        </div>
        <div class="col">
            Pickup Currently Available: <span class="text-muted">${availabilityDetails.pickupAvailable}</span>
        </div>
        <div class="col">
            Delivery Currently Available: <span class="text-muted">${availabilityDetails.deliveryAvailable}</span>
        </div>
        <div class="col">
            Target Address Deliverable: <span class="text-muted">${availabilityDetails.deliverableToTargetAddress}</span>
        </div>
        <div class="col">
            Pickup Estimate: <span class="text-muted">${availabilityDetails.pickupEstimate}</span>
        </div>
        <div class="col">
            Delivery Fee: <span class="text-muted">${availabilityDetails.deliveryFee}</span>
        </div>
        <div class="col">
            Delivery Estimate: <span class="text-muted">${availabilityDetails.deliveryEstimate}</span>
        </div>
        <div class="col">
            Pickup Order Minimum: <span class="text-muted">` + this.restaurantPriceMarkup(availabilityDetails.pickupOrderMinimum) + `</span>
        </div>
        <div class="col">
            Delivery Order Minimum: <span class="text-muted">` + this.restaurantPriceMarkup(availabilityDetails.deliveryOrderMinimum) + `</span>
        </div>
            `;
        }
    }
};