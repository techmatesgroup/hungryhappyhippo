ACC.header = {

	_autoload: [
		['bindAddressAutocomplete', $('#addressSearchForm').length > 0],
		['bindAddressChangedEvent', $('#addressSearchForm').length > 0],
		['populateHeaderAddress', $('#addressSearchForm').length > 0]
	],

    bindAddressAutocomplete: function() {
        // Create the autocomplete object and restrict suggestions to geographical location types
        let autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputAddress'), {types: ['geocode']});

        // For now we only need to request the target formatted address and location latitude/longitude fields
        autocomplete.setFields(['formatted_address', 'geometry']);

        // Set the latitude/longitude values from the selected address suggestion
        autocomplete.addListener('place_changed', function() {
            let geometry = autocomplete.getPlace().geometry;
            if (geometry) {
                $('#inputLatitude').val(geometry.location.lat());
                $('#inputLongitude').val(geometry.location.lng());

                let $addressSearchForm = $('#addressSearchForm');

                let pathName = window.location.pathname;
                $($addressSearchForm).attr('action', pathName !== "/" ? pathName : "/internal/restaurants/search/");

                let href = $addressSearchForm.attr('action') + '?' + $addressSearchForm.serialize();
                console.log(href);
                window.location.href = href;
            }
        });
    },

    bindAddressChangedEvent: function () {
        $('#addressSearchForm').on('submit', function (e) {
            e.preventDefault();
        });

        $("#inputAddress").keyup(function(event) {
            if (event.keyCode === 13) {
                google.maps.event.trigger($('#inputAddress'), 'place_changed');
            }
        });

    },

	populateHeaderAddress: function() {
		let urlParams = new URLSearchParams(window.location.search);

		let latitudeParam = urlParams.get('latitude');
		if (latitudeParam) {
			$('#inputLatitude').val(latitudeParam);
		}
		else {
			let latitudeCookie = ACC.getCookie('latitude');
			if (latitudeCookie) {
				$('#inputLatitude').val(latitudeCookie);
			}
		}

		let longitudeParam = urlParams.get('longitude');
		if (longitudeParam) {
			$('#inputLongitude').val(longitudeParam);
		}
		else {
			let longitudeCookie = ACC.getCookie('longitude');
			if (longitudeCookie) {
				$('#inputLongitude').val(longitudeCookie);
			}
		}

		let address = ACC.getCookie('address');
		if (address) {
			$('#inputAddress').val(address);
		}
		else {
			$('#inputAddress').attr('placeholder', 'Enter a target search location');
		}
	}
}
