ACC.vendorRestaurantDetails = {

    _autoload: [
        ["bindRestaurantDetailsEvents", $('#vendorRestaurantDetailsPageWrapper').length > 0]
    ],

    bindRestaurantDetailsEvents: function () {
        $(".js-menu-category-collapse").on("hide.bs.collapse", function (it) {
            $('.glyphicon_' + $(it.target).attr('id')).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
        });
        $(".js-menu-category-collapse").on("show.bs.collapse", function (it) {
            $('.glyphicon_' + $(it.target).attr('id')).removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
        });
    },
};