ACC.menus = {

    _autoload: [
        ["bindMenusSearchPageEvents", $('#menusSearchPageWrapper').length > 0]
    ],

    bindMenusSearchPageEvents : function () {

    },

    populateSearchResult : function (result) {
        let searchParams = result.menuSearchParams;
        let responseData = result.menuResponseData;
        let searchResults = responseData.data;

        ACC.searchPage.initPagedInfo(responseData);

        $("#searchResultsContainer").text("");

        for (let i = 0; i < searchResults.length; i++) {
            let searchResult = searchResults[i];
            let searchResultMarkup = ACC.menus.markup.searchResultMarkup(searchResult);
            $("#searchResultsContainer").append(searchResultMarkup);
        }
    },

    markup: {

        searchResultMarkup: function (searchResult) {
            return `
					<div class="searchResult border row">
						<div class="col-4 col-lg-3">
							<img src="${searchResult.imageUrl}" width="100%" height="100%">
						</div>
						<div class="col-8 col-lg-9">
                            <hr class="mr-0">
                            <div class="row">
                                <h5>
                                    <span class="text-info">${searchResult.menuName}</span>
                                </h5>
                            </div>
                            <div class="row">
                                <small class="text-muted">vendor: ${searchResult.vendor}</small>
                            </div>
                            <div class="row">
                                <small class="text-muted">restaurantName: ${searchResult.restaurantName}</small>
                            </div>
                            <div class="row">
                                <small class="text-muted">menuCategory: ${searchResult.menuCategory}</small>
                            </div>
                            <div class="row">
                                <small class="text-muted">name: ${searchResult.name}</small>
                            </div>
                            <div class="row">
                                <small class="text-muted">price: ${searchResult.price}</small>
                            </div>
                            <div class="row">
                                <small class="text-muted">description: ${searchResult.description}</small>
                            </div>
                            <hr class="mr-0">
						</div>
					</div>
				`;
        },

    }

};