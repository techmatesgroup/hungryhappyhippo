ACC.searchVendorRestaurants = {

    _autoload: [
        ["bindSearchPageEvents", $('#vendorRestaurantsSearchPageWrapper').length > 0 || $('#menusSearchPageWrapper').length > 0]
    ],

    bindSearchPageEvents: function () {
        $(document).on('change', '#searchCriteriaForm :input', function () {
            $('#inputSearchPageNumber').val(0);
            ACC.searchVendorRestaurants.executeSearchRequest();
        });

        $(document).on('click', '#executeSearchButtonAjax', function () {
            $('#inputSearchPageNumber').val(0);
            ACC.searchVendorRestaurants.executeSearchRequest();
            return false;
        });

        $(document).on('change', '#inputPageOptions', ACC.searchVendorRestaurants.executeSearchRequest);
        $(document).on('change', '#inputSortOptions', ACC.searchVendorRestaurants.executeSearchRequest);

        $(document).on('click', '#searchPaginationControl button', function () {
            $('#inputSearchPageNumber').val($(this).data('page'));
            ACC.searchVendorRestaurants.executeSearchRequest();
            return false;
        });

        $('.js-sliders').each(function () {
            ACC.searchVendorRestaurants.initSlider($(this));
        });
    },

    executeSearchRequest: function () {
        ACC.vendorRestaurants.currentPageNumber = 0; // always show first page if filters/sorting were changed
        let searchParams = $("#searchCriteriaForm").serialize();

        let targetUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + searchParams;
        $.ajax({
            url: targetUrl + "&json=true",
            success: [
                function (result) {
                    ACC.searchVendorRestaurants.populateSearchResult(result);

                    window.history.pushState({"html": result, "pageTitle": result.pageTitle}, "", targetUrl);
                }
            ]
        });
    },

    executeLoadMoreRequest : function () {
        let searchParams = $("#searchCriteriaForm").serialize() + '&pageNumber=' + (++ACC.vendorRestaurants.currentPageNumber);
        let targetUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + searchParams;
        $.ajax({
            url: targetUrl + "&json=true",
            success: [
                function (result) {
                    ACC.searchVendorRestaurants.populateSearchResult(result);
                }
            ]
        });
    },

    populateSearchResult: function (result) {
        if ($('#vendorRestaurantsSearchPageWrapper').length > 0) {

            return ACC.vendorRestaurants.populateSearchResult(result);

        } else if ($('#menusSearchPageWrapper').length > 0) {

            return ACC.menus.populateSearchResult(result);

        }
    },

    initPagedInfo: function (responseData) {
        let pageNumber = responseData.pageNumber;
        let pageSize = responseData.pageSize;
        let dataSize = responseData.dataSize;
        let totalPages = responseData.totalPages;
        let totalResults = responseData.totalResults;

        let $searchPaginationFirstButton = $('#searchPaginationFirstButton');
        let $searchPaginationBackButton = $('#searchPaginationBackButton');
        let $searchPaginationNextButton = $('#searchPaginationNextButton');
        let $searchPaginationLastButton = $('#searchPaginationLastButton');

        if (pageNumber > 0) {
            $searchPaginationFirstButton.data('page', 0);
            $searchPaginationFirstButton.prop('disabled', false);
            $searchPaginationBackButton.data('page', pageNumber - 1);
            $searchPaginationBackButton.prop('disabled', false);
        } else {
            $searchPaginationFirstButton.data('page', 0);
            $searchPaginationFirstButton.prop('disabled', true);
            $searchPaginationBackButton.data('page', 0);
            $searchPaginationBackButton.prop('disabled', true);
        }

        if (pageNumber < totalPages - 1) {
            $searchPaginationNextButton.data('page', pageNumber + 1);
            $searchPaginationNextButton.prop('disabled', false);
            $searchPaginationLastButton.data('page', totalPages - 1);
            $searchPaginationLastButton.prop('disabled', false);
        } else {
            $searchPaginationNextButton.data('page', totalPages - 1);
            $searchPaginationNextButton.prop('disabled', true);
            $searchPaginationLastButton.data('page', totalPages - 1);
            $searchPaginationLastButton.prop('disabled', true);
        }

        let searchResultInfo = '';
        let paginationInfo = '';

        if (totalResults <= 0) {
            searchResultInfo = "No items found matching the provided search criteria/filters";
            paginationInfo = 'No items available';
        } else {
            searchResultInfo = `Showing items ${(pageNumber * pageSize) + 1} to ${(pageNumber * pageSize) + dataSize} out of ${totalResults} total results`;
            paginationInfo = `Showing page ${pageNumber + 1} out of ${totalPages} total result pages at ${pageSize} items per page`;
        }

        $("#searchResultsInfo").text(searchResultInfo);
        $("#searchPaginationInfo").text(paginationInfo);
    },

    displayRangeValue : function (slideEvt) {
        let rangeValues = slideEvt.value;

        if (rangeValues.length >= 2) {
            $($(slideEvt.target).data('from-value-selector')).text(rangeValues[0]);
            $($(slideEvt.target).data('to-value-selector')).text(rangeValues[1]);
        } else {
            $($(slideEvt.target).data('from-value-selector')).text(rangeValues);
        }
    },

    initSlider: function ($sliderInput) {
        $sliderInput.on("change", function (e) {
            e.preventDefault();
            return false;
        });

        $sliderInput.slider({});

        let onSlideStop = function (slideEvt) {
            ACC.searchVendorRestaurants.displayRangeValue(slideEvt);

            let inputFrom = $(slideEvt.target).data('input-from');
            let inputTo = $(slideEvt.target).data('input-to');

            let rangeValues = slideEvt.value;
            if (rangeValues.length >= 2) {
                $(inputFrom).val(rangeValues[0]);
                $(inputTo).val(rangeValues[1]);

                $(inputTo).prop('checked', true);
            } else {
                $(inputFrom).val(rangeValues);

            }
            $(inputFrom).prop('checked', true);

            ACC.searchVendorRestaurants.executeSearchRequest();
        };

        $sliderInput.on("slide", ACC.searchVendorRestaurants.displayRangeValue);
        $sliderInput.on("slideStop", onSlideStop);

        ACC.searchVendorRestaurants.displayRangeValue({
            value: $sliderInput.data('slider-value'),
            target: $sliderInput
        });

    },

};