ACC.vendorRestaurants = {

    _autoload: [
        ["bindRestaurantsSearchPageEvents", $('#vendorRestaurantsSearchPageWrapper').length > 0],
        ["autoSuggestRestaurants", $('#vendorRestaurantsSearchPageWrapper').length > 0 && $('#inputSearchTerms').length > 0]
    ],

    autoSuggestRestaurants: function () {
        let options = {
            url: function (term) {
                return '/internal/vendorrestaurants/suggest/?term=' + term;
            },
            requestDelay: 400,
            listLocation: "restaurantNames"
        };
        $("#inputSearchTerms").easyAutocomplete(options);
    },

    currentPageNumber : 0,

    bindRestaurantsSearchPageEvents: function () {
        let $deliveryButton = $('#deliveryButton');
        let $pickupButton = $('#pickupButton');
        let $serviceOption = $('#serviceOption');

        $deliveryButton.on("click", function () {
            $(this).toggleClass("btn-success").toggleClass("btn-dark")
            $pickupButton.addClass("btn-dark").removeClass("btn-success");
            $serviceOption.val($(this).hasClass('btn-dark') ? '' : $(this).data('service-option'));
            $(this).trigger('change');
        });
        $pickupButton.on("click", function () {
            $(this).toggleClass("btn-success").toggleClass("btn-dark")
            $deliveryButton.addClass("btn-dark").removeClass("btn-success");
            $serviceOption.val($(this).hasClass('btn-dark') ? '' : $(this).data('service-option'));
            $(this).trigger('change');
        });

        $('#loadMoreControl').on('click', function () {
            ACC.searchVendorRestaurants.executeLoadMoreRequest();
        });

        $(document).on('click', '#backToTopOfPageControl', function () {
            $('html,body').animate({scrollTop: 0}, 'slow');
            return false;
        });

        $(document).on('change', '#inputSearchTerms', ACC.searchVendorRestaurants.executeSearchRequest);

    },

    populateSearchResult: function (result) {
        let responseData = result.vendorRestaurantResponseData;
        let searchResults = responseData.data;

        let pageNumber = responseData.pageNumber;
        let pageSize = responseData.pageSize;
        let dataSize = responseData.dataSize;
        let loadedResults = pageNumber == 0 ? 0 : $('.searchResult').length;
        let totalResults = responseData.totalResults;

        let searchResultInfo = '';

        if (totalResults <= 0) {
            searchResultInfo = "No items found matching the provided search criteria/filters";
        } else {
            searchResultInfo = `Showing ${loadedResults + dataSize} items of ${totalResults} total results`;

        }
        $("#searchResultsInfo").text(searchResultInfo);

        let vendorCategories = responseData.filterData.vendorCategories;

        $("#inputSearchCategories").text("");
        for (let i = 0; i < vendorCategories.length; i++) {
            let category = vendorCategories[i];
            let categoryFilterMarkup = ACC.vendorRestaurants.markup.categoryFilterMarkup(category);
            $("#inputSearchCategories").append(categoryFilterMarkup);
        }

        if (ACC.vendorRestaurants.currentPageNumber === 0) {
            $("#searchResultsContainer").text("");
        }

        if (responseData.hasMoreResults) {
            $('#loadMoreControl').removeClass('d-none');
        } else {
            $('#loadMoreControl').addClass('d-none');
        }

        if (searchResults.length) {
            $('#backToTopOfPageControl').removeClass('d-none');
            $('#sortOptionsControl').removeClass('d-none');
        } else {
            $('#backToTopOfPageControl').addClass('d-none');
            $('#sortOptionsControl').addClass('d-none');
        }

        for (let i = 0; i < searchResults.length; i++) {
            let searchResult = searchResults[i];
            let searchResultMarkup = ACC.vendorRestaurants.makeSearchResultMarkup(searchResult);
            $("#searchResultsContainer").append(searchResultMarkup);
        }
    },

    makeSearchResultMarkup: function (searchResult) {
        let searchResultMarkup = ACC.vendorRestaurants.markup.headerAndImageMarkup(searchResult);

        if(searchResult.promotionalContent) {
            searchResultMarkup += ACC.vendorRestaurants.markup.promotionalContentMarkup(searchResult);
        }
        else {
            if(searchResult.promotionalWeight > 0) {
                searchResult.promotionalContent = "Featured";
                searchResultMarkup += ACC.vendorRestaurants.markup.promotionalContentMarkup(searchResult);
            }
            else {
                searchResultMarkup += ACC.vendorRestaurants.markup.declarationWithoutContentMarkup();
            }
        }

        searchResultMarkup += ACC.vendorRestaurants.markup.baseSearchResultMarkup(searchResult);
        return searchResultMarkup;
    },

    markup: {

        categoryFilterMarkup: function (category) {
            if (category.checked) {
                return `
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="inputSearchCategory${category.name}" name="categories" value="${category.name}" checked="checked">
							<label class="form-check-label" for="inputSearchCategory${category.name}">${category.name}</label>
							<span>(${category.count})</span>
						</div>
					`;
            } else {
                return `
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="inputSearchCategory${category.name}" name="categories" value="${category.name}">
							<label class="form-check-label" for="inputSearchCategory${category.name}">${category.name}</label>
							<span>(${category.count})</span>
						</div>
					`;
            }
        },

        headerAndImageMarkup: function (searchResult) {
            return `<div class="searchResult border row">
					<div class="col-4 col-lg-3">
						<img src="${searchResult.imageUrl}" width="100%" height="100%">
					</div>`;
        },

        promotionalContentMarkup: function (searchResult) {
            return `<div class="col-9 col-lg-9">
					<hr class="mr-0">
						<div class="row" id="promotionalContent">
							<h5>
								<span class="text-success">${searchResult.promotionalContent}</span>
							</h5>
						</div>`;
        },

        declarationWithoutContentMarkup: function () {
            return `<div class="col-8 col-lg-9">
					<hr class="mr-0">`;
        },

        baseSearchResultMarkup: function (searchResult) {
            return `<div class="row">
					<h5>
						<span class="text-dark font-weight-bold">${searchResult.name}</span>
						<small class="text-muted font-italic"> - via ${searchResult.vendor}</small>
						<small>
							<a class="font-italic" href="${searchResult.vendorUrl}" target="_blank">Go to vendor site</a>
						</small>
					</h5>
				</div>
					<div class="row">
						<small class="text-muted">Address: ${searchResult.address}</small>
					</div>
					<div class="row">
						<small class="text-muted">Location: ${searchResult.location.lat}, ${searchResult.location.lon}</small>
					</div>
					<div class="row">
						${ACC.vendorRestaurants.markup.restaurantRatingMarkup(searchResult.rating)}
					</div>
							<hr class="mr-0">
							<div class="row">
								<div class="col">
									<p class="text-muted mb-0">Delivery Supported: ${ACC.vendorRestaurants.markup.restaurantBooleanFlagMarkup(searchResult.delivery)}</p>
								</div>
								<div class="col">
									<p class="text-muted mb-0">Pickup Supported: ${ACC.vendorRestaurants.markup.restaurantBooleanFlagMarkup(searchResult.pickup)}</p>
								</div>
							</div>
							<hr class="mr-0">
							<div class="row">
								<div class="col">
								Vendor Categories: <small class="text-muted">${searchResult.vendorCategories}</small>
								</div>
							</div>
							<hr class="mr-0">
							<div class="row justify-content-center">
								<a class="text-info" href="/internal/vendorrestaurants/${searchResult.id}${window.location.search}">View Details</a>
							</div>
							<hr class="mr-0">
					</div>
				</div>`;
        },

		restaurantRatingMarkup: function (rating) {
			return `<span class="rating ${rating != null && rating > 0 ? 'rating-available text-success' : 'rating-unavailable text-secondary'} font-weight-bold">
						${rating != null && rating > 0 ? rating.toFixed(1).replace(/\.0$/, '') + ' out of 5 stars' : 'Not yet rated'}
					</span>`;
		},

		restaurantBooleanFlagMarkup: function(boolean) {
			return `<span class="boolean ${boolean ? 'boolean-yes' : boolean != null ? 'boolean-no' : 'boolean-unavailable'} text-info">
						${boolean ? 'Yes' : boolean != null ? 'No' : 'Unavailable'}
					</span>`;
		},

		restaurantDurationMarkup: function(duration) {
			return `<span class="duration ${duration != null && duration > 0 ? 'duration-available' : 'duration-unavailable'} duration-minutes text-info">
						${duration != null && duration > 0 ? duration.toFixed(0) + ' minutes' : 'Unavailable'}
					</span>`;
		}
    }
};