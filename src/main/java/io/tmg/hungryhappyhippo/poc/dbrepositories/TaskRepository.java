package io.tmg.hungryhappyhippo.poc.dbrepositories;

import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends MongoRepository<TaskModel, String> {

	Page<TaskModel> findByIdContainsIgnoreCaseOrNameContainsIgnoreCase(String id, String name, Pageable pageable);

}
