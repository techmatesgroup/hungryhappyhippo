package io.tmg.hungryhappyhippo.poc.dbrepositories;

import io.tmg.hungryhappyhippo.poc.models.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

    UserModel findByUsername(String username);

    boolean existsByRoles(String role);

    Page<UserModel> findAllByIdIgnoreCaseContainingOrUsernameIgnoreCaseContaining(String id, String username, Pageable pageable);
}
