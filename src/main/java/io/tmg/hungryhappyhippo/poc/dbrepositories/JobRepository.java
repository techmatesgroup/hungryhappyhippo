package io.tmg.hungryhappyhippo.poc.dbrepositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import io.tmg.hungryhappyhippo.poc.models.JobModel;

@Repository
public interface JobRepository extends MongoRepository<JobModel, String> {

	Page<JobModel> findByIdContainsIgnoreCaseOrNameContainsIgnoreCase(String id, String name, Pageable pageable);
}
