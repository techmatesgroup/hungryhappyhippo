package io.tmg.hungryhappyhippo.poc.models.dto;

import io.tmg.hungryhappyhippo.poc.models.validator.PasswordsEqualConstraint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@ToString(of = {"id", "username", "email", "enabled", "roles"})
@Getter
@Setter
@PasswordsEqualConstraint
public class UserDto {

    private String id;

    @NotEmpty
    private String username;
    private String password;
    private String confirmPassword;

    @NotEmpty
    @Email
    private String email;
    private boolean enabled;
    private Set<String> roles;
    private String address;
    @Min(-90)
    @Max(90)
    private double latitude;
    @Min(-180)
    @Max(180)
    private double longitude;
}
