package io.tmg.hungryhappyhippo.poc.models.dto;

import io.tmg.hungryhappyhippo.poc.models.TargetLocationType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class LocationDto {
    public String id;
    @NotEmpty
    public String name;
    public TargetLocationType type;
    @Min(-90)
    @Max(90)
    @NotNull
    public Double latitude;
    @Min(-180)
    @Max(180)
    @NotNull
    public Double longitude;
    public Double radius;
}