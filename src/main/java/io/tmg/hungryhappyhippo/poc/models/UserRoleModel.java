package io.tmg.hungryhappyhippo.poc.models;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@ToString(of = {"id", "name"})
@NoArgsConstructor
public class UserRoleModel {

    @Id
    public String id;

    public String name;
    //private Set<UserModel> users;

    /*
    @ManyToMany(mappedBy = "roles")
    public Set<UserModel> getUsers() {
        return users;
    }
    */

}
