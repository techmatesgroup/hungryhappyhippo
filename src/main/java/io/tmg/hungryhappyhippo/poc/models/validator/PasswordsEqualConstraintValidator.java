package io.tmg.hungryhappyhippo.poc.models.validator;

import io.tmg.hungryhappyhippo.poc.models.dto.UserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordsEqualConstraintValidator implements ConstraintValidator<PasswordsEqualConstraint, UserDto> {

    @Override
    public boolean isValid(UserDto dto, ConstraintValidatorContext context) {
        boolean valid = dto.getPassword().equals(dto.getConfirmPassword());
        if (!valid) {
            context.disableDefaultConstraintViolation();
            context
                    .buildConstraintViolationWithTemplate("Passwords do not match")
                    .addPropertyNode("confirmPassword")
                    .addConstraintViolation();
        }
        return valid;
    }
}