
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement
public class GrubHubMenuResponseDTO {

    private Restaurant restaurant;

}
