package io.tmg.hungryhappyhippo.poc.models;

public enum TaskStatus {

	NONE,
	NEW,
	SCHEDULED,
	RUNNING,
	FINISHED,
	SUCCESS,
	ERROR,
	FAILURE,
	KILLED,
	PAUSED,
	RESUMED,
	BLOCKED,
	DISABLED,
	UNKNOWN,

}
