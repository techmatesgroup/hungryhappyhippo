package io.tmg.hungryhappyhippo.poc.models;

import lombok.NoArgsConstructor;
import lombok.ToString;

/* For now there is no need to persist addresses or define document types */
@ToString(of = {"address"}, callSuper=true)
@NoArgsConstructor
public class AddressModel extends LocationModel {

	public AddressModel(String address, Double latitude, Double longitude) {

		super(address, TargetLocationType.ADDRESS.toString(), latitude, longitude, 0d);
		this.address = address;

	}

	public String address;

}
