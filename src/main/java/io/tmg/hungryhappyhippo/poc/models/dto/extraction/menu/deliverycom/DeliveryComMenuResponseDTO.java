
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement
public class DeliveryComMenuResponseDTO {

    private List<Menu> menu;

}
