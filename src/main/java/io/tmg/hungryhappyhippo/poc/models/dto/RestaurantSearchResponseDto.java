package io.tmg.hungryhappyhippo.poc.models.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Response DTO for restaurant search page
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Response DTO for restaurant search page")
public class RestaurantSearchResponseDto {

	private List<RestaurantDto> restaurants;

	@ApiModelProperty(position = 1, example = "0")
	private Integer pageNumber;

	@ApiModelProperty(position = 2, example = "20")
	private Integer pageSize;

	@ApiModelProperty(position = 3, example = "40")
	private Integer totalPages;

	@ApiModelProperty(position = 4, example = "2000")
	private Long totalResults;

	@ApiModelProperty(position = 5, example = "true")
	private boolean hasMoreResults;


}
