package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/08/2019
 */
@Setter
@Getter
@NoArgsConstructor
public class ItemGroup {

    private List<Item> items;

    @JsonProperty("category_name")
    private String categoryName;

}
