
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Menu {

    private List<Child> children;
    private String description;
    private String id;
    private String name;
    private String type;

}
