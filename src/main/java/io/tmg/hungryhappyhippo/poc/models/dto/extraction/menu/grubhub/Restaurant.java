
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Restaurant {

    @JsonProperty("menu_category_list")
    private List<MenuCategoryList> menuCategoryList;

}
