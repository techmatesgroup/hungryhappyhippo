package io.tmg.hungryhappyhippo.poc.models.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Getter
@Setter
@ToString(of = {"id", "name", "vendor"})
public class VendorRestaurantDto {
	public String id;
	public String restaurantAggregateId;

	@NotEmpty
	public String name;

	@NotEmpty
	public String vendor;
	@NotEmpty
	public String vendorUrl;

	public String imageUrl;
	public String logoUrl;

	@Min(-90)
	@Max(90)
	@NotNull
	public Double latitude;

	@Min(-180)
	@Max(180)
	@NotNull
	public Double longitude;

	@NotEmpty
	public String address;

	public String googlePlaceId;

	public Boolean delivery;
	public Boolean pickup;

	@Min(0)
	@Max(5)
	public Double rating;

	public Collection<String> vendorCategories;
	public Collection<String> keywords;

	public Collection<String> menus;

	public Double promotionalWeight;
	public String promotionalContent;
}
