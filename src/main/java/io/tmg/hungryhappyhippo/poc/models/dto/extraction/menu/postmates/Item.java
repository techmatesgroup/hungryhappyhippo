package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/08/2019
 */
@Setter
@Getter
@NoArgsConstructor
public class Item {

    @JsonProperty("base_price")
    private String basePrice;

    @JsonProperty("category_name")
    private String categoryName;

    @JsonProperty("product_description")
    private String productDescription;

    @JsonProperty("product_name")
    private String productName;

    @JsonProperty("product_uuid")
    private String productUuid;

}
