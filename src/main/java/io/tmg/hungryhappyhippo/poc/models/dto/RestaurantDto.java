package io.tmg.hungryhappyhippo.poc.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * DTO Representation of {@link io.tmg.hungryhappyhippo.poc.models.RestaurantModel}
 */
@Getter
@Setter
@ToString(of = {"id", "name", "vendorRestaurants"})
@ApiModel(description = "Short data of restaurant (for search page)")
public class RestaurantDto {

	@ApiModelProperty(required = true, example = "45xlFGkBY4bSsKP0KWOQ")
	public String id;

	@ApiModelProperty(required = true, example = "Kentucky Fried Chicken", position = 1)
	@NotEmpty
	public String name;

	@ApiModelProperty(position = 2, example = "This is the coolest restaurant ever", allowEmptyValue = true)
	public String description;

	@ApiModelProperty(position = 3, example = "30.2672")
	@Min(-90)
	@Max(90)
	@NotNull
	public Double latitude;

	@ApiModelProperty(position = 4, example = "-97.7426")
	@Min(-180)
	@Max(180)
	@NotNull
	public Double longitude;

	@ApiModelProperty(position = 5, example = "761 7th Ave, New York, NY 10019, США")
	@NotEmpty
	public String address;

	@ApiModelProperty(position = 6, example = "ChIJlUuW0LC1RIYRd8dXa8gUw5A")
	public String googlePlaceId;

	@ApiModelProperty(position = 7, example = "4.0")
	private Double distance;

	@ApiModelProperty(position = 8, example = "Asian, Lunch Specials", allowEmptyValue = true)
	public Set<String> categories;

	@ApiModelProperty(position = 9, example = "beer, chicken", allowEmptyValue = true)
	public Set<String> keywords;

	@ApiModelProperty(position = 10, example = "true")
	public Boolean delivery;

	@ApiModelProperty(position = 11, example = "false")
	public Boolean pickup;

	@ApiModelProperty(position = 12, example = "4.6", allowEmptyValue = true)
	@Min(0)
	@Max(5)
	public Float rating;

	@ApiModelProperty(position = 13, example = "5.0", allowEmptyValue = true)
	public Double promotionalWeight;

	@ApiModelProperty(position = 14, example = "Featured", allowEmptyValue = true)
	public String promotionalContent;

	@JsonProperty("vendors")
	@ApiModelProperty(required = true, position = 15, example = "DoorDash, UberEats")
	public Set<String> vendorRestaurants;

	@ApiModelProperty(required = true, position = 16, example = "/api/v1/restaurant/45xlFGkBY4bSsKP0KWOQ")
	public String restaurantDetailsUrl;

	@ApiModelProperty(position = 17, example = "3")
	public Double priceLevel;

}
