
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement
public class FavorDeliveryMenuResponseDTO {

    private Menu menu;

}
