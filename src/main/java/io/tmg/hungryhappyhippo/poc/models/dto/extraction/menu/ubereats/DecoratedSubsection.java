
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DecoratedSubsection {

    private List<DisplayItem> displayItems;
    private String title;
    private String uuid;

}
