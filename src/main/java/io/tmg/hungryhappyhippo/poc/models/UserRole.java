package io.tmg.hungryhappyhippo.poc.models;

public enum UserRole {
    ANONYMOUS, ADMIN, SITEUSER, RESTAURANTMANAGER;

    public String getRole() {
        return "ROLE_" + name();
    }
}
