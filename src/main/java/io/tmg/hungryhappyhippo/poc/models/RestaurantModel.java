package io.tmg.hungryhappyhippo.poc.models;

import io.tmg.hungryhappyhippo.poc.annotations.CategoryContextField;
import io.tmg.hungryhappyhippo.poc.annotations.CompletionField;
import io.tmg.hungryhappyhippo.poc.annotations.GeoContextField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@ToString(of = {"id", "name", "address"})
@NoArgsConstructor
@Document(indexName = "#{elasticsearchRestaurantIndex}", type = "#{elasticsearchDefaultDocType}")
public class RestaurantModel {

	@Id
	public String id;

	public String name;
	public String description;

	public GeoPoint location;
	public String address;
	public String googlePlaceId;

	public Set<String> categories; // contains forage category names
	public Set<String> keywords = new HashSet<>();

	public Boolean delivery;
	public Boolean pickup;
	public Float rating;

	public Double promotionalWeight;
	public String promotionalContent;

	public Double priceLevel;

	@Field(type = FieldType.Nested)
	public List<VendorRestaurant> vendorRestaurants = new ArrayList<>();

	@CompletionField(
			categoryContext = @CategoryContextField(name = "name", path = "name"),
			geoContext = @GeoContextField(name = "location", path = "location", precision = "5miles")
	)
	public Completion suggest;

	@Getter
	@Setter
	@ToString
	@NoArgsConstructor
	public static class VendorRestaurant {
		public String id;
		public String vendor;
		public String vendorUrl;

		public Boolean delivery;
		public Boolean pickup;
		public Float rating;

		public Collection<String> vendorCategories;
	}
}
