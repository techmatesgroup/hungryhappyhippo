
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Section {

    private String uuid;

}
