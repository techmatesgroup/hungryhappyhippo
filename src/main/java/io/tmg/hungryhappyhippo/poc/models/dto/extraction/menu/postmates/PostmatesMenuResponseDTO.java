
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement
public class PostmatesMenuResponseDTO {

    @JsonProperty("item_groups")
    private List<ItemGroup> itemGroups;

}
