
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MenuItemList {

    private String id;
    private Boolean available;
    private Boolean deleted;
    private String description;
    private String name;
    private Boolean popular;
    private Price price;
    private String uuid;

    @JsonProperty("menu_category_id")
    private String menuCategoryId;

    @JsonProperty("menu_category_name")
    private String menuCategoryName;

    @JsonProperty("minimum_price_variation")
    private Price minimumPriceVariation;

}
