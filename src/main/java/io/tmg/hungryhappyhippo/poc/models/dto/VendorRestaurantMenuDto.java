package io.tmg.hungryhappyhippo.poc.models.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Restaurant menus based on vendor")
public class VendorRestaurantMenuDto {

	@ApiModelProperty("Menu categories")
	private List<MenuCategoryData> menuCategories;

}
