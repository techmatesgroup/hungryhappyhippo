
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DoorDashMenuResponseDTO {

    private List<DoorDashMenuItemResponse> doorDashMenuItemResponseList;

}
