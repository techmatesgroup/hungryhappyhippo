package io.tmg.hungryhappyhippo.poc.models;

public enum TargetLocationType {

	ADDRESS,
	INTERSECTION,
	CITY,
	REGION,
	STATE,
	COUNTRY,

}
