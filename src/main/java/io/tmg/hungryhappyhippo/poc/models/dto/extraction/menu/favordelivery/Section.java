
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Section {

    private String name;

    @JsonProperty("menu_items")
    private List<MenuItem> menuItems;

}
