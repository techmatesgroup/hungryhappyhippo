package io.tmg.hungryhappyhippo.poc.models.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"status"}, callSuper=true)
public class TaskDto extends JobDto {

	public String status;

	public Date lastStartTime;
	public Date lastEndTime;
	public String lastExecutionStatus;
	public String lastExecutionResult;

}
