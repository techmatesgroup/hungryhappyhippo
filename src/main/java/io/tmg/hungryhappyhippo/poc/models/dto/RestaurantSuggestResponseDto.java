package io.tmg.hungryhappyhippo.poc.models.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/25/2019
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Response DTO for restaurant's suggestion")
public class RestaurantSuggestResponseDto {

	@ApiModelProperty(example = "[\n" +
			"    \"KFC\",\n" +
			"    \"K Cupbop\",\n" +
			"    \"Kick Butt Café\",\n" +
			"    \"Kicked Up Grub\"\n" +
			"  ]", allowEmptyValue = true)
	private List<String> restaurantNames;

	public void add(String restaurantName) {
		if (restaurantNames == null) {
			restaurantNames = new ArrayList<>();
		}

		restaurantNames.add(restaurantName);
	}

}
