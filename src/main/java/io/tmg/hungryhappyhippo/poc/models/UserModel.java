package io.tmg.hungryhappyhippo.poc.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Document("user")
@ToString(of = {"id", "username", "email"})
@Getter
@Setter
@NoArgsConstructor
public class UserModel {

    @Id
    private String id;
    @Indexed(unique = true)
    private String username;
    private String password;

    @Indexed(unique = true)
    private String email;
    private boolean enabled = true;
    private Set<String> roles = new HashSet<>();
    private String address;
    private double latitude;
    private double longitude;

    public void addRole(UserRole role) {
        if (roles == null) {
            roles = new HashSet<>();
        }
        roles.add(role.name());
    }

}
