package io.tmg.hungryhappyhippo.poc.models;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

@ToString(of = {"id", "name", "type", "radius"})
@NoArgsConstructor
@Document(indexName = "#{elasticsearchLocationIndex}", type = "#{elasticsearchDefaultDocType}")
public class LocationModel {

	public LocationModel(String name, String type, Double latitude, Double longitude, Double radius) {

		this.id = String.format("location-%s-%s", type, name);
		this.name = name;
		this.type = type;
		this.location = new GeoPoint(latitude, longitude);
		this.radius = radius;

	}

	@Id
	public String id;

	public String name;
	public String type;

	public GeoPoint location;
	public Double radius;

}
