
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Child {

    private List<Child> children;
    private String description;
    private String id;
    private String name;
    private Double price;
    private String type;

    @JsonProperty("popular_flag")
    private Boolean popularFlag;

    @JsonProperty("popular_rank")
    private Boolean popularRank;

}
