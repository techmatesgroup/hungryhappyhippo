package io.tmg.hungryhappyhippo.poc.models.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString(of = {"id", "name", "restaurantName", "vendor"})
public class MenuItemDto {
	public String id;
	@NotEmpty
	public String menuId;
	public String menuName;
	public String menuCategory;
	@NotEmpty
	public String restaurantId;
	@NotEmpty
	public String restaurantName;
	@NotEmpty
	public String vendor;

	@NotEmpty
	public String name;
	public Double price;
	public String imageUrl;
	public String description;
}
