package io.tmg.hungryhappyhippo.poc.models;

import lombok.ToString;

@ToString
public class IndexModel {

	public String id;
	public String description;
	public String status;
	public Integer numberOfShards;
	public Integer numberOfReplicas;
	public String mapping;
	public Long totalDocCount;

}
