
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement
public class UberEatsMenuResponseDTO {

    private Store store;

}
