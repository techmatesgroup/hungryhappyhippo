package io.tmg.hungryhappyhippo.poc.models;

import io.tmg.hungryhappyhippo.poc.annotations.CategoryContextField;
import io.tmg.hungryhappyhippo.poc.annotations.CompletionField;
import io.tmg.hungryhappyhippo.poc.annotations.GeoContextField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Collection;

@Setter
@Getter
@ToString(of = {"id", "name", "vendor", "address"})
@NoArgsConstructor
@Document(indexName = "#{elasticsearchVendorRestaurantIndex}", type = "#{elasticsearchDefaultDocType}")
public class VendorRestaurantModel {

	@Id
	public String id;
	public String restaurantAggregateId;

	public String name;

	public String vendor;
	public String vendorUrl;

	public String imageUrl;
	public String logoUrl;

	public GeoPoint location;
	public String address;
	public String googlePlaceId;

	public Boolean delivery;
	public Boolean pickup;

	public Double rating;

	public Collection<String> vendorCategories; // categories that are returned from vendors (raw data: contains cuisines, menu names)
	public Collection<String> keywords;

	public Double promotionalWeight;
	public String promotionalContent;

	@CompletionField(
			categoryContext = @CategoryContextField(name = "name", path = "name"),
			geoContext = @GeoContextField(name = "location", path = "location", precision = "5miles")
	)
	public Completion suggest;

	public void setIdWithVendor(String vendor, String id) {
		this.id = String.format("%s-%s", vendor, id);
	}

	public String getIdWithoutVendor() {
		return id.replace(vendor + "-", "");
	}

}
