
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Store {

    private JsonNode sectionEntitiesMap;
    private List<Section> sections;

    @JsonIgnore
    private List<DecoratedSubsection> decoratedSubsectionList;

}
