package io.tmg.hungryhappyhippo.poc.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/19/2019
 */
@Getter
@Setter
@ToString
public class CategoryDto {

    private String id;

    @NotEmpty
    @NotBlank
    private String name;

    @NotEmpty
    @NotBlank
    private String imageUrl;

    @JsonIgnore
    private Set<String> keywords;

    @JsonIgnore
    private boolean disableAutoLinkingByRestaurantName;

    @JsonIgnore
    private boolean disableAutoLinkingByVendorCategory;

}
