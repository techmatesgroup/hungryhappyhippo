package io.tmg.hungryhappyhippo.poc.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString(of = {"id", "name", "enabled", "schedule", "priority"})
@NoArgsConstructor
@Document("job")
public class JobModel {

	@Id
	public String id;

	public String name;
	public String description;
	public Boolean enabled;
	public String command;
	public String schedule;
	public Integer priority;

}
