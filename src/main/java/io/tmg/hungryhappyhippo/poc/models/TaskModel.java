package io.tmg.hungryhappyhippo.poc.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString(of = {"status"}, callSuper=true)
@NoArgsConstructor
@Document("task")
public class TaskModel extends JobModel {

	public String status;

	public Date lastStartTime;
	public Date lastEndTime;
	public String lastExecutionStatus;
	public String lastExecutionResult;

}
