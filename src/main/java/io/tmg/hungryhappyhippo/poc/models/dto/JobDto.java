package io.tmg.hungryhappyhippo.poc.models.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"id", "name", "enabled", "schedule", "priority"})
public class JobDto {

	public String id;

	@NotEmpty
	public String name;
	public String description;
	public Boolean enabled;

	@NotEmpty
	public String command;
	public String schedule;
	public Integer priority;

}
