package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement
public class DoorDashMenuItemResponse {

    private String name;

    @JsonProperty("featured_items")
    private List<FeaturedItem> featuredItems;

    @JsonProperty("menu_categories")
    private List<MenuCategory> menuCategories;

}
