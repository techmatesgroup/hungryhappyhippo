
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class MenuCategory {

    private Long id;
    private List<Item> items;
    private String title;

}
