package io.tmg.hungryhappyhippo.poc.models;

public enum TaskEngineStatus {

	RUNNING,
	PAUSED,
	SHUTDOWN,
	UNKNOWN,

}
