package io.tmg.hungryhappyhippo.poc.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/20/2019
 */
@Setter
@Getter
@NoArgsConstructor
public class CheckVendorRestaurantExist {

	private boolean successCheck;
	private String errorMessage;
	private String detailsErrorMessage;
	private VendorRestaurantModel vendorRestaurantModel;

	public CheckVendorRestaurantExist(boolean successCheck, VendorRestaurantModel vendorRestaurantModel) {
		this.successCheck = successCheck;
		this.vendorRestaurantModel = vendorRestaurantModel;
	}

	public CheckVendorRestaurantExist(boolean successCheck, String errorMessage, String detailsErrorMessage, VendorRestaurantModel vendorRestaurantModel) {
		this.successCheck = successCheck;
		this.errorMessage = errorMessage;
		this.detailsErrorMessage = detailsErrorMessage;
		this.vendorRestaurantModel = vendorRestaurantModel;
	}

	@Override
	public String toString() {
		return "{" +
				"id=" + vendorRestaurantModel.getId() +
				", errorMessage='" + errorMessage +
				'}';
	}
}
