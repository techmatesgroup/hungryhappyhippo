package io.tmg.hungryhappyhippo.poc.models;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Setter
@ToString(of = {"id", "name", "restaurantName", "vendor"})
@NoArgsConstructor
@Document(indexName = "#{elasticsearchMenuItemIndex}", type = "#{elasticsearchDefaultDocType}")
public class MenuItemModel {

	@Id
	public String id;
	public String menuId;
	public String menuName;
	public String menuCategory;
	public String restaurantId;
	public String restaurantName;
	public String vendor;

	public String name;
	public Double price;
	public String imageUrl;
	public String description;

	public void setIdWithVendor(String vendor, String id) {
		this.id = String.format("%s-%s", vendor, id);
	}

	public void setMenuIdWithVendor(String vendor, String menuId) {
		this.menuId = String.format("%s-%s", vendor, menuId);
	}
}
