package io.tmg.hungryhappyhippo.poc.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/19/2019
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
@Document(indexName = "#{elasticsearchCategoryIndex}", type = "#{elasticsearchDefaultDocType}")
public class CategoryModel {

    @Id
    private String id;
    private String name;
    private String imageUrl;
    private Set<String> keywords = new HashSet<>();
    private boolean disableAutoLinkingByRestaurantName;
    private boolean disableAutoLinkingByVendorCategory;

    public CategoryModel(String name, String imageUrl, Set<String> keywords, boolean disableAutoLinkingByRestaurantName, boolean disableAutoLinkingByVendorCategory) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.keywords = keywords;
        this.disableAutoLinkingByRestaurantName = disableAutoLinkingByRestaurantName;
        this.disableAutoLinkingByVendorCategory = disableAutoLinkingByVendorCategory;
    }

    public void setIdFromName(String name) {
        if (name != null && !name.isEmpty()) {
            this.id = "category-" + name.replaceAll(" ", "_").toLowerCase();
        }

    }
}
