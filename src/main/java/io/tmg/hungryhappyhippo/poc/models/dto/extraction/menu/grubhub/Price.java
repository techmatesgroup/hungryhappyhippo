
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Price {

    private Long amount;
    private String currency;

}
