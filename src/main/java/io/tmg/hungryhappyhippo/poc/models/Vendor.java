package io.tmg.hungryhappyhippo.poc.models;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Vendor {
    DELIVERY_COM("Delivery.com", "https://www.delivery.com"),
    DOORDASH("DoorDash", "https://www.doordash.com"),
    FAVORDELIVERY("FavorDelivery", "https://favordelivery.com"),
    GRUBHUB("Grubhub", "https://www.grubhub.com"),
    POSTMATES("Postmates", "https://postmates.com"),
    UBEREATS("UberEats", "https://www.ubereats.com");

    final String name;
    final String websiteUrl;

    public static Vendor valueOfId(String id) {
        for (Vendor value : values()) {
            if (value.getId().equals(id)) {
                return value;
            }

        }
        return null;
    }

    public static Vendor getByName(String name) {
        for (Vendor value : values()) {
            if (value.getName().equalsIgnoreCase(name)) {
                return value;
            }

        }

        throw new IllegalArgumentException(String.format("Unable to find a supported vendor that matches the provided vendor name: %s", name));
    }

    public String getId() {
        return String.format("vendor-%s", name);
    }

    public String getName() {
        return name;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

}
