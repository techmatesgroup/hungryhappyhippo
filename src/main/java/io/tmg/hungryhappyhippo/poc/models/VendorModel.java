package io.tmg.hungryhappyhippo.poc.models;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@ToString
@NoArgsConstructor
@Document(indexName = "#{elasticsearchVendorIndex}", type = "#{elasticsearchDefaultDocType}")
public class VendorModel {

	public VendorModel(String name, String websiteUrl) {

		this.id = String.format("vendor-%s", name);
		this.name = name;
		this.websiteUrl = websiteUrl;

	}

	@Id
	public String id;

	public String name;
	public String websiteUrl;

}
