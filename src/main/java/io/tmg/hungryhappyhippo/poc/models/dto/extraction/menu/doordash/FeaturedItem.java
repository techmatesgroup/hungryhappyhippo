
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class FeaturedItem {

    private String description;
    private Long id;
    private String name;
    private Long price;

}
