
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class MenuCategoryList {

    private Boolean available;
    private Long id;
    private String name;
    private Long sequence;
    private String uuid;

    @JsonProperty("diner_description")
    private String dinerDescription;

    @JsonProperty("menu_item_list")
    private List<MenuItemList> menuItemList;

}
