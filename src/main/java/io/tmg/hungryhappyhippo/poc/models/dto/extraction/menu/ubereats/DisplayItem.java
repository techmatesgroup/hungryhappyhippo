
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisplayItem {

    private String itemDescription;
    private Long price;
    private String title;
    private String uuid;

}
