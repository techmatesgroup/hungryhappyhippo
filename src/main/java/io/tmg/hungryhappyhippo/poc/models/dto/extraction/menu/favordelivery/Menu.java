
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Menu {

    private List<Section> sections;

}
