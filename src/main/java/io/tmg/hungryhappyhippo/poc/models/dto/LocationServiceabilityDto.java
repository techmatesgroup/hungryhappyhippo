package io.tmg.hungryhappyhippo.poc.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LocationServiceabilityDto {
	private boolean targetLocationServiceable;
	private LocationDto location;
}
