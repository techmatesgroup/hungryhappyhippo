
package io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MenuItem {

    private String description;
    private String id;
    private Long price;

    @JsonProperty("display_name")
    private String displayName;

}
