package io.tmg.hungryhappyhippo.poc.models.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
public class VendorDto {
	public String id;
	@NotEmpty
	public String name;
	public String websiteUrl;
}
