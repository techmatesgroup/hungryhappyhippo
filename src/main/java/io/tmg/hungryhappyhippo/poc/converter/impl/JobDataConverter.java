package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.models.JobModel;
import io.tmg.hungryhappyhippo.poc.models.dto.JobDto;

import java.util.Objects;

public class JobDataConverter extends AbstractConverter<JobModel, JobDto> {

    public JobDataConverter(Class<JobDto> jobDtoClass) {
        super(jobDtoClass);
    }

    @Override
    public JobDto convert(JobModel source) {
        Objects.requireNonNull(source, "No source to convert");
        return super.convert(source);
    }

}
