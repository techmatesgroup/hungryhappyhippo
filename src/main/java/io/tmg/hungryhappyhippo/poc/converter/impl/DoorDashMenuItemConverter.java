package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.DoorDashMenuItemResponse;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.DoorDashMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.FeaturedItem;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.Item;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.MenuCategory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Converter that maps favorDelivery response dto {@link DoorDashMenuResponseDTO} to menu details data list
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class DoorDashMenuItemConverter extends AbstractConverter<DoorDashMenuResponseDTO, List<MenuCategoryData>> {

    private static final String FEATURED_ITEMS_CATEGORY_NAME = "Most Loved";

    @Override
    public List<MenuCategoryData> convert(DoorDashMenuResponseDTO doorDashMenuResponseDTO) {
        Objects.requireNonNull(doorDashMenuResponseDTO, "No source to convert");

        return parseDTO(doorDashMenuResponseDTO);
    }

    private List<MenuCategoryData> parseDTO(DoorDashMenuResponseDTO responseDTO) {
        if (responseDTO == null || responseDTO.getDoorDashMenuItemResponseList() == null) {
            return Collections.emptyList();
        }
        final List<DoorDashMenuItemResponse> doorDashMenuItemResponses = responseDTO.getDoorDashMenuItemResponseList();

        // convert featured items
        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>(parseFeaturedItems(doorDashMenuItemResponses));

        // convert usual menus
        final List<MenuCategory> allMenuCategories = new ArrayList<>();
        for (DoorDashMenuItemResponse doorDashMenuItemResponse : doorDashMenuItemResponses) {
            allMenuCategories.addAll(doorDashMenuItemResponse.getMenuCategories());
        }
        menuCategoryDataList.addAll(parseMenuCategories(allMenuCategories));

        return menuCategoryDataList;
    }

    private List<MenuCategoryData> parseFeaturedItems(List<DoorDashMenuItemResponse> doorDashMenuItemResponses) {

        final List<FeaturedItem> allFeaturedItems = new ArrayList<>();
        for (DoorDashMenuItemResponse doorDashMenuItemResponse : doorDashMenuItemResponses) {
            if (CollectionUtils.isNotEmpty(doorDashMenuItemResponse.getFeaturedItems())) {
                allFeaturedItems.addAll(doorDashMenuItemResponse.getFeaturedItems());
            }
        }

        if (CollectionUtils.isEmpty(allFeaturedItems)) {
            return Collections.emptyList();
        }

        final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();
        for (FeaturedItem featuredItem : allFeaturedItems) {
            productItemList.add(parseFeaturedItem(featuredItem));
        }

        final MenuCategoryData featuredMenuCategoryData = new MenuCategoryData();
        featuredMenuCategoryData.setMenuCategoryName(FEATURED_ITEMS_CATEGORY_NAME);
        featuredMenuCategoryData.setProductItemList(productItemList);

        return Collections.singletonList(featuredMenuCategoryData);

    }

    private List<MenuCategoryData> parseMenuCategories(List<MenuCategory> responseMenuCategories) {
        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>();

        for (MenuCategory responseMenuCategory : responseMenuCategories) {
            final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();
            if (responseMenuCategory.getItems() != null) {
                for (Item responseProductItem : responseMenuCategory.getItems()) {
                    productItemList.add(parseProductItem(responseProductItem));
                }
            }

            MenuCategoryData menuCategoryData = findExistingMenuByCategory(responseMenuCategory.getTitle(), menuCategoryDataList);
            if (menuCategoryData == null) {
                menuCategoryData = new MenuCategoryData();
                menuCategoryDataList.add(menuCategoryData);
            }

            menuCategoryData.setId(responseMenuCategory.getId() != null ? String.valueOf(responseMenuCategory.getId()) : null);
            menuCategoryData.setMenuCategoryName(responseMenuCategory.getTitle());
            menuCategoryData.setDescription(null);
            menuCategoryData.setProductItemList(productItemList);
        }


        return menuCategoryDataList;
    }

    private MenuCategoryData findExistingMenuByCategory(String menuCategory, List<MenuCategoryData> menuCategoryDataList) {
        return IterableUtils.find(menuCategoryDataList, menuCategoryData -> StringUtils.isNotEmpty(menuCategory) && menuCategory.equalsIgnoreCase(menuCategoryData.getMenuCategoryName()));
    }

    private MenuCategoryData.ProductItem parseFeaturedItem(FeaturedItem featuredItem) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();
        productItem.setId(featuredItem.getId() != null ? String.valueOf(featuredItem) : null);
        productItem.setName(featuredItem.getName());
        productItem.setDescription(featuredItem.getDescription());

        final Long price = featuredItem.getPrice();
        productItem.setPrice(price != null ? Double.valueOf(price) / 100 : null);

        return productItem;

    }

    private MenuCategoryData.ProductItem parseProductItem(Item responseProductItem) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();
        productItem.setId(responseProductItem.getId() != null ? String.valueOf(responseProductItem.getId()) : null);
        productItem.setName(responseProductItem.getName());
        productItem.setDescription(responseProductItem.getDescription());

        final Long price = responseProductItem.getPrice();
        productItem.setPrice(price != null ? Double.valueOf(price) / 100 : null);

        return productItem;

    }
}
