package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Converter that maps restaurant details data
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
public class VendorRestaurantDetailsDataConverter extends AbstractConverter<VendorRestaurantModel, VendorRestaurantDetailsData> {

    public VendorRestaurantDetailsDataConverter(Class<VendorRestaurantDetailsData> vendorRestaurantDetailsDataClass) {
        super(vendorRestaurantDetailsDataClass);
    }

    @Override
    public VendorRestaurantDetailsData convert(VendorRestaurantModel source) {
        Objects.requireNonNull(source, "No source to convert");

        final VendorRestaurantDetailsData target = super.convert(source);

        final Double promotionalWeight = source.getPromotionalWeight();
        final String promotionalContent = source.getPromotionalContent();

        if (StringUtils.isEmpty(promotionalContent)) {
            if (promotionalWeight != null && promotionalWeight > 0) {
                target.setPromotionalContent("Featured");
            }
        }

        return target;

    }
}
