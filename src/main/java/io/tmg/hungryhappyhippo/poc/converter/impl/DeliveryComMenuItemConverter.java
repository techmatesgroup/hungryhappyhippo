package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom.Child;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom.DeliveryComMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom.Menu;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Converter that maps favorDelivery response dto {@link DeliveryComMenuResponseDTO} to menu details data list
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class DeliveryComMenuItemConverter extends AbstractConverter<DeliveryComMenuResponseDTO, List<MenuCategoryData>> {

    @Override
    public List<MenuCategoryData> convert(DeliveryComMenuResponseDTO deliverycomMenuResponseDTO) {
        Objects.requireNonNull(deliverycomMenuResponseDTO, "No source to convert");

        return parseDTO(deliverycomMenuResponseDTO);
    }

    private List<MenuCategoryData> parseDTO(DeliveryComMenuResponseDTO responseDTO) {
        if (responseDTO == null || responseDTO.getMenu() == null) {
            return Collections.emptyList();
        }

        final List<Menu> responseMainMenuList = responseDTO.getMenu();

        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>();
        for (Menu globalMenu : responseMainMenuList) {

            if (isMenuType(globalMenu)) {
                menuCategoryDataList.add(parseGlobalMenuItem(globalMenu));

            } else {
                final List<Child> menus = findAllMenus(globalMenu.getChildren());
                for (Child menu : menus) {
                    menuCategoryDataList.add(parseChildMenuItem(menu));
                }
            }
        }

        return menuCategoryDataList;
    }

    private List<Child> findAllMenus(List<Child> childrenList) {
        final List<Child> resultChildrenList = new ArrayList<>();
        if (childrenList != null) {
            for (Child child : childrenList) {
                if (isMenuType(child)) {
                    resultChildrenList.add(child);
                } else {
                    resultChildrenList.addAll(findAllMenus(child.getChildren()));
                }
            }
        }
        return resultChildrenList;
    }

    private boolean isMenuType(Menu menu) {
        return "menu".equalsIgnoreCase(menu.getType())
                && CollectionUtils.isNotEmpty(menu.getChildren())
                && "item".equalsIgnoreCase(menu.getChildren().get(0).getType());
    }

    private boolean isMenuType(Child menu) {
        return "menu".equalsIgnoreCase(menu.getType())
                && CollectionUtils.isNotEmpty(menu.getChildren())
                && "item".equalsIgnoreCase(menu.getChildren().get(0).getType());
    }

    private MenuCategoryData parseGlobalMenuItem(Menu responseMenuItem) {

        return getMenuCategoryData(responseMenuItem.getChildren(), responseMenuItem.getId(), responseMenuItem.getName(), responseMenuItem.getDescription());

    }

    private MenuCategoryData parseChildMenuItem(Child responseMenuItem) {

        return getMenuCategoryData(responseMenuItem.getChildren(), responseMenuItem.getId(), responseMenuItem.getName(), responseMenuItem.getDescription());

    }

    private MenuCategoryData getMenuCategoryData(List<Child> children, String id, String name, String description) {

        final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();
        if (children != null) {
            for (Child responseProductItem : children) {
                productItemList.add(parseProductItem(responseProductItem));
            }
        }

        final MenuCategoryData menuCategoryData = new MenuCategoryData();
        menuCategoryData.setId(id);
        menuCategoryData.setMenuCategoryName(name);
        menuCategoryData.setDescription(description);
        menuCategoryData.setProductItemList(productItemList);
        return menuCategoryData;

    }

    private MenuCategoryData.ProductItem parseProductItem(Child responseProductItem) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();

        productItem.setId(responseProductItem.getId());
        productItem.setName(responseProductItem.getName());
        productItem.setDescription(responseProductItem.getDescription());
        productItem.setPrice(responseProductItem.getPrice());

        return productItem;

    }

}
