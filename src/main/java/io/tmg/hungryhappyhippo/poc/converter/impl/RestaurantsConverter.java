package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantDto;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.stream.Collectors;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/05/2019
 */
public class RestaurantsConverter extends AbstractConverter<RestaurantModel, RestaurantDto> {

	public RestaurantsConverter(Class<RestaurantDto> restaurantDtoClass) {
		super(restaurantDtoClass);
	}

	@Override
	public RestaurantDto convert(RestaurantModel source) {
		Objects.requireNonNull(source, "No source to convert");

		final RestaurantDto restaurantDto = super.convert(source);

		restaurantDto.setRestaurantDetailsUrl(String.format("/%s/%s%s", API_PREFIX, API_VERSION_1, API_PATH_RESTAURANT.replace("{restaurantId}", source.getId())));
		restaurantDto.setVendorRestaurants(source.getVendorRestaurants().stream().map(RestaurantModel.VendorRestaurant::getVendor).collect(Collectors.toSet()));
		if (StringUtils.isEmpty(source.promotionalContent)) {
			if (source.promotionalWeight != null && source.promotionalWeight > 0) {
				restaurantDto.setPromotionalContent("Featured");
			}
		}
		if (source.location != null) {
			restaurantDto.setLatitude(source.getLocation().getLat());
			restaurantDto.setLongitude(source.getLocation().getLon());
		}

		return restaurantDto;

	}
}
