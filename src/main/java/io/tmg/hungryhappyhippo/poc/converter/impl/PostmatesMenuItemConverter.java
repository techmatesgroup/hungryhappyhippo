package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates.Item;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates.ItemGroup;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates.PostmatesMenuResponseDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static io.tmg.hungryhappyhippo.poc.utils.ParsingUtil.isNumeric;

/**
 * Converter that maps postmates response dto {@link PostmatesMenuResponseDTO} to menu details data list
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/11/2019
 */
@Service
public class PostmatesMenuItemConverter extends AbstractConverter<PostmatesMenuResponseDTO, List<MenuCategoryData>> {

    @Override
    public List<MenuCategoryData> convert(PostmatesMenuResponseDTO postmatesMenuResponseDTO) {
        Objects.requireNonNull(postmatesMenuResponseDTO, "No source to convert");

        return parseDTO(postmatesMenuResponseDTO);
    }

    private List<MenuCategoryData> parseDTO(final PostmatesMenuResponseDTO responseDTO) {
        if (responseDTO == null || responseDTO.getItemGroups() == null) {
            return Collections.emptyList();
        }

        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>(responseDTO.getItemGroups().size());
        for (ItemGroup itemGroup : responseDTO.getItemGroups()) {
            final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();
            if (itemGroup.getItems() != null) {
                for (Item item : itemGroup.getItems()) {
                    productItemList.add(parseProductItem(item));
                }
            }

            final MenuCategoryData menuCategoryData = new MenuCategoryData();
            menuCategoryData.setMenuCategoryName(itemGroup.getCategoryName());
            menuCategoryData.setProductItemList(productItemList);
            menuCategoryDataList.add(menuCategoryData);

        }
        return menuCategoryDataList;

    }

    private MenuCategoryData.ProductItem parseProductItem(final Item item) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();
        productItem.setId(item.getProductUuid());
        productItem.setName(item.getProductName());
        productItem.setDescription(item.getProductDescription());

        final String basePrice = item.getBasePrice();
        productItem.setPrice(isNumeric(basePrice) ? Double.valueOf(basePrice) : null);

        return productItem;

    }

}
