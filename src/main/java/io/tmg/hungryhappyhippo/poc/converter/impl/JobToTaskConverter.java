package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.models.JobModel;
import io.tmg.hungryhappyhippo.poc.models.TaskModel;

import java.util.Objects;
import java.util.UUID;

public class JobToTaskConverter extends AbstractConverter<JobModel, TaskModel> {

    public JobToTaskConverter(Class<TaskModel> taskModelClass) {
        super(taskModelClass);
    }

    @Override
    public TaskModel convert(JobModel source) {
        Objects.requireNonNull(source, "No source to convert");
        TaskModel task = super.convert(source);
        task.id = UUID.randomUUID().toString();
        return task;
    }

}
