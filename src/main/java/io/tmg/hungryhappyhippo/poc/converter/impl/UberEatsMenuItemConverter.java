package io.tmg.hungryhappyhippo.poc.converter.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats.DecoratedSubsection;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats.DisplayItem;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats.Section;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats.Store;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats.UberEatsMenuResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Converter that maps uberEats response dto {@link UberEatsMenuResponseDTO} to menu details data list
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Slf4j
@Service
public class UberEatsMenuItemConverter extends AbstractConverter<UberEatsMenuResponseDTO, List<MenuCategoryData>> {

    private final ObjectReader decoratedSubsectionReader = new ObjectMapper().readerFor(DecoratedSubsection.class);

    @Override
    public List<MenuCategoryData> convert(UberEatsMenuResponseDTO uberEatsMenuResponseDTO) {
        Objects.requireNonNull(uberEatsMenuResponseDTO, "No source to convert");

        try {
            return parseDTO(uberEatsMenuResponseDTO);
        } catch (IOException e) {
            log.warn("Parse error during converting", e);
            throw new RuntimeException(e);
        }
    }

    private List<MenuCategoryData> parseDTO(UberEatsMenuResponseDTO responseDTO) throws IOException {
        if (responseDTO == null || responseDTO.getStore() == null || responseDTO.getStore().getSectionEntitiesMap() == null) {
            return Collections.emptyList();
        }

        // convert internal jsonNode to java objects
        populateDecoratedSections(responseDTO);

        final List<DecoratedSubsection> responseMenuCategoryList = responseDTO.getStore().getDecoratedSubsectionList();

        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>(responseMenuCategoryList.size());
        for (DecoratedSubsection responseMenuCategory : responseMenuCategoryList) {
            final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();
            if (responseMenuCategory.getDisplayItems() != null) {
                for (DisplayItem responseProductItem : responseMenuCategory.getDisplayItems()) {
                    productItemList.add(parseProductItem(responseProductItem));
                }
            }

            final MenuCategoryData menuCategoryData = new MenuCategoryData();
            menuCategoryData.setId(responseMenuCategory.getUuid());
            menuCategoryData.setMenuCategoryName(responseMenuCategory.getTitle());
            menuCategoryData.setDescription(null);
            menuCategoryData.setProductItemList(productItemList);
            menuCategoryDataList.add(menuCategoryData);
        }

        return menuCategoryDataList;
    }

    private MenuCategoryData.ProductItem parseProductItem(DisplayItem responseProductItem) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();
        productItem.setId(responseProductItem.getUuid());
        productItem.setName(responseProductItem.getTitle());
        productItem.setDescription(responseProductItem.getItemDescription());

        final Long price = responseProductItem.getPrice();
        productItem.setPrice(price != null ? Double.valueOf(price) / 100 : null);

        return productItem;

    }

    private void populateDecoratedSections(UberEatsMenuResponseDTO responseDTO) throws IOException {
        final Store store = responseDTO.getStore();
        store.setDecoratedSubsectionList(new ArrayList<>());
        final JsonNode sectionEntitiesMap = store.getSectionEntitiesMap();
        if (CollectionUtils.isNotEmpty(store.getSections())) {
            for (Section section : store.getSections()) {
                JsonNode decoratedSubsections = sectionEntitiesMap.at("/" + section.getUuid() + "/decoratedSubsections");
                if (decoratedSubsections != null && decoratedSubsections.isArray()) {
                    for (JsonNode decoratedSubsectionNode : decoratedSubsections) {
                        store.getDecoratedSubsectionList().add(decoratedSubsectionReader.readValue(decoratedSubsectionNode));
                    }
                }
            }
        }
    }
}
