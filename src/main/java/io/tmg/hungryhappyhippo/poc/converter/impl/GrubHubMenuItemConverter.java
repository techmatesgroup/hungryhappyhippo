package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub.GrubHubMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub.MenuCategoryList;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub.MenuItemList;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub.Price;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Converter that maps grubhubs response dto {@link GrubHubMenuResponseDTO} to menu details data list
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/11/2019
 */
@Service
public class GrubHubMenuItemConverter extends AbstractConverter<GrubHubMenuResponseDTO, List<MenuCategoryData>> {

    @Override
    public List<MenuCategoryData> convert(GrubHubMenuResponseDTO grubHubMenuResponseDTO) {
        Objects.requireNonNull(grubHubMenuResponseDTO, "No source to convert");

        return parseDTO(grubHubMenuResponseDTO);
    }

    private List<MenuCategoryData> parseDTO(GrubHubMenuResponseDTO responseDTO) {
        if (responseDTO == null || responseDTO.getRestaurant() == null || responseDTO.getRestaurant().getMenuCategoryList() == null) {
            return Collections.emptyList();
        }

        final List<MenuCategoryList> responseMenuCategoryList = responseDTO.getRestaurant().getMenuCategoryList();

        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>(responseMenuCategoryList.size());
        for (MenuCategoryList responseMenuCategory : responseMenuCategoryList) {
            final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();
            if (responseMenuCategory.getMenuItemList() != null) {
                for (MenuItemList responseProductItem : responseMenuCategory.getMenuItemList()) {
                    productItemList.add(parseProductItem(responseProductItem));
                }
            }

            final MenuCategoryData menuCategoryData = new MenuCategoryData();
            menuCategoryData.setId(responseMenuCategory.getUuid());
            menuCategoryData.setMenuCategoryName(responseMenuCategory.getName());
            menuCategoryData.setDescription(responseMenuCategory.getDinerDescription());
            menuCategoryData.setProductItemList(productItemList);
            menuCategoryDataList.add(menuCategoryData);
        }
        return menuCategoryDataList;

    }

    private MenuCategoryData.ProductItem parseProductItem(MenuItemList responseProductItem) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();
        productItem.setId(responseProductItem.getUuid());
        productItem.setName(responseProductItem.getName());
        productItem.setDescription(responseProductItem.getDescription());
        productItem.setPrice(getPrice(responseProductItem.getPrice(), responseProductItem.getMinimumPriceVariation()));

        return productItem;

    }

    private Double getPrice(Price... prices) {
        if (prices == null) {
            return null;
        }

        for (Price price : prices) {
            if (price != null && price.getAmount() != null && price.getAmount().intValue() != 0) {
                return Double.valueOf(price.getAmount()) / 100;
            }
        }

        return null;
    }
}
