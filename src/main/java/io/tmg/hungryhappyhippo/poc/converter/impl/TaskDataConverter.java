package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.facade.TaskEngineFacade;
import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.TaskStatus;
import io.tmg.hungryhappyhippo.poc.models.dto.TaskDto;
import io.tmg.hungryhappyhippo.poc.services.TaskService;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

@Slf4j
public class TaskDataConverter extends AbstractConverter<TaskModel, TaskDto> {

	@Resource
	private TaskService taskService;

	@Resource
	private TaskEngineFacade taskEngineFacade;

    public TaskDataConverter(Class<TaskDto> taskDtoClass) {
        super(taskDtoClass);
    }

    @Override
    public TaskDto convert(TaskModel source) {
        Objects.requireNonNull(source, "No source to convert");

        TaskDto task = super.convert(source);

        try {
            if (taskService.isTaskScheduled(source)) {
                task.setStatus(String.valueOf(taskService.getLoadedTaskStatus(source.id)));
                task.setSchedule(String.format("%s (Next fire time: %s)", task.schedule, taskService.getLoadedTaskNextFireTime(source.id)));
            }
            else {
                task.setStatus(StringUtils.isNotBlank(source.status) ? task.status : String.valueOf(TaskStatus.NEW));
            }
        } catch (Exception e) {
            log.warn(String.format("An error occurred when attempting to populate real-time task status and schedule information for task: %s", source.id), e);
        }

        return task;
    }

}
