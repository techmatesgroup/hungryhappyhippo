package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.RestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Converter that maps restaurant details data
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
public class RestaurantDetailsDataConverter extends AbstractConverter<RestaurantModel, RestaurantDetailsData> {

    public RestaurantDetailsDataConverter(Class<RestaurantDetailsData> restaurantDetailsDataClass) {
        super(restaurantDetailsDataClass);
    }

    @Override
    public RestaurantDetailsData convert(RestaurantModel source) {
        Objects.requireNonNull(source, "No source to convert");

        final RestaurantDetailsData target = super.convert(source);

        if (source.getLocation() != null) {
            target.setLatitude(source.getLocation().getLat());
            target.setLongitude(source.getLocation().getLon());
        }

        if (StringUtils.isEmpty(source.getPromotionalContent())) {
            if (source.getPromotionalWeight() != null && source.getPromotionalWeight() > 0) {
                target.setPromotionalContent("Featured");
            }
        }

        return target;
    }
}
