package io.tmg.hungryhappyhippo.poc.converter.impl;

import io.tmg.hungryhappyhippo.poc.converter.AbstractConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery.FavorDeliveryMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery.MenuItem;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery.Section;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Converter that maps favorDelivery response dto {@link FavorDeliveryMenuResponseDTO} to menu details data list
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class FavorDeliveryMenuItemConverter extends AbstractConverter<FavorDeliveryMenuResponseDTO, List<MenuCategoryData>> {

    @Override
    public List<MenuCategoryData> convert(FavorDeliveryMenuResponseDTO favorDeliveryMenuResponseDTO) {
        Objects.requireNonNull(favorDeliveryMenuResponseDTO, "No source to convert");

        return parseDTO(favorDeliveryMenuResponseDTO);
    }

    private List<MenuCategoryData> parseDTO(FavorDeliveryMenuResponseDTO responseDTO) {
        if (responseDTO == null || responseDTO.getMenu() == null || responseDTO.getMenu().getSections() == null) {
            return Collections.emptyList();
        }

        final List<Section> responseMenuCategoryList = responseDTO.getMenu().getSections();

        final List<MenuCategoryData> menuCategoryDataList = new ArrayList<>(responseMenuCategoryList.size());
        for (Section responseMenuCategory : responseMenuCategoryList) {
            final MenuCategoryData menuCategoryData = new MenuCategoryData();
            final List<MenuCategoryData.ProductItem> productItemList = new ArrayList<>();

            if (responseMenuCategory.getMenuItems() != null) {
                for (MenuItem responseProductItem : responseMenuCategory.getMenuItems()) {
                    final MenuCategoryData.ProductItem productItem = parseProductItem(responseProductItem, menuCategoryData);
                    if (productItem != null) {
                        productItemList.add(productItem);
                    }
                }
            }

            menuCategoryData.setMenuCategoryName(responseMenuCategory.getName());
            menuCategoryData.setProductItemList(productItemList);
            menuCategoryDataList.add(menuCategoryData);
        }


        return menuCategoryDataList;
    }

    private MenuCategoryData.ProductItem parseProductItem(MenuItem responseProductItem, MenuCategoryData menuCategoryData) {

        final MenuCategoryData.ProductItem productItem = new MenuCategoryData.ProductItem();

        final Long price = responseProductItem.getPrice();
        if (price == null || price.intValue() == 0) {
            // if price == 0 - it is not product Item. Its item for menu description
            menuCategoryData.setDescription(responseProductItem.getDisplayName());
            return null;
        }

        productItem.setId(responseProductItem.getId());
        productItem.setName(responseProductItem.getDisplayName());
        productItem.setDescription(responseProductItem.getDescription());
        productItem.setPrice(Double.valueOf(price) / 100);

        return productItem;

    }

}
