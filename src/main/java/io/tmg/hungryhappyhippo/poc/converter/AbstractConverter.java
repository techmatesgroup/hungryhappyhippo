package io.tmg.hungryhappyhippo.poc.converter;

import org.apache.commons.collections4.IterableUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Converter support class. Contains default implementation with {@link ModelMapper}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
public abstract class AbstractConverter<SOURCE, TARGET> extends org.modelmapper.AbstractConverter<SOURCE, TARGET> {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private ModelMapper modelMapper;

    private Class<TARGET> targetClass;

    public AbstractConverter() {
    }

    public AbstractConverter(Class<TARGET> targetClass) {
        this.targetClass = targetClass;
    }

    @Override
    public TARGET convert(SOURCE source) {
        if (targetClass == null) {
            log.warn("Please specify `targetClass` to use default mapper");
            return null;
        }

        return modelMapper.map(source, targetClass);
    }

    public List<TARGET> convertAll(Iterable<? extends SOURCE> sources) {
        if (IterableUtils.isEmpty(sources)) {
            return Collections.emptyList();
        }

        List<TARGET> result = new ArrayList<>();

        for (SOURCE source : sources) {
            result.add(this.convert(source));
        }

        return result;
    }

    public List<TARGET> convertAllIgnoreExceptions(Iterable<? extends SOURCE> sources) {
        if (IterableUtils.isEmpty(sources)) {
            return Collections.emptyList();
        }

        List<TARGET> targets = new ArrayList<>();

        for (SOURCE source : sources) {
            try {
                targets.add(this.convert(source));
            } catch (Exception ex) {
                this.getLogger().warn("Exception while converting object!", ex);
            }
        }

        return targets;
    }

    protected Logger getLogger() {
        return LoggerFactory.getLogger(getClass());
    }
}
