package io.tmg.hungryhappyhippo.poc.constants;

import io.tmg.hungryhappyhippo.poc.data.search.SortOptionData;
import org.elasticsearch.search.sort.SortOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
public class HHHConstants {

    public static final class PageInfo {
        public static final List<Integer> ALL_PAGE_SIZES;

        static {
            ALL_PAGE_SIZES = new ArrayList<>();
            ALL_PAGE_SIZES.add(5);
            ALL_PAGE_SIZES.add(10);
            ALL_PAGE_SIZES.add(25);
            ALL_PAGE_SIZES.add(50);
            ALL_PAGE_SIZES.add(100);
        }
    }

    public static class SortInfo {

        public static class Restaurants {

            public static final List<SortOptionData> ALL_SORT_OPTIONS;
            public static final Map<String, SortOptionData> ALL_SORT_OPTIONS_MAP;

            static {
                ALL_SORT_OPTIONS = new ArrayList<>();

                ALL_SORT_OPTIONS.add(new SortOptionData("relevance", "Default", "_score", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("name-asc", "Name Ascending", "name.keyword", SortOrder.ASC));
                ALL_SORT_OPTIONS.add(new SortOptionData("name-desc", "Name Descending", "name.keyword", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("rating-desc", "Rating", "rating", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("distance-asc", "Distance", "location", SortOrder.ASC));

                ALL_SORT_OPTIONS_MAP = new HashMap<>();
                for (SortOptionData sortOption : ALL_SORT_OPTIONS) {
                    ALL_SORT_OPTIONS_MAP.put(sortOption.getId(), sortOption);
                }
            }

        }

        public static class Menus {

            public static final List<SortOptionData> ALL_SORT_OPTIONS;
            public static final Map<String, SortOptionData> ALL_SORT_OPTIONS_MAP;

            static {
                ALL_SORT_OPTIONS = new ArrayList<>();

                ALL_SORT_OPTIONS.add(new SortOptionData("relevance", "Relevance", "_score", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("vendor-asc", "Vendor Ascending", "vendor.keyword", SortOrder.ASC));
                ALL_SORT_OPTIONS.add(new SortOptionData("vendor-desc", "Vendor Descending", "vendor.keyword", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("restaurantName-asc", "Restaurant Ascending", "restaurantName.keyword", SortOrder.ASC));
                ALL_SORT_OPTIONS.add(new SortOptionData("restaurantName-desc", "Restaurant Descending", "restaurantName.keyword", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("menuName-asc", "Menu Ascending", "menuName.keyword", SortOrder.ASC));
                ALL_SORT_OPTIONS.add(new SortOptionData("menuName-desc", "Menu Descending", "menuName.keyword", SortOrder.DESC));

                ALL_SORT_OPTIONS.add(new SortOptionData("price-asc", "Price Ascending", "price", SortOrder.ASC));
                ALL_SORT_OPTIONS.add(new SortOptionData("price-desc", "Price Descending", "price", SortOrder.DESC));

                ALL_SORT_OPTIONS_MAP = new HashMap<>();
                for (SortOptionData sortOption : ALL_SORT_OPTIONS) {
                    ALL_SORT_OPTIONS_MAP.put(sortOption.getId(), sortOption);
                }
            }
        }
    }

    public static class AggregationInfo {
        public static class Restaurants {

            public static final String VENDORRESTAURANTS = "vendorRestaurants";
            public static final String CATEGORY = "aggregationCategoryCount";

        }
    }

    public static class API {
        public static final String API_PREFIX = "api";
        public static final String API_VERSION_1 = "v1";

        public static final String API_PATH_RESTAURANTS_SEARCH = "/restaurants/search";
        public static final String API_PATH_RESTAURANTS_SUGGEST = "/restaurants/suggest";
        public static final String API_PATH_RESTAURANT = "/restaurant/{restaurantId}";
        public static final String API_PATH_RESTAURANT_AVAILABILITY = "/restaurant/availability/{vendorName}/{vendorRestaurantId}";
        public static final String API_PATH_RESTAURANT_MENU = "/restaurant/menu/{vendorName}/{vendorRestaurantId}";

        public static final String API_PATH_LOCATIONS = "/locations";
        public static final String API_PATH_LOCATION_SERVICEABLE = "/location/serviceable";

        public static final String API_PATH_VENDORS = "/vendors";

        public static final String API_PATH_CATEGORIES = "/categories";

        public static final String API_PATH_EMAIL_FEEDBACK = "/email/feedback";

    }

}
