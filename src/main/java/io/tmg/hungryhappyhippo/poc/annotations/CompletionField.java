package io.tmg.hungryhappyhippo.poc.annotations;

import java.lang.annotation.*;

/**
 * Copied from {@link org.springframework.data.elasticsearch.annotations.CompletionField}
 *
 * Added contexts support
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/15/2019
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
@Inherited
public @interface CompletionField {

	String searchAnalyzer() default "simple";

	String analyzer() default "simple";

	boolean preserveSeparators() default true;

	boolean preservePositionIncrements() default true;

	int maxInputLength() default 50;

	CategoryContextField[] categoryContext();

	GeoContextField[] geoContext();

}
