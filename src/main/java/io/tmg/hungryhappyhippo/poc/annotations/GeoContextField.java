package io.tmg.hungryhappyhippo.poc.annotations;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/15/2019
 */
public @interface GeoContextField {

	String name();

	String path();

	String precision();

}
