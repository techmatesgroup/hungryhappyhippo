package io.tmg.hungryhappyhippo.poc.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/15/2019
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LocationCookieCheck {

	/**
	 * Determines if the location params required for method or not
	 * @throws io.tmg.hungryhappyhippo.poc.error.LocationCookieNotFoundException - If required=true and location params is not present
	 */
	boolean required() default true;

}
