package io.tmg.hungryhappyhippo.poc.services.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import io.tmg.hungryhappyhippo.poc.dbrepositories.JobRepository;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.models.JobModel;
import io.tmg.hungryhappyhippo.poc.services.JobService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DefaultJobService implements JobService {

	@Resource
	private JobRepository jobRepository;

	@Override
	public Iterable<JobModel> getJobs() {

		return jobRepository.findAll();

	}

	@Override
	public Page<JobModel> getPage(Pageable pageable, String searchTerms) {

		if (StringUtils.isEmpty(searchTerms)) {
			return jobRepository.findAll(pageable);

		} else {
			return jobRepository.findByIdContainsIgnoreCaseOrNameContainsIgnoreCase(searchTerms, searchTerms, pageable);

		}

	}

	@Override
	public JobModel getById(String jobId) {

		return jobRepository.findById(jobId).orElseThrow(NotFoundException::new);

	}

	@Override
	public Optional<JobModel> findById(String jobId) {
		return jobRepository.findById(jobId);
	}

	@Override
	public void save(JobModel job) {

		jobRepository.save(job);

	}

	@Override
	public void saveAll(Iterable<JobModel> jobModels) {

		jobRepository.saveAll(jobModels);

	}

	@Override
	public void deleteById(String jobId) {

		jobRepository.deleteById(jobId);

	}

	@Override
	public JobModel instance() {

		return new JobModel();

	}

	@Override
	public boolean isJobEnabled(JobModel job) {

		return Boolean.TRUE.equals(job.enabled);

	}

}
