package io.tmg.hungryhappyhippo.poc.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CrudService<MODEL> {

	Page<MODEL> getPage(Pageable pageable, String searchTerms);

	MODEL getById(String id);

	Optional<MODEL> findById(String id);

	void save(MODEL model);

	void saveAll(Iterable<MODEL> models);

	void deleteById(String id);

	MODEL instance();

}
