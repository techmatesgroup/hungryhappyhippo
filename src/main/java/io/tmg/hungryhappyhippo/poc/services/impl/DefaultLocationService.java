package io.tmg.hungryhappyhippo.poc.services.impl;

import javax.annotation.Resource;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.models.AddressModel;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.TargetLocationType;
import io.tmg.hungryhappyhippo.poc.repositories.LocationRepository;
import io.tmg.hungryhappyhippo.poc.services.LocationService;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.PlaceDetailsRequest.FieldMask;
import com.google.maps.PlacesApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlaceDetails;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static io.tmg.hungryhappyhippo.poc.utils.ParsingUtil.isNumeric;

@Slf4j
@Service("locationService")
public class DefaultLocationService implements LocationService {

	@Resource
	public LocationRepository locationRepository;

	@Resource
	public GeoApiContext geoApiContext;

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private Map<String, Float> adminFreeTextSearchKeyFields;

	@Override
	public Iterable<LocationModel> getDefaultLocations() {

		// TODO: CSV mapping does not work correctly here...latitude/longitude to GeoPoint location conversion is not automatic
		// return importService.fromCsv(getClass().getResourceAsStream("/config/locations.csv"), LocationModel.class);

		List<LocationModel> locations = new ArrayList<>();

		double defaultRadius = applicationProperties.getLocation().getDefaultRadius();
		String cityType = TargetLocationType.CITY.name();

		locations.add(new LocationModel("Austin, TX", cityType, 30.2672, -97.7431, defaultRadius));

		return locations;

	}

	@Override
	public Iterable<LocationModel> getLocations() {

		return locationRepository.findAll();

	}

	@Override
	public Page<LocationModel> getPage(Pageable pageable, String searchTerms) {

		if (StringUtils.isEmpty(searchTerms)) {
			return locationRepository.findAll(pageable);

		} else {

			final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
					.withQuery(QueryBuilders.multiMatchQuery(searchTerms)
							.fields(adminFreeTextSearchKeyFields))
					.withPageable(pageable)
					.build();

			return locationRepository.search(searchQuery);
		}

	}

	@Override
	public LocationModel getById(String locationId) {

		return locationRepository.findById(locationId).orElseThrow(NotFoundException::new);

	}

	@Override
	public Optional<LocationModel> findById(String locationId) {
		return locationRepository.findById(locationId);
	}

	@Override
	public void save(LocationModel location) {

		locationRepository.save(location);

	}

	@Override
	public void saveAll(Iterable<LocationModel> locationModels) {

		locationRepository.saveAll(locationModels);

	}

	@Override
	public Iterable<LocationModel> searchLocations(String searchTerms) {

		return locationRepository.search(QueryBuilders.simpleQueryStringQuery(searchTerms));

	}

	@Override
	public void deleteById(String locationId) {

		locationRepository.deleteById(locationId);

	}

	@Override
	public LocationModel instance() {
		return new LocationModel();
	}

	@Override
	public AddressModel getGeocodedAddress(String placeName, String address) {

		AddressModel addressModel = new AddressModel();

		try {
			if (!applicationProperties.getGoogleApi().isEnabled()) {
				log.warn("The Google API is not enabled, so no address forward geocoding request will be made");
				return null;
			}

			String placeIdentifier = address;

			if (StringUtils.isNotBlank(placeName)) {
				placeIdentifier = String.format("%s, %s", placeName, address);
			}

			GeocodingResult[] results =  GeocodingApi.geocode(
					geoApiContext,
					placeIdentifier).await();

			if (results.length > 0) {
				GeocodingResult result = results[0];

				addressModel.id = result.placeId;
				if (result.types.length > 0) {
					addressModel.type = result.types[0].toString();
				}
				addressModel.address = result.formattedAddress;
				addressModel.location = new GeoPoint(result.geometry.location.lat, result.geometry.location.lng);
				addressModel.radius = 0d;

				addressModel.name = placeName;
			}
			else {
				log.warn("Unable to find any results when trying to geocode the following place name and address: {}: {}", placeName, address);
				/* Note: Some place name + address combinations cause Google's API to return 0 results, so here we make a best effort to at least normalize the address without using a location place name */
				return getGeocodedAddress(null, address);
			}
		}
		catch (Exception e) {
			log.warn("An error occurred when trying to geocode the following place name and address: {}: {}", placeName, address, e);
			return null;
		}

		return addressModel;

	}

	@Override
	public AddressModel getReverseGeocodedAddress(double latitude, double longitude) {

		AddressModel addressModel = new AddressModel();

		try {
			if (!applicationProperties.getGoogleApi().isEnabled()) {
				log.warn("The Google API is not enabled, so no latitude/longitude reverse geocoding request will be made");
				return null;
			}

			GeocodingResult[] results =  GeocodingApi.reverseGeocode(
					geoApiContext,
					new LatLng(latitude, longitude)).await();

			if (results.length > 0) {
				GeocodingResult result = results[0];

				addressModel.id = result.placeId;
				if (result.types.length > 0) {
					addressModel.type = result.types[0].toString();
				}
				addressModel.address = result.formattedAddress;
				addressModel.location = new GeoPoint(result.geometry.location.lat, result.geometry.location.lng);
				addressModel.radius = 0d;
				addressModel.name = result.formattedAddress;
			}
			else {
				log.warn("Unable to find any results when trying to reverse geocode the following latitude/longitude coordinates: {}, {}", latitude, longitude);
				return null;
			}
		}
		catch (Exception e) {
			log.warn("An error occurred when trying to reverse geocode the following latitude/longitude coordinates: {}, {}", latitude, longitude, e);
			return null;
		}

		return addressModel;

	}

	@Override
	public PlaceDetails getPlaceDetails(String googlePlaceId) {

		try {
			if (!applicationProperties.getGoogleApi().isEnabled()) {
				log.warn("The Google API is not enabled, so no place details request will be made");
				return null;
			}

			/* Note: It's important to only request the place details information we actively need, otherwise API costs can add up quickly */
			PlaceDetails placeDetails = PlacesApi.placeDetails(geoApiContext, googlePlaceId)
					.fields(
							FieldMask.RATING,
							FieldMask.PRICE_LEVEL
					).await();

			return placeDetails;
		}
		catch (Exception e) {
			log.warn("An error occurred when trying to fetch place details for Google place id: {}", googlePlaceId, e);
			return null;
		}

	}

	@Override
	public LocationModel getClosestSupportedLocation(final String latitude, final String longitude) {

		if (isNumeric(latitude) && isNumeric(longitude)) {
			for (LocationModel locationModel : getLocations()) {
				double distanceToSupportedLocation = GeoDistance.ARC
						.calculate(Double.parseDouble(latitude), Double.parseDouble(longitude),
						locationModel.location.getLat(), locationModel.location.getLon(), DistanceUnit.MILES);
				if (distanceToSupportedLocation <= locationModel.radius) {
					return locationModel;
				}
			}
		}

		return null;

	}
}
