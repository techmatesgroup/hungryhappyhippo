package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/22/2019
 */
public interface GeneralMetricsDataService {

	GeneralMetricsData buildGeneralMetricsInfo();

}
