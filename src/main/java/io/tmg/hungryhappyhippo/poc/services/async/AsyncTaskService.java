package io.tmg.hungryhappyhippo.poc.services.async;

import com.fasterxml.jackson.databind.JsonNode;
import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.AbstractRestaurantExtractor;

import java.util.List;
import java.util.concurrent.Future;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/27/2019
 */
public interface AsyncTaskService {

	Future<List<VendorRestaurantModel>> performAsyncFetchRestaurants(final VendorRestaurantExtractor vendorRestaurantExtractor, final Iterable<LocationModel> targetLocations);

	Future<VendorRestaurantModel> performAsyncFetchRestaurantInternal(final AbstractRestaurantExtractor vendorRestaurantExtractor, final JsonNode restaurant, final LocationModel targetLocation);

	Future<CheckVendorRestaurantExist> performAsyncCheckExistence(final VendorRestaurantExtractor vendorRestaurantExtractor, final VendorRestaurantModel vendorRestaurantModel, final LocationSearchParams locationSearchParams);

	void performSendFeedbackEmail(final FeedbackUserData feedbackUserData);
}
