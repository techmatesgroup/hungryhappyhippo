package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.event.VendorRestaurantDeletedEvent;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.repositories.VendorRestaurantRepository;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry.Option;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.AggregationInfo.Restaurants.CATEGORY;
import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.createPageRequest;
import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.createSortBuilders;
import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.buildSuggestQueryContextMap;

@Service("vendorRestaurantService")
public class DefaultVendorRestaurantService implements VendorRestaurantService {

	@Resource
	private ApplicationEventPublisher publisher;

	@Resource
	private ElasticsearchTemplate elasticsearchTemplate;

	@Resource
	private VendorRestaurantRepository vendorRestaurantRepository;

	@Resource
	private ApplicationProperties properties;

	@Resource
	private Map<String, Float> restaurantFreeTextSearchKeyFields;

	@Resource
	private Map<String, Float> adminFreeTextSearchKeyFields;

	@Override
	public Iterable<VendorRestaurantModel> getRestaurants() {

		return vendorRestaurantRepository.findAll();

	}

    @Override
    public Page<VendorRestaurantModel> getPage(Pageable pageable, String searchTerms) {

		if (StringUtils.isEmpty(searchTerms)) {
			return vendorRestaurantRepository.findAll(pageable);

		} else {

			final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
					.withQuery(QueryBuilders.multiMatchQuery(searchTerms)
							.fields(adminFreeTextSearchKeyFields))
					.withPageable(pageable)
					.build();

			return vendorRestaurantRepository.search(searchQuery);
		}

    }

    @Override
	public VendorRestaurantModel getById(String restaurantId) {

		return vendorRestaurantRepository.findById(restaurantId).orElseThrow(NotFoundException::new);

	}

	@Override
	public Optional<VendorRestaurantModel> findById(String restaurantId) {
		return vendorRestaurantRepository.findById(restaurantId);
	}

	@Override
	public void save(VendorRestaurantModel restaurant) {

		vendorRestaurantRepository.save(restaurant);

	}

	@Override
	public void saveAll(Iterable<VendorRestaurantModel> restaurantModels) {

		if (!IterableUtils.isEmpty(restaurantModels)) {
			vendorRestaurantRepository.saveAll(restaurantModels);
		}

	}

	@Override
	public void deleteById(String restaurantId) {

		vendorRestaurantRepository.findById(restaurantId).ifPresent(restaurant -> {
			vendorRestaurantRepository.deleteById(restaurant.id);
			publisher.publishEvent(new VendorRestaurantDeletedEvent(this, restaurant.id, restaurant.restaurantAggregateId));
		});

	}

	@Override
	public VendorRestaurantModel instance() {
		return new VendorRestaurantModel();
	}

	@Override
	public AggregatedPage<VendorRestaurantModel> searchRestaurants(final RestaurantSearchRequest searchRequest) {

		BoolQueryBuilder commonQueryBuilder = new BoolQueryBuilder();

		if (StringUtils.isNotEmpty(searchRequest.getSearchTerms())) {
			commonQueryBuilder.must(
				QueryBuilders.multiMatchQuery(searchRequest.getSearchTerms())
					.fields(restaurantFreeTextSearchKeyFields)
					.fuzziness(Fuzziness.AUTO)
			);
		}

		if (BooleanUtils.isTrue(searchRequest.getDelivery())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("delivery", true));
		} else if (BooleanUtils.isFalse(searchRequest.getDelivery())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("delivery", false));
		}

		if (BooleanUtils.isTrue(searchRequest.getPickup())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("pickup", true));
		} else if (BooleanUtils.isFalse(searchRequest.getPickup())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("pickup", false));
		}

		if (searchRequest.getMinRating() != null) {
			commonQueryBuilder
					.must(new RangeQueryBuilder("rating").gte(searchRequest.getMinRating()));
		}

		if (searchRequest.getMaxRating() != null) {
			commonQueryBuilder
					.must(new RangeQueryBuilder("rating").lte(searchRequest.getMaxRating()));
		}

		if (!CollectionUtils.isEmpty(searchRequest.getVendors())) {
			BoolQueryBuilder vendorNameBoolQueryBuilder = new BoolQueryBuilder();

			for (String vendor: searchRequest.getVendors()) {
				if (StringUtils.isNotEmpty(vendor)) {
					vendorNameBoolQueryBuilder
						.should(new MatchQueryBuilder("vendor", vendor).operator(Operator.OR));
				}
			}

			commonQueryBuilder.must(vendorNameBoolQueryBuilder);
		}

		if (!CollectionUtils.isEmpty(searchRequest.getCategories())) {
			BoolQueryBuilder categoryNameBoolQueryBuilder = new BoolQueryBuilder();

			for (String category: searchRequest.getCategories()) {
				if (StringUtils.isNotEmpty(category)) {
					categoryNameBoolQueryBuilder
						.must(new MatchQueryBuilder("vendorCategories", category).operator(Operator.AND));
				}
			}

			commonQueryBuilder.must(categoryNameBoolQueryBuilder);
		}

		NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();

		searchQuery.withQuery(commonQueryBuilder);

		searchQuery.addAggregation(AggregationBuilders.terms(CATEGORY)
				.field("vendorCategories.keyword")
				.size(1000));

        if (searchRequest.getLatitude() != null && searchRequest.getLongitude() != null && searchRequest.getDistance() != null) {
			commonQueryBuilder
					.must(new GeoDistanceQueryBuilder("location")
							.point(searchRequest.getLatitude(), searchRequest.getLongitude())
							.distance(searchRequest.getDistance(), DistanceUnit.MILES));
		}

		if (properties.getPromotionengine().isEnabled()) {
        	// use sorting promotionalWeight by desc (as the top of priority) if promotion is enabled
			searchQuery.withSort(SortBuilders.fieldSort("promotionalWeight").order(SortOrder.DESC).unmappedType("double"));
		}

		for (SortBuilder<?> sortBuilder : createSortBuilders(searchRequest)) {
			searchQuery.withSort(sortBuilder);
		}

		searchQuery.withPageable(createPageRequest(searchRequest, properties.getSearch().getDefaultPageSize()));

		return (AggregatedPage<VendorRestaurantModel>) vendorRestaurantRepository.search((searchQuery).build());

	}

	@Override
	public List<String> suggest(String prefix, int size, final LocationSearchParams locationSearchParams) {

		CompletionSuggestionBuilder suggestionBuilder = SuggestBuilders
				.completionSuggestion("suggest")
				.size(size)
				.prefix(prefix, Fuzziness.AUTO)
				.skipDuplicates(true)
				.contexts(buildSuggestQueryContextMap(locationSearchParams));

		SearchResponse suggestResponse = elasticsearchTemplate.suggest(new SuggestBuilder()
			.addSuggestion("suggestions", suggestionBuilder), VendorRestaurantModel.class);

		List<? extends Entry<? extends Option>> objects = suggestResponse.getSuggest().getSuggestion("suggestions").getEntries();
		return objects.stream()
			.map(Entry::getOptions)
			.flatMap(List::stream)
			.map(Option::getText)
			.map(Object::toString)
			.collect(Collectors.toList());

	}

}
