package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.config.ElasticsearchConfig;
import io.tmg.hungryhappyhippo.poc.data.index.request.IndexParams;
import io.tmg.hungryhappyhippo.poc.data.index.result.IndexResultData;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.IndexModel;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.models.VendorModel;
import io.tmg.hungryhappyhippo.poc.services.AdminService;
import io.tmg.hungryhappyhippo.poc.services.CategoryService;
import io.tmg.hungryhappyhippo.poc.services.LocationService;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import io.tmg.hungryhappyhippo.poc.services.VendorService;
import io.tmg.hungryhappyhippo.poc.services.async.AsyncTaskService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.cluster.health.ClusterIndexHealth;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.profiler.Profiler;
import org.springframework.context.ApplicationContext;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@Service
public class DefaultAdminService implements AdminService {

    @Resource
    private LocationService locationService;

    @Resource
    private VendorService vendorService;

    @Resource
    private VendorRestaurantService vendorRestaurantService;

    @Resource
    private RestaurantFacade restaurantFacade;

    @Resource
    private ElasticsearchConfig elasticsearchConfig;

    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private CategoryService categoryService;

    @Resource
    private AsyncTaskService asyncTaskService;

    @Override
    public IndexResultData createAllIndexes() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        IndexResultData vendorResult = createVendorIndex();
        IndexResultData locationResult = createLocationIndex();
        IndexResultData vendorRestaurantResult = createVendorRestaurantIndex();
        IndexResultData restaurantResult = createRestaurantIndex();
        IndexResultData menuItemResult = createMenuItemIndex();
        IndexResultData categoryResult = createCategoryIndex();

        Instant end = Instant.now();

        result.success = vendorResult.success && locationResult.success && vendorRestaurantResult.success && restaurantResult.success && menuItemResult.success && categoryResult.success;
        result.message = String.format("%s | %s | %s | %s | %s | %s", vendorResult.message, locationResult.message, vendorRestaurantResult.message, restaurantResult.message, menuItemResult.message, categoryResult.message);
        result.totalIndexesProcessed = vendorResult.totalIndexesProcessed + locationResult.totalIndexesProcessed + vendorRestaurantResult.totalIndexesProcessed + restaurantResult.totalIndexesProcessed + menuItemResult.totalIndexesProcessed + categoryResult.totalIndexesProcessed;
        result.totalDocumentsProcessed = vendorResult.totalDocumentsProcessed + locationResult.totalDocumentsProcessed + vendorRestaurantResult.totalDocumentsProcessed + restaurantResult.totalDocumentsProcessed + menuItemResult.totalDocumentsProcessed + categoryResult.totalDocumentsProcessed;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData createVendorIndex() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(VendorModel.class);
        elasticsearchTemplate.putMapping(VendorModel.class);
        elasticsearchTemplate.refresh(VendorModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished creating vendor index successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData createLocationIndex() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(LocationModel.class);
        elasticsearchTemplate.putMapping(LocationModel.class);
        elasticsearchTemplate.refresh(LocationModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished creating location index successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData createVendorRestaurantIndex() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(VendorRestaurantModel.class);
        elasticsearchTemplate.putMapping(VendorRestaurantModel.class);
        elasticsearchTemplate.refresh(VendorRestaurantModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished creating vendor restaurant index successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData createRestaurantIndex() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(RestaurantModel.class);
        elasticsearchTemplate.putMapping(RestaurantModel.class);
        elasticsearchTemplate.refresh(RestaurantModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished creating restaurant index successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData createMenuItemIndex() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(MenuItemModel.class);
        elasticsearchTemplate.putMapping(MenuItemModel.class);
        elasticsearchTemplate.refresh(MenuItemModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished creating menu item index successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData createCategoryIndex() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(CategoryModel.class);
        elasticsearchTemplate.putMapping(CategoryModel.class);
        elasticsearchTemplate.refresh(CategoryModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished creating category index successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData refreshIndex(String id) {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.refresh(id);

        Instant end = Instant.now();

        result.success = true;
        result.message = String.format("Finished refreshing '%s' index successfully", id);
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData clearIndex(String id) {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        long totalDocumentCount = elasticsearchTemplate.getClient().admin().indices().prepareStats(id).get().getTotal().getDocs().getCount();

        DeleteQuery query = new DeleteQuery();

        query.setIndex(id);
        query.setType(elasticsearchConfig.elasticsearchDefaultDocType());
        query.setQuery(QueryBuilders.matchAllQuery());

        elasticsearchTemplate.delete(query);
        elasticsearchTemplate.refresh(id);

        Instant end = Instant.now();

        result.success = true;
        result.message = String.format("Finished clearing '%s' index successfully", id);
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = totalDocumentCount;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData deleteIndex(String id) {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.deleteIndex(id);

        Instant end = Instant.now();

        result.success = true;
        result.message = String.format("Finished deleting '%s' index successfully", id);
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 0;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData deleteVendor(String id) {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        vendorService.deleteById(id);

        Instant end = Instant.now();

        result.success = true;
        result.message = String.format("Finished deleting '%s' vendor successfully", id);
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 1;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData deleteLocation(String id) {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        locationService.deleteById(id);

        Instant end = Instant.now();

        result.success = true;
        result.message = String.format("Finished deleting '%s' location successfully", id);
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 1;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData deleteCategory(String id) {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        categoryService.deleteById(id);

        Instant end = Instant.now();

        result.success = true;
        result.message = String.format("Finished deleting '%s' category successfully", id);
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = 1;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public Iterable<IndexModel> getActiveIndexes() {

        List<IndexModel> indexes = new ArrayList<>();

        AdminClient client = elasticsearchTemplate.getClient().admin();
        ClusterHealthResponse indexInfos = client.cluster().prepareHealth().get();

        for (ClusterIndexHealth indexInfo : indexInfos.getIndices().values()) {
            IndexModel indexModel = new IndexModel();
            indexModel.id = indexInfo.getIndex();
            indexModel.status = indexInfo.getStatus().name();
            indexModel.numberOfShards = indexInfo.getNumberOfShards();
            indexModel.numberOfReplicas = indexInfo.getNumberOfReplicas();
            indexModel.totalDocCount = elasticsearchTemplate.getClient().prepareSearch(indexInfo.getIndex()).get().getHits().getTotalHits();
            indexes.add(indexModel);
        }

        return indexes;

    }

    @Override
    public IndexResultData loadSupportedVendors() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(VendorModel.class);
        elasticsearchTemplate.putMapping(VendorModel.class);

        long totalVendorsIndexed = 0;
        for (VendorModel vendor : vendorService.getDefaultVendors()) {
            vendorService.save(vendor);
            totalVendorsIndexed++;
        }

        elasticsearchTemplate.refresh(VendorModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished loading supported vendors successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = totalVendorsIndexed;
        result.totalIndexTime = Duration.between(start, end);

        return result;
    }

    @Override
    public Iterable<VendorModel> getSupportedVendors() {

        try {
            return vendorService.getVendors();
        } catch (Exception e) {
            return Collections.emptyList();
        }

    }

    @Override
    public IndexResultData loadTargetLocations() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(LocationModel.class);
        elasticsearchTemplate.putMapping(LocationModel.class);

        long totalLocationsIndexed = 0;
        for (LocationModel location : locationService.getDefaultLocations()) {
            locationService.save(location);
            totalLocationsIndexed++;
        }

        elasticsearchTemplate.refresh(LocationModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished loading target locations successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = totalLocationsIndexed;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public Iterable<LocationModel> getTargetLocations() {

        try {
            return locationService.getLocations();
        } catch (Exception e) {
            return Collections.emptyList();
        }

    }

    @Override
    public IndexResultData loadVendorRestaurants() {
        return loadVendorRestaurants(getVendorExtractors());
    }

    @Override
    public IndexResultData buildRestaurantsFromVendorRestaurants() {

        Profiler profiler = new Profiler("Building restaurants from vendor restaurant data");
        profiler.setLogger(log);
        profiler.start("Creating index");
        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(RestaurantModel.class);
        elasticsearchTemplate.putMapping(RestaurantModel.class);

        Set<String> restaurants = restaurantFacade.buildRestaurantsFromAllVendorRestaurants();
        categoryService.autoAssignCategoriesToRestaurants();
        long totalRestaurantsIndexed = restaurants.size();

        profiler.start("Refreshing index");
        elasticsearchTemplate.refresh(RestaurantModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished building restaurants from vendor restaurants successfully";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = totalRestaurantsIndexed;
        result.totalIndexTime = Duration.between(start, end);

        profiler.stop().log();

        return result;

    }

    private Collection<VendorRestaurantExtractor> getVendorExtractors() {
        return applicationContext.getBeansOfType(VendorRestaurantExtractor.class).values();
    }

    private IndexResultData loadVendorRestaurants(Collection<VendorRestaurantExtractor> vendorRestaurantExtractors) {

        Profiler profiler = new Profiler("Loading vendor restaurants");
        profiler.setLogger(log);
        profiler.start("Creating index");
        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(VendorRestaurantModel.class);
        elasticsearchTemplate.putMapping(VendorRestaurantModel.class);

        final Iterable<LocationModel> targetLocations = getTargetLocations();
        final List<Future<List<VendorRestaurantModel>>> futureList = new ArrayList<>();

        vendorRestaurantExtractors.forEach(extractor ->
                futureList.add(asyncTaskService.performAsyncFetchRestaurants(extractor, targetLocations)));

        List<VendorRestaurantModel> allRestaurants = new ArrayList<>();

        futureList.forEach(future -> {
            try {

                final List<VendorRestaurantModel> restaurants = future.get();
                if (CollectionUtils.isNotEmpty(restaurants)) {
                    allRestaurants.addAll(restaurants);
                }

            } catch (InterruptedException | ExecutionException e) {
                log.warn("Error getting future - {}", future, e);
            }
        });

        vendorRestaurantService.saveAll(allRestaurants);

        profiler.start("Refreshing index");
        elasticsearchTemplate.refresh(VendorRestaurantModel.class);

        Instant end = Instant.now();

        result.success = true;
        result.message = "Finished loading vendor restaurants successfully for all vendors";
        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = allRestaurants.size();
        result.totalIndexTime = Duration.between(start, end);

        profiler.stop().log();

        return result;

    }

    @Override
    public Iterable<CategoryModel> getCategories() {

        try {
            return categoryService.getCategories();
        } catch (Exception e) {
            return Collections.emptyList();
        }

    }

    @Override
    public IndexResultData loadCategories() {

        IndexResultData result = new IndexResultData();
        Instant start = Instant.now();

        elasticsearchTemplate.createIndex(CategoryModel.class);
        elasticsearchTemplate.putMapping(CategoryModel.class);

        long totalCategoriesIndexed = 0;
        try {

            final Iterable<CategoryModel> categoryModels = categoryService.getDefaultCategories();
            for (CategoryModel categoryModel: categoryModels) {
                categoryService.save(categoryModel);
                totalCategoriesIndexed++;
            }

            result.success = true;
            result.message = "Finished loading categories successfully";

        } catch (Exception e) {
            log.warn("loadDefaultCategories error", e);

            result.success = false;
            result.message = "Load error: " + e.getMessage();
        }

        elasticsearchTemplate.refresh(CategoryModel.class);

        Instant end = Instant.now();

        result.totalIndexesProcessed = 1;
        result.totalDocumentsProcessed = totalCategoriesIndexed;
        result.totalIndexTime = Duration.between(start, end);

        return result;

    }

    @Override
    public IndexResultData executeIndexRequest(IndexParams params) {

        IndexResultData result;

        if ("vendorrestaurants".equalsIgnoreCase(params.getType())) {
            return loadVendorRestaurants(params.getId());
        } else {
            result = new IndexResultData();
            result.success = false;
            result.message = "No indexing was performed, the provided index type parameter is invalid";
            result.totalIndexesProcessed = -1;
            result.totalDocumentsProcessed = -1;
            result.totalIndexTime = Duration.ZERO;
        }

        return result;

    }

    private IndexResultData loadVendorRestaurants(String vendorId) {
        IndexResultData result = new IndexResultData();
        Vendor vendor = Vendor.valueOfId(vendorId);
        if (vendor == null) {
            result.success = false;
            result.message = "Not found vendor with id: " + vendorId;
            result.totalDocumentsProcessed = 0;
            result.totalIndexTime = Duration.ZERO;
            return result;
        }
        List<VendorRestaurantExtractor> vendorRestaurantExtractors = new ArrayList<>();
        for (VendorRestaurantExtractor vendorRestaurantExtractor : getVendorExtractors()) {
            if (vendor.equals(vendorRestaurantExtractor.getVendor())) {
                vendorRestaurantExtractors.add(vendorRestaurantExtractor);
            }
        }
        return loadVendorRestaurants(vendorRestaurantExtractors);
    }

}
