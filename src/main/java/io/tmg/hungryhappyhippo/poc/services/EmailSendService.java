package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;
import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;

import javax.mail.MessagingException;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/21/2019
 */
public interface EmailSendService {

	void sendVendorRestaurantsExistReportEmail(final List<CheckVendorRestaurantExist> results) throws MessagingException;

	void sendGeneralMetricsReport(final GeneralMetricsData generalMetricsData) throws MessagingException;

	void sendFeedbackEmail(final FeedbackUserData feedbackUserData) throws MessagingException;

	void sendServiceHealthCheckReport(final ServiceHealthCheckData serviceHealthCheckReportData) throws MessagingException;
}
