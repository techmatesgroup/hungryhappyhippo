package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;

import java.util.List;

public interface VendorRestaurantService extends CrudService<VendorRestaurantModel> {

	Iterable<VendorRestaurantModel> getRestaurants();

	AggregatedPage<VendorRestaurantModel> searchRestaurants(final RestaurantSearchRequest searchRequest);

	List<String> suggest(String prefix, int size, final LocationSearchParams locationSearchParams);

}
