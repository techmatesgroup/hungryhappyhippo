package io.tmg.hungryhappyhippo.poc.services;

import java.io.InputStream;
import java.util.List;

public interface ImportService {
    <T> List<T> fromCsv(byte[] data, Class<T> clazz);

    <T> List<T> fromCsv(final InputStream inputStream, final Class<T> clazz);
}
