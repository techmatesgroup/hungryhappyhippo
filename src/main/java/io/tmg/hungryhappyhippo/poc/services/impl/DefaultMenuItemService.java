package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.search.MenuSearchRequest;
import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import io.tmg.hungryhappyhippo.poc.repositories.MenuRepository;
import io.tmg.hungryhappyhippo.poc.services.MenuItemService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.createPageRequest;

@Slf4j
@Service
public class DefaultMenuItemService implements MenuItemService {

    @Resource
    private MenuRepository menuRepository;

    @Resource
    private ApplicationProperties applicationProperties;

    @Resource
    private Map<String, Float> adminFreeTextSearchKeyFields;

    @Override
    public Iterable<MenuItemModel> getMenus() {
        return menuRepository.findAll();

    }

    @Override
    public Page<MenuItemModel> getPage(Pageable pageable, String searchTerms) {

        if (StringUtils.isEmpty(searchTerms)) {
            return menuRepository.findAll(pageable);

        } else {

            final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                    .withQuery(QueryBuilders.multiMatchQuery(searchTerms)
                            .fields(adminFreeTextSearchKeyFields))
                    .withPageable(pageable)
                    .build();

            return menuRepository.search(searchQuery);
        }

    }

    @Override
    public MenuItemModel getById(String menuId) {
        return menuRepository.findById(menuId).get();
    }

    @Override
    public Optional<MenuItemModel> findById(String menuId) {
        return menuRepository.findById(menuId);
    }

    @Override
    public void save(MenuItemModel menu) {
        menuRepository.save(menu);
    }

    @Override
    public void saveAll(Iterable<MenuItemModel> menuItemModels) {
        menuRepository.saveAll(menuItemModels);
    }

    @Override
    public void deleteById(String id) {
        menuRepository.deleteById(id);
    }

    @Override
    public MenuItemModel instance() {
        return new MenuItemModel();
    }

    @Override
    public void createMenuItems(List<MenuItemModel> menus) {
        try {
            menuRepository.saveAll(menus);
        } catch (Exception e) {
            log.error("Exception during menu saving: {}", e.getMessage(), e);
        }
    }

    @Override
    public Iterable<MenuItemModel> getMenuByRestaurantId(String restaurantId) {
        return menuRepository.findAllByRestaurantId(restaurantId, PageRequest.of(0, applicationProperties.getSearch().getDefaultPageSize()));
    }

    @Override
    public Page<MenuItemModel> search(MenuSearchRequest searchRequest) {
        BoolQueryBuilder commonQueryBuilder = new BoolQueryBuilder();

        if (searchRequest.getMinPrice() != null) {
            commonQueryBuilder
                    .must(new RangeQueryBuilder("price").gte(searchRequest.getMinPrice()));
        }

        if (searchRequest.getMaxPrice() != null) {
            commonQueryBuilder
                    .must(new RangeQueryBuilder("price").lte(searchRequest.getMaxPrice()));
        }

        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder()
                .withQuery(commonQueryBuilder);

        if (StringUtils.isNotEmpty(searchRequest.getSearchTerms())) {
            commonQueryBuilder.must(QueryBuilders.simpleQueryStringQuery(searchRequest.getSearchTerms()));
        }

        searchQuery.withPageable(createPageRequest(searchRequest, applicationProperties.getSearch().getDefaultPageSize()));

        return menuRepository.search((searchQuery).build());
    }

}
