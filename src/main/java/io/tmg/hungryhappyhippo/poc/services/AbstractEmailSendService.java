package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.email.AttachData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/22/2019
 */
@Slf4j
public abstract class AbstractEmailSendService {

	@Resource
	protected JavaMailSenderImpl javaMailSender;

	@Resource
	protected ApplicationProperties applicationProperties;

	protected MimeMessageHelperBuilder mimeMessageHelper() throws MessagingException {

		return new MimeMessageHelperBuilder(javaMailSender.createMimeMessage(),
				MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());

	}


	protected String[] getRecipients(final String[] mailRecipients) {

		return ArrayUtils.isNotEmpty(mailRecipients) ? mailRecipients : getDefaultNotificationRecipients();

	}

	protected String[] getDefaultNotificationRecipients() {

		return applicationProperties.getMail().getNotificationRecipients();

	}

	public static class MimeMessageHelperBuilder extends MimeMessageHelper {

		MimeMessageHelperBuilder(final MimeMessage mimeMessage, final int multipartMode, final String encoding) throws MessagingException {

			super(mimeMessage, multipartMode, encoding);

		}

		public MimeMessageHelperBuilder withSubject(final String subject) throws MessagingException {

			this.setSubject(subject);
			return this;

		}

		public MimeMessageHelperBuilder withUserFrom(final String userFrom) throws MessagingException {

			if (StringUtils.isNotEmpty(userFrom)) {
				this.setFrom(userFrom);
			}
			return this;

		}

		public MimeMessageHelperBuilder withRecipients(final String[] notificationRecipients) throws MessagingException {

			if (ArrayUtils.isEmpty(notificationRecipients)) {
				throw new IllegalArgumentException("NotificationRecipients are empty!");
			}

			this.setTo(notificationRecipients[0]);

			if (notificationRecipients.length > 1) {
				final String[] ccRecipients = new String[notificationRecipients.length - 1];
				System.arraycopy(notificationRecipients, 1, ccRecipients, 0, notificationRecipients.length - 1);
				this.setCc(ccRecipients);
			}

			return this;

		}

		public MimeMessageHelperBuilder withHtml(final String html) throws MessagingException {

			this.setText(html, true);
			return this;

		}

		public MimeMessageHelperBuilder withAttachments(final List<AttachData> attachmentDataList) throws MessagingException {

			if (CollectionUtils.isNotEmpty(attachmentDataList)) {
				for (AttachData attachmentData : attachmentDataList) {
					this.addAttachment(attachmentData.getFileName(), new ByteArrayResource(attachmentData.getData()));
				}
			}

			return this;

		}
	}

}
