package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.models.JobModel;

public interface JobService extends CrudService<JobModel> {

	Iterable<JobModel> getJobs();

	boolean isJobEnabled(JobModel job);

}
