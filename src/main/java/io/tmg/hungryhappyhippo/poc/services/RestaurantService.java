package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;

import java.util.List;

public interface RestaurantService extends CrudService<RestaurantModel> {

	Iterable<RestaurantModel> getRestaurants();

	RestaurantModel getByNameAndAddress(String name, String address);

	AggregatedPage<RestaurantModel> searchRestaurants(final RestaurantSearchRequest searchRequest);

	List<String> suggest(String prefix, int size, final LocationSearchParams locationSearchParams);

}
