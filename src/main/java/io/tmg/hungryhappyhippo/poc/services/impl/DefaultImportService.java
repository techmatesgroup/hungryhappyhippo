package io.tmg.hungryhappyhippo.poc.services.impl;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import io.tmg.hungryhappyhippo.poc.services.ImportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

@Service
public class DefaultImportService implements ImportService {

    @Resource
    private CsvMapper csvMapper;

    @Override
    public <T> List<T> fromCsv(byte[] data, Class<T> clazz) {
        Objects.requireNonNull(data, "No data to convert");
        Objects.requireNonNull(clazz, "No class specified to convert");

        ObjectReader csvReader = csvMapper.readerFor(clazz).with(CsvSchema.emptySchema().withHeader());
        try {
            return csvReader.<T>readValues(data).readAll();
        } catch (IOException e) {
            throw new RuntimeException("Error converting objects from csv format", e);
        }
    }

    @Override
    public <T> List<T> fromCsv(final InputStream inputStream, final Class<T> clazz) {

        Objects.requireNonNull(inputStream, "No inputStream to convert");
        Objects.requireNonNull(clazz, "No class specified to convert");

        ObjectReader csvReader = csvMapper.readerFor(clazz).with(CsvSchema.emptySchema().withHeader());
        try {
            return csvReader.<T>readValues(inputStream).readAll();
        } catch (IOException e) {
            throw new RuntimeException("Error converting objects from csv format " + e.getMessage(), e);
        }

    }
}
