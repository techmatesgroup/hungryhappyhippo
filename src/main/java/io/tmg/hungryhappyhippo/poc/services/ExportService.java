package io.tmg.hungryhappyhippo.poc.services;

import java.util.List;

public interface ExportService {
    byte[] toCsv(List<? extends Object> object, Class<?> clazz);
}
