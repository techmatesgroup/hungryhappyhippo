package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.repositories.CategoryRepository;
import io.tmg.hungryhappyhippo.poc.services.CategoryService;
import io.tmg.hungryhappyhippo.poc.services.ImportService;
import io.tmg.hungryhappyhippo.poc.services.RestaurantService;
import io.tmg.hungryhappyhippo.poc.strategy.AutoLinkingCategoryStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/19/2019
 */
@Slf4j
@Service
public class DefaultCategoryService implements CategoryService {

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private ImportService importService;

    @Resource
    private RestaurantService restaurantService;

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private Map<String, Float> adminFreeTextSearchKeyFields;

    @Override
    public Iterable<CategoryModel> getDefaultCategories() {

        return importService.fromCsv(getClass().getResourceAsStream("/config/categories.csv"), CategoryModel.class);

    }

    @Override
    public Iterable<CategoryModel> getCategories() {

        return categoryRepository.findAll(Sort.by("name.keyword").ascending());

    }

    @Override
    public void autoAssignCategoriesToRestaurants() {
        final Collection<AutoLinkingCategoryStrategy> autoLinkingStrategies = applicationContext.getBeansOfType(AutoLinkingCategoryStrategy.class).values();

        final Iterable<CategoryModel> categories = getCategories();
        final Iterable<RestaurantModel> allRestaurants = restaurantService.getRestaurants();

        final Map<String, RestaurantModel> changedRestaurantsMap = new HashMap<>();
        allRestaurants.forEach(restaurant ->
                categories.forEach(category -> {
                    for (AutoLinkingCategoryStrategy autoLinkingStrategy : autoLinkingStrategies) {
                        if (autoLinkingStrategy.match(restaurant, category)) {
                            restaurant.setCategories(restaurant.getCategories() != null ? restaurant.getCategories() : new HashSet<>());
                            restaurant.getCategories().add(category.getName());

                            changedRestaurantsMap.put(restaurant.id, restaurant);
                            break;
                        }
                    }
                })
        );
        log.trace("ChangedRestaurantsMap values size - {}", changedRestaurantsMap);
        restaurantService.saveAll(changedRestaurantsMap.values());

    }

    @Override
    public Page<CategoryModel> getPage(Pageable pageable, String searchTerms) {

        if (StringUtils.isEmpty(searchTerms)) {
            return categoryRepository.findAll(pageable);

        } else {

            final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                    .withQuery(QueryBuilders.multiMatchQuery(searchTerms)
                            .fields(adminFreeTextSearchKeyFields))
                    .withPageable(pageable)
                    .build();

            return categoryRepository.search(searchQuery);
        }

    }

    @Override
    public CategoryModel getById(String categoryId) {

        return categoryRepository.findById(categoryId).orElseThrow(NotFoundException::new);

    }

    @Override
    public Optional<CategoryModel> findById(String categoryId) {

        return categoryRepository.findById(categoryId);

    }

    @Override
    public void save(CategoryModel categoryModel) {

        categoryRepository.save(categoryModel);

    }

    @Override
    public void saveAll(Iterable<CategoryModel> categoryModels) {

        categoryRepository.saveAll(categoryModels);

    }

    @Override
    public void deleteById(String categoryId) {

        categoryRepository.deleteById(categoryId);

    }

    @Override
    public CategoryModel instance() {

        return new CategoryModel();

    }
}
