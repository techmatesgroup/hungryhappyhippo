package io.tmg.hungryhappyhippo.poc.services.impl;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import io.tmg.hungryhappyhippo.poc.services.TaskService;
import org.springframework.stereotype.Service;

@Service
public class RuntimeTaskCrudService implements CrudService<TaskModel> {

	@Resource
	private TaskService taskService;

	@Override
	public Page<TaskModel> getPage(Pageable pageable, String searchTerms) {

		try {
			List<TaskModel> tasks = IteratorUtils.toList(taskService.getLoadedTasks().iterator());

			if (StringUtils.isNotEmpty(searchTerms)) {
				CollectionUtils.filter(tasks, taskModel -> StringUtils.containsIgnoreCase(taskModel.id,searchTerms) || StringUtils.containsIgnoreCase(taskModel.name, searchTerms));
			}

			return new PageImpl<>(
					tasks,
					PageRequest.of(pageable.getPageNumber(), pageable.getPageSize()),
					tasks.size()
			);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public TaskModel getById(String id) {

		try {
			return taskService.getLoadedTask(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public Optional<TaskModel> findById(String id) {
		try {
			return Optional.ofNullable(taskService.getLoadedTask(id));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void save(TaskModel model) {

		try {
			taskService.loadTask(model);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void saveAll(Iterable<TaskModel> models) {

		for (TaskModel model : models) {
			save(model);
		}

	}

	@Override
	public void deleteById(String id) {

		try {
			taskService.unloadTask(taskService.getLoadedTask(id));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public TaskModel instance() {

		return new TaskModel();

	}

}
