package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 04/01/2019
 */
public interface ServiceHealthCheckService {

	ServiceHealthCheckData createServiceHealthCheckReport();

}
