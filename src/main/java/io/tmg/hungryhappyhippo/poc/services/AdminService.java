package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.index.request.IndexParams;
import io.tmg.hungryhappyhippo.poc.data.index.result.IndexResultData;
import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.IndexModel;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorModel;

public interface AdminService {

	IndexResultData createAllIndexes();

	IndexResultData createVendorIndex();
	IndexResultData createLocationIndex();
	IndexResultData createVendorRestaurantIndex();
	IndexResultData createRestaurantIndex();
	IndexResultData createMenuItemIndex();
	IndexResultData createCategoryIndex();

	Iterable<IndexModel> getActiveIndexes();

	IndexResultData refreshIndex(String id);
	IndexResultData clearIndex(String id);
	IndexResultData deleteIndex(String id);

	IndexResultData loadSupportedVendors();
	Iterable<VendorModel> getSupportedVendors();
	IndexResultData deleteVendor(String id);

	IndexResultData loadTargetLocations();
	Iterable<LocationModel> getTargetLocations();
	IndexResultData deleteLocation(String id);

	Iterable<CategoryModel> getCategories();
	IndexResultData loadCategories();
	IndexResultData deleteCategory(String id);

	IndexResultData loadVendorRestaurants();
	IndexResultData buildRestaurantsFromVendorRestaurants();

	IndexResultData executeIndexRequest(IndexParams params);

}
