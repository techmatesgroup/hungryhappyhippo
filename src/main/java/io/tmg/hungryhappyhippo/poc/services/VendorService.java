package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.models.VendorModel;

public interface VendorService extends CrudService<VendorModel> {

	Iterable<VendorModel> getDefaultVendors();

	Iterable<VendorModel> getVendors();

	Iterable<VendorModel> searchVendors(String searchTerms);

}
