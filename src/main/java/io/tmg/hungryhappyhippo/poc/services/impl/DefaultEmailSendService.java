package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;
import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;
import io.tmg.hungryhappyhippo.poc.services.AbstractEmailSendService;
import io.tmg.hungryhappyhippo.poc.services.EmailSendService;
import io.tmg.hungryhappyhippo.poc.services.MailContentBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/21/2019
 */
@Service
public class DefaultEmailSendService extends AbstractEmailSendService implements EmailSendService {

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private MailContentBuilder mailContentBuilder;

	@Override
	public void sendVendorRestaurantsExistReportEmail(final List<CheckVendorRestaurantExist> results) throws MessagingException {

		javaMailSender.send(mimeMessageHelper()
				.withSubject(String.format(applicationProperties.getMail().getSubject().getVendorRestaurantUnavailable(), results.size()))
				.withUserFrom(javaMailSender.getUsername())
				.withRecipients(getRecipients(applicationProperties.getMail().getRecipients().getVendorRestaurantUnavailableMail()))
				.withHtml(mailContentBuilder.buildVendorRestaurantExistReport(results))
				.getMimeMessage());

	}

	@Override
	public void sendGeneralMetricsReport(final GeneralMetricsData generalMetricsData) throws MessagingException {

		javaMailSender.send(mimeMessageHelper()
				.withSubject(applicationProperties.getMail().getSubject().getGeneralMetrics())
				.withUserFrom(javaMailSender.getUsername())
				.withRecipients(getRecipients(applicationProperties.getMail().getRecipients().getGeneralMetricsMail()))
				.withHtml(mailContentBuilder.buildGeneralMetricsReport(generalMetricsData))
				.getMimeMessage());

	}

	@Override
	public void sendFeedbackEmail(final FeedbackUserData feedbackUserData) throws MessagingException {

		javaMailSender.send(mimeMessageHelper()
				.withSubject(applicationProperties.getMail().getSubject().getUserFeedback())
				.withUserFrom(javaMailSender.getUsername())
				.withRecipients(getRecipients(applicationProperties.getMail().getRecipients().getUserFeedbackMail()))
				.withHtml(mailContentBuilder.buildUserFeedback(feedbackUserData))
				.withAttachments(feedbackUserData.getAttachDataList())
				.getMimeMessage());

	}

	@Override
	public void sendServiceHealthCheckReport(final ServiceHealthCheckData serviceHealthCheckReportData) throws MessagingException {

		javaMailSender.send(mimeMessageHelper()
				.withSubject(applicationProperties.getMail().getSubject().getServiceHealthCheck())
				.withUserFrom(javaMailSender.getUsername())
				.withRecipients(getRecipients(applicationProperties.getMail().getRecipients().getServiceHealthCheckMail()))
				.withHtml(mailContentBuilder.buildServiceHealthCheckReport(serviceHealthCheckReportData))
				.getMimeMessage());

	}
}
