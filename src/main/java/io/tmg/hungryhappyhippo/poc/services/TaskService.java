package io.tmg.hungryhappyhippo.poc.services;

import java.util.Collection;
import java.util.Date;

import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.TaskStatus;

public interface TaskService extends CrudService<TaskModel> {

	Iterable<TaskModel> getAllPersistedTasks();

	boolean isTaskEngineEnabled();

	boolean isTaskEngineRunning() throws Exception;

	boolean isTaskEnginePaused() throws Exception;

	boolean isTaskEngineShutdown() throws Exception;

	void startTaskEngine() throws Exception;

	void pauseTaskEngine() throws Exception;

	void resumeTaskEngine() throws Exception;

	void clearTaskEngine() throws Exception;

	void destroyTaskEngine() throws Exception;

	boolean isTaskEnabled(TaskModel task);

	boolean isTaskLoaded(TaskModel task) throws Exception;

	boolean isTaskRunning(TaskModel task) throws Exception;

	boolean isTaskPaused(TaskModel task) throws Exception;

	boolean isTaskScheduled(TaskModel task) throws Exception;

	Iterable<TaskModel> getLoadedTasks() throws Exception;

	Iterable<TaskModel> getRunningTasks() throws Exception;

	TaskModel getLoadedTask(String taskId) throws Exception;

	TaskStatus getLoadedTaskStatus(String taskId) throws Exception;

	Date getLoadedTaskPreviousFireTime(String taskId) throws Exception;

	Date getLoadedTaskNextFireTime(String taskId) throws Exception;

	void loadTask(TaskModel task) throws Exception;

	void loadTasks(Collection<TaskModel> tasks) throws Exception;

	void unloadTask(TaskModel task) throws Exception;

	void unloadTasks(Collection<TaskModel> tasks) throws Exception;

	void executeTask(TaskModel task) throws Exception;

	void executeTasks(Collection<TaskModel> task) throws Exception;

	void scheduleTask(TaskModel task) throws Exception;

	void scheduleTasks(Collection<TaskModel> tasks) throws Exception;

	void unscheduleTask(TaskModel task) throws Exception;

	void unscheduleTasks(Collection<TaskModel> tasks) throws Exception;

	void pauseTask(TaskModel task) throws Exception;

	void pauseTasks(Collection<TaskModel> tasks) throws Exception;

	void resumeTask(TaskModel task) throws Exception;

	void resumeTasks(Collection<TaskModel> tasks) throws Exception;

	void stopTask(TaskModel task) throws Exception;

	void stopTasks(Collection<TaskModel> tasks) throws Exception;

	void saveLoadedTasks() throws Exception;

	void saveLoadedTask(TaskModel task) throws Exception;

}
