package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.models.UserModel;

public interface UserService extends CrudService<UserModel> {

	void createUser(UserModel user);

	UserModel getUserByName(String userName);

	UserModel createUserAdmin();

	void enableById(String id, boolean active);

}
