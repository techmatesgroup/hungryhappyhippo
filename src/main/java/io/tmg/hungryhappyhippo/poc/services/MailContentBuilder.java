package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;
import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/21/2019
 */
public interface MailContentBuilder {

	String buildVendorRestaurantExistReport(final List<CheckVendorRestaurantExist> checkVendorRestaurantExistList);

	String buildGeneralMetricsReport(final GeneralMetricsData generalMetricsData);

	String buildUserFeedback(final FeedbackUserData feedbackUserData);

	String buildServiceHealthCheckReport(final ServiceHealthCheckData serviceHealthCheckData);
}
