package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.VendorModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.services.GeneralMetricsDataService;
import io.tmg.hungryhappyhippo.poc.services.RestaurantService;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import io.tmg.hungryhappyhippo.poc.services.VendorService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.cluster.health.ClusterIndexHealth;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/22/2019
 */
@Service
public class DefaultGeneralMetricsDataService implements GeneralMetricsDataService {

	@Resource
	private ElasticsearchTemplate elasticsearchTemplate;

	@Resource
	private RestaurantService restaurantService;

	@Resource
	private VendorRestaurantService vendorRestaurantService;

	@Resource
	private VendorService vendorService;

	@Override
	public GeneralMetricsData buildGeneralMetricsInfo() {

		final List<VendorRestaurantModel> vendorRestaurants = IterableUtils.toList(vendorRestaurantService.getRestaurants());
		final List<RestaurantModel> restaurants = IterableUtils.toList(restaurantService.getRestaurants());
		final Iterable<VendorModel> vendors = vendorService.getVendors();

		final GeneralMetricsData generalMetricsData = new GeneralMetricsData();

		generalMetricsData.setIndexDataList(buildIndexData());
		generalMetricsData.setVendorStatDataList(buildVendorStatDataList(vendors, vendorRestaurants, restaurants));
		generalMetricsData.setHelpfulSummaryData(buildHelpfulSummaryData(vendorRestaurants, restaurants));

		return generalMetricsData;
	}


	private List<GeneralMetricsData.IndexData> buildIndexData() {

		final List<GeneralMetricsData.IndexData> indexDataList = new ArrayList<>();

		ClusterHealthResponse indexInfos = elasticsearchTemplate.getClient().admin().cluster().prepareHealth().get();
		for (ClusterIndexHealth indexInfo : indexInfos.getIndices().values()) {
			indexDataList.add(new GeneralMetricsData.IndexData(
					indexInfo.getIndex(),
					elasticsearchTemplate.getClient().prepareSearch(indexInfo.getIndex()).get().getHits().getTotalHits()
			));
		}

		return indexDataList;

	}

	private List<GeneralMetricsData.VendorStatsData> buildVendorStatDataList(final Iterable<VendorModel> vendors, final List<VendorRestaurantModel> vendorRestaurants, final List<RestaurantModel> restaurants) {

		final List<GeneralMetricsData.VendorStatsData> vendorStatsDataList = new ArrayList<>();

		vendors.forEach(vendor -> {
			final List<VendorRestaurantModel> vendorRestaurantsForCurrentVendor = filterVendorRestaurantsByVendor(vendor, vendorRestaurants);
			final List<RestaurantModel> restaurantsForCurrentVendor = filterRestaurantsByVendor(vendor, restaurants);

			Integer vendorRestaurantsDeliverySupportedCount = 0;
			Integer vendorRestaurantsPickupSupportedCount = 0;
			Integer vendorRestaurantsAllServiceOptionSupportedCount = 0;

			for (VendorRestaurantModel model : vendorRestaurantsForCurrentVendor) {
				if (BooleanUtils.isTrue(model.getDelivery())) {
					vendorRestaurantsDeliverySupportedCount++;
				}
				if (BooleanUtils.isTrue(model.getPickup())) {
					vendorRestaurantsPickupSupportedCount++;
				}
				if (BooleanUtils.isTrue(model.getDelivery()) && BooleanUtils.isTrue(model.getPickup())) {
					vendorRestaurantsAllServiceOptionSupportedCount++;
				}
			}
			Integer restaurantsExclusivelyCount = 0;
			for (RestaurantModel model : restaurantsForCurrentVendor) {
				if (model.getVendorRestaurants().size() == 1) {
					restaurantsExclusivelyCount++;
				}
			}

			final GeneralMetricsData.VendorStatsData vendorStatsData = new GeneralMetricsData.VendorStatsData();
			vendorStatsData.setVendorName(vendor.name);
			vendorStatsData.setVendorRestaurantsCount(vendorRestaurantsForCurrentVendor.size());
			vendorStatsData.setVendorRestaurantsDeliverySupportedCount(vendorRestaurantsDeliverySupportedCount);
			vendorStatsData.setVendorRestaurantsPickupSupportedCount(vendorRestaurantsPickupSupportedCount);
			vendorStatsData.setVendorRestaurantsAllServiceOptionSupportedCount(vendorRestaurantsAllServiceOptionSupportedCount);
			vendorStatsData.setRestaurantsCount(restaurantsForCurrentVendor.size());
			vendorStatsData.setRestaurantsExclusivelyCount(restaurantsExclusivelyCount);
			vendorStatsDataList.add(vendorStatsData);

		});

		return vendorStatsDataList;

	}

	private List<VendorRestaurantModel> filterVendorRestaurantsByVendor(final VendorModel vendorModel, final List<VendorRestaurantModel> vendorRestaurants) {

		return vendorRestaurants.stream()
				.filter(vendorRestaurant -> vendorModel.name.equals(vendorRestaurant.getVendor()))
				.collect(Collectors.toList());

	}

	private List<RestaurantModel> filterRestaurantsByVendor(final VendorModel vendorModel, final List<RestaurantModel> restaurants) {

		return restaurants.stream()
				.filter(restaurant -> restaurant.getVendorRestaurants().stream()
						.anyMatch(vendorRestaurant -> vendorModel.name.equals(vendorRestaurant.getVendor())))
				.collect(Collectors.toList());

	}

	private GeneralMetricsData.HelpfulSummaryData buildHelpfulSummaryData(final List<VendorRestaurantModel> vendorRestaurants, final List<RestaurantModel> restaurants) {

		final GeneralMetricsData.HelpfulSummaryData helpfulSummaryData = new GeneralMetricsData.HelpfulSummaryData();

		Integer vendorRestaurantsWithoutAggregateCount = 0;
		Integer vendorRestaurantsMissingRatingCount = 0;
		Integer vendorRestaurantsMissingPlaceIdCount = 0;

		for (VendorRestaurantModel vendorRestaurantModel : vendorRestaurants) {

			final RestaurantModel aggregate = IterableUtils.find(restaurants,
					restaurantModel -> restaurantModel.getVendorRestaurants() != null && restaurantModel.getVendorRestaurants().stream()
							.anyMatch(vendorRestaurant -> vendorRestaurant.getId().equals(vendorRestaurantModel.getId())));

			if (aggregate == null || StringUtils.isEmpty(vendorRestaurantModel.restaurantAggregateId)) {
				vendorRestaurantsWithoutAggregateCount++;
			}

			if (vendorRestaurantModel.getRating() == null || vendorRestaurantModel.getRating().intValue() == 0) {
				vendorRestaurantsMissingRatingCount++;
			}

			if (StringUtils.isEmpty(vendorRestaurantModel.getGooglePlaceId())) {
				vendorRestaurantsMissingPlaceIdCount++;
			}

		}

		Integer restaurantsWithoutCategoryCount = 0;
		Integer restaurantsWithoutVendorRestaurantsCount = 0;
		Integer restaurantsMissingRatingCount = 0;
		Integer restaurantsMissingPlaceIdCount = 0;
		Integer restaurantsMissingPriceLevelCount = 0;

		for (RestaurantModel restaurantModel : restaurants) {

			if (CollectionUtils.isEmpty(restaurantModel.getCategories())) {
				restaurantsWithoutCategoryCount++;
			}

			if (CollectionUtils.isEmpty(restaurantModel.getVendorRestaurants())) {
				restaurantsWithoutVendorRestaurantsCount++;
			}

			if (restaurantModel.getRating() == null || restaurantModel.getRating().intValue() == 0) {
				restaurantsMissingRatingCount++;
			}

			if (StringUtils.isEmpty(restaurantModel.getGooglePlaceId())) {
				restaurantsMissingPlaceIdCount++;
			}

			if (restaurantModel.getPriceLevel() == null || restaurantModel.getPriceLevel().intValue() == 0) {
				restaurantsMissingPriceLevelCount++;
			}

		}

		helpfulSummaryData.setVendorRestaurantsWithoutAggregateCount(vendorRestaurantsWithoutAggregateCount);
		helpfulSummaryData.setVendorRestaurantsMissingRatingCount(vendorRestaurantsMissingRatingCount);
		helpfulSummaryData.setVendorRestaurantsMissingPlaceIdCount(vendorRestaurantsMissingPlaceIdCount);

		helpfulSummaryData.setRestaurantsWithoutCategoryCount(restaurantsWithoutCategoryCount);
		helpfulSummaryData.setRestaurantsWithoutVendorRestaurantsCount(restaurantsWithoutVendorRestaurantsCount);
		helpfulSummaryData.setRestaurantsMissingRatingCount(restaurantsMissingRatingCount);
		helpfulSummaryData.setRestaurantsMissingPlaceIdCount(restaurantsMissingPlaceIdCount);
		helpfulSummaryData.setRestaurantsMissingPriceLevelCount(restaurantsMissingPriceLevelCount);

		return helpfulSummaryData;

	}

}
