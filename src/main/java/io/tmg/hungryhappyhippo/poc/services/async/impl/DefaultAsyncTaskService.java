package io.tmg.hungryhappyhippo.poc.services.async.impl;

import com.fasterxml.jackson.databind.JsonNode;
import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.AddressModel;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.services.EmailSendService;
import io.tmg.hungryhappyhippo.poc.services.LocationService;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import io.tmg.hungryhappyhippo.poc.services.async.AsyncTaskService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.AbstractRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/27/2019
 */
@Slf4j
@Service
public class DefaultAsyncTaskService implements AsyncTaskService {

	@Resource
	private LocationService locationService;

	@Resource
	private VendorRestaurantService vendorRestaurantService;

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private EmailSendService emailSendService;

	@Async("fetchRestaurantsTaskExecutor")
	@Override
	public Future<List<VendorRestaurantModel>> performAsyncFetchRestaurants(final VendorRestaurantExtractor vendorRestaurantExtractor, final Iterable<LocationModel> targetLocations) {

		return new AsyncResult<>(vendorRestaurantExtractor.fetchRestaurants(targetLocations));

	}

	@Async("fetchRestaurantInternalTaskExecutor")
	@Override
	public Future<VendorRestaurantModel> performAsyncFetchRestaurantInternal(AbstractRestaurantExtractor vendorRestaurantExtractor, JsonNode restaurant, LocationModel targetLocation) {

		VendorRestaurantModel restaurantModel = null;
		try {
			restaurantModel = vendorRestaurantExtractor.fetchRestaurantInternal(restaurant, targetLocation);

			if (restaurantModel != null) {
				Optional<VendorRestaurantModel> indexedVendorRestaurant = vendorRestaurantService.findById(restaurantModel.id);

				if (!indexedVendorRestaurant.isPresent()) {
					if (applicationProperties.getGoogleApi().isEnabled()) {
						AddressModel normalizedAddress = locationService.getGeocodedAddress(restaurantModel.name, restaurantModel.address);
						if (normalizedAddress != null) {
							restaurantModel.googlePlaceId = normalizedAddress.id;
							restaurantModel.address = normalizedAddress.address;
							restaurantModel.location = normalizedAddress.location;
						}
					}
				} else {
					restaurantModel.googlePlaceId = indexedVendorRestaurant.get().googlePlaceId;
					restaurantModel.address = indexedVendorRestaurant.get().address;
					restaurantModel.location = indexedVendorRestaurant.get().location;
					restaurantModel.restaurantAggregateId = indexedVendorRestaurant.get().restaurantAggregateId;
					restaurantModel.promotionalWeight = indexedVendorRestaurant.get().promotionalWeight;
					restaurantModel.promotionalContent = indexedVendorRestaurant.get().promotionalContent;
				}
			}
		} catch (Exception e) {
			log.warn("An error occurred fetching restaurant details for location: {}: {}: {}", targetLocation, restaurant, e.getMessage(), e);
		}

		return new AsyncResult<>(restaurantModel);

	}

	@Async("checkVendorRestaurantExistenceTaskExecutor")
	@Override
	public Future<CheckVendorRestaurantExist> performAsyncCheckExistence(final VendorRestaurantExtractor vendorRestaurantExtractor, final VendorRestaurantModel vendorRestaurantModel, final LocationSearchParams locationSearchParams) {

		try {
			vendorRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantModel.getIdWithoutVendor(), locationSearchParams);
			return new AsyncResult<>(new CheckVendorRestaurantExist(true, vendorRestaurantModel));

		} catch (Exception e) {
			log.warn("performAsyncCheckExistence. id - {}", vendorRestaurantModel.getId(), e);
			return new AsyncResult<>(new CheckVendorRestaurantExist(false, e.getMessage(), e.toString(), vendorRestaurantModel));
		}

	}

	@Async("emailSendTaskExecutor")
	@Override
	public void performSendFeedbackEmail(final FeedbackUserData feedbackUserData) {

		try {
			emailSendService.sendFeedbackEmail(feedbackUserData);
		} catch (Exception e) {
			log.warn("sendFeedbackEmail. FeedbackUserData: {}", feedbackUserData, e);
		}

	}
}
