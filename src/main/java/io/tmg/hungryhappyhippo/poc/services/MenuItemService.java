package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.data.search.MenuSearchRequest;
import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MenuItemService extends CrudService<MenuItemModel> {

	Iterable<MenuItemModel> getMenus();

	void createMenuItems(List<MenuItemModel> menu);

	Iterable<MenuItemModel> getMenuByRestaurantId(String restaurantId);

	Page<MenuItemModel> search(final MenuSearchRequest searchRequest);

}
