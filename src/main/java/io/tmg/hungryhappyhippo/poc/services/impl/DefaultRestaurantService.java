package io.tmg.hungryhappyhippo.poc.services.impl;

import com.google.common.collect.Iterables;
import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.event.RestaurantDeletedEvent;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.repositories.RestaurantRepository;
import io.tmg.hungryhappyhippo.poc.services.RestaurantService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry.Option;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.AggregationInfo.Restaurants.VENDORRESTAURANTS;
import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.createPageRequest;
import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.createSortBuilders;
import static io.tmg.hungryhappyhippo.poc.utils.ESQueryUtil.buildSuggestQueryContextMap;

@Service("restaurantService")
public class DefaultRestaurantService implements RestaurantService {

	@Resource
	private ApplicationEventPublisher eventPublisher;

	@Resource
	private ElasticsearchTemplate elasticsearchTemplate;

	@Resource
	private RestaurantRepository restaurantRepository;

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private Map<String, Float> restaurantFreeTextSearchKeyFields;

	@Resource
	private Map<String, Float>  restaurantFreeTextSearchKeyVendorsFields;

	@Resource
	private Map<String, Float> adminFreeTextSearchKeyFields;

	@Override
	public Iterable<RestaurantModel> getRestaurants() {

		return restaurantRepository.findAll();

	}

	@Override
	public RestaurantModel getByNameAndAddress(String name, String address) {
		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
		queryBuilder.should(QueryBuilders.matchQuery("name", name).fuzziness(Fuzziness.AUTO)).minimumShouldMatch(1);
		queryBuilder.must(QueryBuilders.matchQuery("address.keyword", address));
		Iterable<RestaurantModel> restaurant = restaurantRepository.search(queryBuilder);
		return Iterables.getFirst(restaurant, null);
	}

	@Override
	public Page<RestaurantModel> getPage(Pageable pageable, String searchTerms) {

		if (StringUtils.isEmpty(searchTerms)) {
			return restaurantRepository.findAll(pageable);

		} else {

			final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
					.withQuery(QueryBuilders.multiMatchQuery(searchTerms)
							.fields(adminFreeTextSearchKeyFields))
					.withPageable(pageable)
					.build();

			return restaurantRepository.search(searchQuery);
		}

	}

    @Override
	public RestaurantModel getById(String restaurantId) {

		return restaurantRepository.findById(restaurantId).orElseThrow(() -> new NotFoundException(String.format("Not found restaurant by id - %s", restaurantId)));

	}

	@Override
	public Optional<RestaurantModel> findById(String restaurantId) {
		return restaurantRepository.findById(restaurantId);
	}

	@Override
	public void save(RestaurantModel restaurant) {

		restaurantRepository.save(restaurant);

	}

	@Override
	public void saveAll(Iterable<RestaurantModel> restaurantModels) {

		if (!IterableUtils.isEmpty(restaurantModels)) {
			restaurantRepository.saveAll(restaurantModels);
		}

	}

	@Override
	public void deleteById(String restaurantId) {

		restaurantRepository.findById(restaurantId).ifPresent(restaurant -> {
			restaurantRepository.deleteById(restaurantId);
			eventPublisher.publishEvent(new RestaurantDeletedEvent(this, restaurantId, restaurant.vendorRestaurants.stream().map(RestaurantModel.VendorRestaurant::getId).collect(Collectors.toSet())));
		});

	}

	@Override
	public RestaurantModel instance() {
		return new RestaurantModel();
	}

	@Override
	public AggregatedPage<RestaurantModel> searchRestaurants(final RestaurantSearchRequest searchRequest) {

		BoolQueryBuilder commonQueryBuilder = QueryBuilders.boolQuery();

		if (StringUtils.isNotEmpty(searchRequest.getSearchTerms())) {
			BoolQueryBuilder termsBoolQueryBuilder = new BoolQueryBuilder();

			termsBoolQueryBuilder
					.should(
						QueryBuilders.multiMatchQuery(searchRequest.getSearchTerms())
						.fields(restaurantFreeTextSearchKeyFields)
						.fuzziness(Fuzziness.AUTO)
			);

			termsBoolQueryBuilder
					.should(QueryBuilders.nestedQuery(VENDORRESTAURANTS,
							QueryBuilders.simpleQueryStringQuery(searchRequest.getSearchTerms())
									.fields(restaurantFreeTextSearchKeyVendorsFields), ScoreMode.Avg));
			termsBoolQueryBuilder.minimumShouldMatch(1);

			commonQueryBuilder.must(termsBoolQueryBuilder);
		}

		if (BooleanUtils.isTrue(searchRequest.getDelivery())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("delivery", true));
		} else if (BooleanUtils.isFalse(searchRequest.getDelivery())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("delivery", false));
		}

		if (BooleanUtils.isTrue(searchRequest.getPickup())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("pickup", true));
		} else if (BooleanUtils.isFalse(searchRequest.getPickup())) {
			commonQueryBuilder
					.must(new MatchQueryBuilder("pickup", false));
		}

		if (searchRequest.getMinRating() != null) {
			commonQueryBuilder
					.must(new RangeQueryBuilder("rating").gte(searchRequest.getMinRating()));
		}

		if (searchRequest.getMaxRating() != null) {
			commonQueryBuilder
					.must(new RangeQueryBuilder("rating").lte(searchRequest.getMaxRating()));
		}

		if (searchRequest.getMinPriceLevel() != null) {
			commonQueryBuilder
					.must(new RangeQueryBuilder("priceLevel").gte(searchRequest.getMinPriceLevel()));
		}

		if (!CollectionUtils.isEmpty(searchRequest.getVendors())) {
			BoolQueryBuilder vendorNameBoolQueryBuilder = new BoolQueryBuilder();

			for (String vendorName: searchRequest.getVendors()) {
				if (StringUtils.isNotEmpty(vendorName)) {
					vendorNameBoolQueryBuilder
						.should(QueryBuilders.nestedQuery(VENDORRESTAURANTS,
								QueryBuilders.matchQuery("vendorRestaurants.vendor", vendorName), ScoreMode.Avg));
				}
			}

			commonQueryBuilder.must(vendorNameBoolQueryBuilder);
		}

		if (!CollectionUtils.isEmpty(searchRequest.getCategories())) {
			BoolQueryBuilder categoryNameBoolQueryBuilder = new BoolQueryBuilder();

			for (String category: searchRequest.getCategories()) {
				if (StringUtils.isNotEmpty(category)) {
					categoryNameBoolQueryBuilder
						.should(new MatchQueryBuilder("categories", category).operator(Operator.OR));
				}
			}

			commonQueryBuilder.must(categoryNameBoolQueryBuilder);
		}

		NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();

		searchQuery.withQuery(commonQueryBuilder);

        if (searchRequest.getLatitude() != null && searchRequest.getLongitude() != null && searchRequest.getDistance() != null) {
            commonQueryBuilder
                    .must(new GeoDistanceQueryBuilder("location")
                            .point(searchRequest.getLatitude(), searchRequest.getLongitude())
                            .distance(searchRequest.getDistance(), DistanceUnit.MILES));
        }

		if (applicationProperties.getPromotionengine().isEnabled()) {
			// use sorting promotionalWeight by desc (as the top of priority) if promotion is enabled
			searchQuery.withSort(SortBuilders.fieldSort("promotionalWeight").order(SortOrder.DESC).unmappedType("double"));
		}

		for (SortBuilder<?> sortBuilder : createSortBuilders(searchRequest)) {
			searchQuery.withSort(sortBuilder);
		}

		searchQuery.withPageable(createPageRequest(searchRequest, applicationProperties.getSearch().getDefaultPageSize()));

		return (AggregatedPage<RestaurantModel>) restaurantRepository.search((searchQuery).build());
	}

	@Override
	public List<String> suggest(String prefix, int size, final LocationSearchParams locationSearchParams) {

		CompletionSuggestionBuilder suggestionBuilder = SuggestBuilders
				.completionSuggestion("suggest")
				.size(size)
				.prefix(prefix, Fuzziness.AUTO)
				.skipDuplicates(true)
				.contexts(buildSuggestQueryContextMap(locationSearchParams));

		SearchResponse suggestResponse = elasticsearchTemplate.suggest(new SuggestBuilder()
			.addSuggestion("suggestions", suggestionBuilder), RestaurantModel.class);

		List<? extends Entry<? extends Option>> objects = suggestResponse.getSuggest().getSuggestion("suggestions").getEntries();
		return objects.stream()
			.map(Entry::getOptions)
			.flatMap(List::stream)
			.map(Option::getText)
			.map(Object::toString)
			.collect(Collectors.toList());

	}

}
