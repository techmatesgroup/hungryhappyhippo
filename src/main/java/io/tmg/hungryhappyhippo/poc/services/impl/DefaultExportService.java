package io.tmg.hungryhappyhippo.poc.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import io.tmg.hungryhappyhippo.poc.services.ExportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class DefaultExportService implements ExportService {

    @Resource
    private CsvMapper csvMapper;

    @Override
    public byte[] toCsv(List<?> objects, Class<?> clazz) {
        Objects.requireNonNull(objects, "No objects to convert");
        Objects.requireNonNull(clazz, "No class specified to convert");

        ObjectWriter csvWriter = csvMapper.writer(getSchemaFor(clazz));
        try {
            return csvWriter.writeValueAsBytes(objects);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error converting objects to csv format", e);
        }
    }

    private CsvSchema getSchemaFor(Class<?> clazz) {
        return csvMapper.schemaFor(clazz).withHeader();
    }
}
