package io.tmg.hungryhappyhippo.poc.services.impl;

import javax.annotation.Resource;

import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.QueryBuilders;

import io.tmg.hungryhappyhippo.poc.models.VendorModel;
import io.tmg.hungryhappyhippo.poc.repositories.VendorRepository;
import io.tmg.hungryhappyhippo.poc.services.ImportService;
import io.tmg.hungryhappyhippo.poc.services.VendorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class DefaultVendorService implements VendorService {

	@Resource
	public VendorRepository vendorRepository;

	@Resource
	private ImportService importService;

	@Resource
	private Map<String, Float> adminFreeTextSearchKeyFields;

	@Override
	public Iterable<VendorModel> getDefaultVendors() {

		return importService.fromCsv(getClass().getResourceAsStream("/config/vendors.csv"), VendorModel.class);

	}

	@Override
	public Iterable<VendorModel> getVendors() {

		return vendorRepository.findAll();

	}

	@Override
	public Page<VendorModel> getPage(Pageable pageable, String searchTerms) {

		if (StringUtils.isEmpty(searchTerms)) {
			return vendorRepository.findAll(pageable);

		} else {

			final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
					.withQuery(QueryBuilders.multiMatchQuery(searchTerms)
							.fields(adminFreeTextSearchKeyFields))
					.withPageable(pageable)
					.build();

			return vendorRepository.search(searchQuery);
		}

	}

	@Override
	public VendorModel getById(String vendorId) {

		return vendorRepository.findById(vendorId).orElseThrow(NotFoundException::new);

	}

	@Override
	public Optional<VendorModel> findById(String vendorId) {
		return vendorRepository.findById(vendorId);
	}

	@Override
	public void save(VendorModel vendor) {

		vendorRepository.save(vendor);

	}

	@Override
	public void saveAll(Iterable<VendorModel> vendorModels) {

		vendorRepository.saveAll(vendorModels);

	}

	@Override
	public Iterable<VendorModel> searchVendors(String searchTerms) {

		return vendorRepository.search(QueryBuilders.simpleQueryStringQuery(searchTerms));

	}

	@Override
	public void deleteById(String vendorId) {

		vendorRepository.deleteById(vendorId);

	}

	@Override
	public VendorModel instance() {
		return new VendorModel();
	}

}