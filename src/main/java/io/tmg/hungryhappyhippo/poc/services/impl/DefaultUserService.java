package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.dbrepositories.UserRepository;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.models.UserModel;
import io.tmg.hungryhappyhippo.poc.models.UserRole;
import io.tmg.hungryhappyhippo.poc.services.UserService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("userService")
public class DefaultUserService implements UserService, UserDetailsService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private ApplicationProperties applicationProperties;

    @Override
    public void createUser(UserModel user) {
        user.setUsername(user.getUsername().toLowerCase());
        UserModel userDB = userRepository.findByUsername(user.getUsername());
        if (userDB != null) {
            throw new IllegalArgumentException("That username is taken. Try another.");
        }
        userRepository.insert(user);
    }

    @Override
    public UserModel getUserByName(String userName) {
        return userRepository.findByUsername(userName);
    }

    public Page<UserModel> getPage(Pageable pageable, String searchTerms) {

        if (StringUtils.isEmpty(searchTerms)) {
            return userRepository.findAll(pageable);

        } else {
            return userRepository.findAllByIdIgnoreCaseContainingOrUsernameIgnoreCaseContaining(searchTerms, searchTerms, pageable);

        }

    }

    @Override
    public UserModel getById(String id) {
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public Optional<UserModel> findById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public void save(UserModel model) {
        if (StringUtils.isNotBlank(model.getId())) {
            userRepository.save(model);
        } else {
            userRepository.insert(model);
        }
    }

    @Override
    public void saveAll(Iterable<UserModel> models) {
        models.forEach(model -> {
            if (StringUtils.isNotBlank(model.getId())) {
                userRepository.save(model);
            } else {
                userRepository.insert(model);
            }
        });
    }

    @Override
    public void deleteById(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserModel instance() {
        return new UserModel();
    }

    @Override
    public void enableById(String id, boolean enable) {
        userRepository.findById(id).ifPresent(user -> {
            user.setEnabled(enable);
            userRepository.save(user);
        });
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel user = userRepository.findByUsername(username);

        if (user != null && user.getRoles() != null) {
            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
            user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority("ROLE_" + role)));
            return User.builder()
                    .username(user.getUsername())
                    .password(user.getPassword())
                    .authorities(authorities)
                    .disabled(!user.isEnabled())
                    .build();
        }

        if (applicationProperties.getAdmin().getLogin().equalsIgnoreCase(username)) {
            boolean needAddAdmin = !userRepository.existsByRoles(UserRole.ADMIN.name());;

            if (needAddAdmin) {
                List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new SimpleGrantedAuthority(UserRole.ADMIN.getRole()));
                return new User(username, passwordEncoder.encode(applicationProperties.getAdmin().getPassword()), authorities);
            }
        }
        throw new UsernameNotFoundException("User not found: " + username);
    }

    public UserModel createUserAdmin() {
        UserModel user = new UserModel();
        user.setUsername(applicationProperties.getAdmin().getLogin());
        user.addRole(UserRole.ADMIN);
        user.setPassword(passwordEncoder.encode(applicationProperties.getAdmin().getPassword()));

        return userRepository.insert(user);
    }
}