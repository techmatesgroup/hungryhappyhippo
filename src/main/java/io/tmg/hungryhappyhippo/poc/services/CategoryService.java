package io.tmg.hungryhappyhippo.poc.services;

import io.tmg.hungryhappyhippo.poc.models.CategoryModel;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/19/2019
 */
public interface CategoryService extends CrudService<CategoryModel> {

    Iterable<CategoryModel> getDefaultCategories();

    Iterable<CategoryModel> getCategories();

    void autoAssignCategoriesToRestaurants();

}
