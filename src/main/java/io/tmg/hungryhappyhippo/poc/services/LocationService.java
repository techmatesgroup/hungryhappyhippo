package io.tmg.hungryhappyhippo.poc.services;

import com.google.maps.model.PlaceDetails;

import io.tmg.hungryhappyhippo.poc.models.AddressModel;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;

public interface LocationService extends CrudService<LocationModel> {

	Iterable<LocationModel> getDefaultLocations();

	Iterable<LocationModel> getLocations();

	Iterable<LocationModel> searchLocations(String searchTerms);

	AddressModel getGeocodedAddress(String placeName, String address);

	AddressModel getReverseGeocodedAddress(double latitude, double longitude);

	PlaceDetails getPlaceDetails(String googlePlaceId);

	LocationModel getClosestSupportedLocation(final String latitude, final String longitude);

}
