package io.tmg.hungryhappyhippo.poc.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.google.common.collect.Sets;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.dbrepositories.TaskRepository;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.TaskStatus;
import io.tmg.hungryhappyhippo.poc.services.TaskService;
import io.tmg.hungryhappyhippo.poc.taskengine.GroovyJob;
import org.springframework.stereotype.Service;

@Service("taskService")
public class DefaultTaskService implements TaskService {

	@Resource
	private TaskRepository taskRepository;

	@Resource
	private SchedulerFactoryBean schedulerFactoryBean;

	@Resource
	private ApplicationProperties applicationProperties;

	@Override
	public Iterable<TaskModel> getAllPersistedTasks() {

		return taskRepository.findAll();

	}

	@Override
	public Page<TaskModel> getPage(Pageable pageable, String searchTerms) {

		if (StringUtils.isEmpty(searchTerms)) {
			return taskRepository.findAll(pageable);

		} else {

			return taskRepository.findByIdContainsIgnoreCaseOrNameContainsIgnoreCase(searchTerms, searchTerms, pageable);
		}

	}

	@Override
	public TaskModel getById(String taskId) {

		return taskRepository.findById(taskId).orElseThrow(NotFoundException::new);

	}

	@Override
	public Optional<TaskModel> findById(String taskId) {
		return taskRepository.findById(taskId);
	}

	@Override
	public void save(TaskModel task) {

		taskRepository.save(task);

	}

	@Override
	public void saveAll(Iterable<TaskModel> taskModels) {

		taskRepository.saveAll(taskModels);

	}

	@Override
	public void deleteById(String taskId) {

		taskRepository.deleteById(taskId);

	}

	@Override
	public TaskModel instance() {

		return new TaskModel();

	}

	@Override
	public boolean isTaskEngineEnabled() {

		return applicationProperties.getTaskengine().isEnabled();

	}

	@Override
	public boolean isTaskEngineRunning() throws SchedulerException {

		return schedulerFactoryBean.getScheduler().isStarted();

	}

	@Override
	public boolean isTaskEnginePaused() throws SchedulerException {

		return schedulerFactoryBean.getScheduler().isInStandbyMode();

	}

	@Override
	public boolean isTaskEngineShutdown() throws SchedulerException {

		return schedulerFactoryBean.getScheduler().isShutdown();

	}

	@Override
	public void startTaskEngine() throws SchedulerException {

		schedulerFactoryBean.getScheduler().start();

	}

	@Override
	public void pauseTaskEngine() throws SchedulerException {

		schedulerFactoryBean.getScheduler().standby();

	}

	@Override
	public void resumeTaskEngine() throws SchedulerException {

		startTaskEngine();

	}

	@Override
	public void clearTaskEngine() throws SchedulerException {

		schedulerFactoryBean.getScheduler().clear();

	}

	@Override
	public void destroyTaskEngine() throws SchedulerException {

		schedulerFactoryBean.getScheduler().shutdown();

	}

	@Override
	public boolean isTaskEnabled(TaskModel task) {

		return Boolean.TRUE.equals(task.enabled);

	}

	@Override
	public boolean isTaskLoaded(TaskModel task) throws SchedulerException {

		return schedulerFactoryBean.getScheduler().checkExists(buildTaskInstanceId(task));

	}

	@Override
	public boolean isTaskRunning(TaskModel task) throws SchedulerException {

		return schedulerFactoryBean.getScheduler().getCurrentlyExecutingJobs().stream().anyMatch(t -> (t.getJobDetail().getKey().equals(buildTaskInstanceId(task))));

	}

	@Override
	public boolean isTaskPaused(TaskModel task) throws SchedulerException {

		return TriggerState.PAUSED.equals(schedulerFactoryBean.getScheduler().getTriggerState(buildTaskTriggerId(task)));

	}

	@Override
	public boolean isTaskScheduled(TaskModel task) throws SchedulerException {

		return schedulerFactoryBean.getScheduler().checkExists(buildTaskTriggerId(task));

	}

	@Override
	public Iterable<TaskModel> getLoadedTasks() throws SchedulerException {

		List<TaskModel> tasks = new ArrayList<TaskModel>();

		if (isTaskEngineShutdown() || !isTaskEngineRunning()) {
			return tasks;
		}

		for (JobKey taskId : schedulerFactoryBean.getScheduler().getJobKeys(GroupMatcher.anyJobGroup())) {
			tasks.add(getTaskByTaskInstanceId(taskId));
		}

		return tasks;

	}

	@Override
	public Iterable<TaskModel> getRunningTasks() throws SchedulerException {

		List<TaskModel> tasks = new ArrayList<TaskModel>();

		for (JobExecutionContext activeTask : schedulerFactoryBean.getScheduler().getCurrentlyExecutingJobs()) {
			tasks.add(getTaskByTaskInstanceId(activeTask.getJobDetail().getKey()));
		}

		return tasks;

	}

	@Override
	public TaskModel getLoadedTask(String taskId) throws SchedulerException {

		return getTaskByTaskInstanceId(JobKey.jobKey(taskId));

	}

	@Override
	public TaskStatus getLoadedTaskStatus(String taskId) throws SchedulerException {

		TaskModel task = getLoadedTask(taskId);
		TaskStatus status = TaskStatus.NONE;

		if (isTaskScheduled(task)) {
			TriggerState triggerState = schedulerFactoryBean.getScheduler().getTriggerState(buildTaskTriggerId(task));
			if (TriggerState.NONE == triggerState) {
				status = TaskStatus.NONE;
			}
			else if (TriggerState.PAUSED == triggerState) {
				status = TaskStatus.PAUSED;
			}
			else if (TriggerState.BLOCKED == triggerState) {
				status = TaskStatus.BLOCKED;
			}
			else if (TriggerState.ERROR == triggerState) {
				status = TaskStatus.ERROR;
			}
			else if (TriggerState.COMPLETE == triggerState) {
				status = TaskStatus.FINISHED;
			}
			else if (TriggerState.NORMAL == triggerState) {
				status = TaskStatus.SCHEDULED;
			}
			else {
				status = TaskStatus.UNKNOWN;
			}
		}
		else {
			if (StringUtils.isBlank(task.status)) {
				status = TaskStatus.NEW;
			}
			else {
				try {
					status = TaskStatus.valueOf(task.id);
				}
				catch (Exception e) {
					status = TaskStatus.UNKNOWN;
				}
			}
		}

		return status;

	}

	@Override
	public Date getLoadedTaskPreviousFireTime(String taskId) throws Exception {

		TaskModel task = getLoadedTask(taskId);

		if (!isTaskScheduled(task)) {
			return null;
		}

		Trigger trigger = getTriggerByTaskInstanceId(buildTaskInstanceId(task));

		if (trigger == null) {
			return null;
		}

		return trigger.getPreviousFireTime();

	}

	@Override
	public Date getLoadedTaskNextFireTime(String taskId) throws Exception {

		TaskModel task = getLoadedTask(taskId);

		if (!isTaskScheduled(task)) {
			return null;
		}

		Trigger trigger = getTriggerByTaskInstanceId(buildTaskInstanceId(task));

		if (trigger == null) {
			return null;
		}

		return trigger.getNextFireTime();

	}

	@Override
	public void loadTask(TaskModel task) throws SchedulerException {

		if (!isTaskEnabled(task)) {
			throw new IllegalStateException("A disabled task cannot be scheduled for execution");
		}

		JobDetail taskInstance = buildTaskInstance(task, true);

		schedulerFactoryBean.getScheduler().addJob(taskInstance, true);

		if (StringUtils.isNotBlank(task.schedule)) {
			scheduleTask(task);
		}
		else {
			unscheduleTask(task);
		}

	}

	@Override
	public void loadTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			loadTask(task);
		}

	}

	@Override
	public void unloadTask(TaskModel task) throws SchedulerException {

		schedulerFactoryBean.getScheduler().deleteJob(buildTaskInstanceId(task));

	}

	@Override
	public void unloadTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			unloadTask(task);
		}

	}

	@Override
	public void executeTask(TaskModel task) throws SchedulerException {

		if (!isTaskEnabled(task)) {
			throw new IllegalStateException("A disabled task cannot be executed");
		}

		if (!isTaskLoaded(task)) {
			JobDetail taskInstance = buildTaskInstance(task, true);
			schedulerFactoryBean.getScheduler().addJob(taskInstance, false);
		}

		schedulerFactoryBean.getScheduler().triggerJob(buildTaskInstanceId(task));

	}

	@Override
	public void executeTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			executeTask(task);
		}

	}

	@Override
	public void scheduleTask(TaskModel task) throws SchedulerException {

		if (!isTaskEnabled(task)) {
			throw new IllegalStateException("A disabled task cannot be scheduled");
		}

		if (StringUtils.isBlank(task.schedule)) {
			throw new IllegalStateException("A task without a schedule defined cannot be scheduled");
		}

		JobDetail taskInstance = buildTaskInstance(task, true);
		Trigger taskTrigger = buildTaskTrigger(task);

		schedulerFactoryBean.getScheduler().scheduleJob(taskInstance, Sets.newHashSet(taskTrigger), true);

	}

	@Override
	public void scheduleTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			scheduleTask(task);
		}

	}

	@Override
	public void unscheduleTask(TaskModel task) throws SchedulerException {

		schedulerFactoryBean.getScheduler().unscheduleJob(buildTaskTriggerId(task));

	}

	@Override
	public void unscheduleTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			unscheduleTask(task);
		}

	}

	@Override
	public void pauseTask(TaskModel task) throws SchedulerException {

		schedulerFactoryBean.getScheduler().pauseJob(buildTaskInstanceId(task));

	}

	@Override
	public void pauseTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			pauseTask(task);
		}

	}

	@Override
	public void resumeTask(TaskModel task) throws SchedulerException {

		schedulerFactoryBean.getScheduler().resumeJob(buildTaskInstanceId(task));

	}

	@Override
	public void resumeTasks(Collection<TaskModel> tasks) throws SchedulerException {

		for (TaskModel task : tasks) {
			resumeTask(task);
		}

	}

	@Override
	public void stopTask(TaskModel task) throws UnableToInterruptJobException {

		schedulerFactoryBean.getScheduler().interrupt(buildTaskInstanceId(task));

	}

	@Override
	public void stopTasks(Collection<TaskModel> tasks) throws UnableToInterruptJobException {

		for (TaskModel task : tasks) {
			stopTask(task);
		}

	}

	protected JobKey buildTaskInstanceId(TaskModel task) {

		return JobKey.jobKey(task.id);

	}

	protected TriggerKey buildTaskTriggerId(TaskModel task) {

		return TriggerKey.triggerKey(task.id);

	}

	protected JobDetail buildTaskInstance(TaskModel task, boolean durable) {

		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("task", task);

		JobDetail taskInstance = JobBuilder.newJob(GroovyJob.class)
				.withIdentity(buildTaskInstanceId(task))
				.withDescription(task.description)
				.storeDurably(durable)
				.usingJobData(jobDataMap)
				.build();

		return taskInstance;

	}

	protected Trigger buildTaskTrigger(TaskModel task) {

		Trigger trigger = TriggerBuilder.newTrigger()
				.withIdentity(buildTaskTriggerId(task))
				.withDescription(task.schedule)
				.forJob(buildTaskInstanceId(task))
				.withSchedule(CronScheduleBuilder.cronSchedule(task.schedule))
				.build();

		return trigger;

	}

	protected TaskModel getTaskByTaskInstanceId(JobKey taskId) throws SchedulerException {

		JobDetail taskInstance = schedulerFactoryBean.getScheduler().getJobDetail(taskId);

		if (taskInstance == null) {
			throw new IllegalArgumentException(String.format("Unable to find the provided task id in task engine: %s", taskId));
		}

		Object task = taskInstance.getJobDataMap().get("task");
		if (task == null) {
			throw new IllegalStateException(String.format("The loaded task instance does not have a task model associated with it: %s", taskInstance));
		}

		return (TaskModel) task;

	}

	protected Trigger getTriggerByTaskInstanceId(JobKey taskId) throws SchedulerException {

		TaskModel task = getTaskByTaskInstanceId(taskId);
		Trigger trigger = schedulerFactoryBean.getScheduler().getTrigger(buildTaskTriggerId(task));

		return trigger;

	}

	@Override
	public void saveLoadedTasks() throws Exception {

		saveAll(getLoadedTasks());

	}

	@Override
	public void saveLoadedTask(TaskModel task) throws Exception {

		save(task);

	}

}
