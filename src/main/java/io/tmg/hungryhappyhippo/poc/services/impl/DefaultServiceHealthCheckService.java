package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.IndexModel;
import io.tmg.hungryhappyhippo.poc.services.AdminService;
import io.tmg.hungryhappyhippo.poc.services.ServiceHealthCheckService;
import io.tmg.hungryhappyhippo.poc.utils.CookieUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchProperties;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 04/01/2019
 */
@Slf4j
@Service
public class DefaultServiceHealthCheckService implements ServiceHealthCheckService {

	private static final String VENDOR_ID = "Delivery.com";
	private static final String VENDOR_RESTAURANT_ID = "Delivery.com-86438";
	private static final String RESTAURANT_ID = "taj-palace-indian-restaurant-bar-6700-middle-fiskville-rd-austin-tx-78752-usa";
	private static final String LATITUDE = "30.3290094";
	private static final String LONGITUDE = "-97.70743299999998";
	private static final String ADDRESS = "6700 Middle Fiskville Rd, Austin, TX 78752, USA";

	@Resource
	private ElasticsearchProperties elasticsearchProperties;

	@Resource
	private AdminService adminService;

	@Resource
	private MongoTemplate mongoTemplate;

	@Resource
	private MongoProperties mongoProperties;

	@Resource
	private RestTemplate healthCheckRestTemplate;

	@Resource
	private ApplicationProperties applicationProperties;

	@Override
	public ServiceHealthCheckData createServiceHealthCheckReport() {

		final ServiceHealthCheckData.ElasticsearchInfo elasticsearchInfo = getElasticsearchInfo();
		final ServiceHealthCheckData.MongoDBInfo mongoDBInfo = getMongoDBInfo();
		final ServiceHealthCheckData.RestApiInfo restApiInfo = getRestApiInfo();

		return new ServiceHealthCheckData(elasticsearchInfo, mongoDBInfo, restApiInfo);

	}

	private ServiceHealthCheckData.RestApiInfo getRestApiInfo() {

		final String internalWebUiEndpoint = applicationProperties.getHealthCheck().getInternalWebUiEndpoint();
		final String externalWebUiEndpoint = applicationProperties.getHealthCheck().getExternalWebUiEndpoint();

		final LocationSearchParams locationSearchParams = new LocationSearchParams(LATITUDE, LONGITUDE, ADDRESS);


		final List<ServiceHealthCheckData.RestApiInfo.EndPointData> endPointDataList = new ArrayList<>();

		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get all supported locations", API_PATH_LOCATIONS, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + API_PATH_LOCATIONS, HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get all supported vendors", API_PATH_VENDORS, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + API_PATH_VENDORS, HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get all supported categories", API_PATH_CATEGORIES, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + API_PATH_CATEGORIES, HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Autocomplete restaurants", API_PATH_RESTAURANTS_SUGGEST, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + getRestaurantSuggestUrl(locationSearchParams), HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Search restaurants", API_PATH_RESTAURANTS_SEARCH, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + getRestaurantSearchUrl(locationSearchParams), HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get restaurant details", API_PATH_RESTAURANT, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + getRestaurantDetailsUrl(), HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get vendor restaurant availability details", API_PATH_RESTAURANT_AVAILABILITY, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + getRestaurantAvailabilityUrl(locationSearchParams), HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get vendor restaurant menus", API_PATH_RESTAURANT_MENU, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + getRestaurantMenuUrl(), HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Get location serviceable", API_PATH_LOCATION_SERVICEABLE, checkServiceAvailability(internalWebUiEndpoint, "/" + API_PREFIX + "/" + API_VERSION_1 + getLocationServiceAbleUrl(locationSearchParams), HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("Internal web ui endpoint", internalWebUiEndpoint, checkServiceAvailability(internalWebUiEndpoint, "", HttpMethod.GET))
		);
		endPointDataList.add(
				new ServiceHealthCheckData.RestApiInfo.EndPointData("External web ui endpoint", externalWebUiEndpoint, checkServiceAvailability(externalWebUiEndpoint, "", HttpMethod.GET))
		);

		return new ServiceHealthCheckData.RestApiInfo(endPointDataList);

	}

	private String getRestaurantSuggestUrl(final LocationSearchParams locationSearchParams) {

		return String.format("%s?%s=%s&%s=%s&%s=%s", API_PATH_RESTAURANTS_SUGGEST,
				CookieUtils.ADDRESS, locationSearchParams.getAddress(),
				CookieUtils.LATITUDE, locationSearchParams.getLatitude(),
				CookieUtils.LONGITUDE, locationSearchParams.getLongitude());

	}

	private String getLocationServiceAbleUrl(LocationSearchParams locationSearchParams) {

		return String.format("%s?%s=%s&%s=%s", API_PATH_LOCATION_SERVICEABLE, CookieUtils.LATITUDE, locationSearchParams.getLatitude(), CookieUtils.LONGITUDE, locationSearchParams.getLongitude());

	}

	private String getRestaurantMenuUrl() {

		return API_PATH_RESTAURANT_MENU
				.replace("{vendorName}", VENDOR_ID)
				.replace("{vendorRestaurantId}", VENDOR_RESTAURANT_ID);

	}

	private String getRestaurantAvailabilityUrl(final LocationSearchParams locationSearchParams) {

		return String.format("%s?%s=%s&%s=%s&%s=%s", API_PATH_RESTAURANT_AVAILABILITY
						.replace("{vendorName}", VENDOR_ID)
						.replace("{vendorRestaurantId}", VENDOR_RESTAURANT_ID),
				CookieUtils.ADDRESS, locationSearchParams.getAddress(),
				CookieUtils.LATITUDE, locationSearchParams.getLatitude(),
				CookieUtils.LONGITUDE, locationSearchParams.getLongitude());

	}

	private String getRestaurantDetailsUrl() {

		return API_PATH_RESTAURANT.replace("{restaurantId}", RESTAURANT_ID);

	}

	private String getRestaurantSearchUrl(final LocationSearchParams locationSearchParams) {

		return String.format("%s?%s=%s&%s=%s&%s=%s", API_PATH_RESTAURANTS_SEARCH,
				CookieUtils.ADDRESS, locationSearchParams.getAddress(),
				CookieUtils.LATITUDE, locationSearchParams.getLatitude(),
				CookieUtils.LONGITUDE, locationSearchParams.getLongitude());

	}

	private String checkServiceAvailability(final String serverPath, final String apiPath, HttpMethod method) {

		try {
			final ResponseEntity<String> responseEntity = healthCheckRestTemplate.exchange(serverPath + apiPath,
					method,
					HttpEntity.EMPTY,
					String.class,
					Collections.emptyMap());

			final HttpStatus statusCode = responseEntity.getStatusCode();
			final String body = responseEntity.getBody();

			if (!HttpStatus.OK.equals(statusCode)) {
				return "Wrong http response code - " + statusCode;
			}

			if (StringUtils.isEmpty(body)) {
				return "Response body is empty";
			}

		} catch (Exception e) {
			log.warn("ApiError - {}", apiPath, e);
			return e.toString();
		}

		return "OK"; // no errors

	}

	private ServiceHealthCheckData.MongoDBInfo getMongoDBInfo() {

		final ServiceHealthCheckData.MongoDBInfo mongoDBInfo = new ServiceHealthCheckData.MongoDBInfo();
		try {
			Document result = this.mongoTemplate.executeCommand("{ buildInfo: 1 }");

			mongoDBInfo.setMongoDbAvailable(true);
			mongoDBInfo.setDbInfo(result);
			mongoDBInfo.setMongoDbStatusInfo(String.format("Host/port: %s:%s", mongoProperties.getHost(), mongoProperties.getPort()));
		} catch (Exception e) {
			log.warn("getMongoDBInfo error", e);

			mongoDBInfo.setMongoDbAvailable(false);
			mongoDBInfo.setDbInfo(null);
			mongoDBInfo.setMongoDbStatusInfo(e.toString());
		}


		return mongoDBInfo;
	}

	private ServiceHealthCheckData.ElasticsearchInfo getElasticsearchInfo() {

		final ServiceHealthCheckData.ElasticsearchInfo elasticsearchInfo = new ServiceHealthCheckData.ElasticsearchInfo();
		try {
			List<IndexModel> activeIndexes = IterableUtils.toList(adminService.getActiveIndexes());

			elasticsearchInfo.setIndexModelList(activeIndexes);
			elasticsearchInfo.setEsAvailable(true);
			elasticsearchInfo.setEsStatusInfo(String.format("ClusterName: %s, clusterNodes: %s", elasticsearchProperties.getClusterName(), elasticsearchProperties.getClusterNodes()));

		} catch (Exception e) {
			log.warn("getActiveIndexes error", e);

			elasticsearchInfo.setIndexModelList(Collections.emptyList());
			elasticsearchInfo.setEsAvailable(false);
			elasticsearchInfo.setEsStatusInfo(e.toString());
		}

		return elasticsearchInfo;

	}

}
