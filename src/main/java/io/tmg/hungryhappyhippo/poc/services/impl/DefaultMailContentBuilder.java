package io.tmg.hungryhappyhippo.poc.services.impl;

import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;
import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;
import io.tmg.hungryhappyhippo.poc.services.MailContentBuilder;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/21/2019
 */
@Service
public class DefaultMailContentBuilder implements MailContentBuilder {

	@Resource
	private TemplateEngine templateEngine;

	@Override
	public String buildVendorRestaurantExistReport(final List<CheckVendorRestaurantExist> checkVendorRestaurantExistList) {

		Context context = new Context();
		context.setVariable("checkVendorRestaurantExistList", checkVendorRestaurantExistList);
		return templateEngine.process("mail/vendorRestaurantCheckReport.html", context);

	}

	@Override
	public String buildGeneralMetricsReport(final GeneralMetricsData generalMetricsData) {

		Context context = new Context();
		context.setVariable("generalMetricsData", generalMetricsData);
		return templateEngine.process("mail/generalMetricsData.html", context);

	}

	@Override
	public String buildUserFeedback(final FeedbackUserData feedbackUserData) {

		Context context = new Context();
		context.setVariable("feedbackUserData", feedbackUserData);
		return templateEngine.process("mail/feedbackData.html", context);

	}

	@Override
	public String buildServiceHealthCheckReport(ServiceHealthCheckData serviceHealthCheckData) {

		Context context = new Context();
		context.setVariable("serviceHealthCheckData", serviceHealthCheckData);
		return templateEngine.process("mail/serviceHealthCheckReport.html", context);

	}
}
