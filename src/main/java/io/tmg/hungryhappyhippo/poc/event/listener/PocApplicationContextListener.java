package io.tmg.hungryhappyhippo.poc.event.listener;

import javax.annotation.Resource;

import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.facade.TaskEngineFacade;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PocApplicationContextListener {

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private TaskEngineFacade taskEngineFacade;

	@EventListener
	protected void onContextStarted(ContextStartedEvent event) {

		if (applicationProperties.getTaskengine().isEnabled()) {

			try {
				log.info("The task engine is starting up");
				taskEngineFacade.startTaskEngine();
			}
			catch (Exception e) {
				log.error("An error occurred when starting task engine at application startup", e);
			}

			if (applicationProperties.getTaskengine().isLoadTasksOnStartup()) {
				try {
					log.info("The task engine is loading all persistent tasks");
					if (!taskEngineFacade.isTaskEngineRunning()) {
						taskEngineFacade.startTaskEngine();
					}
					taskEngineFacade.loadTasksFromAllPersistentTasks();
				}
				catch (Exception e) {
					log.error("An error occurred when loading persistent tasks at application startup", e);
				}
			}
			else {
				log.info("The task engine is not configured to load persistent tasks at application startup on this instance");
			}

		}
		else {
			log.info("The task engine is not enabled on this instance");
		}

	}

	@EventListener
	protected void onContextClosed(ContextClosedEvent event) {

		if (applicationProperties.getTaskengine().isSaveTasksOnShutdown()) {
			try {
				log.info("The task engine is persisting loaded tasks and shutting down");
				taskEngineFacade.saveLoadedTasks();
				taskEngineFacade.destroyTaskEngine();
			} catch (Exception e) {
				log.error("An error occurred when persisting loaded tasks and shutting down task engine at application shutdown", e);
			}
		}
		else {
			log.info("The task engine is not configured to persist loaded tasks at application shutdown on this instance");
		}

	}
}
