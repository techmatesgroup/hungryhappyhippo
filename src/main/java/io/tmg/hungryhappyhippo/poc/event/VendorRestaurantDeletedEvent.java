package io.tmg.hungryhappyhippo.poc.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class VendorRestaurantDeletedEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	private String vendorRestaurantId;
    private String restaurantId;

    public VendorRestaurantDeletedEvent(Object source, String vendorRestaurantId, String restaurantId) {
        super(source);
        this.vendorRestaurantId = vendorRestaurantId;
        this.restaurantId = restaurantId;
    }
}
