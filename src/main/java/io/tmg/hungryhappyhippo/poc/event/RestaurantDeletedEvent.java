package io.tmg.hungryhappyhippo.poc.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.ArrayList;
import java.util.Collection;

@Getter
public class RestaurantDeletedEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	private String restaurantId;
    private Collection<String> vendorRestaurantIds = new ArrayList<>();

    public RestaurantDeletedEvent(Object source, String restaurantId, Collection<String> vendorRestaurantIds) {
        super(source);
        this.restaurantId = restaurantId;
        this.vendorRestaurantIds.addAll(vendorRestaurantIds);
    }
}
