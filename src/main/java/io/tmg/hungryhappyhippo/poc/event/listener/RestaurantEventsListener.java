package io.tmg.hungryhappyhippo.poc.event.listener;

import io.tmg.hungryhappyhippo.poc.event.RestaurantDeletedEvent;
import io.tmg.hungryhappyhippo.poc.event.VendorRestaurantDeletedEvent;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.facade.VendorRestaurantFacade;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class RestaurantEventsListener {

	@Resource
	private RestaurantFacade restaurantFacade;
	@Resource
	private VendorRestaurantFacade vendorRestaurantFacade;

	@EventListener
	protected void onVendorRestaurantDeleted(VendorRestaurantDeletedEvent event) {
		restaurantFacade.deleteVendorRestaurantFromRestaurant(event.getVendorRestaurantId(), event.getRestaurantId());
	}

	@EventListener
	protected void onRestaurantDeleted(RestaurantDeletedEvent event) {
		vendorRestaurantFacade.unlinkVendorRestaurantsFromRestaurant(event.getVendorRestaurantIds(), event.getRestaurantId());
	}
}
