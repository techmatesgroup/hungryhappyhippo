package io.tmg.hungryhappyhippo.poc.strategy.impl;

import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.strategy.AutoLinkingCategoryStrategy;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/05/2019
 */
@Service
public class RestaurantNameAutoLinkingStrategy implements AutoLinkingCategoryStrategy {

	@Override
	public boolean match(RestaurantModel restaurantModel, CategoryModel categoryModel) {

		boolean match = false;
		if (!categoryModel.isDisableAutoLinkingByRestaurantName()) {
			match = StringUtils.isNotEmpty(restaurantModel.getName()) && restaurantModel.getName().toLowerCase().contains(categoryModel.getName().toLowerCase());
		}
		return match;

	}

}
