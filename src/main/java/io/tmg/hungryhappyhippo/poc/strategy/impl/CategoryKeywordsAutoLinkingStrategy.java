package io.tmg.hungryhappyhippo.poc.strategy.impl;

import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.strategy.AutoLinkingCategoryStrategy;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/05/2019
 */
@Service
public class CategoryKeywordsAutoLinkingStrategy implements AutoLinkingCategoryStrategy {

	@Override
	public boolean match(RestaurantModel restaurantModel, CategoryModel categoryModel) {

		boolean match = false;
		if (CollectionUtils.isNotEmpty(categoryModel.getKeywords())) {

			final Set<String> vendorRestaurantCategories = new HashSet<>();
			restaurantModel.getVendorRestaurants().forEach(vendorRestaurant -> vendorRestaurantCategories.addAll(vendorRestaurant.getVendorCategories()));

			match = categoryModel.getKeywords().stream().anyMatch(keyword -> {
				final String foundCategoryByKeyword = IterableUtils.find(vendorRestaurantCategories, vendorRestaurantCategory -> vendorRestaurantCategory.toLowerCase().contains(keyword.toLowerCase()));
				return StringUtils.isNotEmpty(foundCategoryByKeyword);
			});
		}
		return match;

	}

}
