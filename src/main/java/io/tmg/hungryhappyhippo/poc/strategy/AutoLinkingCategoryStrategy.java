package io.tmg.hungryhappyhippo.poc.strategy;

import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/05/2019
 */
public interface AutoLinkingCategoryStrategy {

	boolean match(final RestaurantModel restaurantModel, final CategoryModel categoryModel);

}
