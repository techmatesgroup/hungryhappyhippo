package io.tmg.hungryhappyhippo.poc;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties({
        ApplicationProperties.class,
        MongoProperties.class
})
@EnableAdminServer
@SpringBootApplication
public class PocApplication {

    public static void main(String[] args) {

        SpringApplication.run(PocApplication.class, args).start();

    }

}
