package io.tmg.hungryhappyhippo.poc.error.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Error response for REST api
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ApiErrorResponse {

	private String error;
	private String detailsMessage;

}
