package io.tmg.hungryhappyhippo.poc.error.rest;

import org.springframework.http.HttpStatus;

/**
 * Exception for processing rest api requests
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private HttpStatus httpStatus;

	public ApiException() {

		super();
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	}

	public ApiException(final HttpStatus httpStatus, final String errorMessage) {

		super(errorMessage);
		this.httpStatus = httpStatus;

	}

	public ApiException(final HttpStatus httpStatus, final Exception exception) {

		super(exception.toString());
		this.httpStatus = httpStatus;

	}

	public ApiException(final String message, final Throwable cause) {

		super(message, cause);
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	}

	public ApiException(final String message) {

		super(message);
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	}

	public ApiException(final Throwable cause) {

		super(cause);
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}
