package io.tmg.hungryhappyhippo.poc.error;

import lombok.Value;
import org.springframework.context.MessageSource;

import java.util.Locale;

@Value
public class GeneralError {

    public static final String ERROR_MESSAGE = "errorMessage";

    public static final String WARNING_MESSAGE = "warningMessage";

    public static final String NOT_FOUND = "error.objectnotfound";

    public static final String UNEXPECTED = "error.unexpected";

    public static final String LOCATION_COOKIES_NOT_FOUND = "error.location_cookies_not_found";

    public static final int NOT_FOUND_CODE = 1;

    public static final int UNEXPECTED_CODE = 2;

    private final int code;

    private final String message;

    public static String notFoundError(Locale loc, MessageSource source) {
        return error(loc, NOT_FOUND, source);
    }

    public static String unexpectedError(Locale loc, MessageSource source) {
        return error(loc, UNEXPECTED, source);
    }

    public static String locationCookiesNotFoundError(Locale loc, MessageSource source) {
        return error(loc, LOCATION_COOKIES_NOT_FOUND, source);
    }

    private static String error(Locale loc, String message, MessageSource source) {
        return source.getMessage(message, null, loc);
    }

}