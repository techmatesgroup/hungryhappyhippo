package io.tmg.hungryhappyhippo.poc.error;

import io.tmg.hungryhappyhippo.poc.error.rest.ApiErrorResponse;
import io.tmg.hungryhappyhippo.poc.error.rest.ApiException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static io.tmg.hungryhappyhippo.poc.error.GeneralError.ERROR_MESSAGE;
import static io.tmg.hungryhappyhippo.poc.error.GeneralError.WARNING_MESSAGE;
import static io.tmg.hungryhappyhippo.poc.error.GeneralError.locationCookiesNotFoundError;
import static io.tmg.hungryhappyhippo.poc.error.GeneralError.notFoundError;
import static io.tmg.hungryhappyhippo.poc.error.GeneralError.unexpectedError;
import static io.tmg.hungryhappyhippo.poc.utils.ExceptionUtil.getErrorView;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCause;

@Slf4j(topic = "ErrorLog")
@RequiredArgsConstructor
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleNotFoundException(Exception exc, WebRequest req){
        log.error(getRootCause(exc).toString(), exc);
        return getErrorView(req).addObject(ERROR_MESSAGE, notFoundError(req.getLocale(), messageSource));
    }

    @ExceptionHandler(LocationCookieNotFoundException.class)
    public ModelAndView handleLocationCookieNotFoundException(Exception exc, WebRequest req){
        log.error(getRootCause(exc).toString(), exc);
        return getErrorView(req).addObject(WARNING_MESSAGE, locationCookiesNotFoundError(req.getLocale(), messageSource));
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleCustomException(Exception exc, WebRequest req){
        log.error(getRootCause(exc).toString(), exc);
        return getErrorView(req)
            .addObject(ERROR_MESSAGE, unexpectedError(req.getLocale(), messageSource));
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ApiErrorResponse> handleApiException(final ApiException apiException) {

        return new ResponseEntity<>(new ApiErrorResponse(apiException.getHttpStatus().getReasonPhrase(), apiException.getMessage()), apiException.getHttpStatus());

    }

}