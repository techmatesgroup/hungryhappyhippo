package io.tmg.hungryhappyhippo.poc.error;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/15/2019
 */
public class LocationCookieNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LocationCookieNotFoundException() {

        super();

    }

    public LocationCookieNotFoundException(final String message, final Throwable cause) {

        super(message, cause);

    }

    public LocationCookieNotFoundException(final String message) {

        super(message);

    }

    public LocationCookieNotFoundException(final Throwable cause) {

        super(cause);

    }

}