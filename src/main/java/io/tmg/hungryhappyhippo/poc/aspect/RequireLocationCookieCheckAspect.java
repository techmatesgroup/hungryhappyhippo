package io.tmg.hungryhappyhippo.poc.aspect;

import io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.error.LocationCookieNotFoundException;
import io.tmg.hungryhappyhippo.poc.utils.CookieUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/18/2019
 */
@Aspect
@Component
public class RequireLocationCookieCheckAspect {

    @Around("@annotation(io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck)")
    public Object populateLocationContext(ProceedingJoinPoint joinPoint) throws Throwable {

        LocationSearchParams methodLocationSearchParams = null;
        for (Object arg : joinPoint.getArgs()) {
            if (LocationSearchParams.class.isAssignableFrom(arg.getClass())) {
                methodLocationSearchParams = (LocationSearchParams) arg;
            }
        }

        if (methodLocationSearchParams == null) {
            throw new LocationCookieNotFoundException("Not Found locationSearchParams in method - " + joinPoint.getSignature());
        }

        final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw new LocationCookieNotFoundException("Annotation `LocationCookieCheck` is used on service which is not http-request aware. Method: " + joinPoint.getSignature());
        }

        final ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest currentRequest = servletRequestAttributes.getRequest();
        HttpServletResponse currentResponse = servletRequestAttributes.getResponse();

        final LocationSearchParams locationSearchParams = CookieUtils.getLocationSearchParams(currentRequest, currentResponse);

        final LocationCookieCheck locationCookieCheckAnnotation = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(LocationCookieCheck.class);

        if (locationSearchParams != null) {

            methodLocationSearchParams.setLatitude(locationSearchParams.getLatitude());
            methodLocationSearchParams.setLongitude(locationSearchParams.getLongitude());
            methodLocationSearchParams.setAddress(locationSearchParams.getAddress());

        } else if (locationCookieCheckAnnotation.required()) {
            throw new LocationCookieNotFoundException("No location params found");
        }

        return joinPoint.proceed();

    }

}
