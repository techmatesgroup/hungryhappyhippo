package io.tmg.hungryhappyhippo.poc.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class PromotionInterceptor implements HandlerInterceptor {

    @Override
    public void postHandle(final HttpServletRequest request,
                           final HttpServletResponse response, final Object handler,
                           final ModelAndView modelAndView) throws Exception {

        if (modelAndView != null) {
            if (modelAndView.getModelMap().containsAttribute("restaurantResponseData")) {
                //to be implemented here
            }
        }
    }

}
