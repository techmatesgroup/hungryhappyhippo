package io.tmg.hungryhappyhippo.poc.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Getter
@Setter
@ConfigurationProperties(prefix = "app")
public class ApplicationProperties {
    private final Project project = new Project();
    private final Admin admin = new Admin();
    private final TaskEngine taskengine = new TaskEngine();
    private final PromotionEngine promotionengine = new PromotionEngine();
    private final GoogleApi googleApi = new GoogleApi();
    private final Location location = new Location();
    private final Search search = new Search();
    private final DataExtraction dataExtraction = new DataExtraction();
    private final Mail mail = new Mail();
    private final HealthCheck healthCheck = new HealthCheck();

    @Getter
    @Setter
    @ToString
    public static class Project {
        private String version;
        private String name;
        private String description;
    }

    @Getter
    @Setter
    public static class Admin {
        private String login;
        private String password;
    }

    @Getter
    @Setter
    @ToString
    public static class TaskEngine {
        private boolean enabled;
        private boolean loadTasksOnStartup;
        private boolean saveTasksOnShutdown;
    }

    @Getter
    @Setter
    @ToString
    public static class PromotionEngine {
        private boolean enabled;
    }

    @Getter
    @Setter
    @ToString
    public static class GoogleApi {
        private boolean enabled;
        private String key;
    }

    @Getter
    @Setter
    @ToString
    public static class Location {
        private double defaultRadius;
    }

    @Getter
    @Setter
    @ToString
    public static class Search {
        private int defaultPageSize;
        private int maxSearchDistance;
        private List<String> adminFreeTextSearchFields;

        private Restaurant restaurant = new Restaurant();

        @Getter
        @Setter
        @ToString
        public static class Restaurant {
            private List<String> freeTextSearchFields;
            private List<String> freeTextSearchVendorsFields;
        }
    }

    @Getter
    @Setter
    @ToString
    public static class DataExtraction {
        private Deliverycom deliverycom = new Deliverycom();
        private DoorDash doordash = new DoorDash();
        private FavorDelivery favordelivery = new FavorDelivery();
        private Grubhub grubhub = new Grubhub();
        private Postmates postmates = new Postmates();
        private UberEats ubereats = new UberEats();
        private TripAdvisor tripadvisor = new TripAdvisor();

        @Getter
        @Setter
        @ToString
        public static class Deliverycom {
            private String apiClientId;
            private String apiRestaurantSearchEndpoint;
            private String apiRestaurantDetailsEndpoint;
            private String apiMenuEndpoint;
        }
        @Getter
        @Setter
        @ToString
        public static class DoorDash {
            private int apiPageSize;
            private String apiRestaurantLocationSessionEndpoint;
            private String apiRestaurantSearchEndpoint;
            private String apiRestaurantDetailsEndpoint;
            private String apiRestaurantStatusEstimatesEndpoint;
            private String apiRestaurantAddressDeliverableEndpoint;
            private String apiMenuEndpoint;
        }
        @Getter
        @Setter
        @ToString
        public static class FavorDelivery {
            private int apiPageSize;
            private String apiAuthEndpoint;
            private String apiRestaurantSearchEndpoint;
            private String apiRestaurantDetailsEndpoint;
            private String apiMenuEndpoint;
        }
        @Getter
        @Setter
        @ToString
        public static class Grubhub {
            private int apiPageSize;
            private String apiAuthEndpoint;
            private String apiRestaurantSearchEndpoint;
            private String apiRestaurantDetailsEndpoint;
            private String apiMenuEndpoint;
        }
        @Getter
        @Setter
        @ToString
        public static class Postmates {
            private int apiPageSize;
            private String apiRestaurantLocationSessionEndpoint;
            private String apiRestaurantGlobalSearchEndpoint;
            private String apiRestaurantNearbySearchEndpoint;
            private String apiRestaurantDetailsEndpoint;
            private String apiMenuEndpoint;
        }
        @Getter
        @Setter
        @ToString
        public static class UberEats {
            private int apiPageSize;
            private String apiAuthEndpoint;
            private String apiRestaurantSearchEndpoint;
            private String apiRestaurantDetailsEndpoint;
            private String apiMenuEndpoint;
        }
        @Getter
        @Setter
        @ToString
        public static class TripAdvisor {
            private String apiPlaceSearchEndpoint;
        }
    }

    @Getter
    @Setter
    @ToString
    public static class Mail {
        private Subject subject = new Subject();
        private Recipients recipients = new Recipients();

        private String[] notificationRecipients; // default

        @Getter
        @Setter
        @ToString
        public static class Subject {
            private String vendorRestaurantUnavailable;
            private String generalMetrics;
            private String userFeedback;
            private String serviceHealthCheck;
        }

		@Getter
		@Setter
		@ToString
		public static class Recipients {
			private String[] vendorRestaurantUnavailableMail;
			private String[] generalMetricsMail;
			private String[] userFeedbackMail;
			private String[] serviceHealthCheckMail;
		}

    }

    @Getter
    @Setter
    @ToString
    public static class HealthCheck {
        private String internalWebUiEndpoint;
        private String externalWebUiEndpoint;
    }
}
