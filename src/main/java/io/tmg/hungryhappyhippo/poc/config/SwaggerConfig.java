package io.tmg.hungryhappyhippo.poc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Configuration for Swagger
 *
 * To access to swagger-api use http://{host}:{port}/swagger-ui.html
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Resource
	private ApplicationProperties applicationProperties;

	@Bean
	public Docket forageRESTApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select().apis(RequestHandlerSelectors.basePackage("io.tmg.hungryhappyhippo.poc.restcontrollers"))
				.paths(regex("/api/v1/.*"))
				.build()
				.apiInfo(metaData())
				.useDefaultResponseMessages(false);
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder()
				.title("Forage REST API")
				.description(applicationProperties.getProject().getName() + " / " + applicationProperties.getProject().getDescription())
				.license("© 2019 tmg.io")
				.licenseUrl("https://tmg.io")
				.version(applicationProperties.getProject().getVersion())
				.build();
	}

}
