package io.tmg.hungryhappyhippo.poc.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.maps.GeoApiContext;
import de.codecentric.boot.admin.server.domain.values.Registration;
import io.tmg.hungryhappyhippo.poc.ForageStandardServletMultipartResolver;
import io.tmg.hungryhappyhippo.poc.converter.impl.*;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.RestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.dto.JobDto;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantDto;
import io.tmg.hungryhappyhippo.poc.models.dto.TaskDto;
import io.tmg.hungryhappyhippo.poc.taskengine.ConfigurationAwareSchedulerFactoryBean;
import io.tmg.hungryhappyhippo.poc.taskengine.SpringAwareGroovyJobFactory;
import org.modelmapper.ModelMapper;
import org.quartz.SchedulerException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Configuration
public class ServicesConfig {

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private MultipartProperties multipartProperties;

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setFieldMatchingEnabled(true);
		return modelMapper;
	}

	@Bean
	public CsvMapper csvMapper() {
		CsvMapper mapper = new CsvMapper();
		mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
		mapper.registerModule(new JavaTimeModule());
		return mapper;
	}

	@Bean
	public VendorRestaurantDetailsDataConverter vendorRestaurantDetailsDataConverter() {

		return new VendorRestaurantDetailsDataConverter(VendorRestaurantDetailsData.class);

	}

	@Bean
	public RestaurantDetailsDataConverter restaurantDetailsDataConverter() {

		return new RestaurantDetailsDataConverter(RestaurantDetailsData.class);

	}

	@Bean
	public RestaurantsConverter restaurantsConverter() {

		return new RestaurantsConverter(RestaurantDto.class);

	}

	@Bean
	public JobDataConverter jobDataConverter() {

		return new JobDataConverter(JobDto.class);

	}

	@Bean
	public TaskDataConverter taskDataConverter() {

		return new TaskDataConverter(TaskDto.class);

	}

	@Bean
	public JobToTaskConverter jobToTaskConverter() {

		return new JobToTaskConverter(TaskModel.class);

	}


	@Bean
	public SchedulerFactoryBean taskSchedulerFactory() throws SchedulerException {

		SchedulerFactoryBean schedulerFactory = new ConfigurationAwareSchedulerFactoryBean();
		schedulerFactory.setAutoStartup(false);
		schedulerFactory.setSchedulerName("taskEngineScheduler");

		SpringAwareGroovyJobFactory jobFactory = new SpringAwareGroovyJobFactory();
		schedulerFactory.setJobFactory(jobFactory);

		return schedulerFactory;

	}

	@Bean
	public RestTemplate menuRestTemplate() {

		return new RestTemplate();

	}

	@Bean
	public RestTemplate healthCheckRestTemplate() {

		return new RestTemplate();

	}

	@Bean
	public ObjectMapper objectMapper() {

		ObjectMapper objectMapper = new ObjectMapper();

		SimpleModule module = new SimpleModule();
		module.addDeserializer(Registration.class, new JsonDeserializer<Registration>() {
			@Override
			public Registration deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
				JsonNode node = jsonParser.getCodec().readTree(jsonParser);

				return Registration.builder()
						.name(node.get("name").asText())
						.managementUrl(node.get("managementUrl").asText())
						.healthUrl(node.get("healthUrl").asText())
						.serviceUrl(node.get("serviceUrl").asText())
						.metadata(calculateMetadataMap(node.get("metadata")))
						.build();
			}

			private Map<String, String> calculateMetadataMap(JsonNode metadataNode) {
				Iterator<String> metadataFieldNamesIterator = metadataNode.fieldNames();
				Stream<String> metadataFieldNamesStream = StreamSupport.stream(
						Spliterators.spliteratorUnknownSize(metadataFieldNamesIterator, Spliterator.ORDERED), false);

				return metadataFieldNamesStream.collect(Collectors.toMap(Function.identity(),
						e -> metadataNode.get(e).asText()));
			}
		});
		objectMapper.registerModule(module);

		return objectMapper;

	}

	@Bean
	public GeoApiContext geoApiContext() {

		return new GeoApiContext.Builder()
				.apiKey(applicationProperties.getGoogleApi().getKey())
				.build();

	}

	@Bean
	public PasswordEncoder passwordEncoder() {

		return new BCryptPasswordEncoder();

	}

	@Bean(name = DispatcherServlet.MULTIPART_RESOLVER_BEAN_NAME)
	@ConditionalOnMissingBean(MultipartResolver.class)
	public ForageStandardServletMultipartResolver multipartResolver() {

		ForageStandardServletMultipartResolver multipartResolver = new ForageStandardServletMultipartResolver();
		multipartResolver.setResolveLazily(this.multipartProperties.isResolveLazily());
		return multipartResolver;

	}

}
