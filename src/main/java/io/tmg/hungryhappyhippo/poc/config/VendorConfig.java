package io.tmg.hungryhappyhippo.poc.config;

import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.models.VendorModel;

import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.RestaurantRatingExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.DeliverycomRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.DoorDashRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.GrubhubRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.FavorDeliveryRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.UberEatsRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.PostmatesRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.TripAdvisorRestaurantRatingExtractor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class VendorConfig {

	@Bean
	public List<VendorModel> supportedVendors() {

		List<VendorModel> vendors = new ArrayList<>();

		for (Vendor vendor : Vendor.values()) {
			vendors.add(new VendorModel(vendor.getName(), vendor.getWebsiteUrl()));
		}

		return vendors;

	}

	@Bean
	public VendorRestaurantExtractor deliverycomRestaurantExtractor() {
		return new DeliverycomRestaurantExtractor();
	}

	@Bean
	public VendorRestaurantExtractor doorDashRestaurantExtractor() {
		return new DoorDashRestaurantExtractor();
	}

	@Bean
	public VendorRestaurantExtractor favorDeliveryRestaurantExtractor() {
		return new FavorDeliveryRestaurantExtractor();
	}

	@Bean
	public VendorRestaurantExtractor grubhubRestaurantExtractor() {
		return new GrubhubRestaurantExtractor();
	}

	@Bean
	public VendorRestaurantExtractor postmatesRestaurantExtractor() {
		return new PostmatesRestaurantExtractor();
	}

	@Bean
	public VendorRestaurantExtractor uberEatsRestaurantExtractor() {
		return new UberEatsRestaurantExtractor();
	}

	@Bean
	public RestaurantRatingExtractor tripAdvisorRestaurantRatingExtractor() {

		return new TripAdvisorRestaurantRatingExtractor();

	}

	@Bean
	public List<String> allVendorNames() {

		List<String> allVendorNames = new ArrayList<>();
		supportedVendors().forEach(vendor-> allVendorNames.add(vendor.name));

		return allVendorNames;

	}

}
