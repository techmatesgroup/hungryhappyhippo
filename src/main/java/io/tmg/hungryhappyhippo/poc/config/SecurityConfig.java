package io.tmg.hungryhappyhippo.poc.config;

import de.codecentric.boot.admin.server.config.AdminServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.models.UserRole.ADMIN;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    PasswordEncoder passwordEncoder;

    @Resource
    UserDetailsService userService;

    @Resource
    private AdminServerProperties adminServer;

    public SecurityConfig() {

        super();

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.httpBasic().disable();

        http.csrf().ignoringAntMatchers("/api/**");
        http.csrf().ignoringAntMatchers("/actuator/**");
        http.csrf().ignoringAntMatchers(
                this.adminServer.getContextPath() + "/instances/**",
                this.adminServer.getContextPath() + "/actuator/**"
        );

        http
                .authorizeRequests()
                .antMatchers("/css/**", "/webjars/**", "/js/**", "/images/**").permitAll()
                .antMatchers("/swagger-ui.html", "/v2/api-docs", "/swagger-resources/**").permitAll()
                .antMatchers("/", "/home", "/reguser").permitAll()
                .antMatchers("/admin/**").hasRole(ADMIN.name())
                .antMatchers("/management/**").hasRole(ADMIN.name())

                .antMatchers("/actuator/**").hasRole(ADMIN.name())
                .antMatchers(this.adminServer.getContextPath() + "/instances/**").permitAll()
                .antMatchers(this.adminServer.getContextPath()  + "/assets/**").permitAll() // <1>

                .antMatchers("/internal/**").permitAll()
                .antMatchers("/error/**").permitAll()
                .antMatchers("/api/**").permitAll()
                .anyRequest().authenticated()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .failureUrl("/login?error")
                    .successForwardUrl("/loginhome")
                    .permitAll()
                .and()
                    .logout()
                    .logoutSuccessUrl("/")
                    .permitAll()
				.and()
                .httpBasic()
				.and()
                .rememberMe();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
    }

}
