package io.tmg.hungryhappyhippo.poc.config;

import io.tmg.hungryhappyhippo.poc.interceptors.PromotionInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class PocWebMvcConfig implements WebMvcConfigurer {

    @Resource
    private ApplicationProperties applicationProperties;

    @Resource
    private PromotionInterceptor promotionInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        if (applicationProperties.getPromotionengine().isEnabled()) {
            registry.addInterceptor(promotionInterceptor).addPathPatterns("/internal/vendorrestaurants/search/*");
            registry.addInterceptor(promotionInterceptor).addPathPatterns("/internal/restaurants/search/*");
        }

    }

    //TODO: Remove this, as this is only for temporarily allowing cross-domain requests to all endpoints (makes local development easier for FED teams that want to use AWS)
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

}
