package io.tmg.hungryhappyhippo.poc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/06/2019
 */
@Configuration
@ImportResource(locations = {"classpath:spring/spring-integration-config.xml"})
public class SpringIntegrationConfig {


}
