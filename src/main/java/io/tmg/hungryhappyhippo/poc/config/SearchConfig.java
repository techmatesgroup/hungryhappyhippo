package io.tmg.hungryhappyhippo.poc.config;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
public class SearchConfig {

    @Resource
    private ApplicationProperties applicationProperties;

    @Bean
    public Map<String, Float> restaurantFreeTextSearchKeyFields() {

        return extractKeyFields(applicationProperties.getSearch().getRestaurant().getFreeTextSearchFields());

    }

    @Bean
    public Map<String, Float> restaurantFreeTextSearchKeyVendorsFields() {

        return extractKeyFields(applicationProperties.getSearch().getRestaurant().getFreeTextSearchVendorsFields());

    }

    @Bean
    public Map<String, Float> adminFreeTextSearchKeyFields() {

        return extractKeyFields(applicationProperties.getSearch().getAdminFreeTextSearchFields());

    }

    private static Map<String, Float> extractKeyFields(List<String> fields) {

        return fields.stream()
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toMap(key -> key, value -> AbstractQueryBuilder.DEFAULT_BOOST));

    }
}
