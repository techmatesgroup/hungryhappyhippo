package io.tmg.hungryhappyhippo.poc.config;

import io.tmg.hungryhappyhippo.poc.elasticsearch.core.ForageElasticsearchTemplate;
import org.elasticsearch.client.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.convert.ElasticsearchConverter;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "io.tmg.hungryhappyhippo.poc.repositories")
public class ElasticsearchConfig {

    @Bean
    public String elasticsearchVendorRestaurantIndex() {

        return "vendorrestaurantindex";

    }

    @Bean
    public String elasticsearchRestaurantIndex() {

        return "restaurantindex";

    }

    @Bean
    public String elasticsearchVendorIndex() {

        return "vendorindex";

    }

    @Bean
    public String elasticsearchLocationIndex() {

        return "locationindex";

    }

    @Bean
    public String elasticsearchMenuItemIndex() {

        return "menuitemindex";

    }

    @Bean
    public String elasticsearchCategoryIndex() {

        return "categoryindex";

    }

    @Bean
    public String elasticsearchDefaultDocType() {

        return "_doc";

    }

    @Bean
    public ForageElasticsearchTemplate elasticsearchTemplate(Client client,
                                                             ElasticsearchConverter converter) {

        return new ForageElasticsearchTemplate(client, converter);

    }

    @Bean
    public ForageElasticsearchTemplate elasticsearchOperations(Client client,
                                                             ElasticsearchConverter converter) {

        return elasticsearchTemplate(client, converter);

    }

}
