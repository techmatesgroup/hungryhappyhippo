package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.RestaurantRatingExtractor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TripAdvisorRestaurantRatingExtractor extends AbstractRestaurantRatingExtractor implements RestaurantRatingExtractor {

	@Resource
	private ApplicationProperties applicationProperties;

	private static final Map<String, String> serviceApiRequestHeaders;

	static {

		serviceApiRequestHeaders = new HashMap<>();
		serviceApiRequestHeaders.put("X-Requested-With", "XMLHttpRequest");

	}

	@Override
	public Double fetchRestaurantRating(RestaurantModel restaurant) {

		Double rating = null;

		try {
			String targetApiEndpoint = String.format(
					applicationProperties.getDataExtraction().getTripadvisor().getApiPlaceSearchEndpoint(),
					restaurant.location.getLat(),
					restaurant.location.getLon(),
					restaurant.getName()
			);

			Document doc = executeHTMLRequest(targetApiEndpoint, Connection.Method.GET, serviceApiRequestHeaders);
			rating = fetchRestaurantRatingInternal(doc, restaurant);

		} catch (Exception e) {
			log.warn("An error occurred fetching restaurant rating for restaurant {} from the TripAdvisor API: {}", restaurant, e.getMessage(), e);
		}

		return rating;

	}

	protected Double fetchRestaurantRatingInternal(Document html, RestaurantModel restaurant) {

		if (html == null || StringUtils.isBlank(html.toString())) {
			return null;
		}

		Double rating = null;

		String restaurantName = restaurant.getName();
		String restaurantAddress = restaurant.getAddress();

		Elements searchResults = html.select(".location-meta-block");

		for (Element searchResult : searchResults) {
			String searchResultName = getSearchResultName(searchResult);
			String searchResultAddress = getSearchResultAddress(searchResult);
			Double searchResultRating = getSearchResultRating(searchResult);

			if (searchResultAddress != null && searchResultAddress.contains(restaurantAddress)) {
				rating = searchResultRating;
				break;
			}

			if (restaurantName.equalsIgnoreCase(searchResultName)) {
				rating = searchResultRating;
				break;
			}
		}

		return rating;

	}

	protected String getSearchResultName(Element searchResult) {

		Element searchResultName = searchResult.select(".result-title").first();

		if (searchResultName == null) {
			return null;
		}

		return searchResultName.wholeText();

	}

	protected String getSearchResultAddress(Element searchResult) {

		Element searchResultAddress = searchResult.select(".address-text").first();

		if (searchResultAddress == null) {
			return null;
		}

		return searchResultAddress.ownText();

	}

	protected Double getSearchResultRating(Element searchResult) {

		String ratingText = searchResult.select(".ui_bubble_rating").attr("alt");

		if (StringUtils.isBlank(ratingText)) {
			return null;
		}

		if (ratingText.indexOf(' ') <= 0) {
			return null;
		}

		return Double.valueOf(ratingText.substring(0, ratingText.indexOf(' ')));

	}
}
