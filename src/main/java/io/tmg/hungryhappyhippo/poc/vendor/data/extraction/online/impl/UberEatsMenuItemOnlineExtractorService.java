package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl;

import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.ubereats.UberEatsMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.AbstractMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.MenuItemOnlineExtractorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * UberEats implementation of {@link MenuItemOnlineExtractorService}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class UberEatsMenuItemOnlineExtractorService extends AbstractMenuItemOnlineExtractorService implements MenuItemOnlineExtractorService<UberEatsMenuResponseDTO> {

    @Override
    public UberEatsMenuResponseDTO fetchMenuItems(final String vendorRestaurantId) {

        log.trace("Start fetching menus for restaurant - {}", vendorRestaurantId);

        ResponseEntity<UberEatsMenuResponseDTO> response = executeRESTRequest(
                String.format(
                    applicationProperties.getDataExtraction().getUbereats().getApiMenuEndpoint(),
                    vendorRestaurantId
                ),
                HttpMethod.GET,
                Headers.with(fetchAuthenticationToken()),
                UberEatsMenuResponseDTO.class
        );

        return response.getBody();

    }

    private Map<String, String> fetchAuthenticationToken() {

        HttpHeaders headers;
        try {
            ResponseEntity<String> response = executeRESTRequest(applicationProperties.getDataExtraction().getUbereats().getApiAuthEndpoint(),
                    HttpMethod.GET,
                    String.class
            );
            headers = response.getHeaders();
        } catch (HttpClientErrorException e) {
            if (HttpStatus.I_AM_A_TEAPOT.equals(e.getStatusCode())) {
                headers = e.getResponseHeaders();
            } else {
                throw e;
            }
        }

        Map<String, String> authHeaders = new HashMap<>();
        if (headers != null) {
            if (headers.containsKey(HttpHeaders.SET_COOKIE)) {
                final List<String> cookiesList = headers.get(HttpHeaders.SET_COOKIE);
                if (cookiesList != null) {
                    for (String cookie : cookiesList) {
                        if (cookie.startsWith("web-eats")) {
                            authHeaders.put(HttpHeaders.COOKIE, cookie);
                        }
                    }
                }
            }

            if (authHeaders.get(HttpHeaders.COOKIE) == null) {
                throw new RuntimeException("Failed to find authentication cookie in HTTP response");
            }

        }

        authHeaders.put("x-requested-with", "XMLHttpRequest");
        return authHeaders;

    }

}
