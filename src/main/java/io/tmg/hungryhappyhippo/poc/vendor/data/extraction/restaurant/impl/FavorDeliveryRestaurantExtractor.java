package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.databind.JsonNode;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

@Slf4j
public class FavorDeliveryRestaurantExtractor extends AbstractRestaurantExtractor implements VendorRestaurantExtractor {

	@Resource
	private ApplicationProperties applicationProperties;

	private static final Pattern RESTAURANTS_API_AUTH_TOKEN_PATTERN = Pattern.compile("^.*token=([^;]+).*$");
	private static final Pattern RESTAURANT_DELIVERY_FEE_PATTERN = Pattern.compile("^\\$([^\\s]+).*$");

	@Override
	public Vendor getVendor() {
		return Vendor.FAVORDELIVERY;
	}

	@Override
	public List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations) {

		List<VendorRestaurantModel> restaurants = new ArrayList<>();

		String authenticationToken;

		try {
			authenticationToken = fetchAuthenticationToken();
		} catch (Exception e) {
			log.warn("An error occurred fetching authentication token from the FavorDelivery API: {}", e.getMessage(), e);
			return restaurants;
		}

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", String.format("JWT %s", authenticationToken));

		final int pageSize = applicationProperties.getDataExtraction().getFavordelivery().getApiPageSize();
		for (LocationModel targetLocation : targetLocations) {

			int page = 1;
			try {
				int fetchedSize;
				do {
					fetchedSize = 0;
					String targetApiEndpoint = String.format(
							applicationProperties.getDataExtraction().getFavordelivery().getApiRestaurantSearchEndpoint(),
							targetLocation.location.getLat(),
							targetLocation.location.getLon(),
							pageSize,
							page
					);

					ResponseEntity<String> response = executeRESTRequest(
							targetApiEndpoint,
							HttpMethod.GET,
							requestHeaders
					);

					String responseJson = getResponseBody(response);

					if (responseJson != null) {
						JsonNode restaurantList = objectMapper.readTree(responseJson).at("/merchants");

						List<VendorRestaurantModel> result = fetchRestaurantsInternal(restaurantList, targetLocation);

						fetchedSize = result.size();
						restaurants.addAll(result);
						page++;
					}
				} while (fetchedSize == pageSize);

			} catch (Exception e) {
				log.warn("An error occurred fetching restaurants from the FavorDelivery API for location: {}: {}", targetLocation, e.getMessage(), e);
			}
		}

		return restaurants;
	}

	@Override
	public VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurant, LocationModel targetLocation) {

		VendorRestaurantModel restaurantModel = new VendorRestaurantModel();

		String id = getJsonValueAsText(restaurant, "id");
		String name = getJsonValueAsText(restaurant, "name");

		restaurantModel.setIdWithVendor(getVendor().getName(), id);
		restaurantModel.name = name;
		restaurantModel.vendor = getVendor().getName();

		String marketId = getJsonValueAsText(restaurant, "market_id");
		restaurantModel.vendorUrl = String.format("https://favordelivery.com/order-delivery/%s/%s", marketId, id);

		restaurantModel.imageUrl = getJsonValueAsText(restaurant, "display_image_url");
		restaurantModel.logoUrl = getJsonValueAsText(restaurant, "display_icon_url");

		Double latitude = Double.valueOf(getJsonValueAsText(restaurant, "lat"));
		Double longitude = Double.valueOf(getJsonValueAsText(restaurant, "lng"));

		restaurantModel.location = new GeoPoint(latitude, longitude);

		restaurantModel.address = LocationUtil.formatAddress(
				getJsonValueAsText(restaurant, "address"),
				getJsonValueAsText(restaurant, "city"),
				getJsonValueAsText(restaurant, "state"),
				getJsonValueAsText(restaurant, "zipcode")
		);

		/* Note: The whole point of the FavorDelivery service is to provide ad-hoc delivery "favors", so it's fine to hardcode here for now */
		restaurantModel.delivery = true;
		restaurantModel.pickup = false;

		/* Note: The FavorDelivery service does not currently provide any ratings */
		restaurantModel.rating = null;

		Collection<String> categories = new ArrayList<>();
		JsonNode categoryList = restaurant.at("/categories");
		if (categoryList.isArray()) {
			for (final JsonNode category : categoryList) {
				if (category != null && category.isValueNode()) {
					categories.add(category.asText());
				}
			}
		}
		restaurantModel.vendorCategories = categories;
		restaurantModel.keywords = new ArrayList<>(restaurantModel.vendorCategories);

		restaurantModel.suggest = new Completion(new String[]{restaurantModel.name});

		return restaurantModel;

	}

	private String fetchAuthenticationToken() {

		ResponseEntity<String> response = executeRESTRequest(
				applicationProperties.getDataExtraction().getFavordelivery().getApiAuthEndpoint()
		);

		String responseHeaders = getResponseHeaders(response);

		Matcher matcher = RESTAURANTS_API_AUTH_TOKEN_PATTERN.matcher(responseHeaders);

		String authenticationToken;

		if (matcher.find()) {
			authenticationToken = matcher.group(1);
		} else {
			throw new RuntimeException("Failed to find authentication cookie in HTTP response");
		}

		return authenticationToken;

	}

	@Override
	public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams)  throws IOException {

		String authenticationToken = fetchAuthenticationToken();

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", String.format("JWT %s", authenticationToken));

		String targetApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getFavordelivery().getApiRestaurantDetailsEndpoint(),
				restaurantId,
				locationSearchParams.getLatitude(),
				locationSearchParams.getLongitude()
		);

		ResponseEntity<String> response = executeRESTRequest(
			targetApiEndpoint,
			HttpMethod.GET,
			requestHeaders
		);

		String responseJson = getResponseBody(response);

		JsonNode restaurantDetailsNode = objectMapper.readTree(responseJson).at("/merchant");

		Boolean isRestaurantOpen = getJsonValueAsBoolean(restaurantDetailsNode, "is_open");

		Boolean isRestaurantAvailableForPickup = false; /* Note: The FavorDelivery service does not currently support pickup */
		Boolean isRestaurantAvailableForDelivery = isRestaurantOpen; /* Note: The FavorDelivery service will always allow delivery as long as the merchant is open */

		Boolean doesRestaurantDeliverToLocation = null; //TODO: We need to find a way to reliably fetch this...one potential endpoint is @ https://api.askfavor.com/api/v6/warnings?market_id=1&merchant_id=220&latitude=30.2672&longitude=-97.7431

		Double restaurantPickupEstimate = null; /* Note: The FavorDelivery service does not currently support pickup */

		/* Note: The formatted delivery fee string is the only way to ensure we fetch primetime surge charges properly */
		Double restaurantDeliveryFee = null;
		String formattedDeliveryFee = getJsonValueAsText(restaurantDetailsNode, "formatted_delivery_fee");
		if (formattedDeliveryFee != null) {
			Matcher matcher = RESTAURANT_DELIVERY_FEE_PATTERN.matcher(formattedDeliveryFee);

			if (matcher.find()) {
				restaurantDeliveryFee = Double.valueOf(matcher.group(1));
			}
		}

		Double restaurantDeliveryEstimate = null; /* Note: The FavorDelivery service does not currently provide delivery estimates */

		final VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = new VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails();

		availabilityDetails.setOpen(isRestaurantOpen);
		availabilityDetails.setPickupAvailable(isRestaurantAvailableForPickup);
		availabilityDetails.setDeliveryAvailable(isRestaurantAvailableForDelivery);
		availabilityDetails.setDeliverableToTargetAddress(doesRestaurantDeliverToLocation);
		availabilityDetails.setPickupEstimate(restaurantPickupEstimate);
		availabilityDetails.setDeliveryFee(restaurantDeliveryFee);
		availabilityDetails.setDeliveryEstimate(restaurantDeliveryEstimate);

		return availabilityDetails;

	}

}
