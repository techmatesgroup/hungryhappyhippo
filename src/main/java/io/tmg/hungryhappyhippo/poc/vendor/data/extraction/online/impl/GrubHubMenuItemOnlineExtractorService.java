package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl;

import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.grubhub.GrubHubMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.AbstractMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.MenuItemOnlineExtractorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;

/**
 * GrubHub implementation of {@link MenuItemOnlineExtractorService}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/11/2019
 */
@Service
public class GrubHubMenuItemOnlineExtractorService extends AbstractMenuItemOnlineExtractorService implements MenuItemOnlineExtractorService<GrubHubMenuResponseDTO> {

    private static final String MENU_API_AUTH_REQUEST_BODY = "{\"brand\":\"GRUBHUB\",\"client_id\":\"beta_UmWlpstzQSFmocLy3h1UieYcVST\",\"scope\":\"anonymous\"}";

    private String authenticationToken;

    public GrubHubMenuResponseDTO fetchMenuItems(final String vendorRestaurantId) throws IOException {

        log.trace("Start fetching menus for restaurant - {}", vendorRestaurantId);

        if (authenticationToken == null) {
            authenticationToken = authenticate();
        }

        String targetApiEndpoint = String.format(
                applicationProperties.getDataExtraction().getGrubhub().getApiMenuEndpoint(),
                vendorRestaurantId
        );

        try {

            return execute(targetApiEndpoint);

        } catch (HttpStatusCodeException e) {
            if (HttpStatus.UNAUTHORIZED.equals(e.getStatusCode())) {
                // make other attempt with updated auth data
                authenticationToken = authenticate();
                return execute(targetApiEndpoint);
            }

            throw e;
        }

    }

    private GrubHubMenuResponseDTO execute(String targetApiEndpoint) {

        final ResponseEntity<GrubHubMenuResponseDTO> response = executeRESTRequest(targetApiEndpoint
                , HttpMethod.GET
                , Headers.with(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", authenticationToken))
                , GrubHubMenuResponseDTO.class);
        return response.getBody();

    }

    private String authenticate() throws IOException {
        ResponseEntity<String> response = executeRESTRequest(
                applicationProperties.getDataExtraction().getGrubhub().getApiAuthEndpoint(),
                HttpMethod.POST,
                Headers.with(HttpHeaders.AUTHORIZATION, "Bearer").addValue(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE),
                MENU_API_AUTH_REQUEST_BODY,
                String.class
        );

        return getJsonValueAsText(objectMapper.readTree(response.getBody()).at("/session_handle"), "access_token");
    }

}
