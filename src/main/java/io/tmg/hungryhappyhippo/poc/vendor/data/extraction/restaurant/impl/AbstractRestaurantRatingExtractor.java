package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.AbstractHtmlDataExtractor;

public abstract class AbstractRestaurantRatingExtractor extends AbstractHtmlDataExtractor {

	protected abstract Double fetchRestaurantRating(RestaurantModel restaurant);

}
