package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant;

import java.util.List;

import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.Vendor;

public interface VendorRestaurantExtractor {

	Vendor getVendor();

	List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations);

	VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams) throws Exception;

}
