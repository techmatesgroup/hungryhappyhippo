package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl;

import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.favordelivery.FavorDeliveryMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.AbstractMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.MenuItemOnlineExtractorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FavorDelivery implementation of {@link MenuItemOnlineExtractorService}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class FavorDeliveryMenuItemOnlineExtractorService extends AbstractMenuItemOnlineExtractorService implements MenuItemOnlineExtractorService<FavorDeliveryMenuResponseDTO> {

    private static final Pattern MENU_API_AUTH_TOKEN_PATTERN = Pattern.compile("^.*token=([^;]+).*$");

    @Override
    public FavorDeliveryMenuResponseDTO fetchMenuItems(final String vendorRestaurantId) {

        log.trace("Start fetching menus for restaurant - {}", vendorRestaurantId);

        final ResponseEntity<FavorDeliveryMenuResponseDTO> response = executeRESTRequest(
                String.format(
                    applicationProperties.getDataExtraction().getFavordelivery().getApiMenuEndpoint(),
                    vendorRestaurantId
                )
                , HttpMethod.GET
                , Headers.with(HttpHeaders.AUTHORIZATION, String.format("JWT %s", fetchAuthenticationToken()))
                , FavorDeliveryMenuResponseDTO.class);

        return response.getBody();
    }

    public String fetchAuthenticationToken() {
        ResponseEntity<String> response = executeRESTRequest(
                applicationProperties.getDataExtraction().getFavordelivery().getApiAuthEndpoint(),
                HttpMethod.GET,
                String.class
        );

        String responseHeaders = response.getHeaders().toString();

        Matcher matcher = MENU_API_AUTH_TOKEN_PATTERN.matcher(responseHeaders);

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException("Failed to find authentication cookie in HTTP response - " + responseHeaders);
        }
    }

}
