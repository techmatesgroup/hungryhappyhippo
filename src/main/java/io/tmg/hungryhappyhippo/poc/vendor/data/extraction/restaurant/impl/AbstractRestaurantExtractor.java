package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.databind.JsonNode;
import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.ServiceOptionType;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.services.async.AsyncTaskService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.AbstractRestDataExtractor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractRestaurantExtractor extends AbstractRestDataExtractor {

	protected static List<ServiceOptionType> TARGET_SERVICE_OPTIONS;

	static {

		TARGET_SERVICE_OPTIONS = new ArrayList<>();
		TARGET_SERVICE_OPTIONS.add(ServiceOptionType.DELIVERY);
		TARGET_SERVICE_OPTIONS.add(ServiceOptionType.PICKUP);

	}

	@Resource
	protected ApplicationProperties applicationProperties;

	@Resource
	private AsyncTaskService asyncTaskService;

	protected List<VendorRestaurantModel> fetchRestaurantsInternal(JsonNode restaurantList, LocationModel targetLocation) {

		List<Future<VendorRestaurantModel>> futureList = new ArrayList<>(restaurantList.size());
		restaurantList.forEach(restaurant ->
				futureList.add(asyncTaskService.performAsyncFetchRestaurantInternal(this, restaurant, targetLocation)));

		List<VendorRestaurantModel> result = new ArrayList<>(restaurantList.size());
		futureList.forEach(future -> {
			try {
				final VendorRestaurantModel vendorRestaurantModel = future.get();
				if (vendorRestaurantModel != null) {
					result.add(vendorRestaurantModel);
				}
			} catch (InterruptedException | ExecutionException e) {
				log.warn("Error getting future - {}", future, e);
			}
		});

		return result;

	}

	public abstract VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurant, LocationModel targetLocation) throws Exception;

	/**
	 * Returns filtered secondaryList by primaryList(VendorRestaurantModel#id)
	 */
	protected List<VendorRestaurantModel> filterDuplicates(final List<VendorRestaurantModel> primaryList, final List<VendorRestaurantModel> secondaryList) {

		if (primaryList.isEmpty()) {
			return secondaryList;
		}

		final Set<String> uniqueIds = primaryList.stream().map(VendorRestaurantModel::getIdWithoutVendor).collect(Collectors.toSet());

		return secondaryList.stream().filter(item -> !uniqueIds.contains(item.getIdWithoutVendor())).collect(Collectors.toList());

	}

}
