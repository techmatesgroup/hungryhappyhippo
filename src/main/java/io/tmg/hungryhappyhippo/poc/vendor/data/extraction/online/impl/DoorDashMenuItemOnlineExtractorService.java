package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl;

import com.fasterxml.jackson.databind.JsonNode;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.DoorDashMenuItemResponse;
import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.doordash.DoorDashMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.AbstractMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.MenuItemOnlineExtractorService;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * DoorDash implementation of {@link MenuItemOnlineExtractorService}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class DoorDashMenuItemOnlineExtractorService extends AbstractMenuItemOnlineExtractorService implements MenuItemOnlineExtractorService<DoorDashMenuResponseDTO> {

    private static final String menuApiEndpoint = "https://api.doordash.com/v2/restaurant/%s/menu/%s";
    private static final String RESTAURANT_DETAILS_INFO_API_ENDPOINT = "https://api.doordash.com/v2/restaurant/%s/";

    @Override
    public DoorDashMenuResponseDTO fetchMenuItems(final String vendorRestaurantId) throws IOException {

        log.trace("Start fetching menus for restaurant - {}", vendorRestaurantId);

        Collection<String> menus = fetchMenuIdsByRestaurant(vendorRestaurantId);

        List<DoorDashMenuItemResponse> menuItemResponseList = new ArrayList<>(menus.size());
        for (String menuId : menus) {

            final ResponseEntity<DoorDashMenuItemResponse> response = executeRESTRequest(
                    String.format(menuApiEndpoint, vendorRestaurantId, menuId)
                    , HttpMethod.GET
                    , DoorDashMenuItemResponse.class);

            menuItemResponseList.add(response.getBody());

        }


        return new DoorDashMenuResponseDTO(menuItemResponseList);
    }

    private Collection<String> fetchMenuIdsByRestaurant(String restaurantId) throws IOException {
        final ResponseEntity<String> response = executeRESTRequest(String.format(RESTAURANT_DETAILS_INFO_API_ENDPOINT, restaurantId)
                , HttpMethod.GET
                , String.class);

        final JsonNode detailRestaurantNode = objectMapper.readTree(response.getBody());
        Collection<String> menuIds = new ArrayList<>();
        JsonNode menus = detailRestaurantNode.at("/menus");
        if (menus != null && menus.isArray()) {
            for (final JsonNode menu : menus) {
                menuIds.add(getJsonValueAsText(menu, "id"));
            }
        }
        return menuIds;
    }
}
