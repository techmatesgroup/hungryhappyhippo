package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.ServiceOptionType;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

@Slf4j
public class PostmatesRestaurantExtractor extends AbstractRestaurantExtractor implements VendorRestaurantExtractor {

    @Resource
    private ApplicationProperties applicationProperties;

    private static final Map<String, String> DEFAULT_RESTAURANTS_API_REQUEST_HEADERS;
    private static final String RESTAURANT_LOCATION_SESSION_BODY = "{\"lat\":%s,\"lng\":%s}";

    private static final Pattern RESTAURANT_DELIVERY_FEE_PATTERN = Pattern.compile("^\\$([^\\s]+).*$");

    static {

        DEFAULT_RESTAURANTS_API_REQUEST_HEADERS = new HashMap<>();
        DEFAULT_RESTAURANTS_API_REQUEST_HEADERS.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    }

    @Override
    public Vendor getVendor() {
        return Vendor.POSTMATES;
    }

    @Override
    public List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations) {

		List<VendorRestaurantModel> resultList = new ArrayList<>();

		for (LocationModel targetLocation : targetLocations) {
			try {
				for (ServiceOptionType targetServiceOption : TARGET_SERVICE_OPTIONS) {
					final List<VendorRestaurantModel> currentGlobalSearchResult = doGlobalSearch(targetLocation, targetServiceOption);
					resultList.addAll(filterDuplicates(resultList, currentGlobalSearchResult));
				}
			} catch (Exception e) {
				log.warn("An error occurred fetching restaurants from the Postmates API for location: {}: {}", targetLocation, e.getMessage(), e);
			}
		}
		for (LocationModel targetLocation : targetLocations) {
			try {
				for (ServiceOptionType targetServiceOption : TARGET_SERVICE_OPTIONS) {
					final List<VendorRestaurantModel> currentNearbySearchResult = doNearbySearch(targetLocation, targetServiceOption);
					resultList.addAll(filterDuplicates(resultList, currentNearbySearchResult));
				}
			} catch (Exception e) {
				log.warn("An error occurred fetching restaurants from the Postmates API for location: {}: {}", targetLocation, e.getMessage(), e);
			}
		}

		return resultList;

	}
	
	private List<VendorRestaurantModel> doGlobalSearch(LocationModel targetLocation, ServiceOptionType targetServiceOption) throws IOException {

		ResponseEntity<String> response = executeRESTRequest(
				applicationProperties.getDataExtraction().getPostmates().getApiRestaurantGlobalSearchEndpoint(),
				HttpMethod.POST,
				DEFAULT_RESTAURANTS_API_REQUEST_HEADERS,
				getRequestBody(targetLocation.location.getLat(), targetLocation.location.getLon(), targetServiceOption.toString())
		);

		String responseJson = getResponseBody(response);

		JsonNode sectionList = objectMapper.readTree(responseJson).at("/sections");

		if (isJsonNodeArray(sectionList)) {
			for (final JsonNode section : sectionList) {
				if (targetServiceOption.toString().equalsIgnoreCase(getJsonValueAsText(section, "preferred_order_method"))) {
					JsonNode restaurantList = section.at("/inline_results");
					return fetchRestaurantsInternal(restaurantList, targetLocation);
				}
			}
		}

		return Collections.emptyList();

	}

	private List<VendorRestaurantModel> doNearbySearch(LocationModel targetLocation, ServiceOptionType targetServiceOption) throws IOException {

		int page = 0;
		boolean hasMore;
		int resultSize;
		final List<VendorRestaurantModel> restaurants = new ArrayList<>();
		do {
			ResponseEntity<String> response = executeRESTRequest(
					applicationProperties.getDataExtraction().getPostmates().getApiRestaurantNearbySearchEndpoint(),
					HttpMethod.POST,
					DEFAULT_RESTAURANTS_API_REQUEST_HEADERS,
					getNearbyRequestBody(targetLocation.location.getLat(), targetLocation.location.getLon(), targetServiceOption.toString(), page++)
			);

			String responseJson = getResponseBody(response);

			final JsonNode responseJsonNode = objectMapper.readTree(responseJson);
			JsonNode itemList = responseJsonNode.at("/items");

			List<VendorRestaurantModel> result = new ArrayList<>();
			if (isJsonNodeArray(itemList)) {
				result = fetchRestaurantsInternal(itemList, targetLocation);
				restaurants.addAll(result);
			}

			hasMore = getJsonValueAsBoolean(responseJsonNode, "has_more");
			resultSize = result.size();

		} while (hasMore && resultSize > 0);

		if (targetServiceOption.equals(ServiceOptionType.DELIVERY)) {
			restaurants.forEach(vendorRestaurantModel -> vendorRestaurantModel.delivery = true);
		}

		if (targetServiceOption.equals(ServiceOptionType.PICKUP)) {
			restaurants.forEach(vendorRestaurantModel -> vendorRestaurantModel.pickup = true);
		}

		return restaurants;

	}

	private String getNearbyRequestBody(double lat, double lon, String serviceOption, int page) {
		return String.format("" +
				"{" +
				"\"lat\":%s," +
				"\"lng\":%s," +
				"\"fulfillment_type\":\"%s\"," +
				"\"page\":%s" +
				"}", lat, lon, serviceOption.toLowerCase(), page);
	}

    @Override
	public VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurant, LocationModel targetLocation) {

        VendorRestaurantModel restaurantModel = new VendorRestaurantModel();
		final Set<String> categories = new HashSet<>();

        String id = getJsonValueAsText(restaurant, "uuid");

		final LocationSearchParams locationSearchParams = new LocationSearchParams();
		locationSearchParams.setLatitude(String.valueOf(targetLocation.location.getLat()));
		locationSearchParams.setLongitude(String.valueOf(targetLocation.location.getLon()));

		try {
			final JsonNode restaurantDetailsNode = getRestaurantDetails(id, locationSearchParams);
			if (!BooleanUtils.isTrue(getJsonValueAsBoolean(restaurantDetailsNode, "is_restaurant"))) {
				// we don't have to index non-restaurant entities
				return null;
			}

			final JsonNode catalogNode = restaurantDetailsNode.at("/catalog");
			if (catalogNode != null) {
				final JsonNode categoriesNode = catalogNode.at("/categories");
				if (isJsonNodeArray(categoriesNode)) {
					categoriesNode.forEach(jsonNode -> {
						final String categoryName = getJsonValueAsText(jsonNode, "name");
						if (StringUtils.isNotEmpty(categoryName)) {
							categories.add(categoryName);
						}
					});
				}
			}

		} catch (Exception e) {
			log.warn("getRestaurantDetails error - {}", id, e);
		}

        restaurantModel.setIdWithVendor(getVendor().getName(), id);
        restaurantModel.name = getJsonValueAsText(restaurant, "name");
        restaurantModel.vendor = getVendor().getName();
        restaurantModel.vendorUrl = String.format("https://postmates.com/merchant/%s", id);

        JsonNode media = restaurant.at("/media");
        restaurantModel.imageUrl = getJsonValueAsText(media.at("/header_img"), "original_url");
        restaurantModel.logoUrl = getJsonValueAsText(media.at("/icon_img"), "original_url");

        Double latitude = getJsonValueAsDouble(restaurant, "lat");
        Double longitude = getJsonValueAsDouble(restaurant, "lng");

        restaurantModel.location = new GeoPoint(latitude, longitude);

        restaurantModel.address = LocationUtil.formatAddress(
                getJsonValueAsText(restaurant,"street_address_1"),
                getJsonValueAsText(restaurant, "city"),
                getJsonValueAsText(restaurant, "state"),
                getJsonValueAsText(restaurant, "zip_code")
        );

        restaurantModel.delivery = Boolean.FALSE.equals(getJsonValueAsBoolean(restaurant, "disable_delivery"));
        restaurantModel.pickup = getJsonValueAsBoolean(restaurant, "pickup_eligibility");

        /* Note: The Postmates service does not currently provide any ratings */
        restaurantModel.rating = null;

        JsonNode highlightResult = restaurant.at("/_highlightResult");
        if (highlightResult != null) {
            JsonNode categoryList = highlightResult.at("/categories");
            if (isJsonNodeArray(categoryList)) {
                for (final JsonNode category : categoryList) {
                    String categoryName = getJsonValueAsText(category, "value");
                    String matchLevel = getJsonValueAsText(category, "matchLevel");
                    if ("none".equalsIgnoreCase(matchLevel) && StringUtils.isNotBlank(categoryName)) {
                        categories.add(categoryName);
                    }
                }
            }
        }

		final JsonNode primaryPlaceCategoryNode = restaurant.at("/primary_place_category");
		if (primaryPlaceCategoryNode != null) {
			final String categoryName = getJsonValueAsText(primaryPlaceCategoryNode, "name");
			if (StringUtils.isNotBlank(categoryName)) {
				categories.add(categoryName);
			}
		}

        restaurantModel.vendorCategories = categories;
        restaurantModel.keywords = new ArrayList<>(restaurantModel.vendorCategories);

        restaurantModel.suggest = new Completion(new String[]{restaurantModel.name});

        return restaurantModel;

    }

    private String getRequestBody(Double latitude, Double longitude, String serviceOption) throws JsonProcessingException {
        if (StringUtils.isBlank(serviceOption)) {
            serviceOption = "delivery";
        }
        RequestBody requestBody = new RequestBody(
                "restaurant",
                latitude,
                longitude,
                new SectionLimits(0, applicationProperties.getDataExtraction().getPostmates().getApiPageSize(), 0),
                serviceOption.toLowerCase());
        return objectMapper.writeValueAsString(requestBody);
    }

    @Value
    private static class RequestBody {
        String q;
        double lat;
        double lng;
        SectionLimits section_limits;
        String fulfillment_type;
    }

    @Value
    private static class SectionLimits {
        int suggestions;
        int places;
        int items;
    }

	@Override
	public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams)  throws IOException {

		JsonNode restaurantDetailsNode = getRestaurantDetails(restaurantId, locationSearchParams);

		JsonNode restaurantAvailabilityNode = restaurantDetailsNode.at("/hours");
		JsonNode restaurantPickupEstimatesNode = restaurantDetailsNode.at("/ept_range_info");
		JsonNode restaurantDeliveryEstimatesNode = restaurantDetailsNode.at("/edt_range_info");

		Boolean isRestaurantOpen = "open".equalsIgnoreCase(getJsonValueAsText(restaurantAvailabilityNode, "state"));

		boolean active = Boolean.TRUE.equals(getJsonValueAsBoolean(restaurantDetailsNode, "active"));
		Boolean isRestaurantAvailableForPickup = active && isRestaurantOpen && Boolean.TRUE.equals(getJsonValueAsBoolean(restaurantDetailsNode, "is_pickup_enabled"));
		Boolean isRestaurantAvailableForDelivery = active && isRestaurantOpen;

		Boolean doesRestaurantDeliverToLocation = null; //TODO: We need to find a way to reliably fetch this

		Double restaurantPickupEstimate = getJsonValueAsDouble(restaurantPickupEstimatesNode, "upper");

		/* Note: The formatted delivery fee string is the only way to ensure we fetch primetime surge charges properly */
		Double restaurantDeliveryFee = null;
		String formattedDeliveryFee = getJsonValueAsText(restaurantDetailsNode, "delivery_fee_badge");

		if ("Free Delivery".equalsIgnoreCase(formattedDeliveryFee)) {
			restaurantDeliveryFee = 0D;

		} else if (formattedDeliveryFee != null) {
			Matcher matcher = RESTAURANT_DELIVERY_FEE_PATTERN.matcher(formattedDeliveryFee);

			if (matcher.find()) {
				restaurantDeliveryFee = Double.valueOf(matcher.group(1));
			}
		} else {
			/* Note: If the formatted delivery fee isn't available, the delivery fee will be present in another field */
			restaurantDeliveryFee = getJsonValueAsDouble(restaurantDetailsNode, "estimated_delivery_cost");
		}

		Double restaurantDeliveryEstimate = getJsonValueAsDouble(restaurantDeliveryEstimatesNode, "upper");

        final VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = new VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails();

        availabilityDetails.setOpen(isRestaurantOpen);
        availabilityDetails.setPickupAvailable(isRestaurantAvailableForPickup);
        availabilityDetails.setDeliveryAvailable(isRestaurantAvailableForDelivery);
        availabilityDetails.setDeliverableToTargetAddress(doesRestaurantDeliverToLocation);
        availabilityDetails.setPickupEstimate(restaurantPickupEstimate);
        availabilityDetails.setDeliveryFee(restaurantDeliveryFee);
        availabilityDetails.setDeliveryEstimate(restaurantDeliveryEstimate);

        return availabilityDetails;

	}

	private JsonNode getRestaurantDetails(String restaurantId, final LocationSearchParams locationSearchParams) throws IOException {
		Map<String, String> sessionCookie = setSessionLocationContext(locationSearchParams.getLatitude(), locationSearchParams.getLongitude());

		String targetApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getPostmates().getApiRestaurantDetailsEndpoint(),
				restaurantId
		);

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.putAll(DEFAULT_RESTAURANTS_API_REQUEST_HEADERS);
		requestHeaders.putAll(sessionCookie);

		ResponseEntity<String> response = executeRESTRequest(
				targetApiEndpoint,
				HttpMethod.POST,
				requestHeaders,
				getRequestBody(
						Double.valueOf(locationSearchParams.getLatitude()),
						Double.valueOf(locationSearchParams.getLongitude()),
						null)
		);

		String responseJson = getResponseBody(response);

		return objectMapper.readTree(responseJson).at("/data/place");
	}

	protected Map<String, String> setSessionLocationContext(String latitude, String longitude) {

		ResponseEntity<String> responseSessionAddress = executeRESTRequest(
				applicationProperties.getDataExtraction().getPostmates().getApiRestaurantLocationSessionEndpoint(),
				HttpMethod.POST,
				DEFAULT_RESTAURANTS_API_REQUEST_HEADERS,
				String.format(RESTAURANT_LOCATION_SESSION_BODY, latitude, longitude)
		);

		Map<String,String> sessionCookie = new HashMap<>();

		HttpHeaders headers = responseSessionAddress.getHeaders();
		if (headers.containsKey(HttpHeaders.SET_COOKIE)) {
			for (String cookie : headers.get(HttpHeaders.SET_COOKIE)) {
				if (cookie.contains("bfe_session")) {
					sessionCookie.put(HttpHeaders.COOKIE, cookie);
					break;
				}
			}
		}

		return sessionCookie;

	}

}
