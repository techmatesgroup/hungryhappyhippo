package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.databind.JsonNode;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.ServiceOptionType;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.annotation.Resource;

@Slf4j
public class DeliverycomRestaurantExtractor extends AbstractRestaurantExtractor implements VendorRestaurantExtractor {

	@Resource
	private ApplicationProperties applicationProperties;

	@Override
	public Vendor getVendor() {
		return Vendor.DELIVERY_COM;
	}

	@Override
	public List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations) {

		List<VendorRestaurantModel> resultList = new ArrayList<>();

		for (LocationModel targetLocation : targetLocations) {
			try {
				for (ServiceOptionType targetServiceOption : TARGET_SERVICE_OPTIONS) {
					String targetApiEndpoint = String.format(
							applicationProperties.getDataExtraction().getDeliverycom().getApiRestaurantSearchEndpoint(),
							targetServiceOption.toString().toLowerCase(),
							targetLocation.location.getLat(),
							targetLocation.location.getLon(),
							applicationProperties.getDataExtraction().getDeliverycom().getApiClientId()
					);

					ResponseEntity<String> response = executeRESTRequest(
							targetApiEndpoint
					);

					String responseJson = getResponseBody(response);

					JsonNode restaurantList = objectMapper.readTree(responseJson).at("/merchants");

					List<VendorRestaurantModel> currentFetchResult = fetchRestaurantsInternal(restaurantList, targetLocation);
					resultList.addAll(filterDuplicates(resultList, currentFetchResult));
				}
			} catch (Exception e) {
				log.warn("An error occurred fetching restaurants from the Delivery.com API for location: {}: {}", targetLocation, e.getMessage(), e);
			}
		}

		return resultList;
	}

	@Override
	public VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurant, LocationModel targetLocation) {

		VendorRestaurantModel restaurantModel = new VendorRestaurantModel();

		JsonNode locationNode = restaurant.at("/location");
		JsonNode summaryNode = restaurant.at("/summary");
		JsonNode orderingNode = restaurant.at("/ordering");
		JsonNode availabilityNode = orderingNode.at("/availability");
		JsonNode urlContainer = summaryNode.at("/url");

		/* Note: This API can return results way outside the target location range, so this check ensure we only fetch restaurants within the target location's radius */
		Double distance = getJsonValueAsDouble(locationNode, "distance");
		if (targetLocation.radius.compareTo(distance) < 0) {
			return null;
		}

		String id = getJsonValueAsText(restaurant, "id");

		restaurantModel.setIdWithVendor(getVendor().getName(), id);
		restaurantModel.name = getJsonValueAsText(summaryNode, "name");
		restaurantModel.vendor = getVendor().getName();
		restaurantModel.vendorUrl = getJsonValueAsText(urlContainer, "complete");

		restaurantModel.imageUrl = getJsonValueAsText(summaryNode, "merchant_logo");
		restaurantModel.logoUrl = restaurantModel.imageUrl;

		Double latitude = getJsonValueAsDouble(locationNode, "latitude");
		Double longitude = getJsonValueAsDouble(locationNode, "longitude");

		restaurantModel.location = new GeoPoint(latitude, longitude);

		restaurantModel.address = LocationUtil.formatAddress(
				getJsonValueAsText(locationNode, "street"),
				getJsonValueAsText(locationNode, "city"),
				getJsonValueAsText(locationNode, "state"),
				getJsonValueAsText(locationNode, "zip")
		);

		restaurantModel.delivery = getJsonValueAsBoolean(availabilityNode, "delivery_supported");
		restaurantModel.pickup = getJsonValueAsBoolean(availabilityNode, "pickup_supported");

		restaurantModel.rating = getJsonValueAsDouble(summaryNode, "star_ratings");

		final Set<String> categories = new HashSet<>();
		final List<JsonNode> categoriesNodeList = new ArrayList<>();

		final JsonNode categoryList = summaryNode.at("/cuisines");
		if (isJsonNodeArray(categoryList)) {
			categoryList.forEach(categoriesNodeList::add);
		}

		final JsonNode allCategoryList = summaryNode.at("/all_cuisines");
		if (isJsonNodeArray(allCategoryList)) {
			allCategoryList.forEach(categoriesNodeList::add);
		}
		categoriesNodeList.forEach(category -> {
			String categoryName = category.asText();
			if (StringUtils.isNotBlank(categoryName)) {
				categories.add(categoryName);
			}
		});

		restaurantModel.vendorCategories = categories;
		restaurantModel.keywords = new ArrayList<>(restaurantModel.vendorCategories);

		restaurantModel.suggest = new Completion(new String[]{restaurantModel.name});

		return restaurantModel;

	}

	@Override
	public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams) throws IOException {

		String targetApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getDeliverycom().getApiRestaurantDetailsEndpoint(),
				restaurantId,
				locationSearchParams.getAddress(),
				applicationProperties.getDataExtraction().getDeliverycom().getApiClientId()
		);

		ResponseEntity<String> response = executeRESTRequest(
			targetApiEndpoint
		);

		String responseJson = getResponseBody(response);

		JsonNode restaurantDetailsNode = objectMapper.readTree(responseJson).at("/merchant");
		JsonNode restaurantOrderingNode = restaurantDetailsNode.at("/ordering");
		JsonNode restaurantAvailabilityNode = restaurantOrderingNode.at("/availability");

		Boolean isRestaurantOpen = null;
		Boolean isRestaurantAvailableForPickup = null;
		Boolean isRestaurantAvailableForDelivery = null;
		Boolean doesRestaurantDeliverToLocation = null;
		Double restaurantPickupEstimate = null;
		Double restaurantDeliveryFee = null;
		Double restaurantDeliveryEstimate = null;

		Boolean active = getJsonValueAsBoolean(restaurantAvailabilityNode, "active");

		if (!Boolean.TRUE.equals(active)) {
			isRestaurantOpen = false;
			isRestaurantAvailableForPickup = false;
			isRestaurantAvailableForDelivery = false;
		}
		else {
			isRestaurantAvailableForPickup = getJsonValueAsBoolean(restaurantAvailabilityNode, "pickup");
			isRestaurantAvailableForDelivery = getJsonValueAsBoolean(restaurantAvailabilityNode, "delivery");
			isRestaurantOpen = isRestaurantAvailableForPickup || isRestaurantAvailableForDelivery;
			doesRestaurantDeliverToLocation = getJsonValueAsBoolean(restaurantDetailsNode, "deliverable");
			restaurantPickupEstimate = getJsonValueAsDouble(restaurantAvailabilityNode, "pickup_estimate");
			restaurantDeliveryFee = getJsonValueAsDouble(restaurantOrderingNode, "delivery_charge");
			restaurantDeliveryEstimate = getJsonValueAsDouble(restaurantAvailabilityNode, "delivery_estimate");
		}
		Double pickupOrderMinimum = null;
		Double deliveryOrderMinimum = null;


		final JsonNode orderMinimumNode = restaurantOrderingNode.at("/order_minimum");
		if (orderMinimumNode != null) {
			pickupOrderMinimum = getJsonValueAsDouble(orderMinimumNode, "pickup");
			final JsonNode deliveryNode = orderMinimumNode.at("/delivery");
			if (deliveryNode != null) {
				deliveryOrderMinimum = getJsonValueAsDouble(deliveryNode, "lowest");
			}
		}


		final VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = new VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails();
		availabilityDetails.setOpen(isRestaurantOpen);
		availabilityDetails.setPickupAvailable(isRestaurantAvailableForPickup);
		availabilityDetails.setDeliveryAvailable(isRestaurantAvailableForDelivery);
		availabilityDetails.setDeliverableToTargetAddress(doesRestaurantDeliverToLocation);
		availabilityDetails.setPickupEstimate(restaurantPickupEstimate);
		availabilityDetails.setDeliveryFee(restaurantDeliveryFee);
		availabilityDetails.setDeliveryEstimate(restaurantDeliveryEstimate);
		availabilityDetails.setPickupOrderMinimum(pickupOrderMinimum);
		availabilityDetails.setDeliveryOrderMinimum(deliveryOrderMinimum);

		return availabilityDetails;

	}

}
