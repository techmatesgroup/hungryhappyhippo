package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant;

import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;

public interface RestaurantRatingExtractor {

	Double fetchRestaurantRating(RestaurantModel restaurant);

}
