package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online;

import java.io.IOException;

/**
 * Extracts menu data from external API
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/11/2019
 */
public interface MenuItemOnlineExtractorService<DTO> {

    DTO fetchMenuItems(final String vendorRestaurantId) throws IOException;

}
