package io.tmg.hungryhappyhippo.poc.vendor.data.extraction;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;

@Slf4j
public abstract class AbstractRestDataExtractor {

	private static final RestTemplate restTemplate = new RestTemplate(
			Collections.singletonList(new StringHttpMessageConverter(Charset.forName("UTF-8"))));
	protected static final ObjectMapper objectMapper = new ObjectMapper();

	private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

	public static ResponseEntity<String> executeRESTRequest(String url, HttpMethod method, Map<String, String> requestHeaders, String requestBody) {
		ResponseEntity<String> result;

		try {

			HttpHeaders headers = new HttpHeaders();

			headers.set("User-Agent", DEFAULT_USER_AGENT);

			if (requestHeaders != null) {
				headers.setAll(requestHeaders);
			}

			HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);

			log.trace("Calling REST endpoint: url: {}, method: {}, headers: {}, requestBody: {}", url, method.name(), requestEntity.getHeaders(), requestBody);
			result = restTemplate.exchange(url, method, requestEntity, String.class);

		} catch (RestClientException e) {
			if (e instanceof HttpStatusCodeException){
				String errorResponse = ((HttpStatusCodeException)e).getResponseBodyAsString();
				log.warn("An HTTP error occurred interacting with REST API: {} : {}", url, errorResponse, e);
			}
			else {
				log.warn("A special RestClientException occurred interacting with REST API: {} : {}", url, e.getMessage(), e);
			}

			throw e;
		} catch (Exception e) {
			log.warn("An uncaught error occurred interacting with REST API: {} : {}", url, e.getMessage(), e);
			throw e;
		}

		return result;

	}

	public static ResponseEntity<String> executeRESTRequest(String url) {
		return executeRESTRequest(url, HttpMethod.GET);
	}

	public static ResponseEntity<String> executeRESTRequest(String url, HttpMethod method) {
		return executeRESTRequest(url, method, Collections.emptyMap());
	}

	public static ResponseEntity<String> executeRESTRequest(String url, HttpMethod method, Map<String, String> requestHeaders) {
		return executeRESTRequest(url, method, requestHeaders, null);
	}

	public static String getResponseBody(ResponseEntity<String> result) {
		return result.getBody();
	}

	public static String getResponseHeaders(ResponseEntity<String> result) {
		return result.getHeaders().toString();
	}

	protected String getJsonValueAsText(JsonNode node, String key) {

		JsonNode item = node.get(key);

		if (item == null || !item.isValueNode()) {
			return null;
		}

		return item.asText();

	}

	protected Boolean getJsonValueAsBoolean(JsonNode node, String key) {

		JsonNode item = node.get(key);

		if (item == null || !item.isBoolean()) {
			return null;
		}

		return item.asBoolean(Boolean.FALSE);

	}

	protected Double getJsonValueAsDouble(JsonNode node, String key) {

		JsonNode item = node.get(key);

		if (item == null || !item.isNumber()) {
			return null;
		}

		return item.asDouble(-1);

	}

	protected Integer getJsonValueAsInteger(JsonNode node, String key) {

		JsonNode item = node.get(key);

		if (item == null || !item.isNumber()) {
			return null;
		}

		return item.asInt(0);

	}

	protected static boolean isJsonNodeArray(final JsonNode jsonNode) {
		return jsonNode != null && jsonNode.isArray();
	}

}
