package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/11/2019
 */
public class AbstractMenuItemOnlineExtractorService {

    protected Logger log = LoggerFactory.getLogger(getClass());

    private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

    protected ObjectMapper objectMapper = new ObjectMapper();

    @Resource
    protected RestTemplate menuRestTemplate;

    @Resource
    protected ApplicationProperties applicationProperties;

    protected <DTO> ResponseEntity<DTO> executeRESTRequest(String url, HttpMethod method, HttpHeaders headers, String requestBody, Class<DTO> responseClass) {

        ResponseEntity<DTO> result;
        headers = headers != null ? headers : new Headers();

        try {
            headers.set(HttpHeaders.USER_AGENT, DEFAULT_USER_AGENT);

            final HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);

            log.trace("Calling REST endpoint: url: {}, method: {}, headers: {}, requestBody: {}", url, method.name(), requestEntity.getHeaders(), requestBody);
            result = menuRestTemplate.exchange(url, method, requestEntity, responseClass);
            log.trace("Result - {}", result);

        } catch (HttpStatusCodeException e) {
            String errorResponse = e.getResponseBodyAsString();
            log.warn("An HTTP error occurred interacting with REST API: {} : {}", url, errorResponse, e);
            throw e;

        } catch (RestClientException e) {
            log.warn("A special RestClientException occurred interacting with REST API: {} : {}", url, e.getMessage(), e);
            throw e;

        } catch (Exception e) {
            log.warn("An uncaught error occurred interacting with REST API: {} : {}", url, e.getMessage(), e);
            throw e;

        }

        return result;

    }

    protected <DTO> ResponseEntity<DTO> executeRESTRequest(String url, HttpMethod method, HttpHeaders headers, Class<DTO> responseClass) {
        return executeRESTRequest(url, method, headers, null, responseClass);
    }

    protected <DTO> ResponseEntity<DTO> executeRESTRequest(String url, HttpMethod method, Class<DTO> responseClass) {
        return executeRESTRequest(url, method, null, null, responseClass);
    }

    protected static class Headers extends HttpHeaders {

        private static final long serialVersionUID = 1L;

        public static Headers with(String attributeName, String attributeValue) {
            final Headers map = new Headers();
            map.set(attributeName, attributeValue);
            return map;
        }

        public static Headers with(Map<String, String> headersMap) {
            final Headers map = new Headers();
            for (Entry<String, String> entry : headersMap.entrySet()) {
                map.set(entry.getKey(), entry.getValue());
            }
            return map;
        }

        public Headers addValue(String attributeName, String attributeValue) {
            this.set(attributeName, attributeValue);
            return this;
        }

    }

    protected String getJsonValueAsText(JsonNode node, String key) {

        JsonNode item = node.get(key);
        return item != null && item.isValueNode() ? item.asText() : null;

    }

}
