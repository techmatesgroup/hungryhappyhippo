package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import javax.annotation.Resource;

@Slf4j
public class UberEatsRestaurantExtractor extends AbstractRestaurantExtractor implements VendorRestaurantExtractor {

	@Resource
    private ApplicationProperties applicationProperties;

    private static final Map<String, String> DEFAULT_RESTAURANTS_API_REQUEST_HEADERS;
    private static final String RESTAURANTS_API_AUTH_RESPONSE_CSRF_TOKEN_KEY = "x-csrf-token";

    static {

        DEFAULT_RESTAURANTS_API_REQUEST_HEADERS = new HashMap<>();
        DEFAULT_RESTAURANTS_API_REQUEST_HEADERS.put("x-requested-with", "XMLHttpRequest");
        DEFAULT_RESTAURANTS_API_REQUEST_HEADERS.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    }

    @Override
    public Vendor getVendor() {
        return Vendor.UBEREATS;
    }

    @Override
    public List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations) {

        List<VendorRestaurantModel> restaurants = new ArrayList<>();

        Map<String, String> authenticationToken;

        try {
            authenticationToken = fetchAuthenticationToken();
        } catch (Exception e) {
            log.warn("An error occurred fetching authentication token from the UberEats API: {}", e.getMessage(), e);
            return restaurants;
        }

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.putAll(authenticationToken);
        requestHeaders.putAll(DEFAULT_RESTAURANTS_API_REQUEST_HEADERS);

        for (LocationModel targetLocation : targetLocations) {
            try {
                ResponseEntity<String> response = executeRESTRequest(
                        applicationProperties.getDataExtraction().getUbereats().getApiRestaurantSearchEndpoint(),
                        HttpMethod.POST,
                        requestHeaders,
                        getRequestBody(targetLocation.location)
                );

                String responseJson = getResponseBody(response);

                JsonNode restaurantList = objectMapper.readTree(responseJson).at("/feed/storesMap");

                List<VendorRestaurantModel> result = fetchRestaurantsInternal(restaurantList, targetLocation);
                restaurants.addAll(result);
            } catch (Exception e) {
                log.warn("An error occurred fetching restaurants from the UberEats API for location: {}: {}", targetLocation, e.getMessage(), e);
            }
        }

        return restaurants;
    }

    @Override
	public VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurant, LocationModel targetLocation) {

        VendorRestaurantModel restaurantModel = new VendorRestaurantModel();

        JsonNode rawRatingStats = restaurant.at("/rawRatingStats");
        JsonNode location = restaurant.at("/location");
        JsonNode addressNode = location.at("/address");
        JsonNode categoryList = restaurant.at("/categories");
		JsonNode tagList = restaurant.at("/tags");

        String id = getJsonValueAsText(restaurant, "uuid");

        restaurantModel.setIdWithVendor(getVendor().getName(), id);
        restaurantModel.name = getJsonValueAsText(restaurant, "title");
        restaurantModel.vendor = getVendor().getName();
        restaurantModel.vendorUrl = String.format("https://www.ubereats.com/en-US/stores/%s", id);

        restaurantModel.imageUrl = getJsonValueAsText(restaurant, "heroImageUrl");
        restaurantModel.logoUrl = restaurantModel.imageUrl;

        Double latitude = getJsonValueAsDouble(location, "latitude");
        Double longitude = getJsonValueAsDouble(location, "longitude");
        restaurantModel.location = new GeoPoint(latitude, longitude);

        restaurantModel.address = getJsonValueAsText(addressNode, "formattedAddress");

        /* Note: The whole point of the UberEats service is to have Uber drivers deliver food, plus pickup is not currently an option, so it's fine to hardcode here for now */
        restaurantModel.delivery = true;
        restaurantModel.pickup = false;

        restaurantModel.rating = getJsonValueAsDouble(rawRatingStats, "storeRatingScore");

		final Set<String> categories = new HashSet<>();
		final List<JsonNode> categoriesNodeList = new ArrayList<>();

		if (isJsonNodeArray(categoryList)) {
			categoryList.forEach(categoriesNodeList::add);
        }

		if (isJsonNodeArray(tagList)) {
			tagList.forEach(categoriesNodeList::add);
		}

		categoriesNodeList.forEach(category -> {
			final String categoryName = getJsonValueAsText(category, "name");
			if (StringUtils.isNotEmpty(categoryName)) {
				categories.add(categoryName);
			}
		});

        restaurantModel.vendorCategories = categories;
        restaurantModel.keywords = new ArrayList<>(restaurantModel.vendorCategories);

        restaurantModel.suggest = new Completion(new String[]{restaurantModel.name});

        return restaurantModel;

    }

    private Map<String, String> fetchAuthenticationToken() {

        HttpHeaders headers;
        try {
            ResponseEntity<String> response = executeRESTRequest(
                applicationProperties.getDataExtraction().getUbereats().getApiAuthEndpoint()
            );
            headers = response.getHeaders();
        } catch (HttpClientErrorException e) {
            if (HttpStatus.I_AM_A_TEAPOT.equals(e.getStatusCode())) {
                headers = e.getResponseHeaders();
            } else {
                throw e;
            }
        }

        Map<String, String> authHeaders = new HashMap<>();
        if (headers != null) {
            if (headers.containsKey(RESTAURANTS_API_AUTH_RESPONSE_CSRF_TOKEN_KEY)) {
                authHeaders.put(RESTAURANTS_API_AUTH_RESPONSE_CSRF_TOKEN_KEY, headers.getFirst(RESTAURANTS_API_AUTH_RESPONSE_CSRF_TOKEN_KEY));
            } else {
                throw new RuntimeException("Failed to find authentication csrf token in HTTP response");
            }

            if (headers.containsKey(HttpHeaders.SET_COOKIE)) {
                for (String cookie : headers.getOrDefault(HttpHeaders.SET_COOKIE, Collections.emptyList())) {
                    if (cookie.startsWith("web-eats")) {
                        authHeaders.put(HttpHeaders.COOKIE, cookie);
                    }
                }
            }
            if (authHeaders.get(HttpHeaders.COOKIE) == null) {
                throw new RuntimeException("Failed to find authentication cookie in HTTP response");
            }
        }

        return authHeaders;

    }

    private String getRequestBody(GeoPoint location) throws JsonProcessingException {
        RequestBody requestBody = new RequestBody(
                new PageInfo(0, applicationProperties.getDataExtraction().getUbereats().getApiPageSize()),
                new TargetLocation(location.getLat(), location.getLon())
        );
        return objectMapper.writeValueAsString(requestBody);
    }

    @Getter
    private static class RequestBody {
        PageInfo pageInfo;
        TargetLocation targetLocation;
        String feed;

        RequestBody(PageInfo pageInfo, TargetLocation targetLocation) {
            this.pageInfo = pageInfo;
            this.targetLocation = targetLocation;
        }
    }

    @Value
    private static class PageInfo {
        int offset;
        int pageSize;
    }

    @Value
    private static class TargetLocation {
        double latitude;
        double longitude;
    }

	@Override
	public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams)  throws IOException {

		Map<String, String> authenticationToken = fetchAuthenticationToken();

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.putAll(authenticationToken);
		requestHeaders.put("x-requested-with", "XMLHttpRequest");
		requestHeaders.put("x-uber-target-location-latitude", String.valueOf(locationSearchParams.getLatitude()));
		requestHeaders.put("x-uber-target-location-longitude", String.valueOf(locationSearchParams.getLongitude()));

		String targetApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getUbereats().getApiRestaurantDetailsEndpoint(),
				restaurantId
		);

		ResponseEntity<String> response = executeRESTRequest(
				targetApiEndpoint,
				HttpMethod.GET,
				requestHeaders
		);

		String responseJson = getResponseBody(response);

		JsonNode restaurantDetailsNode = objectMapper.readTree(responseJson).at("/store");
		JsonNode restaurantFeesNode = restaurantDetailsNode.at("/fareInfo");
		JsonNode restaurantEstimatesNode = restaurantDetailsNode.at("/etaRange");

		Boolean isRestaurantOpen = getJsonValueAsBoolean(restaurantDetailsNode, "isOrderable");

		Boolean isRestaurantAvailableForPickup = false; /* Note: The UberEats service does not currently support pickup */
		Boolean isRestaurantAvailableForDelivery = isRestaurantOpen; /* Note: The UberEats service will always allow delivery as long as the merchant is open */

		Boolean doesRestaurantDeliverToLocation = null; //TODO: We need to find a way to reliably fetch this

		Double restaurantPickupEstimate = null; /* Note: The UberEats service does not currently support pickup */
		Double restaurantDeliveryEstimate = getJsonValueAsDouble(restaurantEstimatesNode, "raw");

		Double restaurantDeliveryFee = getJsonValueAsDouble(restaurantFeesNode, "serviceFee");

		/* Note: At special times when Uber drivers are not readily available, an extra fee becomes available and added to the final delivery fee */
		Double additiveFee = getJsonValueAsDouble(restaurantFeesNode, "additive");
		if (additiveFee != null && restaurantDeliveryFee != null) {
			restaurantDeliveryFee += additiveFee;
		}

        final VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = new VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails();

        availabilityDetails.setOpen(isRestaurantOpen);
        availabilityDetails.setPickupAvailable(isRestaurantAvailableForPickup);
        availabilityDetails.setDeliveryAvailable(isRestaurantAvailableForDelivery);
        availabilityDetails.setDeliverableToTargetAddress(doesRestaurantDeliverToLocation);
        availabilityDetails.setPickupEstimate(restaurantPickupEstimate);
        availabilityDetails.setDeliveryFee(restaurantDeliveryFee);
        availabilityDetails.setDeliveryEstimate(restaurantDeliveryEstimate);

        return availabilityDetails;

	}

}
