package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl;

import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.postmates.PostmatesMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.AbstractMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.MenuItemOnlineExtractorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Postmates implementation of {@link MenuItemOnlineExtractorService}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/11/2019
 */
@Service
public class PostmatesMenuItemOnlineExtractorService extends AbstractMenuItemOnlineExtractorService implements MenuItemOnlineExtractorService<PostmatesMenuResponseDTO> {

    private static final String REQUEST_BODY_PATTERN = "{\"place_uuid\":\"%s\",\"q\":\"*\"}";

    @Override
    public PostmatesMenuResponseDTO fetchMenuItems(final String vendorRestaurantId) {

        log.trace("Start fetching menus for restaurant - {}", vendorRestaurantId);

        final ResponseEntity<PostmatesMenuResponseDTO> responseEntity =
                executeRESTRequest(applicationProperties.getDataExtraction().getPostmates().getApiMenuEndpoint(),
                HttpMethod.POST,
                Headers.with(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE),
                String.format(REQUEST_BODY_PATTERN, vendorRestaurantId),
                PostmatesMenuResponseDTO.class
        );

        return responseEntity.getBody();

    }

}
