package io.tmg.hungryhappyhippo.poc.vendor.data.extraction;

import lombok.extern.slf4j.Slf4j;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Collections;
import java.util.Map;

@Slf4j
public abstract class AbstractHtmlDataExtractor {

	public static Document executeHTMLRequest(String url, Connection.Method method, Map<String, String> requestHeaders, String requestBody) {
		Document result;

		try {
			log.trace("Calling HTML endpoint: url: {}, method: {}, headers: {}, requestBody: {}", url, method.name(), requestHeaders, requestBody);
			result = Jsoup.connect(url).method(method).headers(requestHeaders).requestBody(requestBody).execute().parse();
		} catch (Exception e) {
			log.warn("An uncaught error occurred interacting with HTML API: {} : {}", url, e.getMessage(), e);
			throw new RuntimeException(e);
		}

		return result;

	}

	public static Document executeHTMLRequest(String url) {
		return executeHTMLRequest(url, Connection.Method.GET);
	}

	public static Document executeHTMLRequest(String url, Connection.Method method) {
		return executeHTMLRequest(url, method, Collections.emptyMap());
	}

	public static Document executeHTMLRequest(String url, Connection.Method method, Map<String, String> requestHeaders) {
		return executeHTMLRequest(url, method, requestHeaders, null);
	}

}
