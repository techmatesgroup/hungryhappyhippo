package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.databind.JsonNode;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Slf4j
public class DoorDashRestaurantExtractor extends AbstractRestaurantExtractor implements VendorRestaurantExtractor {

	@Resource
	private ApplicationProperties applicationProperties;

	private static final Map<String, String> DEFAULT_RESTAURANTS_API_REQUEST_HEADERS;
	private static final String RESTAURANT_LOCATION_SESSION_BODY = "{\"lat\":%s,\"lng\":%s}";

	static {

		DEFAULT_RESTAURANTS_API_REQUEST_HEADERS = new HashMap<>();
		DEFAULT_RESTAURANTS_API_REQUEST_HEADERS.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

	}

	@Override
	public Vendor getVendor() {
		return Vendor.DOORDASH;
	}

	@Override
	public List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations) {

		List<VendorRestaurantModel> restaurants = new ArrayList<>();

		for (LocationModel targetLocation : targetLocations) {
			try {
				String targetApiEndpoint = String.format(
						applicationProperties.getDataExtraction().getDoordash().getApiRestaurantSearchEndpoint(),
						targetLocation.location.getLat(),
						targetLocation.location.getLon(),
						applicationProperties.getDataExtraction().getDoordash().getApiPageSize()
				);

				ResponseEntity<String> response = executeRESTRequest(
						targetApiEndpoint
				);

				String responseJson = getResponseBody(response);

				JsonNode restaurantList = objectMapper.readTree(responseJson).at("/stores");

				List<VendorRestaurantModel> result = fetchRestaurantsInternal(restaurantList, targetLocation);
				restaurants.addAll(result);
			} catch (Exception e) {
				log.warn("An error occurred fetching restaurants from the DoorDash API for location: {}: {}", targetLocation, e.getMessage(), e);
			}
		}

		return restaurants;
	}

	@Override
	public VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurantNode, LocationModel targetLocation) throws IOException {

		VendorRestaurantModel restaurantModel = new VendorRestaurantModel();

		String id = getJsonValueAsText(restaurantNode, "id");

		restaurantModel.setIdWithVendor(getVendor().getName(), id);

		String targetApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getDoordash().getApiRestaurantDetailsEndpoint(),
				id
		);

		ResponseEntity<String> response = executeRESTRequest(targetApiEndpoint);
		final JsonNode detailRestaurantNode = objectMapper.readTree(response.getBody());

		final JsonNode businessNode = detailRestaurantNode.at("/business");
		restaurantModel.name = getJsonValueAsText(businessNode, "name");
		restaurantModel.vendor = getVendor().getName();
		restaurantModel.vendorUrl = String.format("https://www.doordash.com%s", getJsonValueAsText(restaurantNode, "url"));

		restaurantModel.imageUrl = getJsonValueAsText(detailRestaurantNode, "cover_img_url");
		restaurantModel.logoUrl = restaurantModel.imageUrl;

		final JsonNode addressNode = detailRestaurantNode.at("/address");
		Double latitude = getJsonValueAsDouble(addressNode, "lat");
		Double longitude = getJsonValueAsDouble(addressNode, "lng");

		restaurantModel.location = new GeoPoint(latitude, longitude);

		restaurantModel.address = LocationUtil.formatAddress(
				getJsonValueAsText(addressNode, "shortname"),
				getJsonValueAsText(addressNode, "city"),
				getJsonValueAsText(addressNode, "state"),
				getJsonValueAsText(addressNode, "zip_code")
		);

		restaurantModel.delivery = BooleanUtils.isTrue(getJsonValueAsBoolean(detailRestaurantNode, "offers_delivery"));
		restaurantModel.pickup = BooleanUtils.isTrue(getJsonValueAsBoolean(detailRestaurantNode, "offers_pickup"));

		/* Note: Ratings are only valid from the non-detail search response node */
		restaurantModel.rating = getJsonValueAsDouble(restaurantNode, "average_rating");

		Collection<String> categories = new ArrayList<>();
		JsonNode categoryList = detailRestaurantNode.at("/tags");
		if (categoryList.isArray()) {
			for (final JsonNode category : categoryList) {
				String categoryName = category.asText();
				if (StringUtils.isNotBlank(categoryName)) {
					categories.add(categoryName);
				}
			}
		}
		restaurantModel.vendorCategories = categories;
		restaurantModel.keywords = new ArrayList<>(restaurantModel.vendorCategories);

		restaurantModel.suggest = new Completion(new String[]{restaurantModel.name});

		return restaurantModel;

	}

	@Override
	public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams)  throws IOException {

		String targetRestaurantDetailsApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getDoordash().getApiRestaurantDetailsEndpoint(),
				restaurantId
		);

		String targetAddressDeliverableApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getDoordash().getApiRestaurantAddressDeliverableEndpoint(),
				restaurantId,
				locationSearchParams.getLatitude(),
				locationSearchParams.getLongitude()
		);

		String targetEstimatesApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getDoordash().getApiRestaurantStatusEstimatesEndpoint(),
				restaurantId,
				locationSearchParams.getAddress()
		);

		Map<String, String> sessionCookie = setSessionLocationContext(locationSearchParams.getLatitude(), locationSearchParams.getLongitude());

		ResponseEntity<String> responseRestaurantDetails = executeRESTRequest(
				targetRestaurantDetailsApiEndpoint,
				HttpMethod.GET,
				sessionCookie
		);

		String responseRestaurantDetailsJson = getResponseBody(responseRestaurantDetails);

		ResponseEntity<String> responseAddressDeliverable = executeRESTRequest(
			targetAddressDeliverableApiEndpoint
		);

		String responseAddressDeliverableJson = getResponseBody(responseAddressDeliverable);

		ResponseEntity<String> responseServiceEstimates = executeRESTRequest(
				targetEstimatesApiEndpoint,
				HttpMethod.GET,
				sessionCookie
		);

		String responseServiceEstimatesJson = getResponseBody(responseServiceEstimates);

		JsonNode restaurantDetailsNode = objectMapper.readTree(responseRestaurantDetailsJson);
		JsonNode addressDeliverableNode = objectMapper.readTree(responseAddressDeliverableJson);
		JsonNode statusDetailsNode = objectMapper.readTree(responseServiceEstimatesJson).at("/status");

		JsonNode pickupEstimateRangeNode = statusDetailsNode.at("/asap_pickup_minutes_range");
		JsonNode deliveryEstimateRangeNode = statusDetailsNode.at("/asap_minutes_range");

		Boolean isRestaurantOpen = "open".equalsIgnoreCase(getJsonValueAsText(restaurantDetailsNode, "status_type"));

		//TODO: We need to find a way to reliably fetch these "available now" flags
		Boolean isRestaurantAvailableForPickup = getJsonValueAsBoolean(statusDetailsNode, "asap_available");
		Boolean isRestaurantAvailableForDelivery = isRestaurantAvailableForPickup;

		Boolean doesRestaurantDeliverToLocation = getJsonValueAsBoolean(addressDeliverableNode, "deliver_to_address");

		Double restaurantPickupEstimate = null;
		if (pickupEstimateRangeNode.isArray()) {
			for (final JsonNode pickupEstimate : pickupEstimateRangeNode) {
				restaurantPickupEstimate = pickupEstimate.asDouble();
				break;
			}
		}

		Double restaurantDeliveryFee = getJsonValueAsDouble(restaurantDetailsNode, "delivery_fee");
		if (restaurantDeliveryFee != null) {
			restaurantDeliveryFee /= 100;
		}

		Double restaurantDeliveryEstimate = null;
		if (deliveryEstimateRangeNode.isArray()) {
			for (final JsonNode deliveryEstimate : deliveryEstimateRangeNode) {
				restaurantDeliveryEstimate = deliveryEstimate.asDouble();
				break;
			}
		}

		final VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = new VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails();

		availabilityDetails.setOpen(isRestaurantOpen);
		availabilityDetails.setPickupAvailable(isRestaurantAvailableForPickup);
		availabilityDetails.setDeliveryAvailable(isRestaurantAvailableForDelivery);
		availabilityDetails.setDeliverableToTargetAddress(doesRestaurantDeliverToLocation);
		availabilityDetails.setPickupEstimate(restaurantPickupEstimate);
		availabilityDetails.setDeliveryFee(restaurantDeliveryFee);
		availabilityDetails.setDeliveryEstimate(restaurantDeliveryEstimate);

		return availabilityDetails;

	}

	protected Map<String, String> setSessionLocationContext(String latitude, String longitude) {

		ResponseEntity<String> responseSessionLocation = executeRESTRequest(
				applicationProperties.getDataExtraction().getDoordash().getApiRestaurantLocationSessionEndpoint(),
				HttpMethod.POST,
				DEFAULT_RESTAURANTS_API_REQUEST_HEADERS,
				String.format(RESTAURANT_LOCATION_SESSION_BODY, latitude, longitude)
		);

		Map<String,String> sessionCookie = new HashMap<>();

		HttpHeaders headers = responseSessionLocation.getHeaders();
		if (headers.containsKey(HttpHeaders.SET_COOKIE)) {
			for (String cookie : headers.get(HttpHeaders.SET_COOKIE)) {
				if (cookie.contains("dd_guest_id")) {
					sessionCookie.put(HttpHeaders.COOKIE, cookie);
					break;
				}
			}
		}

		return sessionCookie;

	}

}
