package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl;

import io.tmg.hungryhappyhippo.poc.models.dto.extraction.menu.deliverycom.DeliveryComMenuResponseDTO;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.AbstractMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.MenuItemOnlineExtractorService;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * DeliveryCom implementation of {@link MenuItemOnlineExtractorService}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/12/2019
 */
@Service
public class DeliveryComMenuItemOnlineExtractorService extends AbstractMenuItemOnlineExtractorService implements MenuItemOnlineExtractorService<DeliveryComMenuResponseDTO> {

    @Override
    public DeliveryComMenuResponseDTO fetchMenuItems(final String vendorRestaurantId) {

        log.trace("Start fetching menus for restaurant - {}", vendorRestaurantId);

        final String targetApiMenuEndpoint = String.format(
                applicationProperties.getDataExtraction().getDeliverycom().getApiMenuEndpoint(),
                vendorRestaurantId,
                applicationProperties.getDataExtraction().getDeliverycom().getApiClientId()
        );

        final ResponseEntity<DeliveryComMenuResponseDTO> response = executeRESTRequest(
                targetApiMenuEndpoint
                , HttpMethod.GET
                , DeliveryComMenuResponseDTO.class);

        return response.getBody();

    }

}
