package io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl;

import com.fasterxml.jackson.databind.JsonNode;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.ServiceOptionType;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;

import org.apache.http.HttpHeaders;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Slf4j
public class GrubhubRestaurantExtractor extends AbstractRestaurantExtractor implements VendorRestaurantExtractor {

	@Resource
	private ApplicationProperties applicationProperties;

	private static final Map<String, String> RESTAURANTS_API_AUTH_REQUEST_HEADERS;
	private static final String RESTAURANTS_API_AUTH_REQUEST_BODY = "{\"brand\":\"GRUBHUB\",\"client_id\":\"beta_UmWlpstzQSFmocLy3h1UieYcVST\",\"scope\":\"anonymous\"}";

	/* Note: The Grubhub API does not support page sizes greater than 100 */
	private static final int MAX_PAGE_SIZE = 100;

	static {

		RESTAURANTS_API_AUTH_REQUEST_HEADERS = new HashMap<>();
		RESTAURANTS_API_AUTH_REQUEST_HEADERS.put(HttpHeaders.AUTHORIZATION, "Bearer");
		RESTAURANTS_API_AUTH_REQUEST_HEADERS.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

	}

    @Override
	public Vendor getVendor() {
		return Vendor.GRUBHUB;
	}

    @Override
	public List<VendorRestaurantModel> fetchRestaurants(Iterable<LocationModel> targetLocations) {

		List<VendorRestaurantModel> resultList = new ArrayList<>();

		String authenticationToken;

		try {
			authenticationToken = fetchAuthenticationToken();
		} catch (Exception e) {
			log.warn("An error occurred fetching authentication token from the Grubhub API: {}", e.getMessage(), e);
			return Collections.emptyList();
		}

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", authenticationToken));

		int pageSize = applicationProperties.getDataExtraction().getGrubhub().getApiPageSize();
		pageSize = pageSize > MAX_PAGE_SIZE ? MAX_PAGE_SIZE : pageSize; /* Note: The Grubhub API does not support page sizes greater than 100 */
		for (LocationModel targetLocation : targetLocations) {

			try {
				for (ServiceOptionType targetServiceOption : TARGET_SERVICE_OPTIONS) {

					List<VendorRestaurantModel> currentFetchResult = new ArrayList<>();
					int page = 1;
					int fetchedSize;
					do {
						String targetApiEndpoint = String.format(
								applicationProperties.getDataExtraction().getGrubhub().getApiRestaurantSearchEndpoint(),
								targetServiceOption.name().toLowerCase(),
								targetServiceOption.name().toLowerCase(),
								targetLocation.location.getLon(),
								targetLocation.location.getLat(),
								pageSize,
								page);

						ResponseEntity<String> response = executeRESTRequest(
								targetApiEndpoint,
								HttpMethod.GET,
								requestHeaders
						);

						String responseJson = getResponseBody(response);

						JsonNode restaurantList = objectMapper.readTree(responseJson).at("/search_result/results");

						List<VendorRestaurantModel> result = fetchRestaurantsInternal(restaurantList, targetLocation);

						fetchedSize = result.size();
						currentFetchResult.addAll(result);
						page++;

					} while (fetchedSize == pageSize);

					resultList.addAll(filterDuplicates(resultList, currentFetchResult));

				}

			} catch (Exception e) {
				log.warn("An error occurred fetching restaurants from the Grubhub API for location: {}: {}", targetLocation, e.getMessage(), e);
			}
		}

		return resultList;
	}

	@Override
	public VendorRestaurantModel fetchRestaurantInternal(JsonNode restaurant, LocationModel targetLocation) {

		VendorRestaurantModel restaurantModel = new VendorRestaurantModel();

		String id = getJsonValueAsText(restaurant, "restaurant_id");

		restaurantModel.setIdWithVendor(getVendor().getName(), id);
		restaurantModel.name = getJsonValueAsText(restaurant, "name");
		restaurantModel.vendor = getVendor().getName();
		restaurantModel.vendorUrl = String.format("https://www.grubhub.com/restaurant/%s", id);

		JsonNode mediaNode = restaurant.at("/media_image");
		restaurantModel.imageUrl = String.format("%s%s", getJsonValueAsText(mediaNode, "base_url"), getJsonValueAsText(mediaNode, "public_id"));
		restaurantModel.logoUrl = restaurantModel.imageUrl;

		JsonNode addressNode = restaurant.at("/address");
		Double latitude = Double.valueOf(getJsonValueAsText(addressNode, "latitude"));
		Double longitude = Double.valueOf(getJsonValueAsText(addressNode, "longitude"));

		restaurantModel.location = new GeoPoint(latitude, longitude);

		restaurantModel.address = LocationUtil.formatAddress(
				getJsonValueAsText(addressNode, "street_address"),
				getJsonValueAsText(addressNode, "address_locality"),
				getJsonValueAsText(addressNode, "address_region"),
				getJsonValueAsText(addressNode, "postal_code")
		);

		restaurantModel.delivery = getJsonValueAsBoolean(restaurant, "delivery");
		restaurantModel.pickup = getJsonValueAsBoolean(restaurant, "pickup");

		JsonNode ratingsNode = restaurant.at("/ratings");
		restaurantModel.rating = getJsonValueAsDouble(ratingsNode, "actual_rating_value");

		Collection<String> categories = new ArrayList<>();
		JsonNode categoryList = restaurant.at("/cuisines");
		if (categoryList.isArray()) {
			for (final JsonNode category : categoryList) {
				if (category != null && category.isValueNode()) {
					categories.add(category.asText());
				}
			}
		}
		restaurantModel.vendorCategories = categories;
		restaurantModel.keywords = new ArrayList<>(restaurantModel.vendorCategories);

		restaurantModel.suggest = new Completion(new String[]{restaurantModel.name});

		return restaurantModel;

	}

	private String fetchAuthenticationToken() throws IOException {

		ResponseEntity<String> response = executeRESTRequest(
				applicationProperties.getDataExtraction().getGrubhub().getApiAuthEndpoint(),
				HttpMethod.POST,
				RESTAURANTS_API_AUTH_REQUEST_HEADERS,
				RESTAURANTS_API_AUTH_REQUEST_BODY
		);

		String responseJson = getResponseBody(response);

		JsonNode sessionInfo = objectMapper.readTree(responseJson).at("/session_handle");

		return getJsonValueAsText(sessionInfo, "access_token");
	}

	@Override
	public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails fetchRestaurantAvailabilityDetails(String restaurantId, final LocationSearchParams locationSearchParams)  throws IOException {

		String authenticationToken = fetchAuthenticationToken();

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", String.format("Bearer %s", authenticationToken));

		String targetApiEndpoint = String.format(
				applicationProperties.getDataExtraction().getGrubhub().getApiRestaurantDetailsEndpoint(),
				restaurantId,
				locationSearchParams.getLongitude(),
				locationSearchParams.getLatitude()
		);

		ResponseEntity<String> response = executeRESTRequest(
			targetApiEndpoint,
			HttpMethod.GET,
			requestHeaders
		);

		String responseJson = getResponseBody(response);

		JsonNode restaurantAvailabilityNode = objectMapper.readTree(responseJson).at("/restaurant_availability");
		JsonNode restaurantDeliveryFeeNode = restaurantAvailabilityNode.at("/delivery_fee");

		Boolean isRestaurantOpen = getJsonValueAsBoolean(restaurantAvailabilityNode, "open");

		Boolean isRestaurantAvailableForPickup = getJsonValueAsBoolean(restaurantAvailabilityNode, "open_pickup");
		Boolean isRestaurantAvailableForDelivery = getJsonValueAsBoolean(restaurantAvailabilityNode, "open_delivery");

		Boolean doesRestaurantDeliverToLocation = getJsonValueAsBoolean(restaurantAvailabilityNode, "delivery_offered_to_diner_location");

		Double restaurantPickupEstimate = getJsonValueAsDouble(restaurantAvailabilityNode, "pickup_estimate");

		Double restaurantDeliveryFee = getJsonValueAsDouble(restaurantDeliveryFeeNode, "amount");
		if (restaurantDeliveryFee != null) {
			restaurantDeliveryFee /= 100;
		}

		Double restaurantDeliveryEstimate = getJsonValueAsDouble(restaurantAvailabilityNode, "delivery_estimate");

		Double deliveryOrderMinimum = null;
		final JsonNode orderMinimumNode = restaurantAvailabilityNode.at("/order_minimum");
		if (orderMinimumNode != null) {
			final Double orderMinimum = getJsonValueAsDouble(orderMinimumNode, "amount");
			if (orderMinimum != null) {
				deliveryOrderMinimum = orderMinimum / 100;
			}
		}

		final VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = new VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails();

		availabilityDetails.setOpen(isRestaurantOpen);
		availabilityDetails.setPickupAvailable(isRestaurantAvailableForPickup);
		availabilityDetails.setDeliveryAvailable(isRestaurantAvailableForDelivery);
		availabilityDetails.setDeliverableToTargetAddress(doesRestaurantDeliverToLocation);
		availabilityDetails.setPickupEstimate(restaurantPickupEstimate);
		availabilityDetails.setDeliveryFee(restaurantDeliveryFee);
		availabilityDetails.setDeliveryEstimate(restaurantDeliveryEstimate);
		availabilityDetails.setDeliveryOrderMinimum(deliveryOrderMinimum);

		return availabilityDetails;

	}

}
