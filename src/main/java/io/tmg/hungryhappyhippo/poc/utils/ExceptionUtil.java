package io.tmg.hungryhappyhippo.poc.utils;

import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.Iterator;

import static io.tmg.hungryhappyhippo.poc.controllers.AbstractController.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/14/2019
 */
public final class ExceptionUtil {

    private ExceptionUtil() {
    }

    public static ModelAndView getErrorView(WebRequest req) {
        ModelAndView mav = new ModelAndView(LAYOUT).addObject(PAGE_TITLE_ATTR, "Error");
        mav.addAllObjects(req.getParameterMap());
        for (Iterator<String> names = req.getHeaderNames(); names.hasNext(); ) {
            String name = names.next();
            String[] value = req.getHeaderValues(name);
            mav.addObject(name, value);
        }
        mav.addObject(PAGE_CONTENT_ATTR, DEFAULT_CONTENT);
        return mav;
    }

}
