package io.tmg.hungryhappyhippo.poc.utils;

import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.SearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.SortOptionData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.suggest.completion.context.GeoQueryContext;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Utils class for building sort/page query to elasticsearch
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
public final class ESQueryUtil {

    private ESQueryUtil() {

    }

    public static PageRequest createPageRequest(SearchRequest searchRequest, Integer defaultPageSize) {

        int length = searchRequest.getPageSize() != null ? searchRequest.getPageSize() : defaultPageSize;
        int page = searchRequest.getPageNumber() != null ? searchRequest.getPageNumber() : 0;
        return PageRequest.of(page, length);

    }

    public static List<SortBuilder<?>> createSortBuilders(RestaurantSearchRequest searchRequest) {

        if (CollectionUtils.isNotEmpty(searchRequest.getSorts())) {
            List<SortBuilder<?>> sortBuilders = new ArrayList<>(searchRequest.getSorts().size());

            for (SortOptionData sortOptionData : searchRequest.getSorts()) {
                SortBuilder<?> sortBuilder;
                switch (sortOptionData.getField()) {
                    case "location":
                        sortBuilder = SortBuilders.geoDistanceSort(sortOptionData.getField(), searchRequest.getLatitude(), searchRequest.getLongitude());
                        break;

                    case ScoreSortBuilder.NAME:
                        sortBuilder = SortBuilders.scoreSort();
                        break;

                    default:
                        sortBuilder = SortBuilders.fieldSort(sortOptionData.getField());
                        break;
                }

                sortBuilders.add(sortBuilder.order(sortOptionData.getSortOrder()));
            }
            return sortBuilders;
        }

        return Collections.emptyList();

    }

    public static Map<String, List<? extends ToXContent>> buildSuggestQueryContextMap(final LocationSearchParams locationSearchParams) {

        if (locationSearchParams == null) {
            return Collections.emptyMap();
        }

        final Map<String, List<? extends ToXContent>> queryContexts = new HashMap<>();

        queryContexts.put("location", Collections.singletonList(GeoQueryContext.builder().setGeoPoint(new GeoPoint(
                Double.valueOf(locationSearchParams.getLatitude()),
                Double.valueOf(locationSearchParams.getLongitude())
        )).build()));

        return queryContexts;

    }

}
