package io.tmg.hungryhappyhippo.poc.utils;

import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

import static io.tmg.hungryhappyhippo.poc.utils.CookieUtils.ADDRESS;
import static io.tmg.hungryhappyhippo.poc.utils.CookieUtils.LATITUDE;
import static io.tmg.hungryhappyhippo.poc.utils.CookieUtils.LONGITUDE;
import static io.tmg.hungryhappyhippo.poc.utils.ParsingUtil.isNumeric;

public final class LocationUtil {

	private LocationUtil() {}

	public static final Double MIN_DISTANCE = 0.0;
	public static final Double MAX_DISTANCE = 50.0;
	public static final Double MAX_LATITUDE = 90.0;
	public static final Double MIN_LATITUDE = -90.0;
	public static final Double MAX_LONGITUDE = 180.0;
	public static final Double MIN_LONGITUDE = -180.0;

	public static final String DEFAULT_COUNTRY = "USA";

	public static boolean isLocationContextPresent(final HttpServletRequest request) {

		final String latitude = request.getParameter(LATITUDE);
		final String longitude = request.getParameter(LONGITUDE);
		final String address = request.getParameter(ADDRESS);

		return isLocationContextPresent(latitude, longitude, address);

	}

	public static boolean isLocationContextPresent(final LocationSearchParams locationSearchParams) {

		if (locationSearchParams == null) {
			return false;
		}

		final String latitude = locationSearchParams.getLatitude();
		final String longitude = locationSearchParams.getLongitude();
		final String address = locationSearchParams.getAddress();

		return isLocationContextPresent(latitude, longitude, address);

	}

	private static boolean isLocationContextPresent(String latitudeParam, String longitudeParam, String addressParam) {

		final String latitude = latitudeParam != null ?
				(isNumeric(latitudeParam.trim()) ? latitudeParam.trim() : "") : "";
		final String longitude = longitudeParam != null ?
				(isNumeric(longitudeParam.trim()) ? longitudeParam.trim() : "") : "";
		final String address = StringUtils.isNotBlank(addressParam) ?
				addressParam.trim() : "";

		if (latitude.equals("") || longitude.equals("") || address.equals("")) {
			return false;
		}
		else if (Double.compare(MIN_LATITUDE, Double.parseDouble(latitude)) > 0
				|| Double.compare(Double.parseDouble(latitude), MAX_LATITUDE) > 0
				|| Double.compare(MIN_LONGITUDE, Double.parseDouble(longitude)) > 0
				|| Double.compare(Double.parseDouble(longitude), MAX_LONGITUDE) > 0) {
			return false;
		}

		return true;

	}

	public static boolean isSearchDistanceWithinValidRange(final String distanceParam) {

		final String distance = distanceParam != null ?
				(isNumeric(distanceParam.trim()) ? distanceParam.trim() : "") : "";

		if (distance.equals("")) {
			return false;
		}
		else if (Double.compare(MIN_DISTANCE, Double.parseDouble(distance)) >= 0
				|| Double.compare(Double.parseDouble(distance), MAX_DISTANCE) > 0) {
			return false;
		}

		return true;

	}

	public static String formatAddress(String street, String city, String state, String zip) {

		return String.format("%s, %s, %s %s, %s", street, city, state, zip, DEFAULT_COUNTRY);

	}

}
