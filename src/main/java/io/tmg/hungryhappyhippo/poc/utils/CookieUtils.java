package io.tmg.hungryhappyhippo.poc.utils;

import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.UriUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.Charset;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/15/2019
 */
public final class CookieUtils {
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS = "address";

    private static final String LOCATION_CONTEXT_COOKIES_PATH = "/";

    private CookieUtils() {
    }

    public static LocationSearchParams getLocationSearchParams(final HttpServletRequest request, final HttpServletResponse response) {

        if (LocationUtil.isLocationContextPresent(request)) {

            addLocationCookiesFromRequest(request, response);

            return new LocationSearchParams(
                    request.getParameter(LATITUDE),
                    request.getParameter(LONGITUDE),
                    request.getParameter(ADDRESS)
            );

        } else {
            return getLocationParamsFromCookie(request);
        }

    }

    private static void addLocationCookiesFromRequest(final HttpServletRequest request, final HttpServletResponse response) {

        final Cookie latitudeCookie = new Cookie(LATITUDE, encodeValue(request.getParameter(LATITUDE)));
        final Cookie longitudeCookie = new Cookie(LONGITUDE, encodeValue(request.getParameter(LONGITUDE)));
        final Cookie addressCookie = new Cookie(ADDRESS, encodeValue(request.getParameter(ADDRESS)));

        latitudeCookie.setPath(LOCATION_CONTEXT_COOKIES_PATH);
        longitudeCookie.setPath(LOCATION_CONTEXT_COOKIES_PATH);
        addressCookie.setPath(LOCATION_CONTEXT_COOKIES_PATH);

        latitudeCookie.setMaxAge(Integer.MAX_VALUE);
        longitudeCookie.setMaxAge(Integer.MAX_VALUE);
        addressCookie.setMaxAge(Integer.MAX_VALUE);

        response.addCookie(latitudeCookie);
        response.addCookie(longitudeCookie);
        response.addCookie(addressCookie);

    }

    private static LocationSearchParams getLocationParamsFromCookie(final HttpServletRequest request) {

        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }

        String latitude = null;
        String longitude = null;
        String address = null;

        for (Cookie cookie : cookies) {
            switch (cookie.getName()) {
                case LATITUDE:
                    latitude = decodeValue(cookie.getValue());
                    break;

                case LONGITUDE:
                    longitude = decodeValue(cookie.getValue());
                    break;

                case ADDRESS:
                    address = decodeValue(cookie.getValue());
                    break;

                default:
                    break;

            }
        }

        final LocationSearchParams locationContext = new LocationSearchParams(latitude, longitude, address);

        if (StringUtils.isEmpty(latitude) || StringUtils.isEmpty(longitude) || StringUtils.isEmpty(address)) {
            return null;
        }

        return locationContext;

    }

    private static String decodeValue(String value) {
        return UriUtils.decode(value, Charset.defaultCharset());
    }

    private static String encodeValue(String value) {
        return UriUtils.encode(value, Charset.defaultCharset());
    }
}
