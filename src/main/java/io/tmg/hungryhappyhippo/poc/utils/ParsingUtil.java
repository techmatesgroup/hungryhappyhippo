package io.tmg.hungryhappyhippo.poc.utils;

public class ParsingUtil {
    private ParsingUtil() {
    }

    public static boolean isNumeric(String strNum) {
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
