package io.tmg.hungryhappyhippo.poc.restcontrollers;

import io.swagger.annotations.*;
import io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.RestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.facade.MenuItemFacade;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.dto.VendorRestaurantMenuDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;
import static javax.servlet.http.HttpServletResponse.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
@Slf4j
@RestController
@RequestMapping("/" + API_PREFIX + "/" + API_VERSION_1)
public class RestaurantDetailsRESTController extends AbstractRESTController {

	@Resource
	private RestaurantFacade restaurantFacade;

	@Resource
	private MenuItemFacade menuItemFacade;

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request"),
			@ApiResponse(code = SC_FORBIDDEN, message = "Resources is forbidden. Location Cookies not found"),
			@ApiResponse(code = SC_NOT_FOUND, message = "Requested resource not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Get restaurant details",
			notes = "Method allow to get restaurant details information",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = RestaurantDetailsData.class
	)
	@GetMapping(value = API_PATH_RESTAURANT, produces = MediaType.APPLICATION_JSON_VALUE)
	@LocationCookieCheck(required = false)
	public ResponseEntity<RestaurantDetailsData> getRestaurant(@PathVariable String restaurantId, final LocationSearchParams locationSearchParams) {

		log.trace("restaurantId - {}, locationSearchParams - {}", restaurantId, locationSearchParams);
		return ResponseEntity.ok(restaurantFacade.getRestaurantDetailsData(restaurantId, locationSearchParams));

	}


	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request"),
			@ApiResponse(code = SC_FORBIDDEN, message = "Resources is forbidden. Location Cookies not found"),
			@ApiResponse(code = SC_NOT_FOUND, message = "Requested resource not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Get vendor restaurant availability details",
			notes = "Method allow to get restaurant availability information",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails.class
	)
	@GetMapping(value = API_PATH_RESTAURANT_AVAILABILITY, produces = MediaType.APPLICATION_JSON_VALUE)
	@LocationCookieCheck
	public ResponseEntity<VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails> getVendorRestaurantAvailability(
			@PathVariable String vendorName,
			@PathVariable String vendorRestaurantId,
			final LocationSearchParams locationSearchParams) {

		log.trace("vendorName - {}, vendorRestaurantId - {}, locationSearchParams - {}", vendorName, vendorRestaurantId, locationSearchParams);
		return ResponseEntity.ok(restaurantFacade.getVendorRestaurantAvailabilityDetails(vendorRestaurantId, vendorName, locationSearchParams));

	}


	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request"),
			@ApiResponse(code = SC_NOT_FOUND, message = "Requested resource not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Get vendor restaurant menus",
			notes = "Method allow to get restaurant menus",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = VendorRestaurantMenuDto.class
	)
	@GetMapping(value = API_PATH_RESTAURANT_MENU, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VendorRestaurantMenuDto> getVendorRestaurantMenu(@PathVariable String vendorName, @PathVariable String vendorRestaurantId) {

		log.trace("vendorName - {}, vendorRestaurantId - {}", vendorName, vendorRestaurantId);
		return ResponseEntity.ok(new VendorRestaurantMenuDto(menuItemFacade.getMenuCategoryDataList(vendorRestaurantId, vendorName)));

	}

}
