package io.tmg.hungryhappyhippo.poc.restcontrollers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.tmg.hungryhappyhippo.poc.facade.LocationFacade;
import io.tmg.hungryhappyhippo.poc.models.dto.LocationDto;
import io.tmg.hungryhappyhippo.poc.models.dto.LocationServiceabilityDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

@RestController
@RequestMapping("/" + API_PREFIX + "/" + API_VERSION_1)
public class LocationRESTController extends AbstractRESTController {

	@Resource
	private LocationFacade locationFacade;

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Get all supported locations",
			notes = "Method allow to get data for all locations",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = LocationDto.class, responseContainer = "List"
	)
	@GetMapping(value = API_PATH_LOCATIONS, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LocationDto> getAllLocations() {
		return locationFacade.getAllLocations();
	}

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Get location serviceable",
			notes = "Method allow to verify if provided location is serviceable",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = LocationServiceabilityDto.class
	)
	@GetMapping(value = API_PATH_LOCATION_SERVICEABLE, produces = MediaType.APPLICATION_JSON_VALUE)
	public LocationServiceabilityDto getLocationServiceability(
			@RequestParam(name = "latitude", defaultValue = "") String latitude,
			@RequestParam(name = "longitude", defaultValue = "") String longitude) {
		return locationFacade.getLocationServiceability(latitude, longitude);
	}

}
