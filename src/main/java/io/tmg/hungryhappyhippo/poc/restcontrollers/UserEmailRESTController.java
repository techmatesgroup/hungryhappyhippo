package io.tmg.hungryhappyhippo.poc.restcontrollers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.tmg.hungryhappyhippo.poc.facade.UserEmailFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;
import static javax.servlet.http.HttpServletResponse.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/25/2019
 */
@Slf4j
@RestController
@RequestMapping("/" + API_PREFIX + "/" + API_VERSION_1)
public class UserEmailRESTController extends AbstractRESTController {

	@Resource
	private UserEmailFacade userEmailFacade;

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_METHOD_NOT_ALLOWED, message = "Method is not supported"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Send feedback email",
			notes = "Method allow to send users feedback thought email",
			consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
			response = String.class
	)
	@RequestMapping(value = API_PATH_EMAIL_FEEDBACK, method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> email(
			@RequestPart(value = "attachments", required = false) List<MultipartFile> attachments,
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "userEmail", required = false) String userEmail,
			@RequestParam(value = "feedbackMessage") String feedbackMessage,
			@RequestParam(value = "reCaptchaToken", required = false) String reCaptchaToken) {

		// TODO check reCaptchaToken (https://www.google.com/recaptcha/intro/v3.html)

		userEmailFacade.sendFeedbackEmail(userName, userEmail, feedbackMessage, attachments);

		return ResponseEntity.ok("OK");

	}

}
