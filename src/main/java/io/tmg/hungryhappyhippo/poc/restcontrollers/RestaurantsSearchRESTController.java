package io.tmg.hungryhappyhippo.poc.restcontrollers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.RestaurantSearchParams;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSearchResponseDto;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSuggestResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;
import static javax.servlet.http.HttpServletResponse.*;

/**
 * REST api controller for restaurant search/details page
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
@Slf4j
@RestController
@RequestMapping("/" + API_PREFIX + "/" + API_VERSION_1)
public class RestaurantsSearchRESTController extends AbstractRESTController {

	@Resource
	private RestaurantFacade restaurantFacade;

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request"),
			@ApiResponse(code = SC_FORBIDDEN, message = "Resources is forbidden. Location Cookies not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Search restaurants",
			notes = "Method allow to search restaurant by given filters",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = RestaurantSearchResponseDto.class
	)
	@GetMapping(value = API_PATH_RESTAURANTS_SEARCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@LocationCookieCheck
	public ResponseEntity<RestaurantSearchResponseDto> searchRestaurants(final RestaurantSearchParams searchParams, final LocationSearchParams locationSearchParams) {

		log.trace("searchParams - {}, locationSearchParams - {}", searchParams, locationSearchParams);
		return ResponseEntity.ok(restaurantFacade.searchRestaurants(searchParams, locationSearchParams));

	}

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Autocomplete restaurants",
			notes = "Method allow get restaurants for autocomplete",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = RestaurantSuggestResponseDto.class
	)
	@GetMapping(value = API_PATH_RESTAURANTS_SUGGEST, produces = MediaType.APPLICATION_JSON_VALUE)
	@LocationCookieCheck(required = false)
	public ResponseEntity<RestaurantSuggestResponseDto> suggest(
			@RequestParam(name = "term", defaultValue = "") String prefix,
			@RequestParam(name = "size", defaultValue = "10") int size,
			final LocationSearchParams locationSearchParams) {


		return ResponseEntity.ok(restaurantFacade.suggest(prefix, size, locationSearchParams));

	}

}
