package io.tmg.hungryhappyhippo.poc.restcontrollers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.tmg.hungryhappyhippo.poc.facade.VendorFacade;
import io.tmg.hungryhappyhippo.poc.models.dto.VendorDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

@RestController
@RequestMapping("/" + API_PREFIX + "/" + API_VERSION_1)
public class VendorRESTController extends AbstractRESTController {

	@Resource
	private VendorFacade vendorFacade;

	@ApiResponses({
			@ApiResponse(code = SC_OK, message = "OK"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error")
	})
	@ApiOperation(
			value = "Get all supported vendors",
			notes = "Method allow to get data for all vendors",
			produces = MediaType.APPLICATION_JSON_VALUE,
			response = VendorDto.class, responseContainer = "List"
	)
	@GetMapping(value = API_PATH_VENDORS, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<VendorDto> getAllVendors() {
		return vendorFacade.getAllVendors();
	}
}
