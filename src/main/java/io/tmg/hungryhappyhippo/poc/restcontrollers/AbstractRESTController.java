package io.tmg.hungryhappyhippo.poc.restcontrollers;

import io.tmg.hungryhappyhippo.poc.error.LocationCookieNotFoundException;
import io.tmg.hungryhappyhippo.poc.error.NotFoundException;
import io.tmg.hungryhappyhippo.poc.error.rest.ApiErrorResponse;
import io.tmg.hungryhappyhippo.poc.error.rest.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Common controller for REST api
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/22/2019
 */
public abstract class AbstractRESTController {

	@ExceptionHandler(ApiException.class)
	public ResponseEntity<ApiErrorResponse> handleApiException(final ApiException apiException) {

		return new ResponseEntity<>(new ApiErrorResponse(apiException.getHttpStatus().getReasonPhrase(), apiException.getMessage()), apiException.getHttpStatus());

	}

	@ExceptionHandler(LocationCookieNotFoundException.class)
	public ResponseEntity<ApiErrorResponse> handleLocationCookieNotFoundException(final LocationCookieNotFoundException locationCookieNotFoundException) {

		return new ResponseEntity<>(new ApiErrorResponse(HttpStatus.FORBIDDEN.getReasonPhrase(), locationCookieNotFoundException.getMessage()), HttpStatus.FORBIDDEN);

	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ApiErrorResponse> handleNotFoundException(final NotFoundException exception) {

		return new ResponseEntity<>(new ApiErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(), exception.getMessage()), HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(BindException.class)
	public ResponseEntity<ApiErrorResponse> handleBindException(final BindException exception) {

		final String defaultMessage = exception.getFieldError() != null ? exception.getFieldError().getDefaultMessage() : exception.getMessage();
		return new ResponseEntity<>(new ApiErrorResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), defaultMessage), HttpStatus.BAD_REQUEST);

	}

}
