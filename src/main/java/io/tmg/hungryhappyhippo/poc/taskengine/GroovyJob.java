package io.tmg.hungryhappyhippo.poc.taskengine;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Resource;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.InterruptableJob;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.springframework.context.ApplicationContext;
import org.springframework.scripting.groovy.GroovyScriptEvaluator;
import org.springframework.scripting.support.StaticScriptSource;

import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.TaskStatus;
import io.tmg.hungryhappyhippo.poc.services.TaskService;
import lombok.extern.slf4j.Slf4j;

@DisallowConcurrentExecution
@Slf4j
public class GroovyJob implements Job,InterruptableJob {

	AtomicReference<Thread> runningThread = new AtomicReference<Thread>();
	TaskModel runningTask = new TaskModel();

	@Resource
	private TaskService taskService;

	@Resource
	private ApplicationContext applicationContext;

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {

		try {
			setRunningTaskProperties(ctx);

			if (!taskService.isTaskEnabled(runningTask)) {
				log.warn(String.format("A disabled task cannot be executed, skipping execution of task: %s", runningTask.id));
				return;
			}

			GroovyScriptEvaluator scriptRunner = new GroovyScriptEvaluator(applicationContext.getClassLoader());

			Map<String, Object> taskParameters = new HashMap<>();
			taskParameters.put("ctx", applicationContext);

			runningTask.lastStartTime = taskService.getLoadedTaskPreviousFireTime(runningTask.id);
			runningTask.status = String.valueOf(TaskStatus.RUNNING);

			Object result = scriptRunner.evaluate(new StaticScriptSource(runningTask.command), taskParameters);

			runningTask.lastEndTime = new Date();
			runningTask.status = String.valueOf(TaskStatus.FINISHED);
			runningTask.lastExecutionStatus = String.valueOf(TaskStatus.SUCCESS);
			runningTask.lastExecutionResult = result != null ? result.toString() : "(no result)";

		}
		catch (Exception e) {
			runningTask.status = String.valueOf(TaskStatus.FINISHED);
			runningTask.lastEndTime = new Date();
			String exceptionType = e.getClass().getName();
			String exceptionMessage = e.getMessage() != null ? e.getMessage() : "(none)";
			runningTask.lastExecutionResult = exceptionType + ": " + exceptionMessage;

			if (e instanceof InterruptedException) {
				log.info(String.format("Task execution was manually killed for task: %s", runningTask.id));
				runningTask.lastExecutionStatus = String.valueOf(TaskStatus.KILLED);
			}
			else {
				log.error(String.format("Task execution failed for task: %s", runningTask.id), e);
				runningTask.lastExecutionStatus = String.valueOf(TaskStatus.FAILURE);
			}

		}
		finally {
			runningThread.set(null);
		}

	}

	@Override
	public void interrupt() throws UnableToInterruptJobException {

		log.info(String.format("Attempting to kill currently running task: %s: %s", runningTask.id, runningThread.get()));

		Thread thread = runningThread.get();

		if (thread != null) {
			thread.interrupt();
		}

		return;

	}

	protected void setRunningTaskProperties(JobExecutionContext ctx) {

		runningThread.set(Thread.currentThread());
		runningTask = (TaskModel) ctx.getJobDetail().getJobDataMap().get("task");

	}

}
