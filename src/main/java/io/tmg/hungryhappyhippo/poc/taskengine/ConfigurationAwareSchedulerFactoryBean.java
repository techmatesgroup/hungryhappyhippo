package io.tmg.hungryhappyhippo.poc.taskengine;

import javax.annotation.Resource;

import org.springframework.scheduling.SchedulingException;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigurationAwareSchedulerFactoryBean extends SchedulerFactoryBean {

	@Resource
	private ApplicationProperties applicationProperties;

	@Override
	public void start() throws SchedulingException {

		if (applicationProperties.getTaskengine().isEnabled()) {
			log.info("The task engine scheduler will be autostarted on this instance");
		}
		else {
			log.info("The task engine scheduler is not configured to be autostarted on this instance");
		}

	}

}
