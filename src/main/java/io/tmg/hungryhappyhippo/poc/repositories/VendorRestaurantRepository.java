package io.tmg.hungryhappyhippo.poc.repositories;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;

@Repository
public interface VendorRestaurantRepository extends ElasticsearchRepository<VendorRestaurantModel, String> {

	Iterable<VendorRestaurantModel> findByName(String name);

}
