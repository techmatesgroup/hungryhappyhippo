package io.tmg.hungryhappyhippo.poc.repositories;

import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepository extends ElasticsearchRepository<RestaurantModel, String> {

}
