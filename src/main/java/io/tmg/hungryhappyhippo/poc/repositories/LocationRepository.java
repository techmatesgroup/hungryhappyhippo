package io.tmg.hungryhappyhippo.poc.repositories;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import io.tmg.hungryhappyhippo.poc.models.LocationModel;

@Repository
public interface LocationRepository extends ElasticsearchRepository<LocationModel, String> {

	List<LocationModel> findByName(String name);

	List<LocationModel> findByType(String type);

}