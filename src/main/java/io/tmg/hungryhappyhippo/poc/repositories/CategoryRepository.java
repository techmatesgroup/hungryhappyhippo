package io.tmg.hungryhappyhippo.poc.repositories;

import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/19/2019
 */
@Repository
public interface CategoryRepository extends ElasticsearchRepository<CategoryModel, String> {

}
