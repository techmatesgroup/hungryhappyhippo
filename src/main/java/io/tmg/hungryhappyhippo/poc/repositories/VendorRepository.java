package io.tmg.hungryhappyhippo.poc.repositories;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import io.tmg.hungryhappyhippo.poc.models.VendorModel;

@Repository
public interface VendorRepository extends ElasticsearchRepository<VendorModel, String> {

	List<VendorModel> findByName(String name);

}