package io.tmg.hungryhappyhippo.poc.repositories;

import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends ElasticsearchRepository<MenuItemModel, String> {

	Iterable<MenuItemModel> findByName(String name);

	Iterable<MenuItemModel> findAllByRestaurantId(String restaurantId, PageRequest pageable);

}
