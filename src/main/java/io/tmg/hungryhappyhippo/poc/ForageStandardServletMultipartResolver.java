package io.tmg.hungryhappyhippo.poc;

import io.tmg.hungryhappyhippo.poc.error.rest.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import javax.servlet.http.HttpServletRequest;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.API_PREFIX;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/26/2019
 */
@Slf4j
public class ForageStandardServletMultipartResolver extends StandardServletMultipartResolver {

	@Override
	public MultipartHttpServletRequest resolveMultipart(HttpServletRequest request) throws MultipartException {

		try {
			return super.resolveMultipart(request);
		} catch (MultipartException e) {
			log.warn("resolveMultipart error", e);
			throw request.getRequestURI().contains("/" + API_PREFIX + "/") ? new ApiException(e) : e;
		}

	}
}
