package io.tmg.hungryhappyhippo.poc.data.search.response.menu;

import io.tmg.hungryhappyhippo.poc.data.search.response.SearchResponseData;
import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/15/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MenuResponseData extends SearchResponseData<MenuItemModel> {

    private MenuFilterData filterData;

}
