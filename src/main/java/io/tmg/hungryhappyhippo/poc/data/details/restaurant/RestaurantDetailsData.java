package io.tmg.hungryhappyhippo.poc.data.details.restaurant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;

/**
 * Data object for Restaurant Details page
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class RestaurantDetailsData {

    private String id;

    private String name;
    private String description;

    public Double latitude;
    public Double longitude;
    private Double distance;
    private String address;
    private String googlePlaceId;

    private Collection<String> categories;
    private Collection<String> keywords;

    public Boolean delivery;
    public Boolean pickup;
    public Float rating;

    private String promotionalWeight;
    private String promotionalContent;

    private Double priceLevel;

    private Collection<VendorRestaurantDetailsData> vendorRestaurants;

}
