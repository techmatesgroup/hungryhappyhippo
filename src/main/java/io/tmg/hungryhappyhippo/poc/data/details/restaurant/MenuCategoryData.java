package io.tmg.hungryhappyhippo.poc.data.details.restaurant;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Menu category Data object for Restaurant Details page
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class MenuCategoryData {

    private String id;

    @ApiModelProperty(value = "Name of menu/category")
    private String menuCategoryName;


    private String description;

    @ApiModelProperty("List of products for menu")
    private List<ProductItem> productItemList;

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    public static class ProductItem {
        private String id;
        private String name;
        private String description;
        private Double price;
    }

}
