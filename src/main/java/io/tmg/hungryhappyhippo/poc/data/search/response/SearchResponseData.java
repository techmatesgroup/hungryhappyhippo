package io.tmg.hungryhappyhippo.poc.data.search.response;

import io.tmg.hungryhappyhippo.poc.data.search.SortOptionData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/14/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SearchResponseData<ItemModel> {

    private List<ItemModel> data;

    private List<SortOptionData> sorts;
    private List<Integer> supportedPageSizes;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer dataSize;
    private Integer totalPages;
    private Long totalResults;
    private Boolean hasMoreResults;

}
