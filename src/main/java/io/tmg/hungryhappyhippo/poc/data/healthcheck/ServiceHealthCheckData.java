package io.tmg.hungryhappyhippo.poc.data.healthcheck;

import io.tmg.hungryhappyhippo.poc.models.IndexModel;
import lombok.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 04/01/2019
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ServiceHealthCheckData {

	private ElasticsearchInfo elasticsearchInfo = new ElasticsearchInfo();
	private MongoDBInfo mongoDBInfo = new MongoDBInfo();
	private RestApiInfo restApiInfo = new RestApiInfo();

	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ElasticsearchInfo {
		private boolean esAvailable;
		private String esStatusInfo;
		private List<IndexModel> indexModelList;
	}

	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	@AllArgsConstructor
	public static class MongoDBInfo {
		private boolean mongoDbAvailable;
		private String mongoDbStatusInfo;
		private Document dbInfo;
	}

	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RestApiInfo {
		private List<EndPointData> endPointDataList = new ArrayList<>();

		@Setter
		@Getter
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class EndPointData {
			private String description;
			private String endpoint;
			private String status;
		}
	}
}
