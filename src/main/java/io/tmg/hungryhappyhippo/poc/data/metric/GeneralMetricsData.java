package io.tmg.hungryhappyhippo.poc.data.metric;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/22/2019
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
public class GeneralMetricsData {

	// for each ES index
	private List<IndexData> indexDataList = new ArrayList<>();

	// for each vendor
	private List<VendorStatsData> vendorStatDataList = new ArrayList<>();

	private HelpfulSummaryData helpfulSummaryData = new HelpfulSummaryData();

	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	@AllArgsConstructor
	public static class IndexData {
		private String name;
		private Long totalDocCount;
	}

	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	public static class VendorStatsData {
		private String vendorName;

		private Integer vendorRestaurantsCount;
		private Integer vendorRestaurantsDeliverySupportedCount;
		private Integer vendorRestaurantsPickupSupportedCount;
		private Integer vendorRestaurantsAllServiceOptionSupportedCount;

		private Integer restaurantsCount;
		private Integer restaurantsExclusivelyCount;
	}

	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	public static class HelpfulSummaryData {
		private Integer vendorRestaurantsWithoutAggregateCount;
		private Integer vendorRestaurantsMissingRatingCount;
		private Integer vendorRestaurantsMissingPlaceIdCount;

		private Integer restaurantsWithoutCategoryCount;
		private Integer restaurantsWithoutVendorRestaurantsCount;
		private Integer restaurantsMissingRatingCount;
		private Integer restaurantsMissingPlaceIdCount;
		private Integer restaurantsMissingPriceLevelCount;
	}

}
