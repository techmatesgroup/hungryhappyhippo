package io.tmg.hungryhappyhippo.poc.data.details.restaurant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Collection;
import java.util.List;

/**
 * Restaurant Data object for Restaurant Details page
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class VendorRestaurantDetailsData {

    private String id;
    private String restaurantAggregateId;

    private String name;

    private String vendor;
    private String vendorUrl;

    private String imageUrl;
    private String logoUrl;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private GeoPoint location;

    private String address;
    private String googlePlaceId;

    private Boolean delivery;
    private Boolean pickup;

    private Double rating;

    private Collection<String> vendorCategories;
    private Collection<String> keywords;

    private String promotionalWeight;
    private String promotionalContent;

    @ApiModelProperty(required = true, example = "/api/v1/restaurant/availability/DoorDash/DoorDash-124911")
    private String restaurantAvailabilityUrl;

    @ApiModelProperty(required = true, example = "/api/v1/restaurant/menu/DoorDash/DoorDash-124911")
    private String restaurantMenuUrl;

    /* Dynamic Content */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private List<MenuCategoryData> menuCategoryDataList;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private VendorRestaurantAvailabilityDetails restaurantAvailabilityDetails = new VendorRestaurantAvailabilityDetails();

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @ApiModel("Restaurant availability info from vendor")
    public static class VendorRestaurantAvailabilityDetails {
        private Boolean open;

        private Boolean pickupAvailable;
        private Boolean deliveryAvailable;

        private Boolean deliverableToTargetAddress;

        @JsonInclude(value = JsonInclude.Include.NON_NULL)
        private Double pickupEstimate;
        @JsonInclude(value = JsonInclude.Include.NON_NULL)
        private Double pickupOrderMinimum;

        @JsonInclude(value = JsonInclude.Include.NON_NULL)
        private Double deliveryFee;
        @JsonInclude(value = JsonInclude.Include.NON_NULL)
        private Double deliveryEstimate;
        @JsonInclude(value = JsonInclude.Include.NON_NULL)
        private Double deliveryOrderMinimum;

    }

}
