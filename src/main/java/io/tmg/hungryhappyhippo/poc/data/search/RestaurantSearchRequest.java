package io.tmg.hungryhappyhippo.poc.data.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/15/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RestaurantSearchRequest extends SearchRequest {

    private Double latitude;
    private Double longitude;
    private Double distance;

    private Set<String> vendors;
    private Set<String> categories;

    private Boolean delivery;
    private Boolean pickup;

    private Double minDistance;
    private Double maxDistance;

    private Double minRating;
    private Double maxRating;

    private Double minPriceLevel;

}
