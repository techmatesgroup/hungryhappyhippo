package io.tmg.hungryhappyhippo.poc.data.index.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class UserParams {

	private String username;
	private String email;
	private String password;
	private String password_confirm;
	private String address;
	private double latitude;
	private double longitude;

}
