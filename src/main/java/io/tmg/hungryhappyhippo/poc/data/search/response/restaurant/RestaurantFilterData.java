package io.tmg.hungryhappyhippo.poc.data.search.response.restaurant;

import io.tmg.hungryhappyhippo.poc.data.search.response.CategoryFilterData;
import lombok.*;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/15/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RestaurantFilterData {

    private String searchTerms;
    private String serviceOption;

    private String latitude;
    private String longitude;
    private String distance;

    private Double rating;

    private List<CategoryFilterData> vendorCategories;

}
