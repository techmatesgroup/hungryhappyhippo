package io.tmg.hungryhappyhippo.poc.data.search.request.restaurant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/12/2019
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LocationSearchParams {

	private String latitude;
	private String longitude;
	private String address;

}
