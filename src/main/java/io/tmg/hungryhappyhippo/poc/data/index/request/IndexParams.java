package io.tmg.hungryhappyhippo.poc.data.index.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class IndexParams {

	private String type;
	private String id;

}
