package io.tmg.hungryhappyhippo.poc.data.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SearchRequest {

    private String searchTerms;

    private Integer pageNumber;
    private Integer pageSize;

    private Set<SortOptionData> sorts;

}
