package io.tmg.hungryhappyhippo.poc.data.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.elasticsearch.search.sort.SortOrder;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class SortOptionData {

	private String id;
	private String name;
	private String field;
	private SortOrder sortOrder;

}
