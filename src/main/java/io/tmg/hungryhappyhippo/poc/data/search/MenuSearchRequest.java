package io.tmg.hungryhappyhippo.poc.data.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MenuSearchRequest extends SearchRequest {

    private Double minPrice;
    private Double maxPrice;

}
