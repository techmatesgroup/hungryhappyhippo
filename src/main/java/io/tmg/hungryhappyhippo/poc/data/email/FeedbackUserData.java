package io.tmg.hungryhappyhippo.poc.data.email;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/25/2019
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackUserData {

	@NotEmpty
	private String userName;

	@NotEmpty
	private String userEmail;

	@NotEmpty
	private String feedbackMessage;

	public FeedbackUserData(@NotEmpty String userName, @NotEmpty String userEmail, @NotEmpty String feedbackMessage) {
		this.userName = userName;
		this.userEmail = userEmail;
		this.feedbackMessage = feedbackMessage;
	}

	private List<AttachData> attachDataList;

}
