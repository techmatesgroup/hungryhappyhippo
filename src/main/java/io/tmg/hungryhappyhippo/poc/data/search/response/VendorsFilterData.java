package io.tmg.hungryhappyhippo.poc.data.search.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 27.12.2018
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class VendorsFilterData {

    private String name;
    private int count;
    private boolean checked;

}
