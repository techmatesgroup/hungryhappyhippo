package io.tmg.hungryhappyhippo.poc.data.search.response.restaurant;

import io.tmg.hungryhappyhippo.poc.data.search.response.SearchResponseData;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/15/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RestaurantResponseData extends SearchResponseData<RestaurantModel> {

    private RestaurantFilterData filterData;

    public RestaurantResponseData(RestaurantFilterData filterData) {
        this.filterData = filterData;
    }

}
