package io.tmg.hungryhappyhippo.poc.data.search.request.menu;

import io.tmg.hungryhappyhippo.poc.data.search.request.SearchParams;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/14/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MenuSearchParams extends SearchParams {

    private Integer pageSize = 5; // we'll cleanup this field/class since we no longer need pagination for menus
    public String[] priceRange;

}
