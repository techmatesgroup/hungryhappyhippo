package io.tmg.hungryhappyhippo.poc.data.search.response.menu;

import lombok.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MenuFilterData {

    private String searchTerms;

}
