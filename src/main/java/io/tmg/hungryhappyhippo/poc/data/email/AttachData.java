package io.tmg.hungryhappyhippo.poc.data.email;

import lombok.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/25/2019
 */
@Setter
@Getter
@ToString(of = "fileName")
@NoArgsConstructor
@AllArgsConstructor
public class AttachData {

	private String fileName;
	private byte[] data;

}
