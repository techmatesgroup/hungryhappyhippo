package io.tmg.hungryhappyhippo.poc.data.index.result;

import java.time.Duration;

public class IndexResultData extends ResultData {

	public long totalIndexesProcessed;
	public long totalDocumentsProcessed;
	public Duration totalIndexTime;

}
