package io.tmg.hungryhappyhippo.poc.data.search.request;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/14/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SearchParams {

    @ApiParam("Search terms for fuzzy search")
    private String searchTerms;

    @ApiParam(defaultValue = "5")
    private String distance = "5";

    @ApiParam(defaultValue = "0")
    private Integer pageNumber = 0;

    @ApiParam(defaultValue = "25")
    private Integer pageSize = 25;

    @ApiParam("Sort options: name-asc, rating-desc")
    private Set<String> sorts;

    @ApiParam(hidden = true)
    private boolean json;

}
