package io.tmg.hungryhappyhippo.poc.data.search.request.restaurant;

import io.swagger.annotations.ApiParam;
import io.tmg.hungryhappyhippo.poc.data.search.request.SearchParams;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/14/2019
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RestaurantSearchParams extends SearchParams {

    @ApiParam("Vendors: DoorDash, UberEats")
    private Set<String> vendors;

    @ApiParam("Categories: Asian, Dinners")
    private Set<String> categories;

    @ApiParam("Service option: delivery/pickup")
    private String serviceOption;

    @ApiParam("Restaurant rating")
    private Double rating;

    @ApiParam("Restaurant price level")
    private Double priceLevel;

}
