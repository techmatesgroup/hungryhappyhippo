package io.tmg.hungryhappyhippo.poc.data.search.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CategoryFilterData {

    private String name;
    private int count;
    private boolean checked;

}
