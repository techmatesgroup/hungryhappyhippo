package io.tmg.hungryhappyhippo.poc.data.promotion;

public enum PromotionType {
    CONTENT_PROMOTION("contentpromotion"),
    SEARCH_PROMOTION("searchpromotion");

    private String description;

    PromotionType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
