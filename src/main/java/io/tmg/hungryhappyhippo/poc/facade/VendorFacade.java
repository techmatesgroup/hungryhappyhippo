package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.models.dto.VendorDto;

import java.util.List;

public interface VendorFacade {

	List<VendorDto> getAllVendors();

}
