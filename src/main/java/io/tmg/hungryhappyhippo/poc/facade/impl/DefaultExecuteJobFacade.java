package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.data.healthcheck.ServiceHealthCheckData;
import io.tmg.hungryhappyhippo.poc.data.metric.GeneralMetricsData;
import io.tmg.hungryhappyhippo.poc.facade.ExecuteJobFacade;
import io.tmg.hungryhappyhippo.poc.services.EmailSendService;
import io.tmg.hungryhappyhippo.poc.services.GeneralMetricsDataService;
import io.tmg.hungryhappyhippo.poc.services.ServiceHealthCheckService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/22/2019
 */
@Slf4j
@Service("executeJobFacade")
public class DefaultExecuteJobFacade implements ExecuteJobFacade {

	@Resource
	private GeneralMetricsDataService generalMetricsDataService;

	@Resource
	private EmailSendService emailSendService;

	@Resource
	private ServiceHealthCheckService serviceHealthCheckService;

	@Override
	public String createGeneralMetricsReport() throws MessagingException {

		final GeneralMetricsData generalMetricsData = generalMetricsDataService.buildGeneralMetricsInfo();
		log.debug("generalMetricsData - {}", generalMetricsData);

		emailSendService.sendGeneralMetricsReport(generalMetricsData);

		return "Successful!";

	}

	@Override
	public String serviceHealthCheckReport() throws MessagingException {

		final ServiceHealthCheckData serviceHealthCheckData = serviceHealthCheckService.createServiceHealthCheckReport();
		log.debug("serviceHealthCheckData - {}", serviceHealthCheckData);

		emailSendService.sendServiceHealthCheckReport(serviceHealthCheckData);

		return "Successful!";
	}
}
