package io.tmg.hungryhappyhippo.poc.facade;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/25/2019
 */
public interface UserEmailFacade {

	void sendFeedbackEmail(String userName, String userEmail, String feedbackMessage, final List<MultipartFile> attachments);

}
