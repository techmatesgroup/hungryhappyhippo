package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.facade.CategoryFacade;
import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.dto.CategoryDto;
import io.tmg.hungryhappyhippo.poc.services.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultCategoryFacade implements CategoryFacade {

	@Resource
	private CategoryService categoryService;

	@Resource
	private ModelMapper modelMapper;

	@Override
	public List<CategoryDto> getAllCategories() {

		List<CategoryDto> categories = new ArrayList<>();

		for (CategoryModel categoryModel : categoryService.getCategories()) {
			categories.add(modelMapper.map(categoryModel, CategoryDto.class));
		}

		return categories;

	}

}
