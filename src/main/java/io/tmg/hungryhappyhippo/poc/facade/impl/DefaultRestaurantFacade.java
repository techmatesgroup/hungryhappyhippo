package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.constants.HHHConstants;
import io.tmg.hungryhappyhippo.poc.converter.impl.RestaurantDetailsDataConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.RestaurantsConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.RestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.SortOptionData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.RestaurantSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.restaurant.RestaurantResponseData;
import io.tmg.hungryhappyhippo.poc.error.rest.ApiException;
import io.tmg.hungryhappyhippo.poc.facade.AbstractSearchItemFacade;
import io.tmg.hungryhappyhippo.poc.facade.RealTimeDataExtractorFacade;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantDto;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSearchResponseDto;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSuggestResponseDto;
import io.tmg.hungryhappyhippo.poc.services.LocationService;
import io.tmg.hungryhappyhippo.poc.services.RestaurantService;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.http.HttpStatus;

import com.google.maps.model.PlaceDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.stream.Collectors;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.API.*;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 28.12.2018
 */
@Slf4j
@Service("restaurantFacade")
public class DefaultRestaurantFacade
        extends AbstractSearchItemFacade<RestaurantSearchParams, RestaurantSearchRequest, RestaurantResponseData, RestaurantModel>
        implements RestaurantFacade {

    @Resource
    private ApplicationProperties applicationProperties;

    @Resource
    private VendorRestaurantService vendorRestaurantService;

    @Resource
    private LocationService locationService;

    @Resource
    private RestaurantService restaurantService;

    @Resource
    private RestaurantDetailsDataConverter restaurantDetailsDataConverter;

    @Resource
    private RealTimeDataExtractorFacade realTimeDataExtractorFacade;

    @Resource
    private RestaurantsConverter restaurantsConverter;

    @Override
    public RestaurantSearchResponseDto searchRestaurants(final RestaurantSearchParams searchParams, final LocationSearchParams locationSearchParams) {

        final RestaurantSearchRequest searchRequest;
        try {
            searchRequest = createSearchRequest(searchParams, locationSearchParams);
        } catch (Exception e) {
            log.warn("createSearchRequest error", e);
            throw new ApiException(HttpStatus.BAD_REQUEST, e);
        }

        final AggregatedPage<RestaurantModel> result = restaurantService.searchRestaurants(searchRequest);

        final List<RestaurantDto> responseList = restaurantsConverter.convertAll(result.getContent());
        responseList.forEach(restaurantDto ->
                restaurantDto.setDistance(GeoDistance.ARC.calculate(
                        Double.valueOf(locationSearchParams.getLatitude()), Double.valueOf(locationSearchParams.getLongitude()),
                        restaurantDto.getLatitude(), restaurantDto.getLongitude(),
                        DistanceUnit.MILES)));

        final Pageable pageable = result.getPageable();
        return new RestaurantSearchResponseDto(
                responseList,
                pageable.getPageNumber(),
                pageable.getPageSize(),
                result.getTotalPages(),
                result.getTotalElements(),
                (pageable.getPageNumber() * pageable.getPageSize() + result.getNumberOfElements()) < result.getTotalElements()
        );
    }

    @Override
    public RestaurantDetailsData getRestaurantDetailsData(String id, final LocationSearchParams locationSearchParams) {
        final RestaurantModel restaurantModel = restaurantService.getById(id);

        final RestaurantDetailsData restaurantDetailsData = restaurantDetailsDataConverter.convert(restaurantModel);
        if (restaurantDetailsData.getVendorRestaurants() != null) {
            restaurantDetailsData.getVendorRestaurants().forEach(vendorRestaurantDetailsData -> {

                final String availabilityHref = String.format("/%s/%s%s", API_PREFIX, API_VERSION_1, API_PATH_RESTAURANT_AVAILABILITY
                        .replace("{vendorName}", vendorRestaurantDetailsData.getVendor())
                        .replace("{vendorRestaurantId}", vendorRestaurantDetailsData.getId()));

                vendorRestaurantDetailsData.setRestaurantAvailabilityUrl(availabilityHref);

                final String menusHref = String.format("/%s/%s%s", API_PREFIX, API_VERSION_1, API_PATH_RESTAURANT_MENU
                        .replace("{vendorName}", vendorRestaurantDetailsData.getVendor())
                        .replace("{vendorRestaurantId}", vendorRestaurantDetailsData.getId()));

                vendorRestaurantDetailsData.setRestaurantMenuUrl(menusHref);

            });
        }
        if (LocationUtil.isLocationContextPresent(locationSearchParams)) {
            restaurantDetailsData.setDistance(GeoDistance.ARC.calculate(
                    Double.valueOf(locationSearchParams.getLatitude()), Double.valueOf(locationSearchParams.getLongitude()),
                    restaurantDetailsData.getLatitude(), restaurantDetailsData.getLongitude(),
                    DistanceUnit.MILES));
        }

        return restaurantDetailsData;
    }

    @Override
    public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails getVendorRestaurantAvailabilityDetails(String restaurantId, String vendorName, final LocationSearchParams locationSearchParams) {

        return realTimeDataExtractorFacade.getVendorRestaurantAvailabilityDetails(restaurantId, vendorName, locationSearchParams);

    }

    @Override
    public Set<String> buildRestaurantsFromAllVendorRestaurants() {
        log.debug("Building and updating restaurants with loaded vendor restaurant data");
        Set<String> restaurants = new HashSet<>();
        vendorRestaurantService.getRestaurants().forEach(vendorRestaurant -> {
            restaurants.add(updateRestaurantWithVendorRestaurant(vendorRestaurant.id));
        });
        log.debug("Done building and updating restaurants from loaded vendor restaurant data, total updated: {}", restaurants.size());
        return restaurants;
    }

    @Override
    public String updateRestaurantWithVendorRestaurant(String vendorRestaurantId) {
        VendorRestaurantModel vendorRestaurant = vendorRestaurantService.getById(vendorRestaurantId);
        if (StringUtils.isBlank(vendorRestaurant.name)) {
            log.warn("Skipping restaurant with empty name {}", vendorRestaurant.id);
            return null;
        }

        if (StringUtils.isBlank(vendorRestaurant.restaurantAggregateId)) {
            RestaurantModel aggregate = restaurantService.getByNameAndAddress(vendorRestaurant.name, vendorRestaurant.address);
            updateRestaurantWithVendorRestaurant(aggregate, vendorRestaurant);
        } else {
            RestaurantModel aggregate;
                aggregate = restaurantService
                        .findById(vendorRestaurant.restaurantAggregateId)
                        .orElse(restaurantService.getByNameAndAddress(vendorRestaurant.name, vendorRestaurant.address));

            updateRestaurantWithVendorRestaurant(aggregate, vendorRestaurant);
        }
        return vendorRestaurant.restaurantAggregateId;
    }

    @Override
    public void deleteVendorRestaurantFromRestaurant(String vendorRestaurantId, String restaurantId) {
        if (StringUtils.isBlank(restaurantId)) {
            return;
        }

        restaurantService
                .findById(restaurantId)
                .ifPresent(restaurant -> {
                    restaurant.vendorRestaurants.removeIf(vendorRestaurant -> StringUtils.equals(vendorRestaurant.id, vendorRestaurantId));
                    if (CollectionUtils.isEmpty(restaurant.vendorRestaurants)) {
                        restaurantService.deleteById(restaurantId);
                    } else {
                        restaurant.keywords = restaurant.vendorRestaurants.stream()
                                .map(RestaurantModel.VendorRestaurant::getVendorCategories)
                                .flatMap(Collection::stream)
                                .collect(Collectors.toSet());
                        restaurantService.save(restaurant);
                    }
                });
    }

    @Override
    public void moveVendorRestaurantToRestaurant(String vendorRestaurantId, String restaurantId) {
        VendorRestaurantModel vendorRestaurant = vendorRestaurantService.getById(vendorRestaurantId);

        if (vendorRestaurant.restaurantAggregateId == null || !vendorRestaurant.restaurantAggregateId.equals(restaurantId)) {
            deleteVendorRestaurantFromRestaurant(vendorRestaurant.id, vendorRestaurant.restaurantAggregateId);

            RestaurantModel restaurant = restaurantService.getById(restaurantId);
            updateRestaurantWithVendorRestaurant(restaurant, vendorRestaurant);
        }
    }

	@Override
	public RestaurantSuggestResponseDto suggest(String prefix, int size, final LocationSearchParams locationSearchParams) {

        return new RestaurantSuggestResponseDto(restaurantService.suggest(prefix, size, locationSearchParams));

	}

	@Override
    protected RestaurantSearchRequest createSearchRequest(RestaurantSearchParams searchParams, LocationSearchParams locationSearchParams) {

        final RestaurantSearchRequest searchRequest = new RestaurantSearchRequest();

        searchRequest.setSearchTerms(searchParams.getSearchTerms());

        searchRequest.setLatitude(Double.valueOf(locationSearchParams.getLatitude()));
        searchRequest.setLongitude(Double.valueOf(locationSearchParams.getLongitude()));
        searchRequest.setDistance(Double.valueOf(searchParams.getDistance()));

        searchRequest.setVendors(searchParams.getVendors());
        searchRequest.setCategories(searchParams.getCategories());

        if ("delivery".equals(searchParams.getServiceOption())) {
            searchRequest.setDelivery(true);
        }
        if ("pickup".equals(searchParams.getServiceOption())) {
            searchRequest.setPickup(true);
        }

        searchRequest.setMinRating(searchParams.getRating());
        searchRequest.setMinPriceLevel(searchParams.getPriceLevel());

        if (!CollectionUtils.isEmpty(searchParams.getSorts())) {
            Set<SortOptionData> sorts = new HashSet<>();
            for (String sortId : searchParams.getSorts()) {
                if (HHHConstants.SortInfo.Restaurants.ALL_SORT_OPTIONS_MAP.containsKey(sortId)) {
                    sorts.add(HHHConstants.SortInfo.Restaurants.ALL_SORT_OPTIONS_MAP.get(sortId));
                }
            }
            searchRequest.setSorts(sorts);
        }

        searchRequest.setPageNumber(searchParams.getPageNumber());
        searchRequest.setPageSize(searchParams.getPageSize() != null ? searchParams.getPageSize() : applicationProperties.getSearch().getDefaultPageSize());

        return searchRequest;

    }

    private void updateRestaurantWithVendorRestaurant(RestaurantModel restaurant, VendorRestaurantModel vendorRestaurant) {
        // If restaurant aggregate does not already exist, create one
        if (restaurant == null) {
            restaurant = new RestaurantModel();
            restaurant.id = buildRestaurantId(vendorRestaurant.name, vendorRestaurant.address);
            restaurant.name = vendorRestaurant.name;
            restaurant.location = vendorRestaurant.location;
            restaurant.address = vendorRestaurant.address;
            restaurant.googlePlaceId = vendorRestaurant.googlePlaceId;

            if (applicationProperties.getGoogleApi().isEnabled()) {
                if (StringUtils.isNotBlank(restaurant.googlePlaceId)) {
                    PlaceDetails placeDetails = locationService.getPlaceDetails(restaurant.googlePlaceId);
                    if (placeDetails != null) {
                        restaurant.rating = placeDetails.rating;
                        if (placeDetails.priceLevel != null) {
                            restaurant.priceLevel =  Double.valueOf(placeDetails.priceLevel.toString());
                        }
                    }
                }
            }
        }

        RestaurantModel.VendorRestaurant vendorRestaurantModel = new RestaurantModel.VendorRestaurant();
        vendorRestaurantModel.id = vendorRestaurant.id;
        vendorRestaurantModel.vendor = vendorRestaurant.vendor;
        vendorRestaurantModel.vendorUrl = vendorRestaurant.vendorUrl;

        vendorRestaurantModel.delivery = vendorRestaurant.delivery;
        vendorRestaurantModel.pickup = vendorRestaurant.pickup;
        vendorRestaurantModel.rating = vendorRestaurant.rating == null ? null : vendorRestaurant.rating.floatValue();

        vendorRestaurantModel.vendorCategories = vendorRestaurant.vendorCategories;

        restaurant.suggest = new Completion(new String[]{restaurant.name});

        restaurant.vendorRestaurants.removeIf(item -> StringUtils.equals(item.id, vendorRestaurant.id));
        restaurant.vendorRestaurants.add(vendorRestaurantModel);
        restaurant.keywords = restaurant.vendorRestaurants.stream()
                .map(RestaurantModel.VendorRestaurant::getVendorCategories)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        restaurant.delivery = restaurant.delivery == null ? Boolean.FALSE : restaurant.delivery;
        restaurant.pickup = restaurant.pickup == null ? Boolean.FALSE : restaurant.pickup;

        for (RestaurantModel.VendorRestaurant vendorRestaurantItem : restaurant.vendorRestaurants) {
            restaurant.delivery |= Boolean.TRUE.equals(vendorRestaurantItem.delivery);
            restaurant.pickup |= Boolean.TRUE.equals(vendorRestaurantItem.pickup);
        }
        restaurantService.save(restaurant);

        if (!StringUtils.equals(vendorRestaurant.restaurantAggregateId, restaurant.id)) {
            vendorRestaurant.restaurantAggregateId = restaurant.id;
            vendorRestaurantService.save(vendorRestaurant);
        }
    }

    @Override
    public String buildRestaurantId(String name, String address) {

        return (name + " " + address).toLowerCase()
                .replaceAll("[^a-zA-Z0-9]", "-")
                .replaceAll("-+", "-");

    }

}
