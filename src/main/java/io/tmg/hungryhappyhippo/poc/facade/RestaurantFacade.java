package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.data.details.restaurant.RestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.RestaurantSearchParams;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSearchResponseDto;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSuggestResponseDto;

import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 28.12.2018
 */
public interface RestaurantFacade {

    RestaurantSearchResponseDto searchRestaurants(final RestaurantSearchParams searchParams, final LocationSearchParams locationSearchParams);

    RestaurantDetailsData getRestaurantDetailsData(String id, final LocationSearchParams locationSearchParams);

    VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails getVendorRestaurantAvailabilityDetails(final String vendorRestaurantId, final String vendorName, final LocationSearchParams locationSearchParams);

    Set<String> buildRestaurantsFromAllVendorRestaurants();

    String updateRestaurantWithVendorRestaurant(String vendorRestaurantId);

    void deleteVendorRestaurantFromRestaurant(String vendorRestaurantId, String restaurantId);

    void moveVendorRestaurantToRestaurant(String vendorRestaurantId, String restaurantId);

    RestaurantSuggestResponseDto suggest(String prefix, int size, final LocationSearchParams locationSearchParams);

    String buildRestaurantId(String name, String address);
}
