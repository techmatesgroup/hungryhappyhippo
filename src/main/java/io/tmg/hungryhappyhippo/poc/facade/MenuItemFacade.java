package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.data.search.request.menu.MenuSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.menu.MenuResponseData;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
public interface MenuItemFacade {

    /**
     *
     * @param restaurantId Id(that also contains Vendor name)
     * @param vendorName Name of restaurant's vendor
     *
     * @return List of menus
     */
    List<MenuCategoryData> getMenuCategoryDataList(String restaurantId, String vendorName);

    @Deprecated // TODO: cleanup menu search stuff
    MenuResponseData search(final MenuSearchParams searchParams);

}
