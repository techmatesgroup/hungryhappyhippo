package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.constants.HHHConstants;
import io.tmg.hungryhappyhippo.poc.data.search.SearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.request.SearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.SearchResponseData;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
public abstract class AbstractSearchItemFacade
        <Params extends SearchParams, Request extends SearchRequest, Response extends SearchResponseData<ItemModel>, ItemModel> {

    protected abstract Request createSearchRequest(final Params searchParams, final LocationSearchParams locationSearchParams);

    protected void populateResponse(final Response response, final Page<ItemModel> result) {

        response.setSupportedPageSizes(HHHConstants.PageInfo.ALL_PAGE_SIZES);
        response.setPageNumber(result.getPageable().getPageNumber());
        response.setPageSize(result.getPageable().getPageSize());
        response.setDataSize(result.getNumberOfElements());
        response.setTotalPages(result.getTotalPages());
        response.setTotalResults(result.getTotalElements());
        response.setHasMoreResults((result.getPageable().getPageNumber() * result.getPageable().getPageSize() + result.getNumberOfElements()) < result.getTotalElements());

        final List<ItemModel> content = result.getContent();
        response.setData(content);

    }

}
