package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;

import java.util.List;

/**
 * Facade for fetching dynamic data for restaurants: availability info, menu categories, etc
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/15/2019
 */
public interface RealTimeDataExtractorFacade {

    VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails getVendorRestaurantAvailabilityDetails(final String vendorRestaurantId, final String vendorName, final LocationSearchParams locationSearchParams);

    List<MenuCategoryData> getMenuCategoryList(final String restaurantId, final String vendorName);
}
