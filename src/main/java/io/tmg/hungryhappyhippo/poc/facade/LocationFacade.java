package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.models.dto.LocationDto;
import io.tmg.hungryhappyhippo.poc.models.dto.LocationServiceabilityDto;

import java.util.List;

public interface LocationFacade {

	List<LocationDto> getAllLocations();

	LocationServiceabilityDto getLocationServiceability(final String latitude, final String longitude);

}
