package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.constants.HHHConstants;
import io.tmg.hungryhappyhippo.poc.converter.impl.VendorRestaurantDetailsDataConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.RestaurantSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.SortOptionData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.RestaurantSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.CategoryFilterData;
import io.tmg.hungryhappyhippo.poc.data.search.response.restaurant.RestaurantFilterData;
import io.tmg.hungryhappyhippo.poc.data.search.response.restaurant.VendorRestaurantResponseData;
import io.tmg.hungryhappyhippo.poc.facade.AbstractSearchItemFacade;
import io.tmg.hungryhappyhippo.poc.facade.MenuItemFacade;
import io.tmg.hungryhappyhippo.poc.facade.RealTimeDataExtractorFacade;
import io.tmg.hungryhappyhippo.poc.facade.VendorRestaurantFacade;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.CheckVendorRestaurantExist;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSuggestResponseDto;
import io.tmg.hungryhappyhippo.poc.services.EmailSendService;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import io.tmg.hungryhappyhippo.poc.services.async.AsyncTaskService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.VendorRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.springframework.context.ApplicationContext;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static io.tmg.hungryhappyhippo.poc.constants.HHHConstants.AggregationInfo.Restaurants.CATEGORY;
import static java.util.Comparator.comparing;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 28.12.2018
 */
@Slf4j
@Service
public class DefaultVendorRestaurantFacade
        extends AbstractSearchItemFacade<RestaurantSearchParams, RestaurantSearchRequest, VendorRestaurantResponseData, VendorRestaurantModel>
        implements VendorRestaurantFacade {

    @Resource
    private ApplicationProperties applicationProperties;

    @Resource
    private VendorRestaurantService vendorRestaurantService;

    @Resource
    private VendorRestaurantDetailsDataConverter vendorRestaurantDetailsDataConverter;

    @Resource
    private MenuItemFacade menuItemFacade;

    @Resource
    private RealTimeDataExtractorFacade realTimeDataExtractorFacade;

    @Resource
    private RestaurantFacade restaurantFacade;

    @Resource
    private AsyncTaskService asyncTaskService;

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private EmailSendService emailSendService;

    @Override
    public VendorRestaurantResponseData search(final RestaurantSearchParams searchParams, final LocationSearchParams locationSearchParams) {
        final AggregatedPage<VendorRestaurantModel> result = vendorRestaurantService.searchRestaurants(createSearchRequest(searchParams, locationSearchParams));

        final VendorRestaurantResponseData response = new VendorRestaurantResponseData();

        // populate page/data info
        populateResponse(response, result);
        response.setSorts(HHHConstants.SortInfo.Restaurants.ALL_SORT_OPTIONS);
        response.setFilterData(createResponseFilterData(result, searchParams, locationSearchParams));

        return response;
    }

    @Override
    public VendorRestaurantDetailsData getRestaurantDetailsData(String restaurantId, final LocationSearchParams locationSearchParams) {
        final VendorRestaurantModel restaurantModel = vendorRestaurantService.getById(restaurantId);

        final List<MenuCategoryData> menuCategoryDataList = menuItemFacade.getMenuCategoryDataList(restaurantModel.getId(), restaurantModel.getVendor());

        final VendorRestaurantDetailsData restaurantDetailsData = vendorRestaurantDetailsDataConverter.convert(restaurantModel);
        restaurantDetailsData.setMenuCategoryDataList(menuCategoryDataList);

        VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails restaurantAvailabilityDetails;
        try {
            restaurantAvailabilityDetails = realTimeDataExtractorFacade.getVendorRestaurantAvailabilityDetails(restaurantId, restaurantDetailsData.getVendor(), locationSearchParams);
            restaurantDetailsData.setRestaurantAvailabilityDetails(restaurantAvailabilityDetails);
        } catch (Exception ignored) {
        }

        return restaurantDetailsData;
    }

    @Override
    public void unlinkVendorRestaurantsFromRestaurant(Collection<String> vendorRestaurantIds, String restaurantId) {
        vendorRestaurantIds.forEach(vendorRestaurantId -> {
            vendorRestaurantService.findById(vendorRestaurantId)
                    .ifPresent(vendorRestaurant -> {
                        if (StringUtils.equals(vendorRestaurant.restaurantAggregateId, restaurantId)) {
                            vendorRestaurant.restaurantAggregateId = null;
                            vendorRestaurantService.save(vendorRestaurant);
                        }
                    });
        });
    }

    @Override
    public void unlinkVendorRestaurant(String vendorRestaurantId) {

        vendorRestaurantService.findById(vendorRestaurantId)
                .ifPresent(vendorRestaurantModel -> {
                    // clear vendorRestaurant in restaurantAggregate
                    restaurantFacade.deleteVendorRestaurantFromRestaurant(vendorRestaurantId, vendorRestaurantModel.restaurantAggregateId);

                    // clear restaurantAggregateId
                    vendorRestaurantModel.restaurantAggregateId = null;
                    vendorRestaurantService.save(vendorRestaurantModel);
                });

    }

    @Override
    public RestaurantSuggestResponseDto suggest(String prefix, int size, LocationSearchParams locationSearchParams) {

        return new RestaurantSuggestResponseDto(vendorRestaurantService.suggest(prefix, size, locationSearchParams));

    }

    @Override
    public Integer checkVendorRestaurantExistence() throws MessagingException {

        log.debug("checkVendorRestaurantExistence started");

        final Collection<VendorRestaurantExtractor> extractors = applicationContext.getBeansOfType(VendorRestaurantExtractor.class).values();
        final List<Future<CheckVendorRestaurantExist>> futureList = new ArrayList<>();
        vendorRestaurantService.getRestaurants().forEach(vendorRestaurantModel -> {

            final VendorRestaurantExtractor vendorRestaurantExtractor = IterableUtils.find(extractors, extractor -> extractor.getVendor().equals(Vendor.getByName(vendorRestaurantModel.getVendor())));

            final LocationSearchParams locationSearchParams = new LocationSearchParams(
                    String.valueOf(vendorRestaurantModel.getLocation().getLat()),
                    String.valueOf(vendorRestaurantModel.getLocation().getLon()),
                    vendorRestaurantModel.getAddress()
            );

            futureList.add(asyncTaskService.performAsyncCheckExistence(vendorRestaurantExtractor, vendorRestaurantModel, locationSearchParams));

        });

        final List<CheckVendorRestaurantExist> checkVendorRestaurantExistList = new ArrayList<>();
        futureList.forEach(future -> {
            try {
                checkVendorRestaurantExistList.add(future.get(1, TimeUnit.MINUTES));
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                log.warn("CheckExistence future get error", e);
            }
        });

        CollectionUtils.filter(checkVendorRestaurantExistList, existResult -> !existResult.isSuccessCheck());

        log.debug("VendorRestaurants with errors - {}", checkVendorRestaurantExistList);

        if (!checkVendorRestaurantExistList.isEmpty()) {
            checkVendorRestaurantExistList.sort(Comparator.comparing(item -> item.getVendorRestaurantModel().getVendor()));
            emailSendService.sendVendorRestaurantsExistReportEmail(checkVendorRestaurantExistList);
        }

        return checkVendorRestaurantExistList.size();

    }

    @Override
    protected RestaurantSearchRequest createSearchRequest(RestaurantSearchParams searchParams, LocationSearchParams locationSearchParams) {

        final RestaurantSearchRequest searchRequest = new RestaurantSearchRequest();

        searchRequest.setSearchTerms(searchParams.getSearchTerms());

        searchRequest.setLatitude(Double.valueOf(locationSearchParams.getLatitude()));
        searchRequest.setLongitude(Double.valueOf(locationSearchParams.getLongitude()));
        searchRequest.setDistance(Double.valueOf(searchParams.getDistance()));

        searchRequest.setVendors(searchParams.getVendors());
        searchRequest.setCategories(searchParams.getCategories());

        if ("delivery".equals(searchParams.getServiceOption())) {
            searchRequest.setDelivery(true);
        }
        if ("pickup".equals(searchParams.getServiceOption())) {
            searchRequest.setPickup(true);
        }

        searchRequest.setMinRating(searchParams.getRating());

        if (!CollectionUtils.isEmpty(searchParams.getSorts())) {
            Set<SortOptionData> sorts = new HashSet<>();
            for (String sortId : searchParams.getSorts()) {
                if (HHHConstants.SortInfo.Restaurants.ALL_SORT_OPTIONS_MAP.containsKey(sortId)) {
                    sorts.add(HHHConstants.SortInfo.Restaurants.ALL_SORT_OPTIONS_MAP.get(sortId));
                }
            }
            searchRequest.setSorts(sorts);
        }

        searchRequest.setPageNumber(searchParams.getPageNumber());
        searchRequest.setPageSize(searchParams.getPageSize() != null ? searchParams.getPageSize() : applicationProperties.getSearch().getDefaultPageSize());

        return searchRequest;

    }

    private RestaurantFilterData createResponseFilterData(AggregatedPage<VendorRestaurantModel> result, RestaurantSearchParams searchParams, LocationSearchParams locationSearchParams) {
        RestaurantFilterData filterData = new RestaurantFilterData();

        filterData.setRating(searchParams.getRating());
        filterData.setServiceOption(searchParams.getServiceOption());
        filterData.setSearchTerms(searchParams.getSearchTerms());
        filterData.setLatitude(locationSearchParams.getLatitude());
        filterData.setLongitude(locationSearchParams.getLongitude());
        filterData.setDistance(searchParams.getDistance());
        filterData.setVendorCategories(getAllVendorCategoriesWithAggregationData(result, searchParams));

        return filterData;
    }

    private List<CategoryFilterData> getAllVendorCategoriesWithAggregationData(AggregatedPage<VendorRestaurantModel> results, final RestaurantSearchParams searchParams) {
        return ((Terms) results.getAggregation(CATEGORY)).getBuckets().stream()
                .map(bucket -> new CategoryFilterData(
                        bucket.getKeyAsString(),
                        (int) bucket.getDocCount(),
                        searchParams.getCategories() != null && searchParams.getCategories().contains(bucket.getKeyAsString())))
                .sorted(comparing(CategoryFilterData::isChecked)
                        .reversed()
                        .thenComparing(CategoryFilterData::getName, String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList());
    }


}
