package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.constants.HHHConstants;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.data.search.MenuSearchRequest;
import io.tmg.hungryhappyhippo.poc.data.search.SortOptionData;
import io.tmg.hungryhappyhippo.poc.data.search.request.menu.MenuSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.menu.MenuFilterData;
import io.tmg.hungryhappyhippo.poc.data.search.response.menu.MenuResponseData;
import io.tmg.hungryhappyhippo.poc.facade.AbstractSearchItemFacade;
import io.tmg.hungryhappyhippo.poc.facade.MenuItemFacade;
import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import io.tmg.hungryhappyhippo.poc.services.MenuItemService;
import io.tmg.hungryhappyhippo.poc.facade.RealTimeDataExtractorFacade;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/16/2019
 */
@Service
public class DefaultMenuItemFacade
        extends AbstractSearchItemFacade<MenuSearchParams, MenuSearchRequest, MenuResponseData, MenuItemModel>
        implements MenuItemFacade {

    @Resource
    private MenuItemService menuItemService;

    @Resource
    private RealTimeDataExtractorFacade realTimeDataExtractorFacade;

    @Override
    public List<MenuCategoryData> getMenuCategoryDataList(String restaurantId, String vendorName) {

        return realTimeDataExtractorFacade.getMenuCategoryList(restaurantId, vendorName);

    }

    @Override
    public MenuResponseData search(final MenuSearchParams searchParams) {
        final Page<MenuItemModel> result = menuItemService.search(createSearchRequest(searchParams, null));

        final MenuResponseData response = new MenuResponseData();

        // populate page/data info
        populateResponse(response, result);
        response.setSorts(HHHConstants.SortInfo.Menus.ALL_SORT_OPTIONS);

        response.setFilterData(new MenuFilterData(searchParams.getSearchTerms()));

        return response;
    }

    @Override
    protected MenuSearchRequest createSearchRequest(MenuSearchParams searchParams, final LocationSearchParams locationSearchParams) {

        final MenuSearchRequest searchRequest = new MenuSearchRequest();

        final String[] priceRange = searchParams.getPriceRange();
        if (priceRange != null && priceRange.length >= 2) {
            String minPrice = priceRange[0];
            String maxPrice = priceRange[1];
            searchRequest.setMinPrice(NumberUtils.isParsable(minPrice) ? Double.parseDouble(minPrice) : null);
            searchRequest.setMaxPrice(NumberUtils.isParsable(maxPrice) ? Double.parseDouble(maxPrice) : null);
        }

        searchRequest.setSearchTerms(searchParams.getSearchTerms());
        if (!CollectionUtils.isEmpty(searchParams.getSorts())) {
            Set<SortOptionData> sorts = new HashSet<>();
            for (String sortId : searchParams.getSorts()) {
                if (HHHConstants.SortInfo.Menus.ALL_SORT_OPTIONS_MAP.containsKey(sortId)) {
                    sorts.add(HHHConstants.SortInfo.Menus.ALL_SORT_OPTIONS_MAP.get(sortId));
                }
            }
            searchRequest.setSorts(sorts);
        }

        searchRequest.setPageNumber(searchParams.getPageNumber());
        searchRequest.setPageSize(searchParams.getPageSize());

        return searchRequest;
    }
}
