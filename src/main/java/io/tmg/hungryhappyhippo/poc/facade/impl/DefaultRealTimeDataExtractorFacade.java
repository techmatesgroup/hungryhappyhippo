package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.converter.impl.DeliveryComMenuItemConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.DoorDashMenuItemConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.FavorDeliveryMenuItemConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.GrubHubMenuItemConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.PostmatesMenuItemConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.UberEatsMenuItemConverter;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.MenuCategoryData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.error.rest.ApiException;
import io.tmg.hungryhappyhippo.poc.facade.RealTimeDataExtractorFacade;
import io.tmg.hungryhappyhippo.poc.models.Vendor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl.DeliveryComMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl.DoorDashMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl.FavorDeliveryMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl.GrubHubMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl.PostmatesMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.online.impl.UberEatsMenuItemOnlineExtractorService;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.DeliverycomRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.DoorDashRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.FavorDeliveryRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.GrubhubRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.PostmatesRestaurantExtractor;
import io.tmg.hungryhappyhippo.poc.vendor.data.extraction.restaurant.impl.UberEatsRestaurantExtractor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/15/2019
 */
@Slf4j
@Service
public class DefaultRealTimeDataExtractorFacade implements RealTimeDataExtractorFacade {

    @Resource
    private DeliverycomRestaurantExtractor deliverycomRestaurantExtractor;

    @Resource
    private DoorDashRestaurantExtractor doorDashRestaurantExtractor;

    @Resource
    private FavorDeliveryRestaurantExtractor favorDeliveryRestaurantExtractor;

    @Resource
    private GrubhubRestaurantExtractor grubhubRestaurantExtractor;

    @Resource
    private PostmatesRestaurantExtractor postmatesRestaurantExtractor;

    @Resource
    private UberEatsRestaurantExtractor uberEatsRestaurantExtractor;

    @Resource
    private PostmatesMenuItemConverter postmatesMenuItemConverter;

    @Resource
    private PostmatesMenuItemOnlineExtractorService postmatesMenuItemOnlineExtractorService;

    @Resource
    private GrubHubMenuItemConverter grubHubMenuItemConverter;

    @Resource
    private GrubHubMenuItemOnlineExtractorService grubHubMenuItemOnlineExtractorService;

    @Resource
    private UberEatsMenuItemConverter uberEatsMenuItemConverter;

    @Resource
    private UberEatsMenuItemOnlineExtractorService uberEatsMenuItemOnlineExtractorService;

    @Resource
    private FavorDeliveryMenuItemConverter favorDeliveryMenuItemConverter;

    @Resource
    private FavorDeliveryMenuItemOnlineExtractorService favorDeliveryMenuItemOnlineExtractorService;

    @Resource
    private DeliveryComMenuItemConverter deliveryComMenuItemConverter;

    @Resource
    private DeliveryComMenuItemOnlineExtractorService deliveryComMenuItemOnlineExtractorService;

    @Resource
    private DoorDashMenuItemConverter doorDashMenuItemConverter;

    @Resource
    private DoorDashMenuItemOnlineExtractorService doorDashMenuItemOnlineExtractorService;

    @Override
    public VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails getVendorRestaurantAvailabilityDetails(final String vendorRestaurantId, final String vendorName, final LocationSearchParams locationSearchParams) {

        VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails;
        try {
            String vendorRestaurantIdWithoutVendorPrefix = vendorRestaurantId.replace(vendorName + "-", "");

            switch (Vendor.getByName(vendorName)) {
                case DELIVERY_COM:
                    availabilityDetails = deliverycomRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantIdWithoutVendorPrefix, locationSearchParams);
                    break;

                case DOORDASH:
                    availabilityDetails = doorDashRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantIdWithoutVendorPrefix, locationSearchParams);
                    break;

                case FAVORDELIVERY:
                    availabilityDetails = favorDeliveryRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantIdWithoutVendorPrefix, locationSearchParams);
                    break;

                case GRUBHUB:
                    availabilityDetails = grubhubRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantIdWithoutVendorPrefix, locationSearchParams);
                    break;

                case POSTMATES:
                    availabilityDetails = postmatesRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantIdWithoutVendorPrefix, locationSearchParams);
                    break;

                case UBEREATS:
                    availabilityDetails = uberEatsRestaurantExtractor.fetchRestaurantAvailabilityDetails(vendorRestaurantIdWithoutVendorPrefix, locationSearchParams);
                    break;

                default:
                    throw new UnsupportedOperationException(String.format("The following vendor is not supported for fetching real time restaurant details: %s", vendorName));
            }

        } catch (Exception e) {
            log.warn("An error occurred fetching realtime vendor restaurant details: {}: {}", vendorRestaurantId, locationSearchParams, e);
            throw new ApiException(HttpStatus.NOT_FOUND, e);
        }

        postProcessAvailabilityDetails(availabilityDetails);

        return availabilityDetails;

    }

    @Override
    public List<MenuCategoryData> getMenuCategoryList(String restaurantId, String vendorName) {

        String vendorRestaurantId = restaurantId.replace(vendorName + "-", "");

        try {
            final Vendor vendor = Vendor.getByName(vendorName);

            List<MenuCategoryData> menuCategoryDataList;
            switch (vendor) {

                case POSTMATES:
                    menuCategoryDataList = postmatesMenuItemConverter.convert(postmatesMenuItemOnlineExtractorService.fetchMenuItems(vendorRestaurantId));
                    break;

                case GRUBHUB:
                    menuCategoryDataList = grubHubMenuItemConverter.convert(grubHubMenuItemOnlineExtractorService.fetchMenuItems(vendorRestaurantId));
                    break;

                case UBEREATS:
                    menuCategoryDataList = uberEatsMenuItemConverter.convert(uberEatsMenuItemOnlineExtractorService.fetchMenuItems(vendorRestaurantId));
                    break;

                case FAVORDELIVERY:
                    menuCategoryDataList = favorDeliveryMenuItemConverter.convert(favorDeliveryMenuItemOnlineExtractorService.fetchMenuItems(vendorRestaurantId));
                    break;

                case DELIVERY_COM:
                    menuCategoryDataList = deliveryComMenuItemConverter.convert(deliveryComMenuItemOnlineExtractorService.fetchMenuItems(vendorRestaurantId));
                    break;

                case DOORDASH:
                    menuCategoryDataList = doorDashMenuItemConverter.convert(doorDashMenuItemOnlineExtractorService.fetchMenuItems(vendorRestaurantId));
                    break;

                default:
                    throw new UnsupportedOperationException(String.format("The following vendor is not supported for fetching menus: %s", vendorName));
            }

            populateIds(menuCategoryDataList, vendor);
            return menuCategoryDataList;

        } catch (Exception e) {
            log.warn("An error occurred during processing get menus for restaurant - {}", restaurantId, e);
            return Collections.emptyList();
        }

    }

    private void populateIds(List<MenuCategoryData> menuCategoryDataList, Vendor vendor) {
        final String vendorPrefix = vendor.name();
        for (MenuCategoryData menuCategoryData : menuCategoryDataList) {
            menuCategoryData.setId(String.format("%s-%s"
                    , vendorPrefix
                    , StringUtils.isNotEmpty(menuCategoryData.getId()) ? menuCategoryData.getId() : UUID.randomUUID().toString()));

            for (MenuCategoryData.ProductItem productItem : menuCategoryData.getProductItemList()) {
                productItem.setId(String.format("%s-%s"
                        , vendorPrefix
                        , StringUtils.isNotEmpty(productItem.getId()) ? productItem.getId() : UUID.randomUUID().toString()));
            }
        }
    }

    private void postProcessAvailabilityDetails(VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails) {

        if (availabilityDetails != null) {

            if (BooleanUtils.isNotTrue(availabilityDetails.getPickupAvailable())) {
                availabilityDetails.setPickupEstimate(null);
                availabilityDetails.setPickupOrderMinimum(null);
            }

            if (BooleanUtils.isNotTrue(availabilityDetails.getDeliveryAvailable())) {
                availabilityDetails.setDeliveryEstimate(null);
                availabilityDetails.setDeliveryFee(null);
                availabilityDetails.setDeliveryOrderMinimum(null);
            }

            if (availabilityDetails.getDeliverableToTargetAddress() == null) {
                availabilityDetails.setDeliverableToTargetAddress(Boolean.TRUE);
            }

        }

    }
}
