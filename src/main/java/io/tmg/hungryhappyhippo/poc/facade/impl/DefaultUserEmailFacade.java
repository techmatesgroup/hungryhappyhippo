package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.data.email.AttachData;
import io.tmg.hungryhappyhippo.poc.data.email.FeedbackUserData;
import io.tmg.hungryhappyhippo.poc.facade.UserEmailFacade;
import io.tmg.hungryhappyhippo.poc.services.async.AsyncTaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/25/2019
 */
@Slf4j
@Service
public class DefaultUserEmailFacade implements UserEmailFacade {

	@Resource
	private AsyncTaskService asyncTaskService;

	@Override
	public void sendFeedbackEmail(String userName, String userEmail, String feedbackMessage, List<MultipartFile> attachments) {
		try {

			asyncTaskService.performSendFeedbackEmail(new FeedbackUserData(userName, userEmail, feedbackMessage, getAttachmentData(attachments)));

		} catch (Exception skipped) {
			log.warn("sendFeedbackEmail error", skipped);
		}
	}

	private List<AttachData> getAttachmentData(List<MultipartFile> attachments) {

		final List<AttachData> attachmentDataList = new ArrayList<>();
		for (MultipartFile attachment : attachments) {

			String fileName = StringUtils.isNotEmpty(attachment.getOriginalFilename()) ? attachment.getOriginalFilename() : attachment.getName();
			try {
				final byte[] data = attachment.getBytes();
				if (ArrayUtils.isNotEmpty(data)) {
					attachmentDataList.add(new AttachData(fileName, data));
				}
			} catch (IOException e) {
				log.warn("I/O error occurred for - {}", fileName);
			}

		}
		return attachmentDataList;

	}
}
