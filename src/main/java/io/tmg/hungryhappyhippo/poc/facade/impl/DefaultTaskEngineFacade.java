package io.tmg.hungryhappyhippo.poc.facade.impl;

import java.util.List;

import javax.annotation.Resource;

import io.tmg.hungryhappyhippo.poc.config.ApplicationProperties;
import io.tmg.hungryhappyhippo.poc.converter.impl.JobToTaskConverter;
import io.tmg.hungryhappyhippo.poc.converter.impl.TaskDataConverter;
import io.tmg.hungryhappyhippo.poc.facade.TaskEngineFacade;
import io.tmg.hungryhappyhippo.poc.models.JobModel;
import io.tmg.hungryhappyhippo.poc.models.TaskEngineStatus;
import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.dto.TaskDto;
import io.tmg.hungryhappyhippo.poc.services.JobService;
import io.tmg.hungryhappyhippo.poc.services.TaskService;
import org.springframework.stereotype.Service;

@Service
public class DefaultTaskEngineFacade implements TaskEngineFacade {

	@Resource
	private ApplicationProperties applicationProperties;

	@Resource
	private TaskService taskService;

	@Resource
	private JobService jobService;

	@Resource
	private TaskDataConverter taskDataConverter;

	@Resource
	private JobToTaskConverter jobToTaskConverter;

	@Override
	public boolean isTaskEngineEnabled() {

		return taskService.isTaskEngineEnabled();

	}

	@Override
	public boolean isTaskEngineRunning() throws Exception {

		return taskService.isTaskEngineRunning();

	}

	@Override
	public boolean isTaskEnginePaused() throws Exception {

		return taskService.isTaskEnginePaused();

	}

	@Override
	public boolean isTaskEngineShutdown() throws Exception {

		return taskService.isTaskEngineShutdown();

	}

	@Override
	public TaskEngineStatus getTaskEngineStatus() throws Exception {

		TaskEngineStatus taskEngineStatus = TaskEngineStatus.UNKNOWN;

		if (isTaskEngineShutdown() || !isTaskEngineRunning()) {
			taskEngineStatus = TaskEngineStatus.SHUTDOWN;
		}
		else if (isTaskEnginePaused()) {
			taskEngineStatus = TaskEngineStatus.PAUSED;
		}
		else if (isTaskEngineRunning()) {
			taskEngineStatus = TaskEngineStatus.RUNNING;
		}

		return taskEngineStatus;

	}

	@Override
	public void startTaskEngine() throws Exception {

		taskService.startTaskEngine();

	}

	@Override
	public void pauseTaskEngine() throws Exception {

		taskService.pauseTaskEngine();

	}

	@Override
	public void resumeTaskEngine() throws Exception {

		taskService.resumeTaskEngine();

	}

	@Override
	public void clearTaskEngine() throws Exception {

		taskService.clearTaskEngine();

	}

	@Override
	public void destroyTaskEngine() throws Exception {

		taskService.destroyTaskEngine();

	}

	@Override
	public List<TaskDto> getPersistedTasks() {

		return taskDataConverter.convertAll(taskService.getAllPersistedTasks());

	}

	@Override
	public List<TaskDto> getLoadedTasks() throws Exception {

		return taskDataConverter.convertAll(taskService.getLoadedTasks());

	}

	@Override
	public TaskDto getPersistedTaskDetails(String id) {

		return taskDataConverter.convert(taskService.getById(id));

	}

	@Override
	public TaskDto getLoadedTaskDetails(String taskId) throws Exception {

		return taskDataConverter.convert(taskService.getLoadedTask(taskId));

	}

	@Override
	public void loadTasksFromAllJobs() throws Exception {

		for (JobModel job : jobService.getJobs()) {
			loadTaskFromJob(job.id);
		}

	}

	@Override
	public void loadTaskFromJob(String jobId) throws Exception {

		taskService.loadTask(jobToTaskConverter.convert(jobService.getById(jobId)));

	}

	@Override
	public void scheduleTaskFromJob(String jobId) throws Exception {

		taskService.scheduleTask(jobToTaskConverter.convert(jobService.getById(jobId)));

	}

	@Override
	public void loadTasksFromAllPersistentTasks() throws Exception {

		for (TaskModel task : taskService.getAllPersistedTasks()) {
			loadTaskFromPersistentTask(task.id);
		}

	}

	@Override
	public void loadTaskFromPersistentTask(String taskId) throws Exception {

		taskService.loadTask(taskService.getById(taskId));

	}

	@Override
	public void executeTask(String taskId) throws Exception {

		taskService.executeTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void scheduleTask(String taskId) throws Exception {

		taskService.scheduleTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void unscheduleTask(String taskId) throws Exception {

		taskService.unscheduleTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void pauseTask(String taskId) throws Exception {

		taskService.pauseTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void resumeTask(String taskId) throws Exception {

		taskService.resumeTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void stopTask(String taskId) throws Exception {

		taskService.stopTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void removeTask(String taskId) throws Exception {

		taskService.unloadTask(taskService.getLoadedTask(taskId));

	}

	@Override
	public void saveLoadedTasks() throws Exception {

		taskService.saveLoadedTasks();

	}

	@Override
	public void saveLoadedTask(String taskId) throws Exception {

		taskService.saveLoadedTask(taskService.getLoadedTask(taskId));

	}

}
