package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.models.dto.CategoryDto;

import java.util.List;

public interface CategoryFacade {

	List<CategoryDto> getAllCategories();

}
