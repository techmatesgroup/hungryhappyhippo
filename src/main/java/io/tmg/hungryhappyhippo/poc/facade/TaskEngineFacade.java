package io.tmg.hungryhappyhippo.poc.facade;

import java.util.List;

import io.tmg.hungryhappyhippo.poc.models.TaskEngineStatus;
import io.tmg.hungryhappyhippo.poc.models.dto.TaskDto;

public interface TaskEngineFacade {

	boolean isTaskEngineEnabled();

	boolean isTaskEngineRunning() throws Exception;

	boolean isTaskEnginePaused() throws Exception;

	boolean isTaskEngineShutdown() throws Exception;

	TaskEngineStatus getTaskEngineStatus() throws Exception;

	void startTaskEngine() throws Exception;

	void pauseTaskEngine() throws Exception;

	void resumeTaskEngine() throws Exception;

	void clearTaskEngine() throws Exception;

	void destroyTaskEngine() throws Exception;

	List<TaskDto> getPersistedTasks();

	List<TaskDto> getLoadedTasks() throws Exception;

	TaskDto getPersistedTaskDetails(String id);

	TaskDto getLoadedTaskDetails(String taskId) throws Exception;

	void loadTasksFromAllJobs() throws Exception;

	void loadTaskFromJob(String jobId) throws Exception;

	void scheduleTaskFromJob(String jobId) throws Exception;

	void loadTasksFromAllPersistentTasks() throws Exception;

	void loadTaskFromPersistentTask(String taskId) throws Exception;

	void executeTask(String taskId) throws Exception;

	void scheduleTask(String taskId) throws Exception;

	void unscheduleTask(String taskId) throws Exception;

	void pauseTask(String taskId) throws Exception;

	void resumeTask(String taskId) throws Exception;

	void stopTask(String taskId) throws Exception;

	void removeTask(String taskId) throws Exception;

	void saveLoadedTasks() throws Exception;

	void saveLoadedTask(String taskId) throws Exception;

}
