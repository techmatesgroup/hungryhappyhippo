package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.facade.VendorFacade;
import io.tmg.hungryhappyhippo.poc.models.VendorModel;
import io.tmg.hungryhappyhippo.poc.models.dto.VendorDto;
import io.tmg.hungryhappyhippo.poc.services.VendorService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultVendorFacade implements VendorFacade {

	@Resource
	private VendorService vendorService;

	@Resource
	private ModelMapper modelMapper;

	@Override
	public List<VendorDto> getAllVendors() {

		List<VendorDto> vendors = new ArrayList<>();

		for (VendorModel vendorModel : vendorService.getVendors()) {
			vendors.add(modelMapper.map(vendorModel, VendorDto.class));
		}

		return vendors;

	}

}
