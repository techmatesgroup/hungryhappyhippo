package io.tmg.hungryhappyhippo.poc.facade;

import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.RestaurantSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.restaurant.VendorRestaurantResponseData;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSuggestResponseDto;

import javax.mail.MessagingException;
import java.util.Collection;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 28.12.2018
 */
public interface VendorRestaurantFacade {

    VendorRestaurantResponseData search(final RestaurantSearchParams searchParams, final LocationSearchParams locationSearchParams);

    VendorRestaurantDetailsData getRestaurantDetailsData(String restaurantId, final LocationSearchParams locationSearchParams);

    void unlinkVendorRestaurantsFromRestaurant(Collection<String> vendorRestaurantIds, String restaurantId);

    void unlinkVendorRestaurant(String vendorRestaurantId);

    RestaurantSuggestResponseDto suggest(String prefix, int size, final LocationSearchParams locationSearchParams);

    Integer checkVendorRestaurantExistence() throws MessagingException;

}
