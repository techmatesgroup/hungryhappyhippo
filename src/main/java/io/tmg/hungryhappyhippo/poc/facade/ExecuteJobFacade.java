package io.tmg.hungryhappyhippo.poc.facade;

import javax.mail.MessagingException;

/**
 * Common facade for job execution
 * The methods should be invoked by groovy
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/22/2019
 */
public interface ExecuteJobFacade {

	String createGeneralMetricsReport() throws MessagingException;

	String serviceHealthCheckReport() throws MessagingException;

	// TODO move other methods that are declared in job.csv

}
