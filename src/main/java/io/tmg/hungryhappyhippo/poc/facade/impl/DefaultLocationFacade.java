package io.tmg.hungryhappyhippo.poc.facade.impl;

import io.tmg.hungryhappyhippo.poc.facade.LocationFacade;
import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.dto.LocationDto;
import io.tmg.hungryhappyhippo.poc.models.dto.LocationServiceabilityDto;
import io.tmg.hungryhappyhippo.poc.services.LocationService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultLocationFacade implements LocationFacade {

	@Resource
	private LocationService locationService;

	@Resource
	private ModelMapper modelMapper;

	@Override
	public List<LocationDto> getAllLocations() {

		List<LocationDto> locations = new ArrayList<>();

		for (LocationModel locationModel : locationService.getLocations()) {
			locations.add(toDto(locationModel));
		}

		return locations;

	}

	@Override
	public LocationServiceabilityDto getLocationServiceability(String latitude, String longitude) {

		LocationServiceabilityDto locationSupportedResponse = null;

		final LocationModel closestSupportedLocation = locationService.getClosestSupportedLocation(latitude, longitude);
		if (closestSupportedLocation != null) {
			locationSupportedResponse = new LocationServiceabilityDto(true, toDto(closestSupportedLocation));
		}
		else {
			locationSupportedResponse = new LocationServiceabilityDto(false, null);
		}

		return locationSupportedResponse;

	}

	private LocationDto toDto(LocationModel model) {
		final LocationDto location = modelMapper.map(model, LocationDto.class);

		location.setLatitude(model.location.getLat());
		location.setLongitude(model.location.getLon());

		return location;
	}

}
