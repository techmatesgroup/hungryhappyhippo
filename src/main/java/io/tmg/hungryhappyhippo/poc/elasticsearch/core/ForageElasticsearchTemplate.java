package io.tmg.hungryhappyhippo.poc.elasticsearch.core;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.springframework.data.elasticsearch.ElasticsearchException;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.convert.ElasticsearchConverter;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentEntity;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentProperty;
import org.springframework.util.StringUtils;

import static io.tmg.hungryhappyhippo.poc.elasticsearch.core.ForageMappingBuilder.buildMapping;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 03/15/2019
 */
@Slf4j
public class ForageElasticsearchTemplate extends ElasticsearchTemplate {

	public ForageElasticsearchTemplate(Client client, ElasticsearchConverter elasticsearchConverter) {
		super(client, elasticsearchConverter);
	}

	@Override
	public <T> boolean putMapping(Class<T> clazz) {
		if (clazz.isAnnotationPresent(Mapping.class)) {
			String mappingPath = clazz.getAnnotation(Mapping.class).mappingPath();
			if (!StringUtils.isEmpty(mappingPath)) {
				String mappings = readFileFromClasspath(mappingPath);
				if (!StringUtils.isEmpty(mappings)) {
					return putMapping(clazz, mappings);
				}
			} else {
				log.info("mappingPath in @Mapping has to be defined. Building mappings using @Field");
			}
		}
		ElasticsearchPersistentEntity<T> persistentEntity = getPersistentEntityFor(clazz);
		XContentBuilder xContentBuilder = null;
		try {

			ElasticsearchPersistentProperty property = persistentEntity.getRequiredIdProperty();

			xContentBuilder = buildMapping(clazz, persistentEntity.getIndexType(),
					property.getFieldName(), persistentEntity.getParentType());
		} catch (Exception e) {
			throw new ElasticsearchException("Failed to build mapping for " + clazz.getSimpleName(), e);
		}
		return putMapping(clazz, xContentBuilder);
	}

}
