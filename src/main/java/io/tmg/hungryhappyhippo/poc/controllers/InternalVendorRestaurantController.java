package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck;
import io.tmg.hungryhappyhippo.poc.constants.HHHConstants;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.RestaurantSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.restaurant.RestaurantFilterData;
import io.tmg.hungryhappyhippo.poc.data.search.response.restaurant.VendorRestaurantResponseData;
import io.tmg.hungryhappyhippo.poc.facade.VendorRestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantSuggestResponseDto;
import io.tmg.hungryhappyhippo.poc.utils.LocationUtil;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Controller
@RequestMapping(value="/internal/vendorrestaurants")
public class InternalVendorRestaurantController extends AbstractController {

	@Resource
	private VendorRestaurantFacade vendorRestaurantFacade;

	@Resource
	private List<String> allVendorNames;

	@Override
	public String pageTitle() {

		return "Vendor Restaurant Search";

	}

	@Override
	public String pageContent() {

		return "vendorRestaurantSearchPage";

	}

	@GetMapping("/suggest")
	@ResponseBody
	@LocationCookieCheck(required = false)
	public ResponseEntity<RestaurantSuggestResponseDto> suggest(
			@RequestParam(name = "term", defaultValue = "") String prefix,
			@RequestParam(name = "size", defaultValue = "10") int size,
			final LocationSearchParams locationSearchParams) {

		return ResponseEntity.ok(vendorRestaurantFacade.suggest(prefix, size, locationSearchParams));

	}

	@GetMapping("/search")
	@LocationCookieCheck
	public ModelAndView searchRestaurants(final RestaurantSearchParams searchParams, final LocationSearchParams locationSearchParams) {

		log.trace("searchParams - {}, locationSearchParams - {}", searchParams, locationSearchParams);
		if (!LocationUtil.isSearchDistanceWithinValidRange(searchParams.getDistance())) {
			ModelAndView mav = getRestaurantsView(searchParams, new VendorRestaurantResponseData(new RestaurantFilterData()));
			addErrorNotification(mav, "Missing or invalid distance parameters were detected, please specify valid distance");
			return mav;
		}

		VendorRestaurantResponseData results = vendorRestaurantFacade.search(searchParams, locationSearchParams);

		return getRestaurantsView(searchParams, results);

	}

	private ModelAndView getRestaurantsView(RestaurantSearchParams searchParams, VendorRestaurantResponseData result) {
		ModelAndView mav;
		if (searchParams.isJson()) {
			mav = getJsonView(result);
		} else {
			mav = getView(pageTitle());

			mav.addObject("result", result);
			mav.addObject("allVendors", allVendorNames);
			mav.addObject("allPageSizes", HHHConstants.PageInfo.ALL_PAGE_SIZES);
			mav.addObject("allSorts", HHHConstants.SortInfo.Restaurants.ALL_SORT_OPTIONS);
			mav.addObject("filterData", result.getFilterData());
		}
		return mav;
	}

}
