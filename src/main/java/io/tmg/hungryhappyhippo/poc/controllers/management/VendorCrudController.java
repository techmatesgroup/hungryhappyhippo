package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.models.VendorModel;
import io.tmg.hungryhappyhippo.poc.models.dto.VendorDto;
import io.tmg.hungryhappyhippo.poc.services.VendorService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.controllers.management.VendorCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.VendorCrudController.ROOT;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class VendorCrudController extends AbstractCrudController<VendorModel, VendorDto> {

    static final String ROOT = "management";
    static final String PATH = "vendors";

    @Resource
	private VendorService vendorService;
    @Resource
    private ModelMapper modelMapper;

	@Override
	public String pageTitle() {
		return "Vendors Management";
	}

    @Override
    protected String getRootPath() {
        return ROOT;
    }

    @Override
    protected String getModelName() {
        return PATH;
    }

    @Override
    protected Class<VendorDto> getDtoClass() {
        return VendorDto.class;
    }

    @Override
    public VendorService getCrudService() {
        return vendorService;
    }

    @Override
	protected VendorDto toDto(VendorModel model) {
		VendorDto dto = modelMapper.map(model, VendorDto.class);
		
		return dto;
	}

    @Override
	protected VendorModel fromDto(VendorDto dto) {
		VendorModel model = new VendorModel(dto.name, dto.websiteUrl);
		if (StringUtils.isNotBlank(dto.id)) {
		    model.id = dto.id;
        }

		return model;
	}

}
