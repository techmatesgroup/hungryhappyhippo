package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.data.search.request.menu.MenuSearchParams;
import io.tmg.hungryhappyhippo.poc.data.search.response.menu.MenuResponseData;
import io.tmg.hungryhappyhippo.poc.facade.MenuItemFacade;
import io.tmg.hungryhappyhippo.poc.services.MenuItemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Collections;

@Controller
@RequestMapping(value="/internal")
public class InternalMenuController extends AbstractController {

	@Override
	public String pageTitle() {

		return "MenuSearchPage";

	}

	@Override
	public String pageContent() {

		return "menuSearchPage";

	}

	@Resource
	private MenuItemService menuItemService;

	@Resource
	private MenuItemFacade menuItemFacade;

	@GetMapping(value="/menus/{menuId}")
	public ModelAndView getMenu(@PathVariable String menuId) {

		ModelAndView mav = new ModelAndView(pageLayout());

		mav.addObject(pageContent(), Collections.singleton(menuItemService.getById(menuId)));

		return mav;

	}

	@GetMapping(value="/restaurants/{restaurantId}/menu")
	public ModelAndView getRestaurantMenu(@PathVariable String restaurantId) {

		ModelAndView mav = new ModelAndView(pageLayout());

		mav.addObject(pageContent(), menuItemService.getMenuByRestaurantId(restaurantId));

		return mav;

	}

	@GetMapping("/menus/search")
	public ModelAndView searchRestaurants(final MenuSearchParams searchParams) {

		ModelAndView mav;

		MenuResponseData results = menuItemFacade.search(searchParams);

		if (searchParams.isJson()) {
			mav = getJsonView(results);
		} else {
			mav = getView(pageTitle());

			mav.addObject("allPageSizes", results.getSupportedPageSizes());
			mav.addObject("allSorts", results.getSorts());
			mav.addObject("filterData", results.getFilterData());
		}

		return mav;

	}

	@GetMapping("/restaurants/menu/")
	public ModelAndView searchRestaurants(final String vendorRestaurantId, final String vendor) {

		return getJsonView(menuItemFacade.getMenuCategoryDataList(vendorRestaurantId, vendor));

	}

}
