package io.tmg.hungryhappyhippo.poc.controllers.management;

import static io.tmg.hungryhappyhippo.poc.controllers.management.RuntimeTaskCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.RuntimeTaskCrudController.ROOT;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import io.tmg.hungryhappyhippo.poc.services.TaskService;
import io.tmg.hungryhappyhippo.poc.services.impl.RuntimeTaskCrudService;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class RuntimeTaskCrudController extends TaskCrudController {

	static final String ROOT = "management";
	static final String PATH = "runtimetasks";

	@Resource
	private TaskService taskService;

	@Resource
	private RuntimeTaskCrudService runtimeTaskCrudService;

	@Override
	public String pageTitle() {
		return "Runtime Task Management";
	}

	@Override
	protected String getRootPath() {
		return ROOT;
	}

	@Override
	protected String getModelName() {
		return PATH;
	}

	@Override
	protected CrudService<TaskModel> getCrudService() {
		return runtimeTaskCrudService;
	}

}
