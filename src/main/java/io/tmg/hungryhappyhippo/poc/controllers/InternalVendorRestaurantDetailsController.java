package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.facade.VendorRestaurantFacade;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
@Slf4j
@Controller
@RequestMapping(value = "/internal/vendorrestaurants")
public class InternalVendorRestaurantDetailsController extends AbstractController {

    @Resource
    private VendorRestaurantFacade vendorRestaurantFacade;

    @Override
    public String pageTitle() {

        return "Vendor Restaurant Details";

    }

    @Override
    public String pageContent() {

        return "vendorRestaurantDetailsPage";

    }

    @LocationCookieCheck
    @RequestMapping("/{vendorRestaurantId}")
    public ModelAndView getRestaurant(@PathVariable String vendorRestaurantId, final LocationSearchParams locationSearchParams) {

        log.trace("vendorRestaurantId - {}, locationSearchParams - {}", vendorRestaurantId, locationSearchParams);
        final VendorRestaurantDetailsData vendorRestaurantDetailsData = vendorRestaurantFacade.getRestaurantDetailsData(vendorRestaurantId, locationSearchParams);
        return getView(pageTitle()).addObject("restaurantDetailsData", vendorRestaurantDetailsData);

    }

}
