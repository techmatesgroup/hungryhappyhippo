package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.models.LocationModel;
import io.tmg.hungryhappyhippo.poc.models.dto.LocationDto;
import io.tmg.hungryhappyhippo.poc.services.LocationService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.controllers.management.LocationCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.LocationCrudController.ROOT;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class LocationCrudController extends AbstractCrudController<LocationModel, LocationDto> {

    static final String ROOT = "management";
    static final String PATH = "locations";

    @Resource
	private LocationService locationService;
    @Resource
    private ModelMapper modelMapper;

	@Override
	public String pageTitle() {
		return "Locations Management";
	}

    @Override
    protected String getRootPath() {
        return ROOT;
    }

    @Override
    protected String getModelName() {
        return PATH;
    }

    @Override
    protected Class<LocationDto> getDtoClass() {
        return LocationDto.class;
    }

    @Override
    public LocationService getCrudService() {
        return locationService;
    }

    @Override
	protected LocationDto toDto(LocationModel model) {
		LocationDto dto = modelMapper.map(model, LocationDto.class);
		if (model.location != null) {
            dto.latitude = model.location.getLat();
            dto.longitude = model.location.getLon();
        }

		return dto;
	}

    @Override
	protected LocationModel fromDto(LocationDto dto) {
        LocationModel model = new LocationModel(dto.name, dto.type.name(), dto.latitude, dto.longitude, dto.radius);
        if (StringUtils.isNotBlank(dto.id)) {
            model.id = dto.id;
        }
        return model;
	}

}
