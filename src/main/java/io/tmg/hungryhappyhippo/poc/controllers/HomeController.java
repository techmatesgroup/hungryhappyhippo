package io.tmg.hungryhappyhippo.poc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController extends AbstractController {

	@GetMapping(value = "/")
	public ModelAndView home() {

		return getView(pageTitle());

	}

}
