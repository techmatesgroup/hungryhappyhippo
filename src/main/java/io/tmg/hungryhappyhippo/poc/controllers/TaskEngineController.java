package io.tmg.hungryhappyhippo.poc.controllers;

import static io.tmg.hungryhappyhippo.poc.controllers.TaskEngineController.PATH;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import io.tmg.hungryhappyhippo.poc.facade.TaskEngineFacade;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping(value=PATH)
@Slf4j
public class TaskEngineController extends AbstractController {

	static final String PATH = "/admin/taskengine";
	static final String REDIRECT_TO_TASKENGINE = String.format("redirect:%s", PATH);

	@Resource
	private TaskEngineFacade taskEngineFacade;

	@Override
	public String pageTitle() {

		return "TaskEngine";

	}

	@Override
	public String pageContent() {

		return "taskengine";

	}

	@GetMapping("")
	public ModelAndView home() {

		ModelAndView mav = getView(pageTitle());

		try {
			mav.addObject("tasks", taskEngineFacade.getLoadedTasks());
			mav.addObject("taskEngineStatus", taskEngineFacade.getTaskEngineStatus());
		} catch (Exception e) {
			String message = String.format("Loaded tasks could not be fetched from the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(mav, message);
		}

		return mav;

	}

	@PostMapping("/start")
	protected ModelAndView startTaskEngine(RedirectAttributes attributes) {

		log.trace("{}: startTaskEngine", pageTitle());
		try {
			taskEngineFacade.startTaskEngine();
			addSuccessNotification(attributes, "Task engine was successfully started");
		} catch (Exception e) {
			String message = String.format("Task engine could not be started: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/pause")
	protected ModelAndView suspendTaskEngine(RedirectAttributes attributes) {

		log.trace("{}: suspendTaskEngine", pageTitle());
		try {
			taskEngineFacade.pauseTaskEngine();
			addSuccessNotification(attributes, "Task engine was successfully paused");
		} catch (Exception e) {
			String message = String.format("Task engine could not be paused: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/resume")
	protected ModelAndView resumeTaskEngine(RedirectAttributes attributes) {

		log.trace("{}: resumeTaskEngine", pageTitle());
		try {
			taskEngineFacade.resumeTaskEngine();
			addSuccessNotification(attributes, "Task engine was successfully resumed");
		} catch (Exception e) {
			String message = String.format("Task engine could not be resumed: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/clear")
	protected ModelAndView clearTaskEngine(RedirectAttributes attributes) {

		log.trace("{}: clearTaskEngine", pageTitle());
		try {
			taskEngineFacade.clearTaskEngine();
			addSuccessNotification(attributes, "Task engine was successfully cleared");
		} catch (Exception e) {
			String message = String.format("Task engine could not be cleared: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/destroy")
	protected ModelAndView shutdownTaskEngine(RedirectAttributes attributes) {

		log.trace("{}: shutdownTaskEngine", pageTitle());
		try {
			taskEngineFacade.destroyTaskEngine();
			addSuccessNotification(attributes, "Task engine was successfully destroyed");
		} catch (Exception e) {
			String message = String.format("Task engine could not be destroyed: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/job/loadall")
	protected ModelAndView loadTasksFromAllJobs(RedirectAttributes attributes) {

		log.trace("{}: loadTasksFromAllJobs", pageTitle());
		try {
			taskEngineFacade.loadTasksFromAllJobs();
			addSuccessNotification(attributes, "Jobs were successfully loaded into the task engine");
		} catch (Exception e) {
			String message = String.format("Jobs could not be loaded into the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/job/load")
	protected ModelAndView loadTaskFromJob(@RequestParam("id") String jobId, RedirectAttributes attributes) {

		log.trace("{}: loadTaskFromJob. JobId - {}", pageTitle(), jobId);
		try {
			taskEngineFacade.loadTaskFromJob(jobId);
			addSuccessNotification(attributes, "Job was successfully loaded into the task engine");
		} catch (Exception e) {
			String message = String.format("Job could not be loaded into the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/job/schedule")
	protected ModelAndView scheduleTaskFromJob(@RequestParam("id") String jobId, RedirectAttributes attributes) {

		log.trace("{}: scheduleTaskFromJob. JobId - {}", pageTitle(), jobId);
		try {
			taskEngineFacade.scheduleTaskFromJob(jobId);
			String successMessage = "Job was successfully loaded and scheduled into the task engine";
			addSuccessNotification(attributes, successMessage);
		} catch (Exception e) {
			String message = String.format("Job could not be loaded and scheduled in the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/persistent/loadall")
	protected ModelAndView loadTasksFromAllPersistentTask(RedirectAttributes attributes) {

		log.trace("{}: loadTasksFromAllPersistentTask", pageTitle());
		try {
			taskEngineFacade.loadTasksFromAllPersistentTasks();
			String successMessage = "Tasks were successfully loaded from storage into the task engine";
			addSuccessNotification(attributes, successMessage);
		} catch (Exception e) {
			String message = String.format("Tasks could not be loaded from storage into the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/persistent/load")
	protected ModelAndView loadTaskFromPersistentTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: loadTaskFromPersistentTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.loadTaskFromPersistentTask(taskId);
			String successMessage = "Task was successfully loaded from storage into the task engine";
			addSuccessNotification(attributes, successMessage);
		} catch (Exception e) {
			String message = String.format("Task could not be loaded from storage into the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/persistent/saveall")
	protected ModelAndView savePersistentTasksFromAllLoadedTasks(RedirectAttributes attributes) {

		log.trace("{}: savePersistentTasksFromAllLoadedTasks", pageTitle());
		try {
			taskEngineFacade.saveLoadedTasks();
			String successMessage = "Tasks were successfully saved into storage from the task engine";
			addSuccessNotification(attributes, successMessage);
		} catch (Exception e) {
			String message = String.format("Tasks could not be saved into storage from the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/persistent/save")
	protected ModelAndView savePersistentTaskFromLoadedTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: savePersistentTaskFromLoadedTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.saveLoadedTask(taskId);
			String successMessage = "Task was successfully saved into storage from the task engine";
			addSuccessNotification(attributes, successMessage);
		} catch (Exception e) {
			String message = String.format("Task could not be saved into storage from the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/execute")
	protected ModelAndView executeTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: executeTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.executeTask(taskId);
			addSuccessNotification(attributes, "Task was successfully fired for one-time execution");
		} catch (Exception e) {
			String message = String.format("Task could not be fired for one-time execution: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/schedule")
	protected ModelAndView scheduleTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: scheduleTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.scheduleTask(taskId);
			addSuccessNotification(attributes, "Task was successfully scheduled in the task engine");
		} catch (Exception e) {
			String message = String.format("Task could not be scheduled in the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/unschedule")
	protected ModelAndView unscheduleTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: unscheduleTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.unscheduleTask(taskId);
			addSuccessNotification(attributes, "Task was successfully unscheduled in the task engine");
		} catch (Exception e) {
			String message = String.format("Task could not be unscheduled in the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/pause")
	protected ModelAndView pauseTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: pauseTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.pauseTask(taskId);
			addSuccessNotification(attributes, "Task was successfully paused in the task engine");
		} catch (Exception e) {
			String message = String.format("Task could not be paused in the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/resume")
	protected ModelAndView resumeTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: resumeTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.resumeTask(taskId);
			addSuccessNotification(attributes, "Task was successfully resumed in the task engine");
		} catch (Exception e) {
			String message = String.format("Task could not be resumed in the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/stop")
	protected ModelAndView stopTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: stopTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.stopTask(taskId);
			addSuccessNotification(attributes, "Task was successfully stopped in the task engine");
		} catch (Exception e) {
			String message = String.format("Task could not be stopped in the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@PostMapping("/task/remove")
	protected ModelAndView removeTask(@RequestParam("id") String taskId, RedirectAttributes attributes) {

		log.trace("{}: removeTask. TaskId - {}", pageTitle(), taskId);
		try {
			taskEngineFacade.removeTask(taskId);
			addSuccessNotification(attributes, "Task was successfully removed from the task engine");
		} catch (Exception e) {
			String message = String.format("Task could not be removed from the task engine: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(attributes, message);
		}

		return new ModelAndView(REDIRECT_TO_TASKENGINE);
	}

	@GetMapping("/task/modify")
	protected ModelAndView taskDetails(@RequestParam("id") String taskId) {

		log.trace("{}: taskDetails. TaskId - {}", pageTitle(), taskId);
		ModelAndView mav = getView("Modify Task");

		try {
			mav.addObject("item", taskEngineFacade.getLoadedTaskDetails(taskId));
			mav.addObject(PAGE_TITLE_ATTR, "ModifyTask");
			mav.addObject(PAGE_CONTENT_ATTR, "management/tasks/edit");
		} catch (Exception e) {
			String message = String.format("Task could not be fetched for modification: %s", e.getMessage());
			log.error(message, e);
			addErrorNotification(mav, message);
		}

		return mav;
	}

}
