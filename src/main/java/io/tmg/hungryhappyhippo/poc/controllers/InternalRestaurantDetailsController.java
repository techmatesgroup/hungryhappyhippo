package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.annotations.LocationCookieCheck;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.RestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.details.restaurant.VendorRestaurantDetailsData;
import io.tmg.hungryhappyhippo.poc.data.search.request.restaurant.LocationSearchParams;
import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 01/30/2019
 */
@Slf4j
@Controller
@RequestMapping(value = "/internal/restaurants")
public class InternalRestaurantDetailsController extends AbstractController {

    @Resource
    private RestaurantFacade restaurantFacade;

    @Override
    public String pageTitle() {

        return "Restaurant Details";

    }

    @Override
    public String pageContent() {

        return "restaurantDetailsPage";

    }

    @RequestMapping("/{id}")
    @LocationCookieCheck(required = false)
    public ModelAndView getRestaurant(@PathVariable String id, final LocationSearchParams locationSearchParams) {

        log.trace("restaurantId - {}, locationSearchParams - {}", id, locationSearchParams);
        final RestaurantDetailsData detailsData = restaurantFacade.getRestaurantDetailsData(id, locationSearchParams);
        return getView(pageTitle()).addObject("restaurantDetailsData", detailsData);

    }

    @GetMapping("/availability/{vendorRestaurantId}/{vendorName}")
    @LocationCookieCheck
    public ModelAndView getVendorRestaurantAvailability(@PathVariable String vendorRestaurantId, @PathVariable String vendorName, final LocationSearchParams locationSearchParams) {

        log.trace("vendorRestaurantId - {}, vendorName - {}, locationSearchParams - {}", vendorRestaurantId, vendorName, locationSearchParams);
        VendorRestaurantDetailsData.VendorRestaurantAvailabilityDetails availabilityDetails = restaurantFacade.getVendorRestaurantAvailabilityDetails(vendorRestaurantId, vendorName, locationSearchParams);

        return availabilityDetails != null ? getJsonView(availabilityDetails) : null;

    }
}
