package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.models.CategoryModel;
import io.tmg.hungryhappyhippo.poc.models.dto.CategoryDto;
import io.tmg.hungryhappyhippo.poc.services.CategoryService;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.controllers.management.CategoryCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.CategoryCrudController.ROOT;

/**
 * Controller for CRUD operations on {@link CategoryModel}
 *
 * @author Sergey Nikulin [snikulin@tmg.io]
 * @since 02/19/2019
 */
@Controller
@RequestMapping(value = "/" + ROOT + "/" + PATH)
public class CategoryCrudController extends AbstractCrudController<CategoryModel, CategoryDto> {

    static final String ROOT = "management";
    static final String PATH = "categories";

    @Resource
    private CategoryService categoryService;

    @Resource
    private ModelMapper modelMapper;

    @Override
    public String pageTitle() {

        return "Categories management";

    }

    @Override
    protected String getRootPath() {

        return ROOT;

    }

    @Override
    protected String getModelName() {

        return PATH;

    }

    @Override
    protected Class<CategoryDto> getDtoClass() {

        return CategoryDto.class;

    }

    @Override
    protected CrudService<CategoryModel> getCrudService() {

        return categoryService;

    }

    @Override
    protected CategoryDto toDto(CategoryModel model) {

        return modelMapper.map(model, CategoryDto.class);

    }

    @Override
    protected CategoryModel fromDto(CategoryDto dto) {

        CategoryModel model = new CategoryModel(dto.getName(), dto.getImageUrl(), dto.getKeywords(), dto.isDisableAutoLinkingByRestaurantName(), dto.isDisableAutoLinkingByVendorCategory());

        if (StringUtils.isNotBlank(dto.getId())) {
            model.setId(dto.getId());
        } else {
            model.setIdFromName(dto.getName());
        }

        return model;

    }
}
