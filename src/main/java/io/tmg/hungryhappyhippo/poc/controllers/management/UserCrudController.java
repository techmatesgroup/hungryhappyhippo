package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.models.UserModel;
import io.tmg.hungryhappyhippo.poc.models.dto.UserDto;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import io.tmg.hungryhappyhippo.poc.services.UserService;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

import static io.tmg.hungryhappyhippo.poc.controllers.management.UserCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.UserCrudController.ROOT;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class UserCrudController extends AbstractCrudController<UserModel, UserDto> {

	static final String ROOT = "management";
	static final String PATH = "users";

	@Resource
	private UserService userService;
	@Resource
	private ModelMapper modelMapper;
	@Resource
	public PasswordEncoder passwordEncoder;

	@Override
	public String pageTitle() {
		return "Users Management";
	}

	@Override
	protected String getRootPath() {
		return ROOT;
	}

	@Override
	protected String getModelName() {
		return PATH;
	}

	@Override
	protected Class<UserDto> getDtoClass() {
		return UserDto.class;
	}

	@Override
	public CrudService<UserModel> getCrudService() {
		return userService;
	}

	@PostMapping("/enable")
	public String enable(@RequestParam("id") String id, @RequestParam(required = false) String searchTerms) {
		userService.enableById(id, true);

		return getRedirectToHome(searchTerms);
	}

	@PostMapping("/disable")
	public String disable(@RequestParam("id") String id, @RequestParam(required = false) String searchTerms) {
		userService.enableById(id, false);

		return getRedirectToHome(searchTerms);
	}

	@Override
	protected UserDto toDto(UserModel model) {
		UserDto dto = modelMapper.map(model, UserDto.class);
		dto.setPassword(null);

		return dto;
	}

	@Override
	protected UserModel fromDto(UserDto dto) {
		UserModel model = modelMapper.map(dto, UserModel.class);
		if (StringUtils.isNotBlank(dto.getId())) {
			userService.findById(dto.getId())
					.ifPresent(original -> model.setPassword(original.getPassword()));
		} else {
			model.setId(null);
		}
		if (StringUtils.isNotBlank(dto.getPassword())) {
			model.setPassword(passwordEncoder.encode(dto.getPassword()));
		}

		return model;
	}
}
