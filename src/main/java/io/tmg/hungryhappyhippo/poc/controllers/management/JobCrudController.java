package io.tmg.hungryhappyhippo.poc.controllers.management;

import static io.tmg.hungryhappyhippo.poc.controllers.management.JobCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.JobCrudController.ROOT;

import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import io.tmg.hungryhappyhippo.poc.models.JobModel;
import io.tmg.hungryhappyhippo.poc.models.dto.JobDto;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import io.tmg.hungryhappyhippo.poc.services.JobService;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class JobCrudController extends AbstractCrudController<JobModel, JobDto>  {

	static final String ROOT = "management";
	static final String PATH = "jobs";

	@Resource
	private JobService jobService;

	@Resource
	private ModelMapper modelMapper;

	@Override
	public String pageTitle() {
		return "Job Management";
	}

	@Override
	protected String getRootPath() {
		return ROOT;
	}

	@Override
	protected String getModelName() {
		return PATH;
	}

	@Override
	protected Class<JobDto> getDtoClass() {
		return JobDto.class;
	}

	@Override
	protected CrudService<JobModel> getCrudService() {
		return jobService;
	}

	@Override
	protected JobDto toDto(JobModel model) {
		JobDto dto = modelMapper.map(model, getDtoClass());
		return dto;
	}

	@Override
	protected JobModel fromDto(JobDto dto) {
		JobModel model = modelMapper.map(dto, JobModel.class);

		if (StringUtils.isNotBlank(dto.id)) {
			model.id = dto.id;
		}
		else {
			model.id = UUID.randomUUID().toString();
		}

		return model;
	}

}
