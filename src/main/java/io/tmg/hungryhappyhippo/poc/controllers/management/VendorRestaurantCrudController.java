package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.facade.VendorRestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.VendorRestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.dto.VendorRestaurantDto;
import io.tmg.hungryhappyhippo.poc.services.VendorRestaurantService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.Optional;
import java.util.UUID;

import static io.tmg.hungryhappyhippo.poc.controllers.management.VendorRestaurantCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.VendorRestaurantCrudController.ROOT;
import static java.lang.String.format;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class VendorRestaurantCrudController extends AbstractCrudController<VendorRestaurantModel, VendorRestaurantDto> {

    static final String ROOT = "management";
    static final String PATH = "vendorrestaurants";

    @Resource
	private VendorRestaurantService vendorRestaurantService;
	@Resource
	private VendorRestaurantFacade vendorRestaurantFacade;
    @Resource
    private RestaurantFacade restaurantFacade;
    @Resource
    private ModelMapper modelMapper;

	@Override
	public String pageTitle() {
		return "Vendor Restaurants Management";
	}

    @Override
    protected String getRootPath() {
        return ROOT;
    }

    @Override
    protected String getModelName() {
        return PATH;
    }

    private String getMoveView() {
        return format("%s/%s/move", getRootPath(), getModelName());
    }

    @Override
    protected Class<VendorRestaurantDto> getDtoClass() {
        return VendorRestaurantDto.class;
    }

    @Override
    public VendorRestaurantService getCrudService() {
        return vendorRestaurantService;
    }

    @GetMapping("/move")
    public ModelAndView move(@RequestParam("id") String vendorRestaurantId, @RequestParam(required = false) String searchTerms) {
        VendorRestaurantModel vendorRestaurant = getCrudService().getById(vendorRestaurantId);
        ModelAndView mav = getView(pageTitle());
        mav.addObject(PAGE_CONTENT_ATTR, getMoveView());
        mav.addObject("vendorRestaurantId", vendorRestaurant.id);
        mav.addObject("restaurantId", vendorRestaurant.restaurantAggregateId);
        mav.addObject("name", vendorRestaurant.name);
		mav.addObject("searchTerms", searchTerms);

        return mav;
    }

    @PostMapping("/move")
    public ModelAndView move(@RequestParam("vendorRestaurantId") String vendorRestaurantId,
                             @RequestParam("restaurantId") String restaurantId,
                             RedirectAttributes attributes,
							 @RequestParam(required = false) String searchTerms) {
        try {
			String message;
			if (StringUtils.isNotEmpty(restaurantId)) {
				// linking
				restaurantFacade.moveVendorRestaurantToRestaurant(vendorRestaurantId, restaurantId);
				message = "Vendor restaurant was successfully linked to restaurant: " + restaurantId;
			} else {
				// unlinking
				vendorRestaurantFacade.unlinkVendorRestaurant(vendorRestaurantId);
				message = "Vendor restaurant was successfully unlinked";
			}

			addSuccessNotification(attributes, message);
        } catch (Exception e) {
            addErrorNotification(attributes, e.getMessage());
        }

        return new ModelAndView(getRedirectToHome(searchTerms));
    }

	@Override
	protected VendorRestaurantDto toDto(VendorRestaurantModel model) {
		VendorRestaurantDto dto = modelMapper.map(model, VendorRestaurantDto.class);
		if (model.location != null) {
            dto.latitude = model.location.getLat();
            dto.longitude = model.location.getLon();
        }

		return dto;
	}

    @Override
    protected VendorRestaurantModel fromDto(VendorRestaurantDto dto) {
		VendorRestaurantModel model = modelMapper.map(dto, VendorRestaurantModel.class);
		model.location = new GeoPoint(dto.latitude, dto.longitude);
		model.suggest = new Completion(new String[]{model.name});

		if (StringUtils.isBlank(dto.id)) {
			model.setIdWithVendor(dto.vendor, UUID.randomUUID().toString());
        }
        if (StringUtils.isNotBlank(dto.id)) {

			final Optional<VendorRestaurantModel> byIdOptional = vendorRestaurantService.findById(dto.id);
			if (byIdOptional.isPresent()) {
				final VendorRestaurantModel original = byIdOptional.get();
				model.restaurantAggregateId = original.restaurantAggregateId;
			} else {
				// new vendor restaurant should be aggregated by job
				model.restaurantAggregateId = null;
			}
        }

		return model;
	}

}
