package io.tmg.hungryhappyhippo.poc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="")
public class LoginController extends AbstractController {

	@Override
	public String pageTitle() {

		return "Login";

	}

	@Override
	public String pageContent() {

		return "login";

	}

	@GetMapping(value = "/login")
	public ModelAndView login() {

		return getView(pageTitle());

	}

	@PostMapping(value = "/loginhome")
	public String home() {

		return "redirect:/";

	}

}
