package io.tmg.hungryhappyhippo.poc.controllers;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

public abstract class AbstractController {

	public static final String PAGE_LAYOUT_ATTR = "pageLayout";
	public static final String PAGE_TITLE_ATTR = "pageTitle";
	public static final String PAGE_CONTENT_ATTR = "pageContent";

	public static final String LAYOUT = "layout/layout";
	public static final String DEFAULT_TITLE = "Home";
	public static final String DEFAULT_CONTENT = "home";

	@ModelAttribute(PAGE_LAYOUT_ATTR)
	public String pageLayout() {

		return LAYOUT;

	}

	@ModelAttribute(PAGE_TITLE_ATTR)
	public String pageTitle() {

		return DEFAULT_TITLE;

	}

	@ModelAttribute(PAGE_CONTENT_ATTR)
	public String pageContent() {

		return DEFAULT_CONTENT;

	}

	protected ModelAndView getView(String title) {

		return new ModelAndView(pageLayout()).addObject(PAGE_TITLE_ATTR, title);

	}

	protected ModelAndView getJsonView(Object json) {

		ModelAndView mav = new ModelAndView();

		MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
		jsonView.setPrettyPrint(true);

		mav.setView(jsonView);
		mav.addObject(json);

		return mav;

	}

	protected void addErrorNotification(ModelAndView mav, String message) {
		mav.addObject("errorMessage", String.valueOf(message));
	}

	protected void addErrorNotification(RedirectAttributes attributes, String message) {
		attributes.addFlashAttribute("errorMessage", String.valueOf(message));
	}

	protected void addWarningNotification(ModelAndView mav, String message) {
		mav.addObject("warningMessage", String.valueOf(message));
	}

	protected void addWarningNotification(RedirectAttributes attributes, String message) {
		attributes.addFlashAttribute("warningMessage", String.valueOf(message));
	}

	protected void addInfoNotification(ModelAndView mav, String message) {
		mav.addObject("infoMessage", String.valueOf(message));
	}

	protected void addInfoNotification(RedirectAttributes attributes, String message) {
		attributes.addFlashAttribute("infoMessage", String.valueOf(message));
	}

	protected void addSuccessNotification(ModelAndView mav, String message) {
		mav.addObject("successMessage", String.valueOf(message));
	}

	protected void addSuccessNotification(RedirectAttributes attributes, String message) {
		attributes.addFlashAttribute("successMessage", String.valueOf(message));
	}

}
