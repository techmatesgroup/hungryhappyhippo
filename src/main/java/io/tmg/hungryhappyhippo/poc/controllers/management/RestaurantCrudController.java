package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.facade.RestaurantFacade;
import io.tmg.hungryhappyhippo.poc.models.RestaurantModel;
import io.tmg.hungryhappyhippo.poc.models.dto.RestaurantDto;
import io.tmg.hungryhappyhippo.poc.services.CategoryService;
import io.tmg.hungryhappyhippo.poc.services.RestaurantService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.tmg.hungryhappyhippo.poc.controllers.management.RestaurantCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.RestaurantCrudController.ROOT;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class RestaurantCrudController extends AbstractCrudController<RestaurantModel, RestaurantDto> {

    static final String ROOT = "management";
    static final String PATH = "restaurants";

    @Resource
	private RestaurantService restaurantService;

	@Resource
	private RestaurantFacade restaurantFacade;

    @Resource
    private ModelMapper modelMapper;

    @Resource
	private CategoryService categoryService;

	@Override
	public String pageTitle() {
		return "Restaurant Management";
	}

    @Override
    protected String getRootPath() {
        return ROOT;
    }

    @Override
    protected String getModelName() {
        return PATH;
    }

    @Override
    protected Class<RestaurantDto> getDtoClass() {
        return RestaurantDto.class;
    }

    @Override
    public RestaurantService getCrudService() {
        return restaurantService;
    }

	@Override
	public ModelAndView edit(String id, @RequestParam(required = false) String searchTerms) {
		final ModelAndView mav = super.edit(id, searchTerms);
		mav.addObject("categories", categoryService.getCategories());
		return mav;
	}

	@Override
	public ModelAndView create() {
		final ModelAndView mav = super.create();
		mav.addObject("categories", categoryService.getCategories());
		return mav;
	}

	@Override
	protected RestaurantDto toDto(RestaurantModel model) {
        RestaurantDto dto = modelMapper.map(model, RestaurantDto.class);
        if (CollectionUtils.isNotEmpty(model.getVendorRestaurants())) {
            dto.setVendorRestaurants(model.getVendorRestaurants().stream().map(RestaurantModel.VendorRestaurant::getVendor).collect(Collectors.toSet()));
        }
        if (model.location != null) {
            dto.latitude = model.location.getLat();
            dto.longitude = model.location.getLon();
        }

		return dto;
	}

    @Override
    protected RestaurantModel fromDto(RestaurantDto dto) {
        RestaurantModel model = modelMapper.map(dto, RestaurantModel.class);
        model.location = new GeoPoint(dto.latitude, dto.longitude);
		model.suggest = new Completion(new String[]{model.name});

		if (StringUtils.isBlank(dto.id)) {
            model.id = restaurantFacade.buildRestaurantId(model.name, model.address);
			model.vendorRestaurants = new ArrayList<>();
        }
        if (StringUtils.isNotBlank(dto.id)) {

        	final Optional<RestaurantModel> byIdOptional = restaurantService.findById(dto.id);
			if (byIdOptional.isPresent()) {
				final RestaurantModel original = byIdOptional.get();
				model.setVendorRestaurants(original.vendorRestaurants);
			} else {
				model.setVendorRestaurants(new ArrayList<>());
			}
        }

		return model;
	}

}
