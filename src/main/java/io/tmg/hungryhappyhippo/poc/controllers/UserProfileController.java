package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.data.index.request.UserParams;
import io.tmg.hungryhappyhippo.poc.data.index.result.ResultData;
import io.tmg.hungryhappyhippo.poc.models.UserModel;
import io.tmg.hungryhappyhippo.poc.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "")
public class UserProfileController extends AbstractController {


    @Resource
    public UserService userService;

    @Resource
    public PasswordEncoder passwordEncoder;

    @Override
    public String pageTitle() {

        return "UserProfile";

    }

    @Override
    public String pageContent() {

        return "userprofile";

    }

    @GetMapping(value = "/userprofile")
    public ModelAndView home(HttpServletRequest request) {

        UserModel user = userService.getUserByName(request.getRemoteUser());
        ResultData resultData = verifyUser(user, request.getRemoteUser());

        ModelAndView mav = getView(pageTitle());
        if (!resultData.success) {
            addErrorNotification(mav, resultData.message);
            user = new UserModel();
            user.setUsername(request.getRemoteUser());
        }

        return getView(pageTitle()).addObject(pageContent(), user);

    }

    @PostMapping(value = "/userprofile")
    public String saveUserProfile(HttpServletRequest request,
                                  RedirectAttributes redirAttrs,
                                  UserParams userParams) {

        UserModel user = getUser(request.getRemoteUser());
        ResultData result = verifyUser(user, request.getRemoteUser());

        if (result.success) {
            user.setEmail(userParams.getEmail());
            user.setAddress(userParams.getAddress());
            user.setLatitude(userParams.getLatitude());
            user.setLongitude(userParams.getLongitude());

            try {
                userService.save(user);
                result.success = true;
                result.message = "User profile was changed";
            } catch (Exception e) {
                result.success = false;
                result.message = e.getMessage();
            }
        }

        redirAttrs.addFlashAttribute("result", result);

        return "redirect:/userprofile";
    }

    @PostMapping(value = "/changepassword")
    public String changePassword(HttpServletRequest request,
                                 RedirectAttributes redirAttrs,
                                 UserParams userParams) {

        UserModel user = getUser(request.getRemoteUser());
        ResultData result = verifyUser(user, request.getRemoteUser());

        if (StringUtils.isBlank(userParams.getPassword()) ||
                StringUtils.isBlank(userParams.getPassword_confirm()) ||
                !userParams.getPassword().equals(userParams.getPassword_confirm())) {
            result.success = false;
            result.message = "Those passwords didn't match. Try again.";
        }

        if (result.success) {
            user.setPassword(passwordEncoder.encode(userParams.getPassword()));

            try {
                userService.save(user);
                result.success = true;
                result.message = "Password was changed";
            } catch (Exception e) {
                result.success = false;
                result.message = e.getMessage();
            }
        }

        redirAttrs.addFlashAttribute("result", result);

        return "redirect:/userprofile";
    }

    private UserModel getUser(String userName) {
        UserModel user = userService.getUserByName(userName);
        if (user == null && "admin".equalsIgnoreCase(userName)) {
            user = userService.createUserAdmin();
        }
        return user;
    }

    private ResultData verifyUser(UserModel user, String userName) {
        ResultData result = new ResultData();
        result.success = true;

        if (user == null) {
            result.success = false;
            if ("admin".equalsIgnoreCase(userName)) {
                result.message = "Cannot change user 'admin'";
            } else {
                result.message = "User not found";
            }
        }
        return result;
    }
}
