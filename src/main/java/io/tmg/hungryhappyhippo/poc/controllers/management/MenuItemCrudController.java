package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.models.MenuItemModel;
import io.tmg.hungryhappyhippo.poc.models.dto.MenuItemDto;
import io.tmg.hungryhappyhippo.poc.services.MenuItemService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.UUID;

import static io.tmg.hungryhappyhippo.poc.controllers.management.MenuItemCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.MenuItemCrudController.ROOT;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class MenuItemCrudController extends AbstractCrudController<MenuItemModel, MenuItemDto> {

	static final String ROOT = "management";
	static final String PATH = "menuitems";

	@Resource
	private MenuItemService menuItemService;
	@Resource
    private ModelMapper modelMapper;

	@Override
	public String pageTitle() {
		return "Menus Management";
	}

	@Override
	protected String getRootPath() {
		return ROOT;
	}

	@Override
	protected String getModelName() {
		return PATH;
	}

	@Override
	protected Class<MenuItemDto> getDtoClass() {
		return MenuItemDto.class;
	}

	@Override
	public MenuItemService getCrudService() {
		return menuItemService;
	}

    @Override
	protected MenuItemDto toDto(MenuItemModel model) {
		MenuItemDto dto = modelMapper.map(model, MenuItemDto.class);
		if (StringUtils.isBlank(dto.id)) {
			model.id = null;
		}

		return dto;
	}

    @Override
	protected MenuItemModel fromDto(MenuItemDto dto) {
		MenuItemModel model = modelMapper.map(dto, MenuItemModel.class);
		if (StringUtils.isBlank(dto.id)) {
			model.setIdWithVendor(dto.vendor, UUID.randomUUID().toString());
		}

		return model;
	}

}
