package io.tmg.hungryhappyhippo.poc.controllers.management;

import static io.tmg.hungryhappyhippo.poc.controllers.management.TaskCrudController.PATH;
import static io.tmg.hungryhappyhippo.poc.controllers.management.TaskCrudController.ROOT;

import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import io.tmg.hungryhappyhippo.poc.models.TaskModel;
import io.tmg.hungryhappyhippo.poc.models.dto.TaskDto;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import io.tmg.hungryhappyhippo.poc.services.TaskService;

@Controller
@RequestMapping(value="/" + ROOT + "/" + PATH)
public class TaskCrudController extends AbstractCrudController<TaskModel, TaskDto>  {

	static final String ROOT = "management";
	static final String PATH = "tasks";

	@Resource
	private TaskService taskService;

	@Resource
	private ModelMapper modelMapper;

	@Override
	public String pageTitle() {
		return "Task Management";
	}

	@Override
	protected String getRootPath() {
		return ROOT;
	}

	@Override
	protected String getModelName() {
		return PATH;
	}

	@Override
	protected Class<TaskDto> getDtoClass() {
		return TaskDto.class;
	}

	@Override
	protected CrudService<TaskModel> getCrudService() {
		return taskService;
	}

	@Override
	protected TaskDto toDto(TaskModel model) {
		TaskDto dto = modelMapper.map(model, getDtoClass());
		return dto;
	}

	@Override
	protected TaskModel fromDto(TaskDto dto) {
		TaskModel model = modelMapper.map(dto, TaskModel.class);

		if (StringUtils.isNotBlank(dto.id)) {
			model.id = dto.id;
		}
		else {
			model.id = UUID.randomUUID().toString();
		}

		return model;
	}

}
