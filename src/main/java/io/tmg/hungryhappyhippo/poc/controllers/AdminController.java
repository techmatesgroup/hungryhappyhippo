package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.data.index.request.IndexParams;
import io.tmg.hungryhappyhippo.poc.data.index.result.IndexResultData;
import io.tmg.hungryhappyhippo.poc.services.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

@Slf4j
@Controller
@RequestMapping(value="/admin")
public class AdminController extends AbstractController {

	@Override
	public String pageTitle() {

		return "Admin";

	}

	@Override
	public String pageContent() {

		return "admin";

	}

	@Resource
	private AdminService adminService;

	private static final String REDIRECT_TO_ADMIN = "redirect:/admin";

	@GetMapping("")
	public ModelAndView home() {

		ModelAndView mav = getView(pageTitle());

		mav.addObject("indexes", adminService.getActiveIndexes());
		mav.addObject("vendors", adminService.getSupportedVendors());
		mav.addObject("locations", adminService.getTargetLocations());
		mav.addObject("categories", adminService.getCategories());

		return mav;

	}

	@PostMapping("/index/create")
	public String createIndexes(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: createIndexes. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.createAllIndexes();
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/index/refresh")
	public String refreshIndex(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: refreshIndex. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.refreshIndex(indexParams.getId());
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/index/clear")
	public String clearIndex(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: clearIndex. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.clearIndex(indexParams.getId());
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/index/delete")
	public String deleteIndex(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: deleteIndex. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.deleteIndex(indexParams.getId());
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/vendor/delete")
	public String deleteVendor(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: deleteVendor. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.deleteVendor(indexParams.getId());
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/location/delete")
	public String deleteLocation(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: deleteLocation. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.deleteLocation(indexParams.getId());
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/category/delete")
	public String deleteCategory(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: deleteCategory. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.deleteCategory(indexParams.getId());
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/load/vendors")
	public String loadVendors(RedirectAttributes redirAttrs) {

		log.trace("{}: loadVendors", pageTitle());
		IndexResultData result = adminService.loadSupportedVendors();
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/load/locations")
	public String loadLocations(RedirectAttributes redirAttrs) {

		log.trace("{}: loadLocations", pageTitle());
		IndexResultData result = adminService.loadTargetLocations();
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/load/vendorrestaurants")
	public String loadRestaurants(RedirectAttributes redirAttrs) {

		log.trace("{}: loadRestaurants", pageTitle());
		IndexResultData result = adminService.loadVendorRestaurants();
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/build/restaurants")
	public String buildRestaurants(RedirectAttributes redirAttrs) {

		log.trace("{}: buildRestaurants", pageTitle());
		IndexResultData result = adminService.buildRestaurantsFromVendorRestaurants();
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/load/categories")
	public String loadCategories(RedirectAttributes redirAttrs) {

		log.trace("{}: loadCategories", pageTitle());
		IndexResultData result = adminService.loadCategories();
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

	@PostMapping("/load/request")
	public String indexData(RedirectAttributes redirAttrs, final IndexParams indexParams) {

		log.trace("{}: indexData. IndexParams - {}", pageTitle(), indexParams);
		IndexResultData result = adminService.executeIndexRequest(indexParams);
		redirAttrs.addFlashAttribute("result", result);

		return REDIRECT_TO_ADMIN;

	}

}
