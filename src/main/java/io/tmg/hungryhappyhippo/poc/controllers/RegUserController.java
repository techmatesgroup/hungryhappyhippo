package io.tmg.hungryhappyhippo.poc.controllers;

import io.tmg.hungryhappyhippo.poc.data.index.request.UserParams;
import io.tmg.hungryhappyhippo.poc.data.index.result.ResultData;
import io.tmg.hungryhappyhippo.poc.models.UserModel;
import io.tmg.hungryhappyhippo.poc.models.UserRole;
import io.tmg.hungryhappyhippo.poc.services.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "")
public class RegUserController extends AbstractController {


    @Resource
    public UserService userService;

    @Resource
    public PasswordEncoder passwordEncoder;

    @Override
    public String pageTitle() {

        return "RegUser";

    }

    @Override
    public String pageContent() {

        return "reguser";

    }

    @GetMapping(value = "/reguser")
    public ModelAndView home() {

        return getView(pageTitle());

    }

    @PostMapping(value = "/reguser")
    public String regUser(RedirectAttributes redirAttrs,
                          UserParams userParams) {

        ResultData result = new ResultData();
        if (userParams.getPassword() == null ||
                userParams.getPassword_confirm() == null ||
                !userParams.getPassword().equals(userParams.getPassword_confirm())) {
            result.success = false;
            result.message = "Those passwords didn't match. Try again.";
            redirAttrs.addFlashAttribute("result", result);

            return "redirect:/reguser";
        }
        UserModel user = new UserModel();
        user.setUsername(userParams.getUsername());
        user.setEmail(userParams.getEmail());
        user.setPassword(passwordEncoder.encode(userParams.getPassword()));
        user.addRole(UserRole.SITEUSER);

        try {
            userService.createUser(user);
            result.success = true;
            result.message = "User was created";
        } catch (Exception e) {
            result.success = false;
            result.message = e.getMessage();
        }

        redirAttrs.addFlashAttribute("result", result);

        if (result.success) {
            return "redirect:/login";
        } else {
            return "redirect:/reguser";
        }
    }

}
