package io.tmg.hungryhappyhippo.poc.controllers.management;

import io.tmg.hungryhappyhippo.poc.controllers.AbstractController;
import io.tmg.hungryhappyhippo.poc.services.CrudService;
import io.tmg.hungryhappyhippo.poc.services.ExportService;
import io.tmg.hungryhappyhippo.poc.services.ImportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.lang.String.format;

@Slf4j
public abstract class AbstractCrudController<MODEL, DTO> extends AbstractController {

	private static final int EXPORT_BATCH_SIZE = 1000;

	@Resource
	private ImportService importService;

	@Resource
	private ExportService exportService;

	@Resource
	private Map<String, Float> adminFreeTextSearchKeyFields;

	protected abstract String getRootPath();
	protected abstract String getModelName();
	protected abstract Class<DTO> getDtoClass();
	protected abstract CrudService<MODEL> getCrudService();
	protected abstract DTO toDto(MODEL model);
	protected abstract MODEL fromDto(DTO dto);

	protected String getRedirectToHome(String searchTerms) {
		if (StringUtils.isNotBlank(searchTerms)) {
			return format("redirect:/%s/%s?searchTerms=%s", getRootPath(), getModelName(), searchTerms);
		} else {
			return format("redirect:/%s/%s", getRootPath(), getModelName());
		}
	}

	private String getEditView() {
		return format("%s/%s/edit", getRootPath(), getModelName());
	}

	private String getHomeView() {
		return format("%s/%s/home", getRootPath(), getModelName());
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@GetMapping("")
	public ModelAndView home(@PageableDefault Pageable pageable, @RequestParam(required = false) String searchTerms) {

		ModelAndView mav = getView(pageTitle());
		mav.addObject(PAGE_CONTENT_ATTR, getHomeView());
		mav.addObject("items", toDtoPage(getCrudService().getPage(pageable, searchTerms), pageable));
		mav.addObject("searchTerms", searchTerms);
		mav.addObject("searchKeyFields", StringUtils.join(adminFreeTextSearchKeyFields.keySet(), ", "));

		return mav;
	}

	@GetMapping("/edit")
	public ModelAndView edit(@RequestParam("id") String id, @RequestParam(required = false) String searchTerms) {

		log.trace("[EDIT] {}: id - {}, searchTerms - {}", pageTitle(), id, searchTerms);
        MODEL model = getCrudService().getById(id);
        ModelAndView mav = getView(pageTitle());
        mav.addObject(PAGE_CONTENT_ATTR, getEditView());
        mav.addObject("item", toDto(model));
        mav.addObject("searchTerms", searchTerms);

		return mav;
	}

	@GetMapping("/create")
	public ModelAndView create() {

		log.trace("[CREATE] {}", pageTitle());
		MODEL model = getCrudService().instance();
		ModelAndView mav = getView(pageTitle());
		mav.addObject(PAGE_CONTENT_ATTR, getEditView());
		mav.addObject("item", toDto(model));

		return mav;
	}

	@PostMapping("/save")
	public ModelAndView save(@ModelAttribute("item") @Valid DTO dto, BindingResult result, RedirectAttributes attributes, @RequestParam(required = false) String searchTerms) {

		log.trace("[SAVE] {}: dto - {}, bindingResult - {}, searchTerms - {}", pageTitle(), dto, result, searchTerms);
		if (result.hasErrors()) {
			ModelAndView mav = getView(pageTitle());
			mav.addObject(PAGE_CONTENT_ATTR, getEditView());
			addErrorNotification(mav, result.toString());

			return mav;
		}
		try {
			getCrudService().save(fromDto(dto));
			addSuccessNotification(attributes, "Object was successfully saved!");
		} catch (Exception e) {
			addErrorNotification(attributes, e.getMessage());
		}

		return new ModelAndView(getRedirectToHome(searchTerms));
	}

	@PostMapping("/delete")
	protected ModelAndView delete(@RequestParam("id") String id, RedirectAttributes attributes, @RequestParam(required = false) String searchTerms) {

		log.trace("[DELETE] {}: id - {}, searchTerms - {}", pageTitle(), id, searchTerms);
		try {
			getCrudService().deleteById(id);
			addSuccessNotification(attributes, "Object was successfully deleted!");
		} catch (Exception e) {
			log.warn("delete error. Id - {}", id, e);
			addErrorNotification(attributes, e.getMessage());
		}

		return new ModelAndView(getRedirectToHome(searchTerms));
	}

	@PostMapping("/import")
	public ModelAndView importData(@RequestPart(value = "file") MultipartFile file, RedirectAttributes attributes, @RequestParam(required = false) String searchTerms) {

		try {
			Iterable<DTO> dtoList = importService.fromCsv(file.getBytes(), getDtoClass());
			getCrudService().saveAll(fromDtoList(dtoList));
			addSuccessNotification(attributes, "File was successfully imported!");
		} catch (Exception e) {
			log.warn("importData error", e);
			addErrorNotification(attributes, e.getMessage());
		}

		return new ModelAndView(getRedirectToHome(searchTerms));
	}

	@GetMapping("/export")
	public ResponseEntity<byte[]> exportData(@PageableDefault(size = EXPORT_BATCH_SIZE) Pageable pageable, RedirectAttributes attributes, @RequestParam(required = false) String searchTerms) {

		byte[] media = new byte[0];
		try {
			List<DTO> dtoList = new ArrayList<>();
			while (pageable.isPaged()) {
				Page<MODEL> page = getCrudService().getPage(pageable, searchTerms);
				log.debug("Export {} page {} of {}", getModelName(), page.getNumber() + 1, page.getTotalPages());
				dtoList.addAll(toDtoList(page));
				pageable = page.nextPageable();
			}

			media = exportService.toCsv(dtoList, getDtoClass());
		} catch (Exception e) {
			log.warn("exportData error", e);
		}

		return ResponseEntity.ok()
				.contentLength(media.length)
				.headers(getHeaders())
				.body(media);
	}

	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl(CacheControl.noCache().getHeaderValue());
		headers.setContentType(MediaType.parseMediaType("text/csv"));
		headers.set(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s-%s.%s",
				getModelName(),
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm")),
				"csv"));

		return headers;
	}

	private Page<DTO> toDtoPage(Page<MODEL> models, Pageable pageable) {
		return new PageImpl<>(toDtoList(models), pageable, models.getTotalElements());
	}

	private List<DTO> toDtoList(Page<MODEL> models) {
		return StreamSupport.stream(models.spliterator(), true)
				.map(this::toDto)
				.collect(Collectors.toList());
	}

	private Iterable<MODEL> fromDtoList(Iterable<DTO> dtoList) {
		return StreamSupport.stream(dtoList.spliterator(), true)
				.map(this::fromDto)
				.collect(Collectors.toList());
	}
}
